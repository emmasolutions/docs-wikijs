---
title: Ventanas de atribución
description: 
published: true
date: 2025-03-04T11:45:38.413Z
tags: 
editor: markdown
dateCreated: 2025-02-28T09:43:29.880Z
---

Con EMMA es posible personalizar las ventanas de atribución de clic a instalación y de instalación a eventos in-app para cada una de las diferentes redes con las que se trabaja. 

¿Qué son las ventanas de atribución?

- La ventana de atribución de clic, es la que determina el tiempo máximo que puede transcurrir entre un clic y la instalación de la aplicación en el dispositivo para que esta pueda ser atribuida a un origen determinado. 

- La ventana de atribución de eventos in-app, es la que determina el tiempo máximo que puede transcurrir entre una instalación y la realización de eventos in-app para que estos queden atribuidos al origen de la instalación. 

A continuación, veremos en detalle las diferentes tipologías de atribución y la posible configuración de las ventanas. 

# Atribución determinística
La atribución determinística es la metodología principal de atribución en EMMA. Esta tipología está basada en el uso **identificadores únicos del dispositivo, como el identificador de publicidad. Esto hace que sea la atribución más precisa**. 

La ventana de atribución determinística, se puede personalizar. Dependiendo de cada red, se podrán establecer unos valores u otros. 

En la siguiente tabla, se puede ver el nivel de personalización permitido para cada red, así como la configuración por defecto de cada una de ellas:

|  Red   |  Intervalo atribución clic   |  Clic por defecto en EMMA   |  Intervalo atribución eventos   |  Evento por defecto en EMMA   |
| --- | --- | --- | --- | --- |
| ***Apple Search Ads*** | 30 días | 30 días | Entre 1 día y 12 meses | 30 días |
| ***Google*** | Entre 1 y 90 días | 7 días | Entre 1 día y 12 meses | 30 días |
| ***Meta (Facebook)*** | Entre 1 y 28 días | 7 días | Entre 1 día y 12 meses | 30 días |
| ***Microsoft Ads (Bing)*** | Entre 1 y 30 días | 7 días | Entre 1 día y 12 meses | 30 días |
| ***Snapchat*** | Entre 1 y 28 días | 7 días | Entre 1 día y 12 meses | 30 días |
| ***TikTok*** | Entre 1 y 28 días | 7 días | Entre 1 día y 12 meses | 30 días |
| ***Twitter*** | Entre 1 y 30 días | 7 días | Entre 1 día y 12 meses | 30 días |
| ***Otras redes*** | Entre 1 y 30 días | 7 días | Entre 1 día y 12 meses | 30 días |

> Desde EMMA recomendamos que la ventana de atribución configurada en el soporte, sea la misma que la ventana de atribución configurada en EMMA. De esta manera, se evitarán posibles discrepancias. {.is-warning}

# Atribución probabilística
La atribución probabilística es aquella que no se basa en identificadores únicos del dispositivo, sino que se basa en una distribución probabilística para poder asignar la atribución. 

Al no poder basarse en identificadores únicos del dispositivo, la atribución se realiza a través de un algoritmo interno basado en estadísticas/coincidencias.

> La ventana de atribución de la atribución probabilística, no se puede modificar y es de 24 horas.{.is-info} 

# Personalización de ventanas de atribución en EMMA
Para personalizar las ventanas de atribución, solo necesitas seguir estos sencillos pasos: 
- Haz [login](https://ng.emma.io/es/login) en tu cuenta de EMMA. 
- Ve a **Captación > Fuentes de medios**. </br><p style= "width: 500px">![notificar_redes_1.png](/notificar_redes_1.png)</p>
- Busca y edita el proveedor que deseas configurar.
- Avanza hasta el apartado nombrado **Atribución**. </br><p style= "width: 800px">![ventanas_atribución_1.png](/ventanas_atribución_1.png)</p>
- Establece la ventana de atribución deseada dentro de los intervalos permitidos para esa red en concreto. 

# Personalización de ventana de atribución de retargeting en EMMA
En las campañas de **retargeting** también es posible configurar la ventana de atribución de clic a evento in-app para cada uno de los diferentes powlinks. 

La ventana de atribución de **retargeting** es la que determina el tiempo máximo que puede transcurrir entre el clic y la acción in-app. 

Esta configuración, se lleva a cabo en el momento de crear el **powlink** y se puede establecer una ventana diferente para cada powlink de retargeting. Puedes ver más info sobre cómo crear los powlinks [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios).

> La ventana de atribución de **retargeting** se puede personalizar entre **1 y 90 días**. En caso de no seleccionar ningún valor, se usará la ventana por defecto de EMMA de 30 días.{.is-info}