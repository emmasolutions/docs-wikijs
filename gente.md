---
title: Gente
description: Gente
published: true
date: 2024-12-16T13:26:11.138Z
tags: 
editor: markdown
dateCreated: 2024-05-06T12:01:23.415Z
---

# Introducción
En Comportamiento > Gente puedes consultar todos los datos relativos a la actividad de tus usuarios con tu aplicación: desde el tipo de usuarios, las compras que realizan, las pantallas que más emplean, etc.

EMMA pone a tu disposición un sistema de filtros/segmentos para que puedas tener toda esta información, además del etiquetado que hayas establecido para tu app.

Lo primero que debemos tener en cuenta a la hora de acceder a esta pantalla, es el filtro *Usuarios activos* / *Usuarios inactivos*. Puedes ver más información sobre cómo funcionan estos dos filtros [aquí](https://docs.emma.io/es/barras-de-menu#filtros-en-comportamiento-gente). 
- ***Usuarios activos:*** Con este filtro se tienen en cuenta sólo los usuarios activos. Para calcular estos usuarios activos se usa la información del KPI *last_session*. Este dato muestra información en tiempo real.
- ***Usuarios inactivos:*** Con este filtro se tienen en cuenta todos los dispositivos disponibles en la base de datos. 

# Aplicar los filtros
Para poder realizar las consultas que necesites, es necesario que apliques los filtros que quieres emplear. Para ello, sigue estos pasos: 

- Haz clic en el botón verde **+ Añadir grupo** para añadir un nuevo criterio de filtrado. Elige entre todos los [filtros disponibles](https://docs.emma.io/es/gente#filtros-disponibles) y añade todos los que necesites haciendo clic nuevamente en el botón de **+ Añadir grupo**. </br></b><p style="width:1100px">![gente_filtro_rt_1.png](/gente_filtro_rt_1.png)</p>
- Todos los filtros condicionados de la segmentación se añadirán con un "Y" intermedio que significará que los usuarios filtrados deberán cumplir **todas** las condiciones. </b><p style="width:1100px">![gente_12.png](/gente_12.png)</p>
- Haz click en uno de los "Y" intermedios para convertir los condicionales en "O". Este cambio implicará que los usuarios filtrados deberán cumplir **alguna** de las condiciones. </b><p style="width:1100px">![gente_13.png](/gente_13.png)</p>
- Para eliminar un filtro simplemente tienes que seleccionar el icono de la papelera que aparece en rojo a la derecha del filtro. </b><p style="width:900px">![gente_15.png](/gente_15.png)</p>
- Cada vez que actualices alguna información o añadas/elimines un filtro, debes seleccionar el botón Actualizar para que actualicen los datos consultados. </b><p style="width:1100px">![gente_14.png](/gente_14.png)</p>

> Si se quiere consultar una etiqueta, hay que tener en cuenta que en las etiquetas con más de 100k resultados no se mostrará el listado de los mismos y habrá que introducir los valores deseados manualmente. {.is-warning}

# Filtros disponibles
A continuación se detalla el listado de filtros que puedes seleccionar en el dashboard de Gente para consultar los datos.

Puedes elegir por:

## Atributos
|     |     |
| --- | --- |
| Versión App Android | Versión de la aplicación en Android (ej:1.0). |
| Versión EMMA SDK Android | Versión del SDK de EMMA en Android. |
| Modelo Dispositivo Android | Modelo del dispositivo Android del usuario (ej.Samsumg Galaxy). |
| Versión Android | Versión del sistema operativo Android (ej. Android 12). |
| Versión App iOS | Version de la aplicación en iOS (ej.1.0). |
| Versión EMMA SDK iOS | Versión de SDK de EMMA en iOS. |
| Modelo Dispositivo iOS | Modelo del dispositivo iOS del usuario (ej.Iphone X). |
| Versión iOS | Version del sistema operativo iOS (ej.iOS 15). |
| Android Mobile Services | Dispositivos que usan los servicios de Google Mobile Services (dispositivos que tienen acceso a Google Play) o los dispositivos que usan los servicios de Huawei Mobile Services (dispositivos que tienen acceso a App Gallery). |
| ID Dispositivo | Número de identificación único de iOS (IDFA/IDFV) y Android (AAID). Se recoge automáticamente del dispositivo. En el caso del IDFA solo se recoge si el usuario ha dado consentimiento expreso. |
| ID EMMA | Número de identificador de usuario de la plataforma EMMA. Es un ID aleatorio que genera EMMA. |
| ID Cliente | Número de identificador interno para identificar los usuarios de los clientes. Este identificador es ajeno a EMMA y se puede o no enviar desde la app. Suele ser un identificador ya existente en la bbdd de cada cliente para poder identificar a los usuarios. |
| Localización: Pais | País en el que se encuentra el usuario mientras ha estado utilizando la geolocalización en la app/sitio móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| Localización: Ciudad | Ciudad en la que se encuentra el usuario mientras ha estado utilizando la geolocalización en la app/sitio móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| Localización: Estado/Provincia | Estado/Provincia en el que se encuentra el usuario mientras ha estado utilizando la geolocalización en la app/sitio móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| Múltiples Dispositivos | True: Localiza los usuarios únicos que usan uno o más de un dispositivo. </br>False: Localiza los usuarios que sólo usan un dispositivo. |
| Sistema Operativo | iOS, Android, Windows Phone u otro sistema operativo. |
| Valor de pedido | Valor de la orden de compra. |
| Nombre de producto | Productos etiquetados:puedes utilizar todos los campos adicionales de los productos de tu app que se hayan implementado. |
| Notificación Push Abierta | Usuarios que han abierto las notificaciones Push. |
| Notificación Push Activada | Usuarios que tienen activadas las notificaiones Push. |
| Fuente | Información de la fuente del Apptracker a la que está atribuido un usuario si corresponde. |
| Campaña | Información de la campaña del Apptracker a la que está atribuido un usuario si corresponde. |
| Comunicación | Usuarios que han sido impactados por una comunicación inapp u outapp (email y push). |
| TAG | Etiquetas de usuario implementadas. |

**Etiqueta Birthday**
> Si has implementado la etiqueta Birthday y quieres impactar a usuarios en función de su cumpleaños, es importante que tengas en cuenta un par de apuntes sobre dos de los operadores disponibles. </br> </br>- **fue el**: si llevas poco tiempo usando el SDK de EMMA, no podrás usar el operador fue el para impactar a los usuarios que cumplan años en un día determinado. Esto es porque si has empezado a usar el SDK de EMMA el día 15 de abril de 2016 el operador fue el sólo te permitirá retroceder hasta ese día, ya es cuando se empieza a recoger información. Es por ello que si un usuario establece que su fecha de nacimiento es el día 03/03/1988 no podrá ser impactado con este operador.</br></br>- **el cumpleaños es**: con este operador podrás usar un valor de uno en adelante para establecer los días/semanas/meses/años exactos que hace que el usuario ha cumplido años. {.is-info}

## Eventos
|     |     |
| --- | --- |
| Conversión | Transacciones que se han realizado. |
| Primera compra | Primera compra del usuario en la app/sitio móvil. |
| Última compra | Última vez que el usuario realizó una compra en la app/sitio móvil. |
| Visto por última vez | Última vez que el usuario ha entrado en la app. |
| Instalación | Nuevos usuarios que se han descargado la app. |
| Registro | Usuarios que se han registrado en la app. |
| Último Login | Última vez que el usuario se ha logueado en la aplicación. |
| Apertura | Número de veces que el usuario ha entrado en la aplicación. |
| Login | Usuarios que se han logueado en la app. |
| Eventos de usuario | Usuarios con Evento Custom o Automático. Aparecerán los eventos integrados como custom en la app y los automáticos recogidos por nuestro SDK. |

- Los eventos por defecto estarán categorizados como Eventos automáticos:</br>![explorador_6.png](/explorador_6.png)
- Los eventos personalizados estarán categorizados como Eventos de usuario:</br>![explorador_7.png](/explorador_7.png)

Puedes ver más información sobre los eventos por defecto y personalizados [aquí](https://docs.emma.io/es/primeros-pasos/eventos). 

# Variantes de selección disponibles
A continuación se detalla un listado de las variantes de selección disponibles a día de hoy para poder realizar un filtrado de la información más específico.

> Ten en cuenta que estas variables de selección no están todas disponibles para cada filtro. Cada filtro acepta unas u otras variantes de selección en función del tipo de información que se está filtrando. {.is-info}

|     |     |
| --- | --- |
| Es igual a | El evento/etiqueta es igual a X valor/información. |
| No es igual a | El evento/etiqueta no es igual a X valor/información. |
| Contiene | La información contiene la palabra, caracter o conjunto de caracteres buscados. </br> Este operador sólo estará disponible para las [etiquetas de usuario](https://docs.emma.io/es/etiquetas). |
| Existe | El evento/etiqueta está seleccionado, marcado, cubierto. |
| No existe | El evento/etiqueta no está seleccionado, marcado, cubierto. |
| Mayor que | La información es superior a X valor. |
| Menor que | La información es inferior a X valor. |
| Más de | El evento se ha realizado más de X veces. |
| Menos de | El evento se ha realizado menos de X veces. |
| Se hizo | El evento se ha realizado. |
| No se hizo | El evento no se ha realizado. |
| Fue el | El evento ha sido realizado en una determinada fecha. |
| No fue el | El evento no ha sido realizado en una determinada fecha. |
| Fue hace | El evento ha sido realizado hace exactamente un tiempo determinado. |
| No fue hace | El evento no se ha realizado hace exactamente un tiempo determinado. |
| Hace más de | El evento ha sido realizada hace más de X días. |
| No hace más de | El evento no ha sido realizada hace más de X días, no se ha realizado nunca o se ha realizado en otro periodo. |
| Hace menos de | El evento se ha realizado hace menos de X días. |
| No hace menos de | El evento no se ha realizado hace menos de X días, nunca se ha realizado o se ha realizado en otro periodo. |
| Con la propiedad	 | Este operador devolverá todos los atributos que tenga vinculados el evento de la consulta. </br> Este operador sólo estará disponible para los eventos personalizados y los evento de session y login. |
| Se ha enviado | **Outapp:** Usuarios a los que se ha enviado la notificación push o email. </br>**Inapp:** Usuarios que han sido impactados por una comunicación (al menos una impresión). |
| No se ha enviado | **Outapp:** Usuarios a los que no se le ha enviado la notificación push o email. </br> **Inapp:** Usuarios que no han sido impactados por una comunicación. |
| Han hecho clic | **Outapp:** Usuarios que han abierto la notificación push o el email. </br> **Inapp:** Usuarios que han hecho clic en la comunicación inapp. En el caso del strip no aplica ya que es el único formato inapp no interactuable. |
| No han hecho clic | **Outapp:** Usuarios que no han abierto la notificación push o el email. </br> **Inapp:** Usuarios que no han hecho clic en la comunicación inapp. |

# Filtrar desde un csv
EMMA te da la oportunidad de cruzar tus csv's con nuestra base de datos.

Para ello debes seleccionar alguna de las etiquetas de usuario (Tags) que tu app nos esté enviando o atributos.

Si quieres subir un archivo puedes hacerlo en el botón de la nube que aparece a la derecha del filtro.  </b><p style="width:900px">![gente_10.png](/gente_10.png)</p>


Ten en cuenta que debe ser un csv con todos los valores en una única columna, sin espacios en blanco y sin el nombre del TAG en la cabecera.


Tienes dos opciones, cargar manualmente el .CSV a EMMA o si lo prefieres EMMA puede consultar una ruta de un servidor FTP siempre que se necesite. Recuerda poner siempre el usuario y contraseña de la ruta.

# Guardar audiencias
Puedes guardar los filtros seleccionados si los utilizas constantemente para que puedas consultarlos siempre que quieras de una manera rápida y sencilla o si quieres usarlos como [audiencia](https://docs.emma.io/es/audiencias) para enviar comunicaciones a tus usuarios. 

- Una vez seleccionados el/los filtros que quieras consultar, haz click en el botón superior **Guardar como audiencia**. </b><p style="width:900px">![gente_7.png](/gente_7.png)</p>

A la hora de guardar la audiencia lo primero que tienes que hacer es introducir un nombre para poder identificar tu audiencia posteriormente.

Activa el **Segmento de prueba** si quieres que un usuario con rol *Autor* pueda testear sus campañas de comunicación (amplía [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as) la información sobre los distintos roles).

Por último, ya sólo queda guardar tu adueincia. Para ello, tendrás que hacer clic en el botón **Guardar audiencia nueva** para tener acceso a él posteriormente. </b><p style="width:700px">![gente_8.png](/gente_8.png)</p>

Una vez creada tu audiencia, puedes cargarla desde el selector *Carga una audiencia*. <p style="width:400px">![gente_9.png](/gente_9.png)</p>

# Acciones de retargeting
Desde la sección Comportamiento > Gente, vas a poder consultar acciones de las fuentes de retargeting, tales como saber los dispositivos que han realizado x evento atribuido a una fuente de retargeting en un rango de fechas concreto, así como poder ver los dispositivos que han realizado clic o engage en las diferentes fuentes de retargeting. 

## Conocer los dispositivos que han hecho clic o engage
Para poder obtener información de los usuarios que han realizado clic o engagement en una fuente o fuentes de retargeting deberás seguir estos pasos: 

- Haz click en el botón verde **+ Agregar grupo** para añadir un nuevo criterio de filtrado. </b><p style="width:900px">![gente_filtro_rt_1.png](/gente_filtro_rt_1.png)</p>
- En el selector busca la opción **Retargeting**. </br></b><p style="width:900px">![gente_filtro_rt_2.png](/gente_filtro_rt_2.png)</p>
- Una vez seleccionada, elige entre los operadores ***han hecho clic*** o ***han hecho engagement*** en función de la información que deseas obtener. </b><p style="width:900px">![gente_filtro_rt_3.png](/gente_filtro_rt_3.png)</p>
- Selecciona la(s) campaña(s) de retargeting de la(s) que quieres obtener los datos. </b><p style="width:900px">![gente_filtro_rt_4.png](/gente_filtro_rt_4.png)</p>
- Por último, haz clic en el botón **Actualizar** para obtener el resultado de tu consulta. </b><p style="width:900px">![gente_filtro_rt_5.png](/gente_filtro_rt_5.png)</p>

> No es posible saber los dispositivos que han hecho clic en una fuente de retargeting de redes autoatribuidas como Google o Facebook, ya que la información del clic no esta vinculada a los usuarios, ya que las plataformas solo nos permiten recuperar la infromación del volumen de clics realizados.{.is-info}

## Conocer los dispositivos que han realizado un evento atribuido a retargeting
Para poder localizar los dispositivos que han llevado a cabo uno o varios eventos atribuidos a una campaña de retargeting, debes seguir estos pasos: 

- Haz click en el botón verde **+ Agregar grupo** para añadir un nuevo criterio de filtrado. </b><p style="width:900px">![gente_filtro_rt_1.png](/gente_filtro_rt_1.png)</p>
- Selecciona el evento deseado.
- Selecciona el nuevo operador **atribuido a**. </br></b><p style="width:900px">![gente_filtro_rt_6.png](/gente_filtro_rt_6.png)</p>
- Selecciona la fuente o fuentes de retargeting de la que deseas consultar la información. </b><p style="width:900px">![gente_filtro_rt_7.png](/gente_filtro_rt_7.png)</p>
- Selecciona el rango de fechas del que deseas obtener la información. </b><p style="width:900px">![gente_filtro_rt_8.png](/gente_filtro_rt_8.png)</p>
- Haz clic en el botón Actualizar. </br></b><p style="width:900px">![gente_filtro_rt_9.png](/gente_filtro_rt_9.png)</p>



> Ten en cuenta que el dato que reporta apptracker y el dato que reporta gente no tiene que ser coincidente, ya que en apptracker se visualizan eventos totales realizados en un rango de fechas, mientras que gente nos devuelve los dispositivos únicos que han realizado un evento concreto en un rango de fechas. {.is-info}

# Reporting: aprende a leer los resultados
El dashboard de Explorador ofrece los resultados de los datos en tres partes:

## Dispositivos que cumplen el filtrado
La primera de ellas engloba el primer recuadro en donde se muestran los dispositivos totales en BB.DD que cumplen con el filtro seleccionado: </b><p style="width:1100px">![gente_3.png](/gente_3.png)</p>
- 2.288 **Usuarios en total**: muestra los dispositivos totales de la base de datos independientemente del filtro empleado.
- 364 **Usuarios filtrados**: total de usuarios de la base de datos que cumplen la condición del/los filtros seleccionados. En el caso del ejemplo, estamos consultando usuarios que han realizado el evento conversión.

Puedes exportar los dispositivos únicos filtrados seleccionando el recuadro en el que aparece justo debajo del número de usuarios filtrados ***Exportar datos***.<p style="width:200px">![gente_4.png](/gente_4.png)</p>

Se mostrará en la pantalla una ventana emergente desde la que se puede personalizar las columnas que se quieren añadir a dicho exportación y dos opciones de descarga. Haz click 
[aquí](https://docs.emma.io/es/gente#exporta-tus-datos) para conocer el detalle que este reporting puede ofrecer.<p style="width:400px">![gente_6.png](/gente_6.png)</p><p style="width:400px">![gente_2.png](/gente_2.png)</p>

## Detalle de dispositivos que cumplen el filtado
Una vez realizada una consulta, podremos ver información genérica de todos los dispositivos sin necesidad de salir de la pantalla. </b><p style="width:1100px">![gente_5.png](/gente_5.png)</p>

De manera automática se carga una tabla con todos los dispositivos que cumplen la consulta establecida y podemos ver la siguiente información: 
|     |     |
| --- | --- |
| ID | Número de identificador de usuario de la plataforma EMMA. |
| ID de cliente | Tu número de identificación interno para identificar los usuarios de los clientes.|
| ID de dispositivo | Número de identificación único de iOS (IDFA) y Android (AAID). |
| Tipo de ID de dispositivo | **IDFA**, si es *Identifier for Advertisment* de iOS; **IDFV**, si es *Identifier for Vendor* o **GAID** si es *Google Advertiser ID*. |
| Email | Dirección electrónica del usuario registrado en la aplicación/sitio móvil. |
| Sistema Operativo | Sistema operativo del dispositivo en cuestión.|
| Dispositivo | Modelo de dispositivo del usuario a través del cual ha  utilizado la app/sitio móvil. |
| Ciudad | Ciudad en la que se encuentra el usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |

Además, en caso de estar enviando a EMMA el ID de cliente, podemos hacer clic en un id de cliente para acceder la pantalla de *Perfil de usuario*, desde dónde se puede ver en detalle información más concreta de ese usuario específico. Puedes ver más información sobre esta pantalla [aquí](https://docs.emma.io/es/perfil-usuario).

# Exporta tus datos
La exportación de datos es un tipo de reporting en bruto con toda la información disponible a tu alcance.

Tras realizar la consulta deseada, haz clic en el botón ***Exportar datos*** para que se abra la ventana emergente para seleccionar los KPIs de los que queremos descargar información. 

Una vez seleccionados, tan solo hay que hacer cic en la opción ***Descargar*** para una descarga directa o ***Enviar por email*** para que la exportación se envíe automáticamente al email. <p style="width:400px">![gente_2.png](/gente_2.png)</p><p style="width:400px">![gente_1.png](/gente_1.png)</p>

> La exportación directa **sólo** está disponible para exportaciones de menos de 10.000 usuarios.{.is-info}

En el documento exportado aparecerá el desglose por dispositivo y de cada uno de ellos obtendrás la siguiente información en función de las columnas que hayas seleccionado en la ventana emergente:

|     |     |
| --- | --- |
| ID | Número de identificador de usuario de la plataforma EMMA. |
| ID de cliente | Tu número de identificación interno para identificar los usuarios de los clientes.|
| Email | Dirección electrónica del usuario registrado en la aplicación/sitio móvil. |
| ID de dispositivo | Número de identificación único de iOS (IDFA) y Android (AAID). |
| Dispositivo | Modelo de dispositivo del usuario a través del cual ha  utilizado la app/sitio móvil. |
| Sistema Operativo | Sistema operativo del dispositivo en cuestión.|
| Versión del SO | Última versión del sistema operativo configurado en el dispositivo del usuario. |
| IP | Dirección donde el usuario ha conectado la app/sitio web. |
| Operador | Compañía de teléfono contratada por el usuario. |
| Idioma aceptado | Idioma del dispositivo. |
| Tipo de conexión | Indica si la conexión del usuario es a través de  WIFI o 3G. |
| Versión de la app | Ultima versión instalada en el dispositivo del usuario. |
| Versión de EMMA | Versión del SDK configurada en la última versión de la aplicación/sitio móvil. |
| Instalado el | Fecha en que el usuario ha abierto la aplicación por primera vez una vez descargada. |
| Desinstalado el | Fecha en que el usuario ha desinstalado la aplicación en el dispositivo. |
| Registrado el | Fecha en que el usuario se ha registrado por primera vez. |
| Primer inicio de sesión | La primera vez que el usuario ha realizado un inicio de sesión en la aplicación/sitio móvil. |
| Último inicio de sesión | La última vez que el usuario ha realizado un inicio de sesión en la aplicación/sitio móvil. |
| Usuario leal el | Momento en que el usuario se convierte en usuario loyal (inicio de sesión de usuario por quinta vez). |
| Comprador leal el | Momento en que el usuario se convierte en loyal buyer (a partir de la segunda compra por parte del usuario). |
| Última sesión el | Última vez que el usuario abre la aplicación/sitio móvil. |
| Conversiones | Número de compras que ha realizado el usuario. |
| Última conversión el | La última vez que el usuario compró a través de la app/sitio móvil. |
| Primera conversión el	| La primera vez que el usuario compró a través de la app/sitio móvil. |
| Ssegundos hasta la primera conversión | Momento en que el usuario hhizo la primera compra desde que instaló por primera vez la app/sitio móvil. |
| Latitud | Coordenadas de latitud del usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. |
| Longitud | Coordenadas de longitud del usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. |
| Ciudad | Ciudad en la que se encuentra el usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| País | País en el que se encuentra usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| Inactivo | Indica aquellos usuarios activos para recibir  comunicación Push (número 0) y aquellos usuarios inactivos (número 1). |
| ID de referrer | Identificador único de la source de campaña que informa sobre el origen del usuario por la campaña. |
| De la campaña | Campaña de la que procede el usuario. |
| De la fuente | Source de la campaña de la que procede el usuario. |
| TAGs | Valores atribuidos a los tags que se reciben desde la app en caso de existir. |

