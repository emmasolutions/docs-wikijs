---
title: Etiquetas
description: Etiquetas
published: true
date: 2022-02-28T10:40:25.304Z
tags: 
editor: markdown
dateCreated: 2021-06-07T12:23:22.992Z
---

En EMMA las etiquetas (*tags)* se utilizan para **identificar características únicas o estados del usuario**. Con este tipo de etiquetado se le asigna a un valor único a cada usuario y que se sobreescribirá automáticamente si un nuevo valor se dispara.

A diferencia de los eventos, las etiquetas no se dan de alta en EMMA, sino que se deben implementar en la app y una vez se envíe la información a EMMA desde la app, la etiqueta aparecerá automáticamente en EMMA con la información correspondiente para cada caso/usuario.

> Ten en cuenta que no todos los usuarios que se han instalado la app tienen por qué tener información en las etiquetas, ya que está información normalmente se envía desde la app a EMMA después de que el usuario haya pasado por el proceso de registro o login de la app o por alguna sección de configuración específica dentro de la app. 
Para poder ver para qué usuarios se ha enviado información en las etiquetas visita nuestra sección de [segmentos](https://docs.emma.io/es/analisis-de-comportamiento/segmentos). 
{.is-info}

Estos son algunos ejemplos habituales sobre la medición con Etiquetas:

-   País: España, Portugal, Reino Unido, US,...
-   Edad: 18, 19, 20, 21…
-   Número de teléfono: 650050550, 696362656, 652987541...
-   Notificaciones: Activado, Desactivado
-   Compartir en redes sociales: Sí, No.
-   Cumpleaños: 1988-11-01,1992-02-26...

Consulta nuestras guías de integración de [iOS](https://developer.emma.io/es/ios/integracion-sdk#propiedades-del-usuario-tags) y [Android](https://developer.emma.io/es/android/integracion-sdk#perfil-del-usuario) para ampliar información sobre cómo integrar las **Etiquetas.**

# Gestión de etiquetas

Dentro de la sección de EMMA **Gestión** vas a encontrar una pantalla llamada **Etiquetas,** desde donde podrás configurar las etiquetas que tu aplicación esté enviando a EMMA desde el SDK. Puedes ver cómo usarlos en las notificaciones [*Push*](/es/comunicacion/mensajes-out-app/push-notifications) y en las comunicaciones [*InApp*](https://docs.emma.io/es/mensajes-inapp)*.*

![](/etiquetas_i.png)

Para poder visualizar el listado de etiquetas en esta pantalla se debe haber [etiquetado](/es/otros#c%C3%B3mo-etiqueto-mi-app) previamente la aplicación, de no haberlo hecho, esta pantalla se mostrará vacía.

Aquí podrás ver todas las etiquetas que se están enviando a EMMA y podrás configurar el tipo de información que contendrá cada uno.

> Es responsabilidad de cada aplicación incluir en las etiquetas la información que ha acordado previamente. Así, si en esta pantalla se le asigna a un campo de valor numérico el tipo texto, EMMA lo interpretará como texto. 
{.is-warning}

![](/etiquetas_ii.png)

Haciendo *mouse over* sobre el menú contextual de cada etiqueta, podremos editarla para seleccionar el tipo de información que contiene cada etiqueta

-   **Texto**
-   **Número**
-   **Fecha**

![](/etiquetas_iii.png)

Una vez se selecciona el valor deseado, debemos hacer clic en el botón **\+ Guardar Etiqueta** para que los cambios realizados se apliquen.

Recuerda que según el tipo de información con la que esté configurada la etiqueta (texto, número o fecha), afectará a cómo se podrán filtrar y segmentar dichas etiquetas en EMMA.

En la siguiente tabla puedes ver que tipo de operador acepta cada tipo de tag.

-   Etiquetas de texto
    -   es igual a
    -   es distintos de
    -   contiene
    -   existe
    -   no existe
-   Etiquetas numéricas
    -   es igual a
    -   es distinto de
    -   existe
    -   no existe
    -   más de
    -   menos de
-   Etiquetas de fecha
    -   existe
    -   no existe
    -   fue el
    -   fue hace
    -   hace más de
    -   hace menos de
    -   el cumpleaños es
