---
title: Cuadro de mandos
description: Cuadro de mandos
published: true
date: 2023-05-03T11:53:55.452Z
tags: 
editor: markdown
dateCreated: 2021-05-31T11:44:23.643Z
---

En el cuadro de mandos puedes consultar todos os datos de tu aplicación de una manera rápida y visual que te permiten tener una idea global de todo lo que está pasando dentro de la aplicación como en el Resumen Ejecutivo. La principal, y gran diferencia, con el Resumen Ejecutivo es que esta pantalla te permite personalizar las gráficas para ver los datos más relevantes para tu app. A continuación te explicamos cómo personalizar los datos de tu aplicación. 

![3.png](/3.png)

# Crear cuadros de mandos

Haz login en EMMA y dirígete a la sección **General > Cuadro de mandos**.

![](/cuadro_mandos_1.png)

Como en todas las pantallas de EMMA, los datos se muestran en función del rango de fechas seleccionado, pudiendo seleccionar entre intervalos por defecto o especificar un rango de fechas personalizados. Puedes ver más información al respecto del filtrado de fechas [aquí](https://docs.emma.io/es/barras-de-menu#men%C3%BA-supra-principal). 

![](/cuadro_mandos_16.png)

El primer paso es crear un nuevo **Cuadro de mandos** en el que ir agregando todas los componentes de los que queremos que se muestre información. Si nunca has creado un cuadro de mandos, por defecto al acceder a esta pantalla ya estarás en el formulario de creación. En caso de que ya hayas creado algún cuadro de mandos anteriormente, tan solo tienes que hacer clic en el botón **\+ Nuevo cuadro de mandos** a la derecha de la pantalla para acceder al formulario de creación.

![](/cuadro_mandos_9.png)

![](/cuadro_mandos_2.png)

A continuación, puedes empezar a añadir todos los componentes que necesites para tu tablero. Para ello, tan solo tienes que hacer clic en el botón **\+ Añadir componente** y cubrir el formulario de creación con los datos pertinentes para cada caso en particular.

![](/cuadro_mandos_4.png)

-   **Título:** Establece, de manera opcional, un título identificativo para tu nuevo componente. 
-   **Descripción:** Establece, de manera opcional, una descripción para tu nuevo componente. 
-   **Tipo:** Selecciona el tipo de gráfica en la que quieres que se muestren los datos. Puedes elegir entre las siguientes opciones: 
    -   Gráfica de líneas
    -   Gráfica  de barras
    -   Gráfica de donut
    -   Contador
> La gráfica de tipo **donut** tiene un límite de 10 secciones/porciones. Si tu agrupación devuelve un resultado con más de 10 variables no se mostrarán todas en la gráfica. 
{.is-info}
-   **KPI:** Selecciona el KPI del cual quieres obtener información entre los siguientes: 
    -   [Instalaciones](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#filtros-disponibles)
    -   [Desinstalaciones](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#filtros-disponibles)
    -   [Aperturas](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#filtros-disponibles)
    -   [Usuarios activos](https://docs.emma.io/es/resumen-ejecutivo#introducci%C3%B3n)
    -   [Usuarios registrados](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#filtros-disponibles)
    -   [Compradores](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#filtros-disponibles)
    -   [Ingresos](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#contador-de-eventos)
-   **Agrupar por:** La agrupación sólo está disponible para las gráficas de tipo **barra** y **donut**. Puedes seleccionar que los datos se muestren agrupados en base a los siguientes elementos: 
    -   Fecha: Agrupa los datos por fecha.
    -   Sistema Operativo: Agrupa los datos por sistema operativo (Android o iOS).
    -   País: Agrupa los datos en función del país vinculado al dispositivo. 
    -   Campaña: Agrupa los datos por el nombre de la campaña de Apptracker. Más info sobre la gestión de campañas [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). 
    -   Tipo de fuente: Agrupa los datos en función de su origen, orgánico o de campaña. 
    -   Fuente de medios: Agrupa los datos en base al proveedor seleccionado a la hora de crear la fuente de Apptracker. Más info sobre la gestión de fuentes [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios). 
    -   Fuente: Agrupa los datos en base al nombre establecido a la fuente de Apptracker. Más info sobre la gestión de fuentes [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios). 
-   **Subagrupar por:** La subagrupación sólo está disponible para la gráfica de tipo **barras**. Puedes seleccionar que los datos se muestren subagrupados en base a las siguientes opciones: 
    -   Sistema operativo: Subagrupa los datos por sistema operativo (Android o iOS).
    -   País: Subagrupa los datos en función del país vinculado al dispositivo. 
    -   Campaña: Subagrupa los datos por el nombre de la campaña de Apptracker. Más info sobre la gestión campañas [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as).
    -   Tipo de fuente: Subagrupa los datos en función de su origen, orgánico o de campaña.
    -   Fuente de medios: Subagrupa los datos en base al proveedor seleccionado a la hora de crear la fuente de Apptracker. Más info sobre la gestión de fuentes de Apptracker [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios).

Además, también se pueden aplicar (o no) determinados filtros para obtener información granulada a medida de las necesidades específicas: 

-   **Sistema operativo:** Filtrar por Android o iOS para que solo se muestren datos de uno u otro sistema operativo. 
-   **País:** Filtra la información por país para que sólo se muestre la información de un país específico. Sólo se puede seleccionar un país a la vez. 
-   **Campaña:** Filtra por una campaña específica para que solo se muestren datos de esa campaña en concreto. Más info sobre gestión de campañas [aquí.](https://docs.emma.io/es/adquisicion/Campa%C3%B1as)
-   **Tipo de fuente:** Filtra por Orgánico o No-orgánico para obtener sólo información orgánica o de campaña. 

Una vez configurado el componente haz clic en el botón **Guardar componente** para guardar los cambios. Puedes agregar tantos componentes como necesites. Una vez añadidos todos los componentes deseados, establece un nombre identificativo para tu cuadro de mandos y haz clic en el botón **\+ Guardar** en la parte derecha de la pantalla. 

![](/cuadro_mandos_8_bis.png)

Puedes expandir el componente con el icono del recuadro, editarlo para cambiar la configuración haciendo clic en el icono del lápiz o eliminarlo haciendo clic en el icono de la papelera. Si seleccionas eliminar el componente deberás aceptar un pop up de confirmación para proceder con su borrado. 

![](/cuadro_mandos_6.png)

# Compartir cuadros de mandos

Una vez guardado tu cuadro de mandos, por defecto es un cuadro ***No compartido***, es decir, solo el usuario que lo ha creado tiene acceso a él. Pero si queremos compartirlo con otros usuarios tan solo tenemos que hacer clic en el botón **Compartir** que está en la parte derecha de la pantalla. 

![](/cuadro_mandos_9_bis_2.png)

A continuación, se abrirá un pop up en el que podemos seleccionar cómo queremos compartir nuestro cuadro de mandos. Las opciones disponibles son: 

-   **No compartido:** Opción por defecto. Sólo el usuario que ha creado el cuadro de mandos tendrá acceso a él. 
-   **Privado:** Además del creador del cuadro de mandos, sólo los usuarios a los que específicamente se le da acceso tendrán acceso al mismo. 
-   **Compartido:** Cualquier usuario con acceso a la app podrá acceder a ese cuadro de mandos. 

Saber si un cuadro de mandos no está compartido, es privado o compartido es sencillo. 

-   Los cuadros de mandos **No compartidos** no mostrarán nada al lado del nombre. 
-   Los cuadros de mandos **Privados** mostrarán un icono de un candado al lado del nombre. 
-   Los cuadros de mandos **Compartidos** mostrarán un icono de compartir al lado del nombre. 

Una vez realizada la configuración deseada, haz clic en el botón **Guardar.** 

![](/cuadro_mandos_17.png)

Además, en caso de ser necesario, también podremos descargar un archivo PDF con el contenido de nuestro cuadro de manos y la información respectiva en el rango de fechas seleccionado. Para ello, tan solo tenemos que hacer clic en el botón **PDF** en el pop up de compartir cuadro de mandos. 

![](/cuadro_mandos_10.png)

# Eliminar y editar cuadros de mandos

Los cuadros de mandos creados pueden ser editados para hacer las modificaciones pertinentes que sean necesarias o eliminados, pero solo el usuario que ha creado el cuadro de mandos puede realizar estas acciones. 

Para **editar** tu cuadro de mandos tan solo tienes que seleccionar el cuadro de mandos en cuestión y hacer clic en el botón **Editar** de la parte derecha de la pantalla. Con esto ya puedes editar individualmente cada uno de los componentes, eliminarlos o añadir nuevos componentes. 

![](/cuadro_mandos_9_bis.png)

Para **eliminar** tu cuadro de mandos debes seleccionar el cuadro en cuestión, hacer clic en el botón **Editar** y posteriormente hacer clic en el botón **Borrar.**

![](/cuadro_mandos_11.png)

A continuación se abrirá un pop up de confirmación para eliminar el cuadro de mandos. Haz clic sobre la opción **Ok** para eliminarlo o por el contrario, haz clic sobre **Cancelar** para mantener el cuadro de mandos. 

![](/cuadro_mandos_13.png)