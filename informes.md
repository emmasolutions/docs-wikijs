---
title: Informes
description: Informes
published: true
date: 2025-03-04T11:04:12.229Z
tags: 
editor: markdown
dateCreated: 2021-05-31T11:43:26.716Z
---

# Introducción

EMMA dispone de una pantalla permanente desde la que se podrá realizar la descarga de informes en el momento en el que estos sean necesarios.

Para llegar a esta pantalla de informes, tan solo se debe ir al menú principal y buscar la sección ***General > Informes.*** 

![informes_1.png](/informes_1.png)

Es importante tener en cuenta que, los datos que se van a descargar en cualquiera de los informes disponibles, siempre serán los correspondientes al periodo de fechas que se tenga previamente seleccionado. <p style="width:550px;">![fecha_informes.png](/fecha_informes.png)</p>


> Solo se permite la descarga del informe con un rango de fechas máximo de 90 días.
{.is-info}


Todos los informes tienen dos opciones de descarga:

![informes_3.png](/informes_3.png)

1.  **Descargar informe** > Con esta opción se produce la descarga directa del informe en el propio navegador. Dependiendo de la carga de datos que se tengan que procesar puede tardar un poco más en generarse.
2.  **Enviar por email** > Con esta opción se ejecuta el informe en backend y se envía por email una vez que haya sido creado. Dependiendo de la carga de datos puede tardar un poco más de tiempo en generarse. 

> El informe vía email se enviará al email con el que se haya accedido a la plataforma de EMMA. {.is-info}

Además, hay múltiples informes que permiten realizar una programación del mismo, para que se envíen de manera automática o incluso a través de un SFTP. 
Sea cual sea la opción, se puede seleccionar la frecuencia de descarga del informe entre las siguientes: 
  - **Cada hora:** Cada hora se recibe un informe con los datos de la hora anterior.
  - **Diario:** Cada día se recibe un informe con los datos del día anterior.
  - **Semanal:** Cada semana se recibe un informe con los datos de la semana anterior.
  - **Mensual:** Cada mes se recibe un informe con los datos del mes anterior.

Puedes ver más información sobre cómo programar tus informes de comunicación [aquí](https://docs.emma.io/es/informes#programando-tus-informes-de-comunicaci%C3%B3n). 

> Los informes que se generan son almacenados durante un periodo determinado de tiempo (4 o 24 horas en función del rango de fechas seleccionado), de manera que durante ese periodo, se descargará siempre el mismo informe. </br>
Los informes que no incluyan el día en curso, serán almacenados durante 24 horas. Esto permite que, si dentro de esas 24 horas se desea volver a descargar el mismo archivo para el mismo periodo de fechas, como ya se ha generado anteriormente la descarga será más rápida. </br>
Los informes que incluyan el día en curso, serán almacenados durante 4 horas. Todas las descargas del mismo periodo de fechas que se realicen dentro de esas 4 horas, mostrarán los mismos datos. Transcurridas 4 horas desde la primera generación del informe se generará un nuevo informe con los datos actualizados. {.is-info}

# Informes de adquisición

Dentro de los ***Informes de adquisición*** de EMMA, están disponibles para su descarga varios tipos de informes. A continuación, podrás ver más detalladamente cuáles son y en qué consiste cada uno de ellos. 

- Informe de [instalaciones asistidas](https://docs.emma.io/es/informes#informe-de-instalaciones-asistidas)
- Informe de [fuentes de medios](https://docs.emma.io/es/informes#informe-de-fuentes-de-medios)
- Informe de [parámetros](https://docs.emma.io/es/informes#informe-de-par%C3%A1metros)
- Informe de [instalaciones por usuarios únicos](https://docs.emma.io/es/informes#informe-de-instalaciones-por-usuarios-%C3%BAnicos)
- Informe de [clics atribuidos]()
- Informe de [clics no atribuidos](https://docs.emma.io/es/informes#informe-de-clics-no-atribuidos)

## Informe de instalaciones asistidas

El informe de *Instalaciones asistidas* es un informe referente a campañas de captación a través del que se podrá obtener información de la campaña ganadora de cada instalación, es decir, la campaña a la que está atribuida cada instalación, así como las campañas que han asistido a esa instalación, es decir, las campañas (hasta 3) con las que el usuario ha interactuado (clic) antes de instalar la app. 

> Si no se selecciona ninguna campaña, fuente y/o canal, por defecto se descargará información de todas las campañas que tengan instalaciones en el rango de fechas seleccionado.{.is-info}

![informes_40.png](/informes_40.png)

Al descargar el informe de *Instalaciones asistidas* obtendremos un CSV con la siguiente información: 

|     |     |
| --- | --- |
| **Fecha** | Fecha en la que se ha realizado la instalación. |
| **Campaña ganadora** | Nombre de la campaña ganadora, es decir, la que ha conseguido la atribución de la instalación. |
| **Fuente ganadora** | Nombre de la fuente ganadaora, es decir, la fuente que ha conseguido la atribución de la instalación. |
| **Canal ganador** | Nombre del canal vinculado a la fuente ganadora. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal*. |
| **Campaña asistida 1** | Nombre de la campaña asistida, es decir, la que ha asistido la atribución de la instalación. |
| **Fuente asistida 1** | Nombre de la fuente asistida, es decir, la fuente que ha asistdo la atribución de la instalación. |
| **Canal asistido 1** | Nombre del canal vinculado a la fuente asistida. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal*. |
| **Campaña asistida 2** | Nombre de la campaña asistida, es decir, la que ha asistido la atribución de la instalación. |
| **Fuente asistida 2** | Nombre de la fuente asistida, es decir, la fuente que ha asistido la atribución de la instalación. |
| **Canal asistido 2** | Nombre del canal vinculado a la fuente ganadora. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal*. |
| **Campaña asistida 3** | Nombre de la campaña asistida, es decir, la que ha asistido la atribución de la instalación. |
| **Fuente asistida 3** | Nombre de la fuente asistida, es decir, la fuente que ha asistido la atribución de la instalación. |
| **Canal asisitido 3** | Nombre del canal vinculado a la fuente ganadora. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal*. |



## Informe de Fuentes de medios

El informe de *Fuentes de medios* es un informe de rendimiento de campañas de captación en el que se obtendrá la información del periodo de fechas seleccionado. 

La información de las campañas y fuentes se mostrará desglosada por días y sólo se mostrarán aquellas fuentes que, en el periodo de fechas seleccionado, hayan tenido actividad (clics y/o instalaciones).

Gracias a los selectores disponibles, podremos seleccionar las campañas, fuentes y/o eventos de los que queremos descargar la información. 
> Si no se selecciona ninguna campaña y/o fuente, se descargarán por defecto todas aquellas que tengan datos (instalaciones y/o clics) en el rango de fechas seleccionado. {.is-info} 

</br><p style="width:1000px">![informes_26.png](/informes_26.png)</p>

> Es necesario seleccionar al menos 1 evento para poder descargar el informe. Se podrán seleccionar como máximo 15 eventos en cada informe. En caso de seleccionar más, el dashboard mostrará un mensaje de aviso y no permitirá ejecutar el informe. {.is-info}

</br><p style="width:1000px">![informes_13.png](/informes_16.png)</p>

De esta manera, si descargamos un informe de *Fuentes de medios* obtendremos un archivo .csv con la siguiente información:
|     |     |
| --- | --- |
| **Fecha** | Fecha en la que se ha realizado el clic y/o la instalación. |
| **S.O** | Sistema operativo. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su creación. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **Campaña** | Nombre de la campaña que ha generado clics y/o instalaciones. |
| **Tipo de campaña** | Información del tipo de campaña en cuestión, **Basic** o **Retargeting**. |
| **Fuente** | Nombre de la fuente que ha generado clics y/o instalaciones. |
| **Creada el** | Fecha en la que se ha creado la fuente en cuestión. |
| **Powlink** | Powlink de la fuente en cuestión. |
| **Powlink corto** | Powlink corto de la fuente en cuestión. Si no tiene porque es una fuente "vieja" aparecerá N/A. |
| **URL Android** | URL de redirección para dispositivos Android establecida en la configuración de la campaña en cuestión. |
| **URL iOS** | URL de redirección para dispositivos iOS establecida en la configuración de la campaña en cuestión. |
| **URL Huawei** | URL de redirección para dispositivos Huawei con App Gallery establecida en la configuración de la campaña en cuestión. |
| **URL Alternativa** | URL de redirección establecida en la configuración de la campaña para todos aquellos dispositivos que no son ni Android, ni iOS ni Huawei con App Gallery. |
| **Deeplink** | Deeplink configurado en la fuente en cuestión. En caso de no tener configurado un deeplink, este campo aparecerá vacío.
| **Creada por** | Email del usuario que ha creado la fuente en cuestión. |
| **Clics** | Número de clics generados ese día para esa fuente en concreto. |
| **Instalaciones** | Número de instalaciones generadas ese día para esa fuente en concreto. |
| **Reengagement** | Número de interacciones únicas en cada fuente en cuestión. Este campo solo aplica a campañas de Retargeting. |
| **Evento\_X** | Interacciones totales con el evento\_x ese día para esa fuente en concreto. Es decir, eventos totales realizados. |
| **Evento\_X Únicos*** | Interacciones únicas con el evento\_x ese día para esa fuente en concreto. Es decir, eventos únicos realizados.|

> *Para obtener esta información, EMMA se basa en IDs de clientes únicos. Si no se está enviando esta información (ID de cliente) desde la app o si hay usuarios que por cualquier motivo no tienen ID de cliente, no se tienen en cuenta y se mostrará 0 en el informe.{.is-info}

![informes_4.png](/informes_4.png)

## Informe de Parámetros

El informe de *Parámetros*, es un informe de rendimiento de campañas de adquisición con parámetros por instalación en el que se obtendrá la información de los parámetros de una fuente atribuidos a una instalación.</br><p style="width:1000px">![informes_27.png](/informes_27.png)</p>

A través de los selectores de campañas, fuentes y eventos, podremos establecer la configuración pertinente para descargar la información.
Por defecto, si no seleccionamos ninguna campaña y/o fuente, se descargarán todas aquellas que tengan datos (instalaciones) en el rango de fechas seleccionado. 

> Es necesario seleccionar al menos 1 evento para poder descargar el informe. Se podrán seleccionar como máximo 15 eventos en cada informe. En caso de seleccionar más, el dashboard mostrará un mensaje de aviso y no permitirá ejecutar el informe.{.is-info}

</br><p style="width:1000px">![informes_17.png](/informes_17.png)</p>

La información descargada será de todas las campañas que hayan generado instalaciones en el periodo de fechas previamente seleccionado. 

De esta manera, si se descarga un informe de *Parámetros* obtendremos un archivo .csv con la siguiente información:

|     |     |
| --- | --- |
| **ID** | Número de identificador de usuario de la plataforma EMMA. |
| **ID de dispositivo** | Número de identificación único de Android (AAID) e iOS (IDFA). |
| **Correo electrónico** | Dirección electrónica del usuario registrado en la aplicación móvil. |
| **ID de cliente** | Tu número de identificación interno para identificar los usuarios de los clientes. |
| **Instalación el** | Fecha en que el usuario ha abierto la aplicación por primera vez una vez descargada. |
| **Versión de la aplicación** | Última versión de la app instalada en el dispositivo del usuario. |
| **Ciudad** | Ciudad en la que se encuentra el usuario mientras ha estado utilizando la geolocalización en la app móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| **País** | País en el que se encuentra el usuario mientras ha estado utilizando la geolocalización en la app móvil. En caso de no usar geolocalización se realiza una traducción de la IP. |
| **ID de Referrer** | Identificador único de la fuente de la campaña que informa sobre el origen del usuario por la campaña. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su creación. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **De campaña** | Nombre de la campaña de la que procede el usuario. |
| **De fuente** | Nombre de la fuente de la campaña de la que procede el usuario. |
| **Exid** | Identificador de red personalizado como ID de clic. |
| **Camid** | Identificador de encargo extra como ID de campaña. |
| **DP** | Dispositivo (iPhone, iPad, Galaxy A1). |
| **Nombre de la aplicación** | Nombre de la app donde se hace el clic. |
| **Categorías de aplicación** | Categoría de la app donde se hace clic. |
| **Eat\_sub\[NUM\]** | Donde \[NUM\] es un número del 1 al 22. Información de parámetros extra. |
| **Evento_X** | Interacciones con el evento_x ese día para esa fuente en concreto. |

## Informe de Instalaciones por usuarios únicos
El informe de instalaciones por usuarios únicos nos permite saber de las instalaciones de las campañas y/o fuentes deseadas y de un rango de fechas determinado, cuántas son de un cliente ya existente (ID de cliente) en base de datos. 
De esta manera, del volumen total de instalaciones podemos determinar cuantas son de usuarios que ya eran clientes y cuantas no. 

Este informe también cuenta con unos selectores que nos permitirá determinar las campañas y/o fuentes de las que queremos obtener la información.

Por defecto, si no seleccionamos ninguna campaña y/o fuente se descargará la información de todas aquellas campañas que hayan tenido instalaciones en el rango de fechas seleccionado.</br><p style="width:1000px">![informes_28.png](/informes_28.png)</p>

Al descargar este informe obtendremos un archivo .csv con la siguiente información:
|     |     |
| --- | --- |
| **ID de dispositivo** | Id del dispositivo que ha instalado la app. |
| **ID de cliente** | Id de cliente vinculado al dispositivo que ha instalado la app. Si no se envía esta información o no se tiene en el momento de la descarga del informe, este campo aparecerá vacío. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su cración. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **Campaña** | Nombre de la campaña de la que procede el usuario. |
| **Fuente** | Nombre de la fuente de la campaña de la que procede el usuario. |
| **¿Es un usuario nuevo?** | Verificación de si el id de cliente existía previamente en base de datos o no, es decir, verificación de si se trata o no de un nuevo cliente. |


## Informe de clics atribuidos
El informe de clics atribuidos, es un informe de adquisición que nos permite descargar información de las campañas y/o fuentes que hayan tenido clics desglosando el número de clics realizado por cada dispositivo en las campañas pertinentes en el rango de fechas seleccionado. 

Este informe también cuenta con unos selectores que nos permitirá determinar las campañas y/o fuentes de las que queremos obtener la información.

Por defecto, si no seleccionamos ninguna campaña y/o fuente, se descargará la información de todas aquellas campañas que hayan tenido clics en el rango de fechas seleccionado. </br><p style="width:1000px">![informes_29.png](/informes_29.png)</p>

Al descargar este informe obtendremos un archivo .csv con la siguiente información:
|     |     |
| --- | --- |
| **Fecha** | Fecha en la que el dispositivo ha realizado el clic. |
| **Clicks** | Número de clics realizados por cada dispositivo. |
| **Dispositivo** | Modelo del dispositivo. En algunos casos en no se puede saber está información y en estos casos se mostrará el literal “undefined”. |
| **ID de cliente** | Id de cliente vinculado al dispositivo que ha realizado los clics. Si no se envía esta información, este campo aparecerá vacío. |
| **S.O.** | Sistema operativo del dispositivo desde el que se ha hecho clic. En algunos casos no se puede obtener esta información. En esos casos se mostrará el literal “Unknown”. |
| **Acción** | Se mostrará el literal “instalación” o “engagement” en función de si es un clic de instalación o de retargeting. |
| **ID de dispositivo** | Id del dispositivo que ha generado los clics. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su creación. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **Campaña** | Campaña a la que están vinculados los clics. |
| **Fuente** | Fuente a la que están vinculados los clics dentro de la campaña. |


## Informe de clics no atribuidos
El informe de clics no atribuidos, es un informe de adquisición que nos permite descargar información de las campañas que hayan tenido clics desglosando el número de clics realizado por cada dispositivo en las campañas pertinentes en el rango de fechas seleccionado. 

Este informe también cuenta con unos selectores que nos permitirá determinar las campañas y/o fuentes de las que queremos obtener la información.

Por defecto, si no seleccionamos ninguna campaña y/o fuente se descargará la información de todas aquellas campañas que hayan tenido clics en el rango de fechas seleccionado.</br><p style="width:1000px">![informe__clics_no_atribuidos.png](/informe__clics_no_atribuidos.png)</p>

> Este informe sólo permite descargar información de los últimos 2 días.
{.is-info}

Al descargar este informe obtendremos un archivo .csv con la siguiente información:
|     |     |
| --- | --- |
| **Fecha** | Fecha en la que el dispositivo ha realizado el clic. |
| **Clicks** | Número de clics realizados por cada dispositivo. |
| **Dispositivo** | Modelo del dispositivo. En algunos casos en no se puede saber está información y en estos casos se mostrará el literal “undefined”. |
| **ID de cliente** | Al tratarse de un clic no atribuido, no se puede saber el ID del cliente, por lo tanto esta campo aparececrá siempre vacío en este informe. |
| **S.O.** | Sistema operativo del dispositivo desde el que se ha hecho clic. En algunos casos no se puede obtener esta información. En esos casos se mostrará el literal “Unknown”. |
| **Acción** | Se mostrará el literal “instalación” o “engagement” en función de si es un clic de instalación o de retargeting. |
| **ID de dispositivo** | Al tratarse de un clic no atribuido, no se puede saber el ID del dispositivo, por lo tanto este campo aparecerá siempre vacío en este informe. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su creación. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **Campaña** | Campaña a la que están vinculados los clics. |
| **Fuente** | Fuente a la que están vinculados los clics dentro de la campaña. |

> Hablamos de un clic no atribuido cuando no ha generado una instalación o engagement. 
{.is-info}

# Informes de eventos
## Informe de eventos
El informe de eventos es un informe que nos va a permitir conocer el volumen de eventos totales y únicos generados en un rango de fechas determinado.</br><p style="width:1000px">![informes_31.png](/informes_31.png)</p>

> Es necesario seleccionar al menos 1 evento para poder descargar el informe. Se podrán seleccionar como máximo 15 eventos en cada informe. En caso de seleccionar más, el dashboard mostrará un mensaje de aviso y no permitirá ejecutar el informe.
{.is-info}

</br><p style="width:1000px">![informes_13.png](/informes_13.png)</p>

Asimismo se puede establecer una programación para este informe configurando la periodicidad y los emails destinatarios de dicho informe. Puedes ver más información sobre cómo programar los informes [aquí](https://docs.emma.io/es/informes#programando-tus-informes).

En este informe se descargará la información de los eventos seleccionados previamente de la siguiente manera: 
|     |     |
| --- | --- |
| **Evento únicos** | Número de veces únicas que se ha realizado un evento en el rango de fechas seleccionado.|
| **Evento totales** | Número de veces totales que se ha realizado un evento en el rango de fechas seleccionado. |

> En el informe se descargará dos columnas (únicos y totales) por cada evento seleccionado.{.is-info}

## Informe de eventos detallado
El informe de eventos detallados es un informe que nos va a permitir conocer en detalle los eventos únicos y totales realizados en un rango de fechas determinado y detallado por día y sistema operativo. 

> Para poder descargar el informe es necesario seleccionar al menos un evento y como máximo 15. 
{.is-info}

</br><p style="width:1000px">![informes_32.png](/informes_32.png)</p>

En este informe se descargará la información de los eventos seleccionados previamente de la siguiente manera: 
|     |     |
| --- | --- |
| **Día** | Día x del rango de fechas seleccionado. Cada día generará una nueva linea en el informe.|
| **SO** | Sistema operativo que ha generado los eventos en el día en cuestión. |
| **Nombre del evento** | Nombre de cada evento de los seleccionados para descargar el informe. |
| **Token del evento** | Token de cada uno de los eventos seleccionados para descargar el informe. Puedes ver más información sobre qué es un token [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados). |
| **Únicos** | Eventos únicos realizados. |
| **Totales** | Eventos totales realizados. |

## Propiedades de eventos
El informe de propiedades de eventos es un informe que nos va a permitir conocer la concurrencia de las propiedades de uno o varios eventos en concreto. En este informe, se podrá seleccionar el evento o eventos deseado así como las propiedades que se desean descargar.</br><p style="width:1000px">![informes_33.png](/informes_33.png)</p>

> Es necesario seleccionar al menos 1 evento para poder descargar el informe. Se podrán seleccionar como máximo 15 eventos en cada informe. En caso de seleccionar más, el dashboard mostrará un mensaje de aviso y no permitirá ejecutar el informe. </br></br>Las porpiedes no tienen límite y se podrán seleccionar todas las existentes.
{.is-info}

</br><p style="width:1000px">![informes_13.png](/informes_13.png)</p>

Asimismo se puede establecer una programación para este informe configurando la periodicidad y los emails destinatarios de dicho informe. Puedes ver más información sobre cómo programar los informes [aquí](https://docs.emma.io/es/informes#programando-tus-informes).

> Para poder hacer uso de este informe es necesario que desde la app se estén envinado eventos con propiedades. Puedes ver más info al respecto de los eventos con propiedades [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados). 
{.is-warning}

En este informe se obtendrá la siguiente información:
|     |     |
| --- | --- |
| **Nombre del evento** | Se mostrará el nombre del evento descargado.|
| **Propiedades x del evento** | Se mostrará una columna por cada una de las propiedades seleccionadas. Si por ejemplo se seleccionan 4 propiedades, se mostrarán 4 columnas, una por cada propiedad.|
| **Eventos totales** | Número de veces totales que se ha realizado la combinación de las propiedades del evento. |
| **Eventos únicos** | Número único de veces que se ha realizado la combinación de las propiedades del evento. |

> Solo se podrá descargar como máximo un rango de 90 días de una vez.
{.is-info}

## Informe de eventos atribuidos
El informe de eventos atribuidos es un informe que nos va a permitir ver los dispositivos que han realizado un evento concreto en un rango de fechas determinado y que además está atribuído a una campaña específica. 

> Se pueden seleccionar varias campañas/fuentes pero un único evento en cada exportación.
{.is-info}

</br><p style="width:1000px">![informes_34.png](/informes_34.png)</p>

En este informe obtendremos la siguiente información: 
|     |     |
| --- | --- |
| **ID de cliente** |ID de cliente vinculado al dispositivo que ha realizado el evento. |
| **ID de dispositivo** | ID de dispositivo que ha realizado el evento. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su creación. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **Campaña** |Nombre de la campaña a la que está atribuido el evento en el apptracker. |
| **Fuente** | Nombre de la fuente de medios a la que está atribuido el evento en el apptracker. |
| **S.O.** | Sistema operativo del dispositivo que ha realizado el evento. |
| **Dispositivo** | Modelo del dispositivo que ha realizado el evento. |
| **Fecha** | Fecha en la que el dispositivo ha realizado el evento. |
| **Nombre del evento** | Nombre del evento realizado. |

## Informe de eventos por cliente
El informe de eventos por cliente es un informe que nos va a permitir ver el id de cliente que ha realizado un evento concreto y la fecha exacta de la realización dentro de un rango de fechas determinado. 

> Se pueden seleccionar un máximo de 15 eventos a la vez en una única exportación. El periodo de fechas máximo de exportación, son los últimos 3 meses, al igual que en el resto de informes. {.is-info}

</br><p style="width:1000px">![informes_25.png](/informes_25.png)</p>

En este informe obtendremos la siguiente información: 
|     |     |
| --- | --- |
| **ID de cliente** |ID de cliente vinculado al dispositivo que ha realizado el evento. |
| **Token** | Token del evento identificativo en EMMA. Puedes ver la correlación entre token y nombre [aquí](https://docs.emma.io/es/primeros-pasos/eventos#gestionar-eventos). |
| **Fecha** | Fecha en la que el dispositivo ha realizado el evento. |

# Informes de datos en bruto

## Instalaciones

El informe de instalaciones es un informe con datos en bruto de todas las instalaciones, tanto orgánicas como de captación, en el periodo de fechas seleccionado. 

> A la hora de realizar la descarga del informe de instalaciones, aunque están disponibles ambas opciones, si se va a realizar una descarga de datos importante, recomendamos la opción de descarga vía email.
{.is-info}

</br><p style="width:1000px">![informes_35.png](/informes_35.png)</p>


En este informe obtendremos la siguiente información:

|     |     |
| --- | --- |
| **ID** | Número de identificador de usuario de la plataforma EMMA. |
| **Id de dispositivo** | Número de identificación único de Android (AAID) e iOS (IDFA). |
| **Correo electrónico** | Dirección electrónica del usuario registrado en la aplicación móvil. |
| **ID de cliente** | Tu número de identificación interno para identificar los usuarios de los clientes. |
| **Instalación el** | Fecha en que el usuario ha abierto la aplicación por primera vez una vez descargada. |
| **Registrado el** | Fecha en que el usuario se ha registrado por primera vez. |
| **Primer Login** | La primera vez que el usuario ha realizado un inicio de sesión en la aplicación móvil. |
| **Último Login** | La última vez que el usuario ha realizado un inicio de sesión en la aplicación móvil. |
| **Dispositivo** | Modelo de dispositivo del usuario a través del cual ha utilizado la app móvil. |
| **Versión de la aplicación** | Última versión de la app instalada en el dispositivo del usuario. |
| **Versión del SO** | Última versión del sistema operativo instalada en el dispositivo del usuario. |
| **EMMA Build** | Versión del SDK configurada en la última versión de la aplicación móvil. |
| **Calificado** | Indica aquellos usuarios que han calificado la aplicación móvil (número 1) y aquellos usuarios que no la han calificado (número 0). |
| **Inactivo** | Indica aquellos usuarios activos para recibir notificaciones Push (número 0) y aquellos usuarios inactivos (número 1). |
| **Latitud** | Coordenadas de latitud del usuario, mientras ha estado utilizando la geolocalización en la app móvil. En caso de no usar geolocalización, se realiza una traducción de la IP. |
| **Longitud** | Coordenadas de longitud del usuario, mientras ha estado utilizando la geolocalización en la app móvil. En caso de no usar geolocalización, se realiza una traducción de la IP. |
| **Ciudad** | Ciudad en la que se encuentra el usuario, mientras ha estado utilizando la geolocalización en la app móvil. En caso de no usar geolocalización, se realiza una traducción de la IP. |
| **País** | País en el que se encuentra el usuario, mientras ha estado utilizando la geolocalización en la app móvil. En caso de no usar geolocalización, se realiza una traducción de la IP. |
| **ID de Referrer** | Identificador único de la fuente de la campaña que informa sobre el origen del usuario por la campaña. |
| **IP** | Dirección IP desde donde el usuario ha conectado la app. |
| **Usuario leal el** | Momento en que el usuario se convierte en usuario leal (inicio de sesión de usuario por quinta vez). |
| **Comprador leal el** | Momento en que el usuario se convierte en comprador leal (a partir de la segunda compra por parte del usuario). |
| **Última sesión el** | Última vez que el usuario ha abierto la aplicación móvil. |
| **Conversiones** | Número de compras que ha realizado el usuario. |
| **Última conversión el** | Última vez que el usuario compró a través de la app móvil. |
| **Primera conversión el** | Primera vez que el usuario compró a través de la app móvil. |
| **Segundos hasta la primera conersión** | Segundos transcurridos desde que el usuario instala la app hasta que realiza la primera compra a través de la misma. |
| **Idioma aceptado** | Idioma del dispositivo. |
| **Operador** | Compañía de teléfono contratada por el usuario. |
| **Tipo de conexión** | Indica si la conexión del usuario es a través de WIFI o 3G. |
| **Canal** | Nombre del canal vinculado a la fuente en cuestión en el momento de su creación. Si no se ha asignado ningún canal, aparece por defecto *Sin Canal* |
| **De campaña** | Nombre de la campaña de la que procede el usuario. |
| **De fuente** | Nombre de la fuente de la campaña de la que procede el usuario. |
| **Postback de instalación** | Notificación de la instalación enviada al *"Provider".* |
| **TAGs** | Valores atribuidos a cada una de las etiquetas que se reciben desde la app en caso de existir. Podemos seleccionar las etiquetas que queremos que incluya el informe desde el selector.|
| **Events** | Contador del número de veces que cada dispositivo ha realizado alguno de los eventos que se envían desde la app. Podemos seleccionar los eventos que queremos que incluya el informe desde el selector.  |

# Informes de comunicación

## Informe de Campañas activas

El informe de *Campañas activas* es un informe relativo a las campañas de comunicación activas en un periodo de tiempo determinado. 

Este informe nos permite ver en un documento todas las campañas que están o estuvieron activas en el periodo de fechas seleccionado así como, poder obtener información relativa a las mismas como impresiones, impresiones únicas y clics.  </br><p style="width:500px">![informes_36.png](/informes_36.png)</p>

Al igual que en el resto de informes, al descargar este, obtendremos un archivo .csv con la siguiente información:

|     |     |
| --- | --- |
| **ID del informe** | Identificador de la campaña en EMMA. Se corresponde con el ID de la pantalla de Informe de comunicaciones. |
| **ID de campaña** | Identificador de la campaña en EMMA. Se corresponde con el ID de la pantalla de Mensajes. |
| **Campaña** | Nombre de la campaña de comunicación. |
| **Formato** | Formato de la comunicación. Puede ser Adball, Banner, Coupon, Dynamic Tab, Email, Native Ad, Push, Startview y Strip. |
| **Plantilla** | Nombre de la plantilla de Native Ad usada en cada una de las comunicaciones Native Ad. |
| **Pantalla** | Relativo a las comunicaciones automáticas para reglas. Nombre de la pantalla en la que se está mostrando la comunicación. |
| **Frecuencia** | Frecuencia establecida a la campaña de comunicación. Si se muestra 0 significa que no se ha establecido frecuencia y esta es infinita mientras la campaña esté activa. |
| **Límite de impactos diarios** | Límite de impactos diarios establecido para la campaña de comunicación. Si se muestra 0 significa que no se ha establecido límite y este es infinito mientras la campaña esté activa. |
| **Límite de impactos semanales** | Límite de impactos semanales establecido para la campaña de comunicaciónSi se muestra 0 significa que no se ha establecido límite y este es infinito mientras la campaña esté activa. |
| **Límite de impactos mensuales** | Límite de impactos mensuales establecido para la campaña de comunicación. Si se muestra 0 significa que no se ha establecido límite y este es infinito mientras la campaña esté activa. |
| **Comienzo** | En las campañas clásicas, fecha y hora de inicio de la campaña. En las campañas automáticas para reglas, fecha de inicio de la regla. |
| **Fin** | En las campañas clásicas, fecha y hora de finalización de la campaña. En las campañas automáticas para reglas, fecha de finalización de la regla (las reglas pueden no tener fin). |
| **Prioridad** | Prioridad establecida a la regla del 1 al 9. A mayor número, mayor prioridad. |
| **Impresiones** | Número de veces que la comunicación ha sido mostrada por campaña (impresiones totales). |
| **Impresiones únicas** | Número de dispositivos en los que se ha mostrado la comunicación. |
| **Clics** | Número de veces que el usuario interactúa con la comunicación por campaña. |
| **CTR** | Porcentaje de dispositivos que han hecho clic en la comunicación entre las impresiones totales por campaña. |

## Informe de Visualización de campañas In-App

El informe de *Visualización de campañas* es un informe que nos aporta información sobre los usuarios que han visto cada comunicación, así como información genérica para poder identificar la campaña y el estado de la misma que ha visualizado el usuario. 

> La descarga de este informe está límitado a los últimos 30 días. No se podrá realizar una descarga con un rango de fechas superior.
{.is-warning}

Este informe nos permite ver en un documento todas los usuarios que han visualizado X comunicaciones así como, conocer entre otros, el id de campaña, el estado o la fecha de la impresión.

</br><p style="width:500px">![informes_37.png](/informes_37.png)</p>

Al igual que en el resto de informes, al descargar este, obtendremos un archivo .csv con la siguiente información:

|     |     |
| --- | --- |
| **Campaña** | Nombre de la campaña de comunicación. |
| **ID de campaña** | Identificador de la campaña en EMMA. Se corresponde con el ID de la pantalla de Mensajes. |
| **ID de cliente** | ID de Cliente vinculado al dispositivo que ha visualizado la comunicación. |
| **Frecuencia** | Frecuencia establecida a la campaña de comunicación. Si se muestra 0 significa que no se ha establecido frecuencia y esta es infinita mientras la campaña esté activa. |
| **Estado** | Estado en el que se encuentra la campaña. Puede ser Pausada, Activa... Puedes ver más información sobre los diferentes estados de una campaña [***aquí***](https://docs.emma.io/es/mensajes-inapp#estados-de-las-campa%C3%B1as). |
| **Formato** | Formato de la comunicación. Puede ser Adball, Banner, Coupon, Dynamic Tab, Email, Native Ad, Push, Startview y Strip. |
| **Tipo** | Tipo de comunicación: Clásica o automática. |
| **Fecha Primera Visualización** | Fecha y hora en la que el usuario visualizó por primera vez la comunicación en cuestión. |
| **Inicio** | Fecha y hora de inicio de la campaña. En las campañas automáticas no se muestra esta información ya que no existe como tal una fecha de inicio de la campaña. |
| **Fin** | Fecha y hora de fin de la campaña. En las campañas automáticas no se muestra esta información ya que no existe como tal una fecha de fin de la campaña. |
| **ID de plantilla** | ID de la plantilla usada en la comunicación. Este campos solo aplica al formato Native AD. En el resto de formatos in-app aparecerá vacío. Puedes ver más información sobre el formato Native ad [aquí](https://docs.emma.io/es/mensajes-inapp#native-ad). |
| **Categoría** | Nombre de la categoría configurada en la comunicación (en caso de que aplique). Puedes ver más información sobre las categorías [aquí](https://docs.emma.io/es/categorias). |
| **Test A/B** | Si la comunicación tiene variaciones de Test A/B o no. Puedes ver más información sobre el Test A/B [aquí](https://docs.emma.io/es/test-a-b). |
| **Primer clic** | Fecha en la que cada usuario ha realizado el primer clic sobre la comunicación en cuestión. |

## Informe de Visualización de campaña Out-App (Push)
El informe de Visualización de campañas Out-App es un informe que nos aporta información sobre los usuarios que han recibido una notificación push y si han abierto o no esa notifiación, así como información más genérica para poder identifcar al usuario, la campaña y el estado de la misma. 
> Este informe está basado en los IDs de cliente que han recibido una notificación push. Es posible que haya dispositivos que no tengan vinculado un ID de cliente, pero que sí reciban la notificación push. Esos casos, no se mostrarán en este informe de ninguna manera. 
{.is-warning}

Este informe nos permite ver en un documento todas los usuarios (ID cliente) que han recibido y abierto X notificación push así como conocer, entre otros, el estado o la fecha de la apertura y envío de la comunicación.

</br><p style="width:500px">![informes_38.png](/informes_38.png)</p>

Al igual que en el resto de informes, al descargar este, obtendremos un archivo .csv con la siguiente información:
|     |     |
| --- | --- |
| **Campaña** | Nombre de la campaña de push. |
| **ID de cliente** | ID de Cliente vinculado al dispositivo que ha recibido la notifiación push específica. |
| **Estado de la campaña** | Estado en el que se encuentra la campaña. Puede ser Pausada, Activa... Puedes ver más información sobre los diferentes estados de una campaña [***aquí***](https://docs.emma.io/es/mensajes-inapp#estados-de-las-campa%C3%B1as). |
| **Tipo de campaña** | Tipo de comunicación: Clásica, Automática para reglas o Automática para rutas de cliente. |
| **Fecha de apertura** | Fecha y hora de en la que el usuario ha abierto la notificación push. Si la notificación no ha sido abierta, este campo saldrá vacío. |
| **Fecha de envío** | Fecha y hora en la que se ha enviado la notificación push. |

# Informes de ventas
El informe de ventas es un informe que nos va a permitir ver detalladamente un listado de las compras que se han llevado a cabo a través de la aplicación en un rango de fechas determinado, lo que nos permitirá poder validar los datos con una fuente externa o simplemente llevar un control de las ventas realizadas. </br><p style="width:500px">![informes_39.png](/informes_39.png)</p>

En este informe se obtendrá la siguiente información: 
|     |     |
| --- | --- |
| **ID del pedido** | ID del pedido reliazado. Se trata de un ID único para cada commpra que se envía a través de la app.|
| **ID de cliente** | ID de Cliente vinculado al dispositivo que ha realizado la compra. |
| **Cantidad** | Importe total de cada una de las diferentes compras realizadas. |
| **Código de moneda** | Se muestra el código de la divisa que se tenga seleccionado en EMMA en [preferencias de la app](https://docs.emma.io/es/configuracion#localizaci%C3%B3n). Este código es en base al estándar internacional ISO. |
| **Creada el** | Fecha y hora de en la que el usuario ha realizado la compra a través de la aplicación. Se mostrará la información según la zona horaria seleccionada en [preferencias de la app](https://docs.emma.io/es/configuracion#localizaci%C3%B3n). |

# Programando tus informes
EMMA da la posibilidad de programar múltiples de sus informes para que se envíen de manera automática a uno o varios correos electrónicos o para que se envíen directamente a un SFTP determinado. 
Para programar tus informes y que se envíen automáticamente tan solo tienes que seguir estos pasos: 

- Haz clic en el icono del reloj para programar en el informe deseado. </br><p>![programar_informes_1.png](/programar_informes_es_5.png)</p>
- Cubre el formulario de configuración:</br><p style="width:1000px;">![programar_informes_2.png](/programar_informes_2.png)</p>
  - **Periodo:** Selecciona entre las opciones ***Cada hora***, ***Diario***, ***Semanal***, o ***Mensual***. Con la opción ***Cada hora*** el informe se enviará cada hora con los datos de la hora anterior. Con la opción ***Diario*** el informe se enviará cada día con los datos del día inmediatamente anterior. Con la opción ***Semanal*** el informe se enviará el día de la semana seleccionado (lunes-domingo) con los datos de los 7 días inmediatamente anteriores. Con la opción ***Mensual*** el informe se enviará el día 1 de cada mes con los datos del mes anterior. 
  - **Destinatarios del informe:** Establece los correos electrónicos de aquellas personas que deban recibir el informe. 
- Activa el check **SFTP** si quieres enviar el informe a un SFTP y establece la configuración necesaria: </br><p style="width:1000px;">![programar_informes_3.png](/programar_informes_3.png)</p>
  - **Host:** Establece la ruta exacta del SFTP al que se va a enviar el informe. Ejemplo: sftp://host/path
  - **Usuario:** Configura un usuario con acceso al SFTP configurado. 
  - **Contraseña:** Configura la contraseña del usuario con acceso al SFTP configurado en el paso anterior. 


> Los informes programados se enviarán en el periodo seleccionado a las 9:00 UTC. </br> Ten en cuenta también que se enviará en formato .zip o .csv en función de la configuración que esté establecida en [Preferencias App](https://docs.emma.io/es/configuracion#informes). 
{.is-info}

# Guardar la configuración de los informes
Si realizas múltiples configuraciones de un mismo informe para obtener los datos detallados que necesitas, puedes guardar esa configuración para no tener que estar seleccionando todos los KPIs cada vez que necesites obtener la información. 

## Guardar configuración

Para guardar una configuración nueva, tan solo tienes que seguir estos pasos: 
- Localizar el informe en cuestión.
- Realizar la configuración deseada. 
- Hacer clic en el icono de guardado. </br><p style="width: 150px">![informes_21.png](/informes_21.png)</p>
- Establcer un nombre identificativo y hacer clic en el icono de guardar. </br><p style="width: 500px">![informes_22.png](/informes_22.png)</p>

> El icono de guardado aparece siempre desbloqueado, pero solo permite guardar una configuración si se han cubierto los campos obligatorios del informe.{.is-warning}

## Cargar configuración 

Puedes guardar tantas configuraciones como necesites y para cargarlas simplemente tienes que seguir estos pasos: 
- Localizar el informe en cuestión. 
- Hacer clic en el icono de guardado. </br><p style="width: 150px">![informes_21.png](/informes_21.png)</p>
- Seleccionar la configuración guardada que deseas cargar. </br><p style="width: 300px">![informes_23.png](/informes_23.png)</p>

> Para eliminar una configuración, basta con hacer clic en el icono de la papelera que aparece a la derecha del nombre de cada configuración. </br><p style="width: 200px">![informes_24.png](/informes_24.png)</p>{.is-info}
