---
title: Facebook
description: Facebook
published: true
date: 2024-01-25T12:55:55.363Z
tags: 
editor: markdown
dateCreated: 2020-11-25T23:10:25.883Z
---

![meta.png](/meta.png)

# Seguimiento con Facebook App Installs

[*Facebook App* Installs](https://developers.facebook.com/docs/ads-for-apps/mobile-app-ads?locale=es_ES) es el formato de Facebook orientado para llegar a nuevos usuarios mediante dispositivos móviles.

![](/fb_9.png)

En esta guía se explican todos los pasos de configuración necesarios para comenzar a medir tus campañas con *Facebook App Installs*. 

## Programa de Medición Móvil Avanzada de Facebook (AMM)

En 2021, Facebook ha anunciado que el programa de Medición Móvil Avanzada (AMM) quedará obsoleto el 29 de octubre de 2021. Esto significa que desde EMMA ya no vamos a poder acceder a los datos de las instalaciones de clics de anuncios de Facebook a nivel de dispositivo. Por lo que, al igual que pasó con los datos de las instalaciones de impresiones, Facebook no nos va a facilitar información del emplazamiento del anuncio (Facebook o Instagram), ni de la campaña ni del grupo de anuncios del que proviene cada instalación. La única información que nos va compartir es que es una instalación “Restricted”. 

> A partir del 29 de octubre de 2021, todas las instalaciones de tus campañas de Facebook e Instagram, en EMMA se van a ver bajo una misma fuente denominada *“Facebook / Instagram Restricted”.* 
{.is-warning}


Este cambio no va a afectar al rendimiento de las campañas activas en dicha plataforma, simplemente afecta a como la información será visualizada desde EMMA a partir del 29 de octubre cuando el programa AMM quede obsoleto. 

El 22 de abril de 2020 Facebook realizó un cambio que afectaba directamente al Programa de Medición Móvil Avanzada (AMM) por el cual restringían la información de aquellas instalaciones de impresiones de los anuncios de Facebook. Con este cambio, EMMA dejó de recibir la información de los anuncios a nivel de dispositivo y no podíamos saber de que emplazamiento venía la instalación (Facebook o Instagram) así como tampoco podíamos saber  la información de la campaña y el grupo de anuncios al que pertenecía esa instalación. Es por eso, que las instalaciones de impresiones en anuncios de Facebook, se empezaron a mostrar bajo una fuente denominada *“Facebook / Instagram Restricted”*. 


## Configuración en Facebook

-   Registra tu App con Facebook y obtén el ***Facebook App ID*** (lo necesitarás más adelante). Puedes seguir instrucciones específicas [aquí](https://developers.facebook.com/docs/ads-for-apps/mobile-app-ads?locale=es_ES) en “*Step 1: Register your Mobile App with Facebook*”

![](/fb_10.png)

-   Acepta los [**términos y condiciones de Facebook**](https://www.facebook.com/ads/manage/advanced_mobile_measurement/app_based_tos/) desde todas las cuentas con rol de administrador en *Facebook Developers.*
-   Obtén el Facebook App ID, lo necesitarás más adelante para realizar al configuración en EMMA. Este ID se obtiene de la consola de *Facebook for Developers.*

## Configuración en EMMA

https://youtu.be/iiZe3tQrDII

Para empezar a medir tus campañas en EMMA tienes que seguir los siguientes pasos para finalizar la configuración:

-   Haz [login](https://ng.emma.io) en EMMA y dirígete a **Preferencias App** desde el menú desplegable pasando por encima de tu nombre de usuario (arriba a la derecha)</br><p>![pref_app.png](/pref_app.png)</p>
-   Configura la información de **AppStore** (url de la app en la store) y **AppStore ID** para **iOS** y la información de **Google Play** (url de la app en la store) y **Google Play ID** para **Android.**
![](/fb_12.png)
![](/fb_13.png)

Para sacar el *App Store ID* puedes copiar el ID a través del enlace generado en AppStore, como en el siguiente ejemplo:
![](/fb_14.png)

Para sacar el *Google Play ID* puedes copiar el ID a través del enlace generado en GooglePlay, como en el siguiente ejemplo:
![](/fb_15.png)

-   Una vez configurado esto, dirígete a ***Captación > Fuentes de Medios**.*
![](/fb_16.png)

-   Busca la fuente de medios **Facebook** y en el menú lateral haz clic en editar
![](/fb_17.png)

-   Configura el **Facebook App Id** obtenido anteriormente y haz clic en **Guardar.**
![](/fb_18.png)

- Si tienes la versión del SDK de Android 4.11.1 o posterior, deberás cubrir el campo ***Facebook Referrer clave de desencriptado*** para poder medir instalaciones de las campañas de Facebook App Install. ![fb_36.png](/fb_36.png)
[Aquí](https://docs.emma.io/es/guias-de-integracion/facebook#c%C3%B3mo-conseguir-la-clave-de-descifrado-para-el-facebook-referrer) puedes ver más información sobre cómo obtener la calve de desencriptado. 

-   Si todo se ha realizado correctamente, encontrarás las campañas de Facebook en la sección ***Captación > Apptracker**.* Una vez la campaña esté activa, los datos de rendimiento entrarán automáticamente EMMA.
![](/fb_19.png)

## Cómo conseguir la clave de descifrado para el Facebook Referrer

La clave de descifrado para el Facebook Referrer es necesaria para que EMMA pueda recibir la información de Facebook y pueda descrifrar la información de los metadatos de Play Store. 

La clave de descifrado tiene 64 caracteres compuestos por letras minúsuclas (a-f) y números (0-9). A continuación puedes ver un ejemplo de clave de descifrado: 

1ad04f32e2df7be2308a22c0af230765a2dae1af6face8ad952e03fc351024ec

Para conseguir esta clave, sólo tienes que seguir estos pasos: 
-  Inicia sesión en Facebook para desarrolladores. 
- Selecciona la aplicación para la que quieres obtener la clave de descifrado. 
- En el menú lateral izquierdo, haz clic en la opción **Configuración > Básica**. <p style="width:300px">![fb_37.png](/fb_37.png)</p>
- Ve a la sección de **Android**. 
- Dentro de la sección de Android, dirígite a la sección de **Google Play** para encontrar la clave de descifrado del install referrer. ![fb_38.png](/fb_38.png)
- Copia el Install Referrer y confíguralo en el campo correspondiente en EMMA. 

### Consideraciones a tener en cuenta sobre el uso del install referrer

Facebook no puede enviar en todos los casos el install referrer e a Google Play y por tanto hay una serie de casuísticas a tener en cuenta. 

- El install referrer solo se envía en caso de instalaciones click-through, es decir, en los casos en los que el usuario haga clic en un anuncio de Facebook, sea llevado directamente a la store y en ese momento instale le aplicación. 
En el caso de que el usuario haga clic en el anuncio, vaya a la store, pero NO descargue la app en ese momento, sino que vaya posteriormente a la store y la descargue, ya no estará el install referrer y por tanto no se podrá atribuir la instalación en EMMA, pero si en Facebook (siempre que esté dentro de la ventana de atribución configurada). 

- Las instalaciones view-throogh no generar install referrer, por tanto Facebook medirá estas instalaciones, pero EMMA no podrá. 

## Consideraciones para iOS 14

A raíz de todos los cambios que ha traído consigo iOS 14 relativos a la privacidad de privacidad de Apple, se elimina el uso del IDFA (Identificador de publicidad). 

Hasta la llegada de iOS 14, cada dispositivo móvil tenía un IDFA único que permitía poder identificar al usuario de manera anónima y saber desde que canal se había descargado la aplicación. 

Este cambio tiene un impacto muy importante en las campañas de Facebook, ya que, con este cambio, Apple le ha dado el poder de decisión al usuario y es este último quién tiene que permitir el uso del IDFA para que Facebook pueda atribuir de manera correcta. En este escenario, es necesario que los usuarios que se instalan una app a través de un anuncio de Facebook cumplan las siguientes condiciones: 

1.  Haber permitido el uso del IDFA en la app de Facebook.
2.  Permitir el uso del IDFA en la app que se instale a través de un anuncio de Facebook.

Si alguna de estas dos condiciones no se cumplen, Facebook no podrá realizar la atribución del usuario ya que no tiene manera alguna de vincular el usuario que ha instalado la app con el clic o la visualización del anuncio en su plataforma. 

## Medición de eventos in app (opcional)

Además de medir las campañas de Facebook en EMMA , también puedes enviar cualquier evento inapp que estés midiendo en EMMA a Facebook para medirlos. Sigue estos pasos para configurar los eventos para Facebook:

Haz [login](https://ng.emma.io) en la página web de EMMA y dirígete a ***Captación > Fuentes de medios**.*
![](/fb_16.png)

-   Edita la fuente de medios de **Facebook**.
-   En la sección de **Eventos,** haz clic en el botón **añadir evento** para añadir los eventos que quieres enviar a Facebook. Es importante que en la columna ***Identificador del Evento para la Fuente de medios*** selecciones el evento de Facebook con el que se vinculará el evento evento enviado desde EMMA.
![](/fb_20.png)

-   Decide con qué evento de Facebook vas a querer vincular el evento que se envía desde EMMA .
    -   fb\_mobile\_level\_achieved
    -   fb\_mobile\_activate\_app
    -   fb\_mobile\_add\_payment\_info
    -   fb\_mobile\_add\_to\_cart
    -   fb\_mobile\_add\_to\_wishlist
    -   fb\_mobile\_complete\_registration
    -   fb\_mobile\_tutorial\_completion
    -   fb\_mobile\_initiated\_checkout
    -   fb\_mobile\_purchase
    -   fb\_mobile\_rate
    -   fb\_mobile\_search
    -   fb\_mobile\_spent\_credits
    -   fb\_mobile\_achievement\_unlocked
    -   fb\_mobile\_content\_view
    -   CUSTOM

###  Eventos CUSTOM

Es importante que tengas en cuenta las siguientes anotaciones para los eventos CUSTOM:

-   Facebook no utiliza estos eventos para optimizar el tráfico, sólo para medir y segmentar la audiencia en Facebook Analytics for Apps. Si quieres usar los eventos para optimizar el tráfico deberás seleccionar uno de los eventos por defecto que ofrece Facebook.
-   Este tipo de eventos pueden ser usados para crear audiencias en Facebook.
-   En el Ads Manager no podrás diferenciar estos eventos. Todos serán mostrados bajo la etiqueta Custom.
-   En Analytics for Apps, podrás diferenciar los eventos CUSTOM unos de otros, ya que podrás ver el nombre real del evento

# Campañas de SKAdNetwork

EMMA permite medir camapñas de Facebook Ads orientadas a dispositivos iOS con versión del sistema operativo superior a iOS 14.5, lo que se conoce como campañas  de SkAdNetowork. Al permitir la medición de esta tipología de campañas, podremos obtener en EMMA los datos agregados de las instalaciones y los eventos realizados a través de estas campañas.

> Es importante tener en cuenta que debido a la política de privacidad de Apple, a través de las campañas de SkAdNetwork no podemos obtener información del usuario que se ha instalado la app o realizado eventos inapp, ya que Apple no comparte esta información con terceros. La única información que comparte con terceros es que se ha realizado una instalación o un evento vinculado a una determinada campaña. </br>
Es decir, solo podremos obtener datos agrerados de instalaciones y eventos inapp, sin poder saber bajo ningún concepto qué usuario ha realizado esas acciones.{.is-info}

## Habilitar SkAdNetowrkt en EMMA para instalaciones
Para poder empezar a medir este tipo de campañas en EMMA, lo primero es validar estos dos pasos:

- Inicia sesión en tu cuenta de [EMMA](https://ng.emma.io).

- Ve a preferencias de la app y asegúrate que el en el apartado de configuración relativo a iOS, el campo App Store ID está configurado. De no ser así, tan solo tienes que obtener este id de la url de la store de tu app y configurarlo.
![google_29.png](/google_29.png)

- Asegurate de que has realizado los pasos previos de esta guía.

Una vez validados los pasos anteriores, es necesario hacer esta configuración para poder medir SKAdNetwork de Facebook Ads. 
- Ve a la sección **Captación > Fuentes de medios**. </br><p style="width:300px">![google_35.png](/google_35.png)</p>

- Busca y edita el proveedor Facebook.
![fb_17.png](/fb_17.png)

- Si no tienes la **Conexión con Facebook** realizada, deberás hacer la conexión.</br> <p style="width:700px">![fb_25.png](/fb_25.png)</p>

- Una vez realizada la conexión, ya podrás habilitar el check de SKAdNetwork. </br> <p style="width:700px">![fb_42.png](/fb_42.png)</p>

- A continuación debes seleccionar las cuentas de Facebook dónde quieres realizar el seguimiento de SKAdNetwork. </br><p style="width:700px">![google_32.png](/google_32.png)</p>

> Además de esta configuración, es recomendable integrar el SDK de EMMA (versión 4.12 o posterior) para poder realizar un seguimiento del usuario a nivel de eventos y poder recibir asimismo una copia directa del postback de SkAdNetwork para obtener más información en EMMA.
{.is-info}

En cuanto se empiecen a recibir datos de las campañas de SKAdNetwork, aparecerá automáticamente en EMMA una nueva campaña llamada **SkAdNetwork Facebook** que mostrará toda la información agregada. 
![fb_44.png](/fb_44.png)

### Consideraciones a tener en cuenta

Para realizar el seguimiento de campañas con SKAdNetwork en Facebook es necesario habilitar el check “Campaña de datos para iOS 14 y versiones posteriores”. Sin esta opción habilitada no se contabilizaran las conversiones de SKAdNetwork.

![fb_43.png](/fb_43.png)

La conexión para los datos de SKAdNetwork se realiza a través de la API, esto quiere decir que hay que tener en cuenta lo siguiente:

- SKAdNetwork no transmite los datos en tiempo real a Meta y puede sufrir retrasos de un mínimo de 24 horas. Por lo tanto, los resultados se registrarán en función de la hora a la que se hayan enviado a Meta y no la hora en la que realmente ocurrieron.


## Medición de eventos de SKAdNetwork
Para poder hacer atribución de eventos de SKAdNetwork es necesario tener instalada la versión del SDK de EMMA 4.12 o posterior. 

Para configurar los eventos debemos seguir estos pasos: 
- Haz login en tu cuenta de EMMA. 
- Ve a la sección **Gestión > Eventos** </br><p style="width:300px">![crear_eventos_1.png](/crear_eventos_1.png)</p>
- Busca y edita el evento deseado. </br><p style="width:700px">![editar_evento_1.1.png](/editar_evento_1.1.png)</p>
- En la parte final de la pantalla debes habilitar el check de SKAdNetwork y añadir un valor de conversión entre 1 y 63. </br><p style="width:1000px">![google_36.png](/google_36.png)</p>

Los valores de conversión deben ser únicos, no puede usarse el mismo valor en múltiples eventos. En caso de seleccionar un valor en uso, EMMA nos avisará y nos mostrará el valor más cercano disponible para su configuración.</br><p style="width:800px">![google_37.png](/google_37.png)</p>

> Ten en cuenta que desde SKAdNetwork nos notifican solo la última conversión, el último evento realizado dentro de la ventana de atribución, por lo que si un usuario realiza 3 eventos de forma consecutiva dentro de la misma ventana de atribución, solo nos llegará la notificación del último y será este el que aparezca en EMMA. 
{.is-warning}

# Facebook API: seguimiento para medir impresiones, clicks y coste (opcional)

Con la nueva conexión con la API de Facebook puedes hacer 2 cosas:

-   Obtener las impresiones, los clicks y el coste de las campañas a tiempo real en EMMA. Esto es totalmente opcional y no afecta a la medición de instalaciones y eventos.
-   Compartir audiencias directamente con Facebook. En este caso, esta conexión con la API es obligatoria. En este [artículo](https://docs.emma.io/es/guias-de-integracion/facebook#compartir-audiencias-con-facebook) puedes ver más información sobre esto.

> Ten en cuenta que, para poder llevar a cabo está conexión vía API, es necesario que tengas una campaña activa en tu panel de Business Manager de Facebook.
{.is-info}


Para hacer esta conexión se deben seguir estos pasos:

-   Vete a [*Facebook Developers*](https://developers.facebook.com/), entra en tu App y vete a la sección *Facebook Login.* Ahí autoriza la url de [https://in.emma.io/social/init-facebook-api](https://in.emma.io/) en *Valid OAuth redirect URIs y* marca el *Web OAuth Login* como YES. Además debes marcar la opción *Use Strict Mode for Redirect URIs* como YES.
![](/fb_21.png)

> Vete a *Settings > Advanced* y revisa que el botón *Native or Desktop App* esté seleccionado como NO
![](/fb_22.png)
{.is-info}

-   Vete a *Facebook Ads Manager* y loguéate con un rol de administrador de la cuenta publicitaria.
-   Dirígete a tu cuenta de EMMA y entra en ***Captación > Fuentes de medios.*** Busca la fuente *Facebook* y edítala.
![](/fb_16.png)

-   Una vez dentro del panel de edición de la fuente Facebook, busca la sección ***Impresiones, clics y coste*** y haz clic sobre el botón verde ***Iniciar sesión en Facebook***:
![](/fb_25.png)

Inicia sesión con un usuario administración de la cuenta publicitaria para que la conexión se realice correctamente y guarda los cambios realizados.

> La conexión con la API de Facebook caduca cada dos meses. Desde EMMA te enviaremos un email 7 días antes de que caduque confirmando la fecha de caducidad. Asimismo, una vez haya caducado la conexión, también enviáremos un email avisando de que la conexión a caducado. 
Haciendo clic en los enlaces de estos emails, podrás actualizar la conexión con la API de Facebook siguiendo los pasos 4 y 5 de esta guía. 
{.is-warning}



# Campañas de Re-Targeting de Facebook

Las campañas en Facebook pueden generar tanto acciones de nuevos usuarios (captación) como de usuarios antiguos (re-engagement) en función del segmento establecido. Por ello, si se quiere crear una campaña que apunte únicamente a nuevos usuarios es importante hacer una exclusión en la campaña de una audiencia custom con los usuarios que ya tienen la app o la tuvieron anteriormente.

Las acciones de re-engagement se generan por usuarios que ya tenían la app previamente instalada y pueden venir de diferentes campañas de Facebook. Por otro lado las acciones de captación vienen de aquellos usuarios que se instalan por primera vez la aplicación.

Además, si es la primera vez que vas a medir Facebook con EMMA, antes debes seguir los pasos del apartado [Seguimiento con Facebook](https://docs.emma.io/es/guias-de-integracion/facebook#seguimiento-con-facebook-app-installs) para poder realizar la conexión con la plataforma social correctamente y poder medir todas tus campañas.

## Re-Targeting con Facebook

Para poder medir las campañas de Re-Targeting de Facebook a través de EMMA, además de la configuración explicada en la guía citada anteriormente, es necesario habilitar los eventos que quieres enviar a Facebook para usar en tus campañas de Re-Targeting. Para ello tienes que:

-   Haz [login](https://ng.emma.io) en tu cuenta de EMMA y ve a la sección **Captación > Fuentes de medios**.
![](/fb_16.png)

-   Edita la fuente de medios de **Facebook**.
-   En la sección de **Eventos,** haz clic en el botón **añadir evento** para añadir los eventos que quieres enviar a Facebook. Es importante que en la columna ***Identificador del Evento para la Fuente de medios*** selecciones el evento de Facebook con el que se vinculará el evento evento enviado desde EMMA.
![](/fb_20.png)

-   Decide con qué evento de Facebook vas a querer vincular el evento que se envía desde EMMA .
    -   fb\_mobile\_level\_achieved
    -   fb\_mobile\_activate\_app
    -   fb\_mobile\_add\_payment\_info
    -   fb\_mobile\_add\_to\_cart
    -   fb\_mobile\_add\_to\_wishlist
    -   fb\_mobile\_complete\_registration
    -   fb\_mobile\_tutorial\_completion
    -   fb\_mobile\_initiated\_checkout
    -   fb\_mobile\_purchase
    -   fb\_mobile\_rate
    -   fb\_mobile\_search
    -   fb\_mobile\_spent\_credits
    -   fb\_mobile\_achievement\_unlocked
    -   fb\_mobile\_content\_view
    -   CUSTOM
-   En la sección ***Eventos retargeting*** selecciona los eventos que serán usados para campañas de Retargeting. Ten en cuenta que solo podrás habilitar como eventos de retargeting los eventos que hayas configurado previamente en la sección ***Eventos***.
![fb_35.png](/fb_35.png) 

> Es importante que tengas en cuenta que Facebook no utiliza los eventos CUSTOM para optimizar el tráfico, sólo para medir y segmentar la audiencia en Facebook Analytics for Apps.</br>
Si quieres que Facebook use estos eventos para optimizar el tráfico deberás seleccionar uno de los eventos por defecto que ofrece Facebook para vincularlo.
{.is-warning}


# Compartir Audiencias con Facebook (opcional)

https://www.youtube.com/watch?v=_x6Rhkm5ubw

La creación de audiencias de Facebook es la mejor forma de orientar tus campañas a un público objetivo basándote en distintos criterios de segmentación.

Facebook permite crear estas audiencias desde su propia plataforma publicitaria  o bien automatizar la creación y actualización de las mismas desde el propio motor de segmentación de EMMA.

El método de compartir audiencias desde EMMA tiene dos ventajas claras:

1.  Podrás usar todos los criterios de segmentación que te ofrece EMMA **sin necesidad de volcar los datos manualmente y periódicamente en Facebook**. Más rápido, sencillo y con menos integración técnica.
2.  Podrás crear **audiencias basadas en ID's internos de tu compañía** no compatibles en el creador de audiencias de Facebook. Por ejemplo un listado de "id's de clientes".

¿Te parece interesante?

Antes de empezar asegúrate de que tienes completada la configuración de Facebook en EMMA. Es importante que cumplas los pasos del apartado [Seguimiento con Facebook](https://docs.emma.io/es/guias-de-integracion/facebook#seguimiento-con-facebook-app-installs) para poder realizar la conexión con la plataforma social correctamente y poder medir todas tus campañas.

## Conexión con la API de Facebook

Para poder compartir audiencias directamente con Facebook es necesario que tengas la conexión con la API de Facebook realizada. Para ello, sigue la configuración explicada [aquí](https://docs.emma.io/es/guias-de-integracion/facebook#facebook-api-seguimiento-para-medir-impresiones-clicks-y-coste-opcional). 

## Habilitar audiencias para la cuenta de anunciante

Una vez realizada la conexión con la API, en la fuente *Facebook* (Captación > Fuentes de medios) se habilitará inmediatamente el campo de ***Audiencias.*** 

Mientras no está la conexión con la API realizada se muestra el siguiente mensaje. 

![](/fb_1.png)

Una vez se ha realizado la conexión con la API ya podemos seleccionar del listado la cuenta de anunciante correspondiente con la que queremos compartir las audiencias de manera directa. 

![](/fb_2.png)

## Compartir audiencias con Facebook

Ahora sí, ha llegado el momento de compartir tus audiencias con Facebook, pero no te preocupes, es un proceso super sencillo. Tan solo tienes que seguir estos pasos para compartirlas.

-   Ve a la sección **Comportamiento > Audiencias** y crea una audiencia que deseas compartir con Facebook. Si ya tienes audiencias creadas y quieres compartir una de esas no hace falta que la vuelvas a crear. En esta [guía](https://docs.emma.io/es/audiencias) puedes ver más información sobre cómo crear audiencias.
![](/fb_29.png)

-   Una vez tengas tu audiencia lista, dirígete nuevamente a la sección **Captación > Fuentes de medios** y edita la fuente **Facebook.**
-   A continuación, dentro de la edición de la fuente **Facebook,** ve al apartado de **Audiencias** y haz clic en el botón **Añadir cuenta de anunciante**.![fb_45.png](/fb_45.png)
- Estalece la cuenta de anunciante y selecciona la audiencia que deseas compartir con Facebook de la lista y guarda cambios. ![](/fb_30.png)
- Listo, tu audiencia ya se está compartiendo automáticamente con Facebook.
- Si quieres vincular audiencias con diferentes cuentas de anunciante, repite el proceso de **Añadir cuenta de anunciante** y la vinculación de la audiencia tantas veces como sea necesario.

Por defecto, en las audiencias, EMMA comparte únicamente el ID de dispositivo, por lo que, en caso de desear compartir también el email del usuario, se debe habilitar el check de **Compartir EMAIL con Facebook**. 
![fb_46.png](/fb_46.png)
> La información de email se envía codificada mediante sha256.{.is-info}


Una vez compartida la audiencia con Facebook, desde la pantalla de audiencias (Comportamiento > Audiencias) podremos ver qué audiencias están siendo compartidas con Facebook ya que en la columna **Compartido** estas mostrarán el icono de Facebook.![](/fb_31.png)

## Dejar de compartir audiencias con Facebook

Si quieres que se dejen de actualizar los datos de la audiencia con Facebook tan solo tienes que seguir estos pasos: 

-   Ve a **Captación > Fuentes de medios** y edita la fuente de **Facebook.**
-   En el apartado audiencia, despliega el selector de audiencias a compartir y haz clic sobre aquellas que quieras dejar de compartir (puedes escribir el nombre de las mismas para localizarlas más fácilmente). Automáticamente verás como el check que las mantenía seleccionadas desaparecerá.
![](/fb_32.png)
![](/fb_33.png)

-   Guarda los cambios para que la audiencia deje de ser compartida con Facebook.

> Ten en cuenta que el efecto de esta acción es simplemente que la audiencia deje de actualizar la información nueva en la audiencia de Facebook, pero esta audiencia no actualizada seguirá estando disponible en Facebook y podrá seguir siendo usado para campañas a menos que se elimine también allí.
{.is-info}

# Discrepancias con Facebook Mobile Ads Dashboard

En ocasiones aparecen pequeñas diferencias entre los resultados reportados en el Dashboard de Facebook y el AppTracker de EMMA.

El motivo de que esto ocurra es porque, dentro de Facebook Ads Manager, Facebook utiliza un día de visualización y un modelo de atribución de click de 28 días. EMMA sólo reporta las instalaciones generadas de clicks producidos dentro de este margen de 28 días.

Adicionalmente, en Facebook Ads Manager los resultados se muestran basándose en impresiones o 'click-time', no instalaciones o conversiones como se utiliza en EMMA.

**Por favor tenga esto en cuenta cuando compare su reporting entre Facebook Ads Manager y EMMA.**

Aunque trabajemos constantemente para minimizar al máximo estas discrepancias, debes tener en cuenta las siguientes razones que las justifican:

|     |     |     |
| --- | --- | --- |
| **Motivo** | **Facebook** | **EMMA** |
| **Fecha Seguimiento Instalación** | Facebook trackea nuevas instalaciones en un período determinado de click/view, que puede llegar hasta los 28 días. | EMMA trackea como nuevas instalaciones aquellas que realmente se descargan por primera vez y una vez esta haya sido abierta por primera vez. |
| **Ventana de Atribución** | Facebook atribuye 1 día a la impresión y 28 días al click basándose en la impresión y período en el que ocurrió. | EMMA reporta sólo las instalaciones generadas por clicks dentro del mencionado período de 28 días.<br><br>EMMA no reporta Ad View Installs.<br><br>**\*\*Esta es la principal causa de diferencias entre EMMA y Facebook. Para más detalle, vea el screenshot debajo sobre cómo ver las diferentes ventanas de atribución en Facebook.** |
| **Atribución multicanal** | La atribución funciona como se especifica arriba en este caso concreto, no para otros canales. Facebook sólo atribuye a Facebook. | EMMA usa una atribución a partir del último click y atribuye a múltiples canales. |
| **Diferencia horaria** | Facebook trabaja bajo el huso horario GMT-7 por lo que el día finaliza más tarde que en nuestro huso horario. | En EMMA trabajamos bajo el huso horario CET, por lo que es posible que los instalaciones entre ambas plataformas estén asignadas a días diferentes. |

Para ver las diferentes ventanas de atribución en Facebook (y comparar las instalaciones atribuidas con las reportadas por EMMA), ve a Facebook y configura la ventana de atribución como se muestra a continuación:

![](/fb_26.png)

No dudes en contactar con [xservices@emma.io](mailto:%20xservices@emma.io) en caso de que querer aclarar todavía más este punto.

# Preguntas frecuentes

-   **¿Puedo ver los clics de las campañas de** ***Facebook App Installs*** **en EMMA?**  
    La integración *3rd party* con los socios oficiales de Facebook incluye únicamente las instalaciones. Sin embargo Facebook permite hacer una conexión con su API para obtener los datos de impresiones, clicks y coste de la campaña. Sigue estas [instrucciones](https://docs.emma.io/es/guias-de-integracion/facebook#facebook-api-seguimiento-para-medir-impresiones-clicks-y-coste-opcional) para habilitarlo.
-   **¿Por qué el coste y los clics que muestra EMMA no coinciden con los de Facebook?**   
    Si estás trabajando a la vez campañas de captación y de retargeting con Facebook, es posible que los costes y clics que muestra EMMA no coincidan con los datos que muestra Facebook. Esto es debido a que una misma campaña puede generar acciones de captación y engagement. Cuando se da esta situación, en EMMA diferenciamos en dos secciones diferentes las acciones de captación y las de engagement (secciones basic y retargeting del apptracker de EMMA).  
      
    En estos casos específicos, Facebook no ofrece datos diferenciados para captación y retargeting como nosotros y hemos optado por mostrarlos "repetidos". De este modo, podremos ver a nivel de fuentes en EMMA la misma fuente en la sección basic y en la sección de retargeting y ambas mostrarán el mismo número de clics y coste. Eso sí, a nivel de fuente (sin sumar basic y retargeting) los clics y coste serán idénticos a los datos que muestra Facebook, pero a nivel de campaña habrá discrepancia.
-   **¿Puedo ver las instalaciones de la App en Facebook y EMMA?**  
    Sí, con esta integración podrás ver las conversiones en ambas plataformas, tanto en EMMA como en Facebok.
-   **¿Qué nivel de detalle de campañas de Facebook da EMMA?**  
    En EMMA se creará una nueva campaña en el Apptracker por cada campaña creada en Facebook (las campañas de EMMA tendrán el mismo nombre establecido en Facebook y si este se cambia, también se actualiza en EMMA). Además, también ofrece un desglose a nivel de grupo de anuncios en el reporting del Apptracker añadiendo una nueva source por cada grupo de anuncios.
-   **¿Cómo puedo probar mi configuración de** ***Facebook App Installs setup*****?**  
    Facebook no permite probar la configuración en un entorno de prueba por lo que recomendamos lanzar una campaña de prueba, con un límite presupuestario bajo, para verificar si las primeras instalaciones se están registrando correctamente en EMMA.
-   **¿Por qué en EMMA no visualizo reengagement de mis campañas de Re-Targeting?**  
    Dependiendo del sistema de conexión que se haya empleado para la medición de Facebook App Installs, en EMMA se pueden mostrar o no mostrar reengagements.
-   **¿Por qué en la sección de Re-Targeting de EMMA visualizo campañas de Facebook de captación?**  
    Hay dos maneras de crear segmentos en las campañas de Facebook. Subiendo un fichero manualmente con los usuarios o utilizando un sistema automático para establecer el target.   
    Si has creado una campaña de captación el día 3 de Mayo con un fichero manual en el que hay usuarios que no tienen la app instalada puede pasar lo siguiente:   
    \- Un usuario que está en el archivo y cumple con el filtro (no tiene la app instalada) se instala la app orgánicamente X días después de que se haya iniciado la campaña de captación.  
    \- Cómo se ha subido un archivo manual y no se actualiza dinámicamente, ese usuario que se ha instalado la app orgánicamente sigue estando dentro del segmento de la campaña de captación.  
    \- X días después de haber instalado la app de manera orgánica, ese usuario interactúa con la campaña de captación y hace click.  
    \- Esa información es considerada de Re-Targeting ya que el usuario ya tenía la app, pero aparece atribuida a la campaña de captación porque el usuario sigue estando en el segmento.  
      
    Este es el principal motivo por el que tus campañas de captación pueden estar generando reengagements y acciones de Re-Targeting.
-   **¿Cómo puedo comprobar si un segmento ha sido enviado correctamente a Facebook?**  
    Puedes revisar los segmentos enviados a Facebook desde la sección de Audiencias. Para acceder a esta sección tienes que hacer click en el menú contextual superior izquierdo, hacer click en **All tools** y seleccionar la opción **Audiences** dentro de la sección **Assets.**
![](/fb_27.png)

-   **¿Por qué no puedo ver en la sección de Audiencias de Facebook la audiencia que he compartido con Facebook?**  
    Si tienes toda la conexión correctamente realizada y envías una audiencia a Facebook pero no consigues verlo puede que hayas superado el **rate** **limit** de tu cuenta.  
      
    Para verificar en que nivel está tu **rate** **limit** debes ir a [*Facebook para Desarrolladores,*](https://developers.facebook.com/) seleccionar tu app y en la sección ***Dashboard*** podrás ver en qué nivel está tu **rate limit.** Si el nivel es 100% deberás esperar 1 hora más o menos para que se restablezca y puedas volver a realizar envíos de segmentos desde EMMA.
![](/fb_28.png)

-   **¿Cada cuánto se actualiza la información de las audiencias?**  
    EMMA realiza una actualización de la audiencia una vez al día, a las 04:00 am para evitar problemas de carga correcta por el volumen del tráfico. Cada vez que se realiza esta sincronización la información actual de la audiencia es enviada automáticamente a Facebook.
