---
title: Instalaciones asistidas
description: 
published: true
date: 2025-03-06T07:20:44.811Z
tags: 
editor: markdown
dateCreated: 2025-01-31T12:10:02.228Z
---

# Instalaciones asistidas
Con la funcionalidad de instalaciones asistidas, vamos a poder tener un control exhaustivo de las instalaciones de un canal que han sido asistidas por otros canles, así como saber también a cuántas instalaciones ha asistido un canal. 


Para acceder a la pantalla de *Instalaciones asistidas* tan solo tenemos que ir al apartado Captación > Instalaciones Asistidas:</br><p style="width: 250px;">![installs_asistidas_1.png](/installs_asistidas_1.png)</p>

Esta pantalla consta de 3 filtros, que van a permitir filtrar la información según nuestras necesidades para llegar a la granularidad de los datos deseada. Además, los datos de la pantalla se van a ver afectados también por el rango de fechas seleccionado. </br><p style="width: 250px;">![installs_asistidas_3.png](/installs_asistidas_3.png)</p>

> Es importante tener en cuenta que, tanto el rango de fechas seleccionado, como la agrupación y los filtrros aplicados, van a tener efecto sobre todos los datos mostrados en esta pantalla.{.is-info}

Los filtros disponibles son: 
- **Campañas:** Permite seleccionar una o varias campañas. Tras haber seleccionado las campañas, se verán datos solo de la selección realizada.
- **Canales:** Permite seleccionar uno o varios canales. Tras haber seleccionado los canales, se verán datos solo de la selección realizada.
- **Sistema operativo:** Permite seleccionar un sistema operativo concreto. Tras haber seleccionado el sistema operativo, se verán datos solo de la selección realizada.

La pantalla de instalaciones asistidas se divide en cuatro secciones principales: 
- [Resumen](https://docs.emma.io/es/instalaciones_asistidas#resumen)
- [Instalaciones asistida y no-asistidas](https://docs.emma.io/es/instalaciones_asistidas#instalaciones-asistidas-y-no-asistidas)
- [Instalaciones ganadoras y asistidas por canal](https://docs.emma.io/es/instalaciones_asistidas#instalaciones-ganadoras-y-asistidas-por-canal)
- [Tabla de detalle por canal](https://docs.emma.io/es/instalaciones_asistidas#tabla-de-detalle-por-canal)

## Resumen
En este primer apartado vamos a poder ver datos de resumen y genéricos de todos los canales o según el filtro aplicado. 
![installs_asistidas_2.png](/installs_asistidas_2.png)
La información que podemos ver es la siguiente: 
- **Instalaciones totales:** Son todas las instalaciones generadas. Es la suma de las instalaciones no-asistidas y las asistidas.
- **Instalaciones no-asistidas:** Instalaciones del total que no han sido asistidas de otros canales.
- **Instalaciones asistidas:** Instalaciones del total que sí han sido asistidas por otros canales.
- **% instalaciones asistidas:** Se muestra el % de instalaciones asistidas sobre el total. 

## Instalaciones asistidas y no-asistidas
La primera gráfica de la pantalla, muestra en un gráfico de barras la comparativa por día de las instalaciones asistidas vs las instalaciones no asistidas. De esta manera, se puede ver la evolución y comparativa en un gráfico. </br><p style="width:;">![installs_asistidas_4.png](/installs_asistidas_4.png)</p>
## Instalaciones ganadoras y asistidas por canal
La gráfica de instalaciones ganadoras y asistidas por canal permite llevar a cabo un análisis por canal de las instalaciones atribuidas a dicho canal (ganadoras) y las instalaciones que ha asistido ese canal. </br><p style="width:;">![installs_asistidas_5.png](/installs_asistidas_5.png)</p>

En esta gráfica podemos ver la siguiente información: 
-	**Instalaciones ganadoras:** Instalaciones finales atribuidas a un canal en concreto.
- **Instalaciones asistidas:** Instalaciones que ha asistido un canal en concreto, pero que están atribuidas a otro canal ganador.

Por ejemplo, si el canal Google Ads - ACI tiene 67 instalaciones ganadores y 23 asistidas, significa lo siguiente: 
- El canal Google Ads - ACI tiene 67 instalaciones ganadoras (atribuidas).
- El canal Google Ads - ACI ha asistido 23 instalaciones que están atribuidas a otros canales.

Por defecto esta gráfica muestra un top de 5 de canles, pero se puede modificar para que muestre un top 10 o un top 15. </br><p style="width:;">![installs_asistidas_11.png](/installs_asistidas_11.png)</p>
## Tabla de detalle por canal
Ya por último, en la tabla de detalle por canal, se puede ver de manera detallada los datos de cada uno de los diferentes canales. </br><p style="width:;">![installs_asistidas_6.png](/installs_asistidas_6.png)</p>
- **Canal:** Nombre del canal configurado en EMMA. Puedes ver más info acerca de los canales [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios#crear-fuentes)
- **Ganadoras:** Instalaciones atribuidas al canal en cuestión.
- **Porcentaje ganadoras:** Porcentaje que suponen las instalaciones ganadoras del canal sobre las instalaciones totales (la suma de todos los canales).
- **Asistidas:** Instalaciones que ha asistido cada canal, pero que están atribuidas a otro canal ganador.
- **Porcentaje asistidas:** Porcentaje que suponen las instalaciones asistidas del canal sobre las asistencias totales (la suma de todos los canales).

> Haciendo clic en el nombre de cada canal, se podrá acceder al detalle de dicho canal para ver información más específica. Puedes ver más info sobre el detalle [aquí](https://docs.emma.io/es/instalaciones_asistidas#detalle-de-instalaciones-asistidas).{.is-info}

# Detalle de instalaciones asistidas
La pantalla de detalle de instalaciones asistidas nos permitirá realizar un análisis detallado a nivel de cada uno de los diferentes canales, puediendo ver la información específica relativa a cada canal. 

Para acceder a la pantalla de *Detalle de Instalaciones asistidas*, tan solo hay que hacer clic en el nombre de un canal de la desde la [pantalla principal de *Instalaciones asistidas*](https://docs.emma.io/es/instalaciones_asistidas#tabla-de-detalle-por-canal).

Esta pantalla consta de 2 filtros, que van a permitir filtrar la información según nuestras necesidades para llegar a la granularidad de los datos deseada. </br><p style="width: 250px;">![installs_asistidas_7.png](/installs_asistidas_7.png)</p>

Los filtros disponibles son: 
- **Campañas:** Permite seleccionar una o varias campañas. Tras haber seleccionado las campañas, se verán datos solo de la selección realizada.
- **Sistema operativo:** Permite seleccionar un sistema operativo concreto. Tras haber seleccionado el sistema operativo, se verán datos solo de la selección realizada.


La pantalla de detalle de instalaciones asistidas se divide en cuatro secciones principales: 
- [Resumen](https://docs.emma.io/es/instalaciones_asistidas#resumen-1)
- [Volumen y evolución por tipo de instalación
](https://docs.emma.io/es/instalaciones_asistidas#volumen-y-evoluci%C3%B3n-por-tipo-de-instalaci%C3%B3n)
- [Volumen de asistencias de las principales fuentes](https://docs.emma.io/es/instalaciones_asistidas#volumen-de-asistencias-de-las-principales-fuentes)
- [Tabla de detalle de la atribución](https://docs.emma.io/es/instalaciones_asistidas#tabla-de-detalle-de-la-atribuci%C3%B3n)

## Resumen
En este primer apartado vamos a poder ver datos de resumen y genéricos del canal en concreto al que se haya accedido.
![installs_asistidas_8.png](/installs_asistidas_8.png)
La información que podemos ver es la siguiente: 
- **Instalaciones ganadoras:** Son todas las instalaciones atribuidas al canal en concreto.
- **Instalaciones asistidas:** Instalaciones ganadoras del canal que han sido asistidas por otros canales.
- **Asistencias a instalaciones** Instalaciones a las que el canal en concreto ha contribuido (asistido). Estas instalaciones son ganadoras (atribuidas) de otros canales. 

## Volumen y evolución por tipo de instalación
En la gráfica de *Volumen y evolución por tipo de instalación* se podrá analizar de manera diaria, semanal o mensual el volumen de las instalaciones ganadoras del canal y el volumen de las instalaciones a las que ha asistido dicho canal. </br><p style="width:600px;">![installs_asistidas_12.png](/installs_asistidas_12.png)</p>

En esta gráfica se puede ver la siguiente información: 
- **Instalaciones ganadoras:** Instalaciones atribuidas al canal.
- **Instalaciones asistidas:** Instalaciones atribuidas(ganadoras) a otro canal pero a las que ha asistido el canal del análisis. 

## Volumen de asistencias de las principales fuentes
En la gráfica de *Volumen de asistencias de las principales fuentes* se podrá analizar la aportación de cada fuente en concreto dentro de las asistencias a instalaciones de otros canales. Es decir, podremos ver un desglose a nivel de las asistencias que ha generado cada fuente.  </br><p style="width:500px;">![installs_asistidas_13.png](/installs_asistidas_13.png)</p>

Teniendo esto en cuenta, la gráfica de la captura muestra la siguiente información: 
- La fuente 1 ha asistido 14 instalaciones que están atribuidas a otros canales. 
- La fuente 2 ha asistido 7 instalaciones que están atribuidas a otros canales. 
- La fuente 3 ha asistido 5 instalaciones que están atribuidas a otros canales. 
- La fuente 4 ha asistido 3 instalaciones que están atribuidas a otros canales. 
- La fuente 5 ha asistido 3 instalaciones que están atribuidas a otros canales. 
- El resto de fuentes del canal han asistido 8 instalaciones, que están atribudas a otros canales. 

> Esta gráfica muestra un top 5 de las fuentes que han generado más asistencias a instalaciones de otros canales y una sexta opción con las asistencias que han generado el resto de fuentes restantes del canal. {.is-info}

## Tabla de detalle de la atribución
Ya por último, en la tabla de la atribución, se puede ver de manera detallada los datos de cada uno de las diferentes fuentes y campañas, que han generado instalaciones ganadores y asistencias a instalaciones de otros canales. </br><p style="width:;">![installs_asistidas_10.png](/installs_asistidas_10.png)</p>
- **Fuente:** Nombre de la fuente en EMMA.
- **Campaña:** Nombre de la campaña en EMMA. 
- **Nº de ganadoras:** Instalaciones atribuídas a la fuente en cuestión.
- **Porcentaje ganadoras:** Porcentaje que suponen las instalaciones ganadoras de la fuente sobre las instalaciones totales (la suma de todas las fuentes).
- **Nº de asistencias:** Instalaciones que ha asistido cada fuente y que están atribuidas a otro canal ganador.
- **Porcentaje asistidas:** Porcentaje que suponen las instalaciones asistidas de la fuente sobre las asistencias totales (la suma de todas las fuentes).

