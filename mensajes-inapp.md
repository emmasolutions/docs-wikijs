---
title: Mensajes Inapp
description: Mensajes Inapp
published: true
date: 2025-03-04T12:15:04.217Z
tags: 
editor: markdown
dateCreated: 2021-06-07T12:40:32.951Z
---

# Crear campañas InApp

EMMA pone a tu alcance distintos tipos de comunicaciones In-App para comunicarte de la manera más adecuada con los usuarios de tu App. En esta guía vamos a conocer en detalle cada uno de los distintos formatos In-App disponibles así como la creación de comunicaciones para impactar a los usuarios de la App.

Para empezar a crear tu campaña de comunicación haz [login](https://ng.emma.io) en EMMA y dirígete a **Comunicación** ***\> Mensajes**.*

Selecciona el botón verde ***+Nuevo mensaje**,* situado en la parte derecha de la pantalla de mensajes para empezar a crear tu campaña.

![inapp_23.png](/inapp_23.png)

Para crear tu campaña, deberás seguir los siguientes pasos:

-   Seleccionar el tipo de campaña
-   Definir las propiedades del formato
-   Seleccionar el target/público objetivo
-   Programar el mensaje
-   Revisar la comunicación y confirmar

Elige el formato de comunicación que vas a utilizar en tu campaña entre:

-   [Adball](/es/mensajes-inapp#adball)
-   [Banner](/es/mensajes-inapp#banner)
-   [Cupón](/es/mensajes-inapp#cup%C3%B3n)
-   [Tab Dinámico](/es/mensajes-inapp#dynamic-tab)
-   [Native Ad](/es/mensajes-inapp#native-ad)
-   [Prisma](/es/mensajes-inapp#prisma)
-   [Startview](/es/mensajes-inapp#startview)
-   [Strip](/es/mensajes-inapp#strip)


Para conocer los formatos en mayor detalle, haz clic en cada uno de los formatos listados arriba.

## Propiedades

Una vez seleccionado el formato, se abrirá automáticamente el formulario de creación de la comunicación. En la primera sección del formulario (**Propiedades**) tendremos que completar información básica de la campaña.

![](/comunicacio%CC%81n_i.png)

-   **Nombre:** Establece un nombre para la campaña.
-   **Tipo de campaña:** Puedes elegir si tu campaña la gestionarás de modo manual o será automatizada a través de EMMA Rules o automatizada a través de rutas de clientes:
    -   **Clásica:** campaña gestionada manualmente. Se establece una fecha de inicio y fin para la campaña en los siguientes pasos de la configuración.
    -   **Automática para reglas**: automatización de la campaña con EMMA Rules. Para conocer más sobre esta opción, visita nuestro artículo sobre [Reglas](/es/reglas).
    -   **Automática para rutas de clientes:** automatización de la campaña con EMMA Rutas de Clientes. Para conocer más sobre esta opción, visita nuestro artículo sobre [Rutas de clientes](/es/analisis-de-comportamiento/customer-journeys). Esta tipología de campaña no permite variaciones de test ab.
-   **Reporting activo:** Por defecto el check está habilitado. Si está habilitado, implica que se va a recopilar la información generada por esta comunicación y podrá ser consultada en la sección [**Informe de comunicaciones**](/es/comunicacion/campa%C3%B1as/reporting-campa%C3%B1as)**.** Si está deshabilitado, no se recopilará la información de la campaña y no se podrá ver en el reporting.
-   **Propietario:** Usuario propietario de la campaña. Por defecto es el usuario que la crea. Este campo solo aplica si están activados los roles de prevalidación de campañas. Puedes ver más información al respecto en nuestra guía específica sobre [prevalidación de campañas](/es/prevalidacion-de-campa%C3%B1as).
-   **Responsables:** Selecciona los usuarios responsables para esta campaña. Sólo los usuarios con rol *Gestor* pueden ser responsables de una campaña. Este campo solo aplica si están activados los roles de prevalidación de campañas. Puedes ver más información al respecto en nuestra guía específica sobre [prevalidación](/es/prevalidacion-de-campa%C3%B1as) de campañas.
-   **Categorías:** Selecciona la categoría que debe ir vinculada a la comunicación para el control de saturación. Es un campo opcional, por lo que puede quedar sin completar. Más información sobre el control de saturación [aquí](/es/control-de-saturacion#impactos-por-categor%C3%ADa).
- **Parámetros:** Se pueden configurar dos tipos de parámetros:
    -   **Parámetros para ser recogidos por el SDK:** Haciendo clic en el botón **+Añadir**, podemos configurar los parámetros necesarios para que sean recogidos por el SDK y ponerlos a disposición de la app para su posterior tratamiento. Añadir estos parámetros es totalmente opcional.
    -   **Parámetros para filtrar en el informe de rendimiento**: Si se han creado [parámetros de comunicación](https://docs.emma.io/es/parametros-comunicacion) específicos, se pueden añadir para que puedan tener efecto en las comunicaciones para poder aplicar filtros y agrupaciones en el [informe de rendimiento](/es/informe-rendimiento).

> Si configuras una campaña automática para reglas o para rutas de clientes, en el resumen de las campañas, esta aparecerá en estado **Lista** hasta que se asigne a una rule o se vincule a una ruta. Cuando la rule haya sido enlazada, la campaña cambiará al estado **Activa**.
{.is-info}

## Contenido

Cubre los datos requeridos para configurar tu campaña:

-   **URL de destino:** En los formatos que apliquen (Startview, Adball, Banner, Prisma y Tab dinámico), introduce la URL a la que quieres redirigir a los usuarios cuando hagan clic en la comunicación. Puede ser un sitio web o una sección específica de tu App (vía deeplink).  
   > **IMPORTANTE:** Todas las URLS de redirección, estén o no establecidas directamente en EMMA, deben cumplir el protocolo HTTPS para evitar errores de redirección al abrir los webviews embebdios de EMMA.
    {.is-warning}
    
> Los formatos Adball, Banner y Startview permiten el uso de motor de sustitución para anexar etiquetas en la URL establecida en EMMA y que así el contenido de la URL puede ser personalizado en base a dichas etiquetas. 
Para poder llevar a cabo la personalización, desde la landing de destino, se debe de poder recuperar el parámetro y el valor de la etiqueta para poder emplearlo para la peresonalización.
{.is-info}
-   **Imagen:** En aquellos formatos que lo requieran (Adball, Banner, Cupón, Prisma y Tab dinámico), introduce la imagen que quieres que se muestra en la comunicación.
    
-   **Mensaje:** En aquellos formatos que lo requieran (Cupón y Strip), introduce el texto que quieres que se muestre en la comunicación. Utiliza nuestro **motor de sustitución de textos** para insertar el valor individual de las etiquetas atribuidas a tus usuarios.   
    Para ello, en el campo de Mensaje debemos insertar los tags que queramos utilizar desplegando la opción *‘Etiquetas’.* El listado de etiquetas que aparezcan en este listado dependerá las propiedades/etiquetas que tu App esté enviando a EMMA. Puedes ver cómo enviar etiquetas a través de nuestro SDK de [Android](https://developer.emma.io/es/android/integracion-sdk#completar-el-perfil-del-usuario-mediante-tags) e [iOS.](https://developer.emma.io/es/ios/integracion-sdk#propiedades-del-usuario-tags)
    
    Aquellos usuarios que reciban una comunicación con un texto compuesto por las variables del motor de sustitución pero no tengan asignada ninguna propiedad para esa Etiqueta, verán únicamente el texto sin las variables.
    

![](/comunicacio%CC%81n_ii.png)

![](/comunicacio%CC%81n_iii.png)

-   Cubre los campos específicos de cada formato de comunicación. Visita los diferentes tipo de comunicación para conocer las propiedades específicas de cada uno de ellos.
    -   [Adball](/es/mensajes-inapp#adball)
    -   [Banner](/es/mensajes-inapp#banner)
    -   [Cupón](/es/mensajes-inapp#cup%C3%B3n)
    -   [Tab Dinámico](/es/mensajes-inapp#dynamic-tab)
    -   [Native Ad](/es/mensajes-inapp#native-ad)
    -   [Prisma](/es/mensajes-inapp#prisma)
    -   [Startview](/es/mensajes-inapp#startview)
    -   [Strip](/es/mensajes-inapp#strip)
-   Selecciona *Siguiente* para seguir con la programación, *Volver* para volver al paso previo o *Cancelar* para eliminar los cambios no guardados en la campaña.

## Programación
En la sección de programación, procedremos a llevar a cabo la configuración de visualización de la comunicación en cuestión. 

### Fecha de visualización
En el caso de que haya seleccionado como tipo de campaña ***Manual (Clásica)*** en esta pantalla debes seleccionar la **fecha de visualización** del mensaje. Deberás indicar el día y hora en la que se deseas que se muestre.</br><p style="width:600px">![](/comunicacio%CC%81n_vi.png)</p>

-   **Fecha de inicio:** Selecciona la fecha y hora de inicio de la comunicación. Por ejemplo 27 de Julio a las 10:35. La fecha de inicio nunca puede ser anterior a la fecha de creación.
-   **Fecha de fin:** Selecciona la fecha y hora de fin de la comunicación. Por ejemplo 27 de Agosto a las 23:55. La fecha de fin nunca puede ser anterior a la fecha inicio.
### Horario de visualización
En el apartado **horario de visualización** se puede especificar si la comunicación se debe visualizar durante todo el día (dejando el check habilitado) o si por el contrario sólo se debe mostrar durante una franja horaria determinada. En este caso, se tendrá que deshabilitar el check y delimitar la franja horaria en la que se quiere mostrar la comunicación desplazando la barra verde.</br><p style="width:1000px">![](/comunicacio%CC%81n_vii.png)</p>
> Ten en cuenta que la fecha de inicio y fin de la campaña puede verse afectada por el tiempo de visualización. Es decir, pueden darse diversos escenarios si la fecha seleccionada coincide o no en la franja horaria de exposición:</br></br>-   Si la campaña empieza cuando el tiempo de exposición ya ha pasado, la comunicación no se mostrará hasta el día siguiente en la franja horaria correspondiente.</br></br>-   Si el día de finalización de la campaña termina a una hora previa del tiempo de exposición, la comunicación no se mostrará ese día.</br></br>-   Si el inicio o fin de la campaña no coincide con la totalidad de la franja horaria de exposición, pero sí parcialmente, la comunicación se mostrará durante el tiempo coincidente.
{.is-info}
### Frecuencia
La frecuencia nos permite establecer el número de veces totales que la campaña será mostrada a cada usuario durante su periodo de actividad. 

Por defecto, la frecuencia es ilimitada (siempre). </br><p style="width:600px">![comunicación_21.png](/comunicación_21.png)</p>

Pero si así se necesita, se puede configurar un número específico de frecuencia. Para ello basta con deshabilitar el *switch* y establecer el valor deseado. </br><p style="width:800px">![comunicación_22.png](/comunicación_22.png)</p>

    
### Interacción de usuarios
En el apartado ***Interacción de usuarios*** podremos determinar el comportamiento del mensaje en función de las acciones de rechazo que realice o no el usuario. 
> Es importante recordar que el control por clic de rechazo es permanente. Una vez que el usuario cumple la condición, no volverá a ver nunca más ese mensaje. {.is-warning}

Por defecto el *check* está activado, esto significa que no se va a aplicar ningún control a nivel de rechazo del mensaje (clic en la cruceta de cierre). </br><p style="width:600px">![inapp_1.png](/inapp_1.png)</p>

Si se desactiva el *check* se podrá establecer el número de rechazos (clic en la cruceta de cierre) a partir de los cuales el mensaje va a dejar de mostrarse al usuario. </br><p style="width:800px">![inapp_2.png](/inapp_2.png)</p>

> Esta funcionalidad sólo está disponible para aquellos formatos que cuenten con la cruceta de cierre, como son el Adball, Startview, Prisma y Native Ad en caso de que se haya integrado una cruceta de cierre. </br></br> Hay que tener en cuenta que este límite funciona vinculado a la configuración de impactos que se haya realizado.
{.is-info}
### Prioridad
Por defecto, las comunicaciones de EMMA se muestran en orden de creación decreciente, de más reciente creación primero, a menos reciente. 

Sin embargo, con el campo prioridad, podemos establecer el orden en el que queremos que se visualicen las comunicaciones, siendo 9 la mayor prioridad y por tanto la comunicación que se visualizará primero y 1 la menor prioridad y la comunicación que se visualizará en último lugar. 

Por defecto, la prioridad está desactivada, por lo que el comportamiento de visualización de comunicación será de más reciente creación a menos. </br><p style="width:600px">![comunicación_23.png](/comunicación_23.png)</p>

Si queremos establecer una prioridad, tan solo debemos habilitar el *switch* y establecer la prioridad deseada para cada comunicación.</br><p style="width:800px">![comunicación_24.png](/comunicación_24.png)</p>

> Es importante tener en cuenta que las prioridades tienen aplicación dentro del mismo formato y no entre formatos. Es decir, si tengo un adball con prioridad 9, otro con prioridad 5 y una startview con prioridad 8, no se mostrará adball, startview, adball, ya que las prioridades no funcionan entre formatos. {.is-warning}

Además, la prioridad la podremos configurar directamente desde la tabla de mensajes, en el campo editable **Prioridad** sin necesidad de tener que editar la comunicación. </br><p>![comunicación_25.png](/comunicación_25.png)</p>

Desde la columna **Prioridad** podemos realizar las siguientes acciones: 
- Ordenar la tabla en orden creciente o decreciente.
- Modificar la prioridad, añadir prioridad o eliminar la prioridad.

Para modificar la prioridad basta con hacer clic en la celda correspondiente y ya se desplegará el selector para ello. 

Podremos diferencias las comunicaciones con prioridad de las comunicaciones sin prioridad ya que en las primeras se visualizará la prioridad configurada, mientras que las segundas (sin prioridad) estarán representadas con un "-"

### Límite de impactos
Por último, podemos establecer el **límite de impactos**, es decir, las veces que se le mostrará al usuario la campaña, en tres períodos:

-   Límite de visualización al día.
-   Límite de visualización de los últimos 7 días.
-   Límite de visualización de los últimos 30 días.
</br><p style="width:800px">![](/comunicacio%CC%81n_viii.png)</p>

Si seleccionas un límite, una vez lanzada la campaña el usuario no podrá ver más de las veces que se definieron. Eeste valor se podrá modificar posteriormente aunque la campaña esté activa. Por ejemplo, si si se edita el límite semanal, pasando de 2 a 5, y el usuario ya había visto la campaña 2 veces esa semana, podrá volver a verla 3 veces más hasta completar el nuevo límite que se ha establecido.

Con el check de control de saturación podemos configurar si esa campaña va a seguir las normas del control de saturación o las va a ignorar. Puedes ver más información acerca del **Control de saturación** [*aquí*](https://docs.emma.io/es/control-de-saturacion).


## Objetivo

Una vez llegamos al paso de Objetivo, debemos establecer el público objetivo al que queremos impactar con la comunicación. Para ello debemos seleccionar del listado una audiencia de las disponibles o crear una nueva haciendo clic en el botón **+ Crear nueva audiencia**.  
Si el listado de audiencias está vacío es porque no hay ninguna audiencia disponible para usar o que no has creado ninguna audiencia todavía. En esta [guía](/es/audiencias) puedes ver más información sobre cómo crear audiencias.

![inapp_22.png](/inapp_22.png)

Si quieres establecer un grupo de control a tu comunicación, debes habilitar el check de grupo de control y establecer el % de usuarios que visualizarán la comunicación original y el % de usuarios que **no** visualizarán la comunicación. Para ver más información acerca del grupo de control, visita nuestra guía sobre [*Grupo de control*](/es/grupo-de-control)***.***

![](/comunicacio%CC%81n_iv.png)

Una vez seleccionada la audiencia deseada y establecido (o no) el grupo de control, pulsa en el botón **Siguiente** para seguir con la configuración de la campaña.

## Confirmación

Antes de lanzar la campaña para tu público revisa la configuración de tu mensaje en la pantalla final de resumen. Aquí podrás revisar toda la configuración realizada en los pasos anteriores.

![](/comunicacio%CC%81n_ix.png)

Selecciona **Enviar mensaje** para publicar tu mensaje, **Volver** para volver al paso previo, **Cancelar** para eliminar los cambios no guardados en la campaña y **Guardar como borrador** para guardar tu campaña en borradores pero no publicarla.

En el caso de que tengas activo el sistema de roles para la pre-validación de campañas podrás también enviar la campaña al modo **Probar mensaje, Enviar a validar o** **Aprobar mensaje** en función del rol del usuario. Haz click [aquí](/es/prevalidacion-de-campa%C3%B1as) para conocer más sobre el sistema de pre-validación de campañas.

Como última medida de seguridad, EMMA mostrará un nuevo mensaje de confirmación antes de lanzar la campaña.

![](/comunicacio%CC%81on_xiii.png)

# Estados de las campañas

Una vez que has creado tus campañas pueden encontrarse en uno u otro estado, dependiendo de las caracterísitcas que cumpla. Los posibles estados en los que se puede encontrar una comunicación son:

-   **Activa:** Tu campaña se ha iniciado, esta activa y está siendo mostrada a los usuarios que corresponda.
-   **Aprobada:** Este estado solo aplica si está en uso el sistema de prevalidación de campañas. Más info al respecto [aquí](/es/prevalidacion-de-campa%C3%B1as). Significa que la campaña ha sido aprobada y está a la espera de que sea lanzada definitivamente. Mientras la campaña se encuentra en este estado no está siendo visible para ningún usuario.
-   **Eliminada:** La campaña ha sido eliminada.
-   **Borrador:** La campaña ha sido guardada como borrador. En este estado no se muestra a ningún usuario y puede ser recuperada en cualquier momento para lanzarla.
-   **Finalizada:** La campaña ha finalizado porque se ha cumplido la fecha de fin establecida. En este estado ningún usuario puede visualizarla.
-   **Pausada:** La campaña esta pausada y no se está mostrando a ningún usuario.
-   **Lista:** Este estado solo aplica para campañas automáticas y significa que la campaña está lista para ser usada en una [regla](https://docs.emma.io/es/reglas) o en una [ruta de cliente](/es/analisis-de-comportamiento/customer-journeys). En este estado la comunicación no se está mostrando a ningún usuario.
-   **Programada:** La comunicación está a la espera de cumplir la fecha de inicio establecida para activarse y empezar a mostrarse a los usuarios.
-   **Testing:** Este estado solo aplica si está en uso el sistema de prevalidación de campañas. Más info al respecto [aquí](/es/prevalidacion-de-campa%C3%B1as). Significa que la campaña esta en modo pruebas y se está mostrando a los usuarios que cumplen el segmento establecido.
-   **Para validar:** Este estado solo aplica si está en uso el sistema de prevalidación de campañas. Más info al respecto [aquí](/es/prevalidacion-de-campa%C3%B1as). Significa que la campaña ha sido creado y está a la espera de que sea validad y aprobada para posteriormente ser lanzada. Mientras la campaña se encuentra en este estado no está siendo visible para ningún usuario.

# Tipo de mensaje InApp

En EMMA existen diferentes tipo de mensajes InApp:

-   **Adball:** El Adball es un formato comunicativo con forma de burbuja que aparecerá en la pantalla del usuario al abrir la aplicación. Para ver más información acerca de cómo configurar un adball haz clic [aquí](/es/mensajes-inapp#adball).
-   **Banner:** El banner es el formato básico para tus comunicaciones que puedes utilizar para comunicar mensajes específicos en una pantalla. Para ver más información acerca de cómo configurar un banner haz clic [aquí](/es/mensajes-inapp#banner).
-   **Cupón:** El Cupón es un formato que te da la opción de canjear cupones si tienes un lugar físico controlando el número de redenciones, el código promocional y la fecha de activación y finalización. Para ver más información acerca de cómo configurar un cupón haz clic [aquí](/es/mensajes-inapp#cup%C3%B3n).
-   **Tab Dinámico:** El Tab Dinámico es un formato comunicativo que se incluye como una nueva pestaña dentro de tu *TabBar,* por lo que si tu aplicación no dispone de *TabBar* no podrás activar este formato. Para ver más información acerca de cómo configurar un banner haz clic [aquí](/es/mensajes-inapp#tab-din%C3%A1mico).
-   **Native Ad:** El Native Ad es un formato que incluye tus comunicaciones respetando siempre el formato y estilo de los contenidos de la plataforma. Se desplazan los espacios típicos reservados para la publicidad y se integran dentro del contenido convirtiéndose en parte de la experiencia del usuario cuando visita el medio. Para ver más información acerca de cómo configurar un cupón haz clic [aquí](/es/mensajes-inapp#native-ad).
-   **Prisma:** PRISMA es un formato  muy innovador y muy parecido a una StartView pero que presenta una diferencia sustancial, se comporta como un Slider y tiene un estilo 3D. Para ver más información acerca de cómo configurar un cupón haz clic [aquí](/es/mensajes-inapp#prisma).
-   **Startview:** La Startview es un intersticial que se despliega en toda la pantalla y que se muestra al abrir la aplicación o cuando el usuario realice una acción como abrir una pestaña concreta, hacer click en un botón... Para ver más información acerca de cómo configurar un cupón haz clic [aquí](/es/mensajes-inapp#startview).
-   **Strip:** Strip es un formato comunicativo que ocupa la barra de notificaciones del dispositivo (donde normalmente aparecen la hora, la batería…). Para ver más información acerca de cómo configurar un cupón haz clic [aquí](/es/mensajes-inapp#strip).

# Consultar, editar o eliminar campañas

Los mensajes programados se pueden consultar, editar, clonar o eliminar. A continuación se muestran los pasos a seguir para poder realizar estas acciones:

Una vez hecho el [login](https://ng.emma.io) en EMMA, dirígete a **Comunicación > Mensajes**\*.\* Aquí podrás ver una tabla con todas las campañas programadas y guardadas:

## Consultar campañas

Busca en la tabla la campaña que quieras consultar y haz clic sobre el menú contextual que se muestra a la izquierda de la columna **Nombre de campaña** y selecciona la opción ***Previsualizar.***

![](/comunicacio%CC%81n_xiv.png)

A continuación se desplegará la pantalla final de resumen de campaña. Desde esta pantalla puedes editar o clonar la campaña. Cierra esta pantalla haciendo clic en **Volver a la lista**\*.\*

![](/comunicacio%CC%81n_xv.png)

Consulta el [historial de las modificaciones de tu campaña](/es/prevalidacion-de-campa%C3%B1as#historial-de-cambios), haciendo clicc en el menú contextual a la izquierda del nombre de la campaña y seleccionando la opción de **Historia.**

![](/comunicacio%CC%81n_xvi.png)

## Editar campañas

Para editar tu campaña tienes que hacer clic en el menú contextual a la izquierda del nombre de la campaña y posteriormente seleccionando el botón de ***Editar**.*

![](/comunicacio%CC%81n_xvii.png)

Haz clic en el menú contextual y selecciona ***Clonar*** para clonar una de tus campañas ya programadas.

Haz clic en el menú contextual y selecciona ***Pausar/Lanza*** para pausar o activar tus campañas.

## Eliminar campañas

Elimina la campaña seleccionando el icono de la papelera en el menú contextual. Ten en cuenta que esta acción no podrá ser deshecha.

![](/comunicacio%CC%81n_ixx.png)

Cuando se seleccione esta opción, saldrá un mensaje de confirmación para eliminar la campaña. Selecciona ***Borrar*** si quieres continuar el borrado o ***Cancelar*** si no estás seguro de borrarla.

![](/comunicacio%CC%81n_xx.png)

# Adball

El Adball es un formato comunicativo con forma de burbuja que aparecerá en la pantalla del usuario al abrir la aplicación.

Al hacer click en la comunicación, se desplegará la url que hayas introducido en la URL de destino (esta URL debe ser HTTPS)*.* Te recomendamos que el contenido web esté estructurado para que su visualización se adapte a todos los tipos de dispositivos móviles.

Para cerrarla simplemente se arrastrará hasta el fondo de la pantalla.

![](/adball_i.png)

Cuando se activan varios AdBalls apuntando a usuarios coincidentes que pueden estar en segmentos diferentes, es importante tener en cuenta que si coinciden en el período de programación van a seguir un mismo patrón de comportamiento: **siempre prevalecerá el último AdBall programado.**

Debes tener en cuenta las siguientes situaciones en caso de programación simultánea:

1.  Si has seleccionado un capping de frecuencia, siempre prevalecerá el capping de frecuencia del último AdBall creado. Es decir, aunque el primer Adball programado impacte al mismo segmento, no se llegará a mostrar mientras el segundo este activo.
2.  Si no estableces ningún capping de frecuencia (Siempre)  en el último Adball creado y los Adball previos tiene ese mismo mismo capping o uno diferente, en ambos casos se seguirá mostrando solo el último AdBall porque sigue prevaleciendo su capping de frecuencia.

![](/adball_2_1.png)

Realiza la configuración específica del formato Adball:

-   **URL de destino:** URL que se desplegará/mostrará al hacer clic en el Adball. Dicha URL puede llevar anexadas etiqutas para su personalización e incluso parámetros con valores fijos. Por ejemplo: https://www.milanding.es?nombre={name}&favorito={local_favorito}&código=123
> Los formatos Adball, Banner y Startview permiten el uso de motor de sustitución para anexar etiquetas en la URL establecida en EMMA y que así el contenido de la URL puede ser personalizado en base a dichas etiquetas. 
Para poder llevar a cabo la personalización, desde la landing de destino se debe de poder recuperar el parámetro y el valor de la etiqueta para poder emplearlo para la peresonalización.
{.is-info}
-   **Posición vertical:** Selecciona la posición vertical en la que aparecerá el Adball:
    -   Arriba
    -   Medio
    -   Abajo
-   **Posición horizontal:** Selecciona la posición horizontal en la que aparecerá el Adball:
    -   Izquierda
    -   Derecha
-	**Seguimiento de clics:** Selecciona el modelo de medición de clics para el Adball entre las siguientes opciones: 
    -   **Tap:** Se medirá el clic cuando se abra el adball. Cuando se hace clic explícito en el adball en sí. 
    -   **Cambio URL:** Se medirá como clic cada cambio de URL que se produzca dentro del webview. Si un usuario está navegando por el webview y se producen 4 cambios de url, se medirán 4 clics. 
    -   **Deeplink:** Se medirá el clic cada vez que se ejecute un deeplink desde el adball, ya sea de manera directa (el deeplink se ejecuta al hacer clic en el adball) o de manera indirecta (en la landing configurada hay un botón que contiene el deeplink).
    -   **Clic en URL:** Se medirá el clic en el momento en el que el usuario llegue a una URL específica configurada en la comunicación. Si seleccionamos esta opción, tendremos que específicar la URL que queremos que se mida como clic en el campo **Clic en URL**.
-   **Imagen:** Carga la imagen que quieres que se visualice cuando se muestre el Adball. En esta [guía](/es/especificaciones-de-los-fomratos-de-comunicacion) puedes ver más información sobre las especificaciones de la imagen para un Adball.
-   **Variaciones de Test A/B:** Si aplica, añade las variaciones de Test A/B que sean necesarias. Puedes ver más info sobre el Test A/B [aquí](/es/test-a-b).

> Poder seleccionar la posición del Adball solo se podrá usar a partir de la versión 4.1 del EMMA SDK de iOS y Android.
{.is-danger}

> Desde [preferencias de la app](https://docs.emma.io/es/configuracion#preferencias-de-la-app), podemos definir el tipo de medición por defecto de los clics en el Adball. Independientemente del tipo definido en esta sección, siempre lo podremos modificar a la hora de crear la comunicación al que más se ajuste en cada momento. 
{.is-info}



# Banner

El Banner es el formato básico para tus comunicaciones que puedes utilizar para comunicar mensajes específicos en una pantalla.

![](/banner_i.png)

Puedes elegir el número de impresiones, cuanto tiempo permanecerá en la pantalla y la posición (arriba o abajo).  Además, puedes establecer que cuando el usuario haga clic en él, sea redireccionado a una URL externa o a otra sección dentro de la aplicación (vía deeplink).

![](/banner_ii.png)

La configuración del Banner es la siguiente:

-   **URL de destino:** URL a la que quieres redirigir a los usuarios cuando hagan clic en el Banner. Puede ser un sitio web (en este caso debe ser una URL con protocolo HTTPS) o a una sección específica de la aplicación (vía deeplink). 
En caso de configurar una URL, esta puede llevar anexadas etiqutas para su personalización e incluso parámetros con valores fijos. Por ejemplo: https://www.milanding.es?nombre={name}&favorito={local_favorito}&código=123
> Los formatos Adball, Banner y Startview permiten el uso de motor de sustitución para anexar etiquetas en la URL establecida en EMMA y que así el contenido de la URL puede ser personalizado en base a dichas etiquetas. 
Para poder llevar a cabo la personalización, desde la landing de destino se debe de poder recuperar el parámetro y el valor de la etiqueta para poder emplearlo para la peresonalización.
{.is-info}
-   **Cómo abrir la URL:** Selecciona el entorno en el que quieres que se despliegue la redirección del Banner:
    -   Inapp:  Al hacer clic en el Banner se abrirá un webview a pantalla completa que mostrará el contenido de la URL configurada.
    -   Navegador por defecto: Al hacer clic en el Banner, la URL configurada se cargará en el navegador por defecto que el usuario tenga configurado en el dispositivo móvil.
-   **Posición en la pantalla:** Selecciona la posición de la pantalla en la que quieres que se muestre el Banner. Puedes elegir entre:
    -   Abajo
    -   Arriba
-   **Frecuencia:** Establece la frecuencia de tu Banner. Puedes ver más información sobre la frecuencia [aquí](/es/mensajes-inapp#contenido).
-   **Visualización:** Establece el tiempo de visualización (en segundos) durante los cuales se mostrará el Banner. Por defecto se muestra siempre. Deshabilita el selector para establecer la duración de la visualización.
-   **Imagen:** Selecciona la creatividad (jpg, png, gif) que quieres que se muestre en el Banner. Puedes ver las especificaciones de la creatividad [aquí](/es/especificaciones-de-los-fomratos-de-comunicacion). Si es más grande se hará un crop centrado de la creatividad.
-   **Variaciones de Test A/B:** Si aplica, añade las variaciones de Test A/B que sean necesarias. Puedes ver más info sobre el Test A/B [aquí](/es/test-a-b).

# Cupón

![cupon.png](/cupon.png)

El Cupón es un formato que te da la opción de canjear cupones si tienes un lugar físico controlando el número de redenciones, el código promocional y la fecha de activación y finalización. Con el Cupón podrás generar promociones segmentadas y controlar si tus usuarios las canjean.

![](/cupo%CC%81n_i.png)

Simplemente con introducir el mensaje que quieras que aparezca en el Cupón y con el código promocional que selecciones, EMMA generará un código QR para que tus usuarios canjeen la promoción en el lugar físico.

Es importante que en la opción de **Límite de redenciones totales** selecciones el número máximo de redenciones que un dispositivo puede hacer del Cupón y en el campo **Límite de redenciones diarias** selecciones el número de redenciones que puede hacer un dispositivo por día. Además con la opción **Ocultar al límite** puedes decidir si una vez cumplido el número máximo de redenciones, el cupón deja o no de mostrarse a los dispositivos que hayan alcanzado dicho límite.

En el caso de campañas automáticas, también se puede seleccionar que el Cupón tenga una disponibilidad de 90 días. Con esto se podrá establecer una fecha de caducidad al Cupón, por ejemplo, si el usuario recibe un Cupón por si cumpleaños sólo tendrá 3 meses para poder canjearlo. Una vez pasado ese tiempo el Cupón dejará de estar visible para el usuario y no podrá canjearlo.

![](/cupo%CC%81n_ii.png)

La configuración específica del Cupón es la siguiente:

-   **Mensaje:** Mensaje que se quiere mostrar al usuario cuando visualice el Cupón, por ejemplo, las condiciones específicas de ese Cupón.
-   **Código de promoción:** Código promocional interno para identificar el coupon.
-   **Límite de redenciones totales:** Establece el número de redenciones máximas que un usuario puede hacer del Cupón.
-   **Límite de redenciones diarias:** Establece el número de redenciones máximas por día que un usuario puede realizar del Cupón.
-   **Ocultar al límite:** Si habilitamos o deshabilitamos esta opción el Cupón se puede ocultar o no cuando el usuario alcanza el número máximo de redenciones posibles de ese Cupón.
-   **Ocultar transcurridos 90 días:** Esta opción sólo estará disponible para campañas automáticas para reglas. Se podrá decidir si el Cupón se ocultará o no 90 después de su generación.
-   **Imagen:** Carga la imagen que el usuario visualizará cuando se le muestre el Cupón.
-   **Variaciones de Test A/B:** Si aplica, añade las variaciones de Test A/B que sean necesarias. Puedes ver más info sobre el Test A/B [aquí](/es/test-a-b).

Recuerda que también puedes establecer unos días de visualización específicos para un Cupón determinado, de este modo, el Cupón se mostrará unos días sí y otros no en función de tu elección. Esta configuración se lleva a cabo en el paso **3\. Programación** de la creación del Cupón.

Los días que aparecen en el input, serán los días que el Cupón se visualizará, mientras que los días que no aparecen serán los días deshabilitados y por ello el Cupón no se mostrará en esos días. Si queremos que el Cupón se muestre de Lunes a Domingo simplemente debemos seleccionar todos los días.

![](/cupo%CC%81n_iv.png)

> Ten en cuenta que los Cupones se muestran a los usuarios por el campo **id de cliente** Es decir, si en el segmento del Cupón hay un usuario con el **id de cliente** coincidente con el de otro u otros usuarios que no están en ese segmento, el Cupón se les mostrará igual.
{.is-warning}

### Caso de uso
Los cupones están pensados, sobre todo, para apps de restauración, y es en estas apps dónde suelen usarse. 
Pensemos en una app de restauración. La app puede estar compuesta por:
- Una pantalla *home*, donde el usuario aterriza nada más abrir la app.
- Una sección de *carta*, en la que se pueden ver todos los productos que se pueden consumir. 
- Una sección de *restaurantes* en la que se puede localizar todos los restaurantes existentes en el panorama nacional.
- Una sección de promociones u ofertas. 

Sería en esta última sección de promociones u ofertas donde podríamos ver los cupones como tal. Una vez se accede a esta sección, el usuario podrá ver las diferentes ofertas proporcionadas en la app, estos serían los cupones en sí. 
Lo que hace el cupón es mostrar la oferta u ofertas activas en el momento en un listado y, cuando se accede al detalle de cada una de las ofertas podremos ver la imagen promocional del cupón (la misma que en el listado de promociones general) con una descripición de la oferta en sí y un código QR para que el usuario pueda canjear la promoción en el local físico. 

De esta manera, se podrá saber en EMMA el número de canjes/redenciones que ha tenido un cupón en concreto así cómo los dispositivos que han llevado a cabo esos canjes/redenciones. 

# Tab dinámico

El Tab Dinámico es un formato comunicativo que se incluye como una nueva pestaña dentro de tu *TabBar,* por lo que si tu aplicación no dispone de *TabBar* no podrás activar este formato.
> Este formato sólo está disponible para el sistema operitvo iOS. Debido a limitaciones del sistema operativo Android no es posible su uso en este OS. 
{.is-info}

![](/tab_dina%CC%81mico_i.png)

Al igual que los demás formatos, te recomendamos que el contenido web que introduzcas en el campo **URL de destino** esté estructurado para que su visualización se adapte a todos los tipos de dispositivos móviles y que cumpla con el protocolo HTTPS para evitar errores de carga al mostrar el contenido de la URL.

![](/tab_dina%CC%81mico_ii.png)

Los campos a configurar para crear un Tab Dinámico son:

-   **URL de destino:** Contenido que queremos que se le muestre al usuario al hacer clic en el Tab Dinámico. Puede ser una URL externa o puede ser un deeplink que lleve al usuario a una sección específica dentro de la app.
-   **Título de la pestaña:** Nombre que se mostrará en el nuevo tab que se mostrará en el menú.
-   **Posición de la pestaña:** Posición que ocupará el Tab Dinámico dentro de tu menú TabBar.
-   **Imagen:** Establece la imagen que se mostrará en ese nuevo tab que aparecerá en el menú. Puedes ver las especificaciones de la creatividad [aquí.](/es/especificaciones-de-los-fomratos-de-comunicacion)
-   **Variaciones de Test A/B:** Si aplica, añade las variaciones de Test A/B que sean necesarias. Puedes ver más info sobre el Test A/B [aquí](/es/test-a-b).

# Native Ad

## Tipo

El Native Ad es un formato que incluye tus comunicaciones respetando siempre el formato y estilo de los contenidos de la App. Se desplazan los espacios típicos reservados para la publicidad y se integran dentro del contenido convirtiéndose en parte de la experiencia del usuario cuando visita el medio.

![](/native_i.png)

Para poder ver este tipo de campañas, tendrás que definir en tu código de aplicación cómo se verán estos anuncios nativos. En esta guía puedes ver la integración necesaria para [Android](https://developer.emma.io/es/android/integracion-sdk) y en esta otra la de [iOS](https://developer.emma.io/es/ios/integracion-sdk#nativead).

Después, podrás escoger entre las plantillas disponibles, y en función de la elegida, tendrás que poner los textos, imágenes y enlaces que quieras mostrar.

![](/native_ii.png)

Una vez seleccionada la plantilla, si a la hora de crearla se le asignaron tags, este es el momento de seleccionar el tag deseado para este template. Se podrá elegir un tag o ninguno, pero no se podrán seleccionar varios a la vez.

Recuerda que las plantillas se definen desde la pantalla de [Native Template](/es/mensajes-inapp#crear-plantillas) y el SDK proporciona los campos de esta para que el desarrollador de la app maquete el **Native Ad*+ de forma personalizada para tu App.

![](/native_iii.png)

> En el momento de crear un Native Ad, en el paso **3. Programación**, existe un campo llamado ***Prioridad***. Este campo sirve para establecer una prioridad de visualización al Native Ad en cuestión, pero solo aplica a los Native Ad múltiples que están implementados de una manera especial (el equipo que integró el SDK de EMMA deberá confirmarte si tu app usa batch Native Ad o no). </br>
>Al activar la prioridad, en lugar de devolver todos los native ad disponibles con esa plantilla, sólo devolverá el que tenga mayor prioridad y así sucesivamente.
>
>En caso de que **NO** se usen batch Native Ad, **este campo no se debe habilitar ya que provocará que la comunicación no se visualice en el dispositivo**.
{.is-warning}


## Crear plantillas

Como decíamos anteriormente, Native Ad es el formato comunicativo que podrás configurar con EMMA, que te permitirá incorporar y adaptar tus comunicaciones al contenido propio de la app.

Ahora veremos cómo crear tu primera plantilla de Native Ad, que podrás configurar posteriormente como puedes ver a continuación.

-   Haz [login](https://ng.emma.io) en EMMA y dirígete a **Gestión > Plantillas Nativas.**
-   Haz clic en el botón **\+ Nueva plantilla.**

![](/native_iv.png)

-   En el formulario que se ha desplegado, tienes que indicar los campos que tendrá este template. Puedes añadir todos los campos de texto, imagen o enlace que necesites para tu formato nativo. Además, en este punto también se pueden añadir etiquetas identificativas. Estas etiquetas van a servir, por ejemplo, para poder identificar Native Ads que usen el mismo template más de una vez, es decir, a la hora de hacer un Native Ad que utilice este template, se podrá elegir una de las etiquetas y así la App pueda identificar ese anuncio en concreto de otros que usen el mismo template.

![](/native_v.png)

-   **Nombre**: Campo de nombre para esta plantilla.
-   **¿Convertir en Plugin?:** Si convertimos la plantilla en *Plugin* pasará a ser un formato de comunicación más dentro de los ya disponibles de EMMA en la sección de **Comunicación > Mensajes.** Este campo es irreversible y una vez guardada la plantilla no podrá sufrir modificaciones.
-   **ID:** Este es el identificador de la plantilla. Debe ser un valor único para que la App sepa reconocer qué tipo de plantilla es y cómo debe de mostrarla en la App. Por defecto EMMA convierte el nombre configurado en el ID, pero se puede modificar por el que se quiera.
-   **Etiquetas de identificación:** Escribe el nombre que quieras para la etiqueta. Puedes añadir varias etiquetas. Estas etiquetas van a servir para poder identificar Native Ads que usen el mismo template más de una vez, es decir, a la hora de hacer un Native Ad que utilice este template, se podrá elegir una de las etiquetas y así la App pueda identificar ese anuncio en concreto, de otros que usen el mismo template.
-   **Plantilla pública:** Habilitando este selector podrás establecer que tu plantilla sea o no pública. Si tu plantilla es pública, podrá ser usada en todas las apps que tengas en tu cuenta de EMMA. Si por el contrario la plantilla es privada, sólo podrá ser empleada en la app en la que se ha creado.
-   **Plantilla contenedor:** Si habilitas la plantilla como contenedor el contenido de la comunicación será una lista de elementos en la que cada elemento estará compuesto por los campos de texto, imagen y enlace configurados en la plantilla.
-   **Campo Texto**: Añade los campos de texto necesarios. Puedes indicar qué tipo de campo de texto es (Título, Subtítulo, Cuerpo o Personalizado con el que puedes poner la categoría que prefieras). Además, en cualquier campo de texto, se pueden añadir valores predefinidos que estarán disponibles para su selección a la hora de crear la comunicación. 
-   **Campo Imagen**: Añade los campos de imagen necesarios. Puedes indicar qué tipo de campo de imagen es (Imagen principal, Imagen secundaria, Personalizado con el que puedes poner la categoría que prefieras).
-   **Campo Enlace**: Añade los campos de enlace necesarios. Puedes indicar qué tipo de campo de enlace es (CTA o Personalizado con el que puedes poner la categoría que prefieras).

> Habilitando o deshabilitando el botón **Obligatorio** podrás establecer que los campos sean de obligatorio cumplimiento o no a la hora de usar la plantilla.
> 
> Cada campo es individual, por lo que podrás establecer que unos sean obligatorios y otros no en función de tus necesidades. De este modo podrás usar una misma plantilla para todas las comunicaciones ya que no será necesario tocar el *templateID* en la app.
{.is-info}

-   Por último, haz clic en **Guardar plantilla.**

## Gestionar plantillas

Una vez hayas creado tu primera plantilla de Native Ad podrás editar, clonar, eliminar o exportar las Plantillas.

-   **Editar** te permite modificar cualquier propiedad de la plantilla salvo el selector de *Plugin*. Recuerda que si editas el Identificador puede que deje de funcionar el Native Ad en tu App
-   **Clonar** te permite hacer una nueva plantilla a partir de una previamente guardada.
-   También podrás **eliminarla** en cualquier momento.

Puedes **exportar** esta misma tabla haciendo click sobre el botón **Descargar datos**.

Para poder realizar alguna opción sobre cada plantilla, haz *mouse over* sobre el menú contextual que aparece a la izquierda de la tabla y se desplegará un menú desde el que podrás Editar, clonar o eliminar la plantilla en cuestión.

![](/native_viii.png)

En la pantalla de **Plantillas Nativas,** se mostrarán por defecto sólo las plantillas privadas, pero a través de un filtro podemos seleccionar las plantillas que queremos visualizar. Con la opción **Plantillas sólo privadas** visualizaremos únicamente las plantillas privadas, es decir las que son exclusivas de la app. Con la opción **Plantillas privadas y públicas** podemos consultar todas las plantillas, independientemente de si son exclusivas de la app o si son públicas para todas las app de la cuenta.

En la tabla de resumen de las plantillas podemos ver las siguiente información:

-   **ID:** ID de la plantilla.
-   **Nombre:** Nombre dado a la plantilla.
-   **Público:** En esta columna podremos ver si la plantilla es pública o no.
-   **Tags:** Listado de tags atribuídos a la plantilla.
-   **Contenedor:** En esta columna podremos ver si la plantilla es un contenedor o no.
-   **Texto:** Número de campos de texto que tiene la plantilla.
-   **Imágenes:** Número de campos de imagen que se han establecido para la plantilla.
-   **Enlaces:** Número de enlaces que contiene la plantilla.

Ten en cuenta que usando el **Buscador** podrás localizar fácilmente tus plantillas.

![](/native_ix.png)

## Usando las plantillas de Native Ad

En el momento que crees la plantilla, esta ya podrá ser seleccionada en la pantalla de **Mensajes** a la hora de crear un Native Ad.

![](/native_x.png)

Para ver cómo crear tu primer native haz click [aquí](/es/mensajes-inapp#tipo).

# Prisma

![prisma.png](/prisma.png)

PRISMA es un nuevo formato de comunicación In-App muy innovador y muy parecido a una StartView, pero que presenta una diferencia sustancial, ¡se comporta como un Slider y tiene un estilo 3D!

Gracias a su diseño, esta comunicación es un formato mucho más atractivo para los usuarios que además, motivará la interacción de los mismos con la comunicación.

De esta manera, podrás establecer hasta 10 comunicaciones/creatividades en un solo formato comunicativo, cada una de ellas con su propio CTA asociado.
![prisma_7.png](/prisma_7.png)

La configuración específica del prisma es la siguiente: 
![prisma_3.png](/prisma_3.png)

-   **Cómo abrir la URL:** Selecciona el entorno en el que quieres que se despliegue la redirección del Prisma.
    -   *In App*: El contenido de la URL configurada se abrirá en un webview embebido en la app.
    -   *Navegador por defecto*: El contenido de la URL configurada se abrirá fuera de la App, en el navegador por defecto que el usuario tenga configurado en su dispositivo móvil.
-    **Frecuencia:** Establece la frecuencia de tu Banner. Puedes ver más información sobre la frecuencia [aquí](https://docs.emma.io/es/mensajes-inapp#contenido). 
- **Main picture:** Selecciona tu creatividad (jpg, png o gif) que quieres que se muestre en la cara del prisma. Puedes ver las especificaciones de la creatividad [aquí](https://docs.emma.io/es/especificaciones-de-los-fomratos-de-comunicacion). Si es más grande se hará un crop centrado de la creatividad.
- **CTA:** Establece la url o deeplink al que va a ser redireccionado el usuario al hacer clic en él.
- **Añadir elemento**: Añade el número de elementos que necesites para tu prisma hasta un máximo de 10.
- **Variaciones de Test A/B:** Si aplica, añade las variaciones de Test A/B que sean necesarias. Puedes ver más info sobre el Test A/B [aquí](/es/test-a-b).

> ¿Qué es un elemento?
Un elemento es cada uno de los bloques que componen las caras del prisma. Cada elemento está compuesto por una imagen y un CTA. 
{.is-info}

# Startview

![startview.png](/startview.png)

La Startview es un intersticial que se despliega en toda la pantalla y que se muestra al abrir la aplicación o cuando el usuario realice una acción como abrir una pestaña concreta, hacer click en un botón...

![](/startview_i.png)

Para la Starview, a diferencia de otros formatos, no tienes que subir un archivo gráfico sino introducir una url con el contenido que quieras mostrar en la opción de **URL de destino** (es importante que la URL que se establezca cumpla con el protocolo HTTPS). En caso de querer mostrar un vídeo, si quieres que este se muestre a pantalla completa debes usar una URL embebida.

> Para que en iOS se muestre correctamente el vídeo hay que añadir el parámetro **?playsinline=1** al final de la URL. Quedando una URL del estilo de esta: [https://www.youtube.com/embed/APuWqCmZXyw?palysinline=1](https://www.youtube.com/embed/APuWqCmZXyw?palysinline=1).
{.is-info}

El tamaño **mínimo** del intersticial debe ser **500x750px,** pero se recomienda un tamaño de **640x960px.** Pero se recomienda desarrollar un ***responsive layout*** con HTML y CSS que adapte tu formato a todas las pantallas del mercado para evitar problemas de visualización.

![](/startview_2_1.png)

La configuración específica de la Startview es:

-   **URL de destino:** Establecer la URL que quieres que se muestre al usuario cuando visualice la Startview. Dicha URL puede llevar anexadas etiquetas para su personalización e incluso parámetros con valores fijos. Por ejemplo: https://www.milanding.es?nombre={name}&favorito={local_favorito}&código=123
> Los formatos Adball, Banner y Startview permiten el uso de motor de sustitución para anexar etiquetas en la URL establecida en EMMA y que así el contenido de la URL pueda ser personalizado en base a dichas etiquetas. 
Para poder llevar a cabo la personalización, desde la landing de destino se debe de poder recuperar el parámetro y el valor de la etiqueta para poder emplearlo para la personalización.
{.is-info}
-	**Seguimiento de clics:** Selecciona el modelo de medición de clics para el Startview entre las siguientes opciones: 
    -   **Tap:** Se medirá el clic cuando se abra el adball. Cuando se hace clic explícito en el adball en sí. 
    -   **Cambio URL:** Se medirá como clic cada cambio de URL que se produzca dentro del webview. Si un usuario está navegando por el webview y se producen 4 cambios de url, se medirán 4 clics. 
    -   **Deeplink:** Se medirá el clic cada vez que se ejecute un deeplink desde el adball, ya sea de manera directa (el deeplink se ejecuta al hacer clic en el adball) o de manera indirecta (en la landing configurada hay un botón que contiene el deeplink).
    -   **Clic en URL:** Se medirá el clic en el momento en el que el usuario llegue a una URL específica configurada en la comunicación. Si seleccionamos esta opción, tendremos que especíificar la URL que queremos que se mida como clic en el campo **Clic en URL**.
-   **Botón de cerrar:** Establece si quieres que la Startview tenga o no una cruceta de cierre. Si tiene la cruceta de cierre, el usuario podrá cerrar la Startview y seguir navegando por la aplicación con total normalidad. Si no tiene la cruceta de cierre, el usuario no podrá cerrar la Startview y se quedará bloqueado sin poder navegar por la app hasta que cierre la app.
-   **Variaciones de Test A/B:** Si aplica, añade las variaciones de Test A/B que sean necesarias. Puedes ver más info sobre el Test A/B [aquí](/es/test-a-b).

> Desde [preferencias de la app](https://docs.emma.io/es/configuracion#preferencias-de-la-app), podemos definir el tipo de medición por defecto de los clics en el Adball. Independientemente del tipo definido en esta sección, siempre lo podremos modificar a la hora de crear la comunicación al que más se ajuste en cada momento. 
{.is-info}

Puedes programar varias Startviews simultáneas apuntando a usuarios coincidentes que pueden estar en segmentos diferentes, pero es importante tener en cuenta que si coinciden en el periodo de programación van a seguir un mismo patrón de comportamiento que el resto de comunicaciones: **siempre prevalecerá la última Startview programada**.

Debes tener en cuenta las siguientes situaciones en caso de programación simultánea:

1.  Si has seleccionado un capping de frecuencia, siempre prevalecerá el capping de frecuencia de la última Startview creada. Es decir, aunque la primera Startview programada impacte al mismo segmento, no se llegará a mostrar mientras la última Startview esté activa.
2.  Si no estableces ningún capping de frecuencia (Siempre) en la última Startview creada y las Starviews previas tiene ese mismo mismo capping o uno diferente, en ambos casos se seguirá mostrando solo la última Startview porque sigue prevaleciendo su capping de frecuencia.

# Strip

Strip es un formato comunicativo que ocupa la barra de notificaciones del dispositivo (donde normalmente aparecen la hora, batería, etc. del dispositivo).

Con el Strip la barra de notificaciones se ocultará y se mostrará un mensaje de derecha a izquierda con el texto que hayas introducido.

![](/strip_i.png)

La configuración específica del Strip es la siguiente:

![](/strip_ii.png)

-   **Mensaje:** Introduce el texto que quieres que se muestre en el Strip.
-   **Visualización:** Configura el tiempo de visualización de tu Strip. Deshabilita el check si quieres establecer el tiempo (en segundo) durante el que se mostrará el Strip en la app.
-   **Color del texto:** Selecciona en código hexadecimal el color de la tipografía del mensaje.
-   **Color del fondo:** Selecciona en código hexadecimal el color de fondo del Strip.

> Es importante que tengas en cuenta que, debido a limitaciones del sistema operativo, el formato Strip en iOS solo está disponible en los modelos igual o posterior al iPhone X.
{.is-danger}

# Preguntas frecuentes
**1. Al acceder a la sección de Comunicación > Mensajes veo un error en el dashboard y al crear un native ad no me aparecen las plantillas en el listado.**
Si estás viendo un error como este o similar y no puedes seleccionar una plantilla de native ad, seguramente se deba a que tienes instalado en tu navegador un AdBlocker que afecta al correcto funcionamiento de EMMA. 
Desinstala o desactiva el AdBlocker y ya no deberías tener problemas.
![error_adblock.png](/error_adblock.png)
**2. ¿Puedo añadir un gif animado en los formatos inapp?**
Los únicos formatos que a día de hoy permiten un GIF animado son: 
- Banner: No existe ningún tipo de limitación y se permite el uso tanto en Android como en iOS. 
- Prisma: Debido a limitaciones del sistema operativo, actualmente solo es posible usar un GIF animado en Android. En iOS se visualizará una imagen estática.

**3. ¿Cuáles de los formatos In-App están disponibles para ser usados en rutas de clientes?**
Todos los formatos In-app pueden ser usados para rutas de clientes. 
**4. ¿Qué formatos In-App permiten la creación de variaciones de Test A/B?**
La posibilidad de añadir variaciones de Test A/B está disponible en todos nuestro formatos comunicativos In-App.
**5. ¿Qué se puede configurar en el campo CTA?**
La mayoría de los formatos In-App permiten el uso de una URL o un deeplink. Puedes ver más info al respecto en cada uno de los formatos específicos. 
Según las necesidades de cada momento podrás configurar una URL clásica, por ejemplo [https://emma.io/](https://emma.io/) o, en aquellos formatos que lo permitan, un deeplink, por ejemplo miapp://ajustes, para redireccionar a los usuarios a una sección específica dentro de tu app.
**6. ¿Qué dimensiones deben tener las creatividades para cada uno de los formatos In-App disponibles?**
Cada formato tiene sus caracterísitca y permiten un tamaño determinado. En este [artículo](https://docs.emma.io/es/especificaciones-de-los-fomratos-de-comunicacion) puedes ver detalladamente las dimesiones recomendadas para cada uno de los formatos comunicativos disponibles en EMMA. 
**7. ¿Es necesario algún tipo de implementación en la app para poder disfrutar del formato PRISMA?**
Si, es necesario realizar una pequeña integración de nuestro SDK que afecta a este nuevo formato al igual que para el resto de formatos que ofrece EMMA. En esta guía está detallado el proceso de integración para Android y para iOS y en este repositorio se encuentran los ejemplos de integración para Android e iOS
**8. ¿Cuántas caras puede tener mi prisma?**  
Prisma permite añadir un máximo de 10 caras/creatividades diferentes cada una con su propio CTA asociado.
**9.¿En el prisma, se puede hacer swipe up para que haga la redirección?**  
No. Para que se ejecute la redirección configurada en el CTA es necesario hacer tap en el icono del ojo situado en la parte inferior del Prisma.
**10.En el formato prisma, ¿el avance a través del slider es unidireccional o bidireccional?**
El avance es bidireccional, puedes desplazarte por el Prisma de derecha a izquierda y viceversa, de izquierda a derecha en cualquier momento.