---
title: Rutas de clientes
description: Rutas de clientes
published: true
date: 2022-03-09T12:49:10.106Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:52:17.702Z
---

# Introducción

Gracias a las rutas de clientes de EMMA podrás crear flujos de comunicaciones automatizadas con los usuarios en función del ciclo de vida que estos tengan dentro de tu app. De tal manera que podrás ir creando distintos flujos de conversión y segmentando a los usuarios en función de si realizan o no determinadas acciones clave dentro de la aplicación para llegar a realizar los eventos objetivo establecidos. 

Con las rutas de clientes de EMMA, no sólo podrás crear flujos en función del ciclo de vida del usuario, sino que podrás comunicarte con ellos de la manera más adecuada teniendo en cuenta el status en el que se encuentran en cada momento, pudiendo hacer uso de todas las herramientas comunicativas de las que dispone EMMA, tanto comunicaciones In-App como Out-App (StartView, NativeAd, AdBall, Push, Email…). 

# Componentes de las rutas de clientes

Las rutas de clientes están compuestas por diferentes bloques que nos permiten ir construyendo de manera adecuada las flujos de comunicación con los usuarios de la app. Los bloques que componen las rutas son: 

-   Fuentes de entrada
-   Acciones
-   Control del flujo

## Fuentes de entrada

En este bloque, podremos seleccionar de entre dos opciones, cual será la que inicie la ruta. Es decir, en este bloque especificaremos que usuarios entraran en la ruta en cuestión. 

![](/rutas_fuentes_entrada.png)

-   **Evento >** Especificaremos el evento en cuestión que han de realizar los usuarios para entrar en la ruta.
-   **Audiencia >** Podemos seleccionar del listado los usuarios que queremos que entren en la ruta en cuestión seleccionando una audiencia previamente creada o, en caso de no tener una audiecia creada, podemos ir a crearla directamente desde el botón **+ Crear nueva audiencia**. Puedes ver más información acerca de las audiencias y cómo crearlas [aquí](https://docs.emma.io/es/audiencias). 
Si hemos seleccionado una [audiencia externa](https://docs.emma.io/es/audiencias) para lanzar nuestra ruta, tendremos un input con dos acciones que nos permitirán decidir que hacer con los usuarios que se envían a la audienca externa:
    -   **Si el usuario está en la ruta, vuelve al paso inicial**: Si se envía a la audiencia un usuario que ya está en la ruta, este volverá al inicio de la misma y volverá a iniciar el flujo. Se vuelve a procesar toda la información enviada. 
    -   **Si el usuario está en la ruta, se mantiene en el paso en el que estaba**: Si se envía a la audiencia un usuario que ya está en la ruta, este se queda en el paso en el que se encuentre en ese momento y seguirá avanzando con total normalidad por el resto de la ruta completando el flujo que corresponda en cada caso. En este caso, la segunda vez que se envía el usuario a través de la API se descarta, descartando consigo toda la información enviada. </br> <p style="width:700px">![rutas_i.png](/rutas_i.png)</p>

> Si una ruta tiene como fuente de entrada una audiencia externa, los usuarios entraran en la ruta en tiempo real. {.is-info}




## Acciones

Dentro del bloque de **Acciones** seleccionaremos la acción que queremos llevar acabo, es decir, el tipo de comunicación que se enviará a los usuarios, a elegir entre 3 posibilidades: 
![rutas_vi.png](/rutas_vi.png)

-   **Enviar Push:** Seleccionando esta opción se enviará una notificación push a los usuarios, la seleccionada en el momento de añadir esta acción al flujo.
-   **Activar In-App:** Seleccionando esta opción activaremos una comunicación In-App que estará disponible para que los usuarios la visualicen en el momento adecuado. Seleccionaremos la comunicación concreta en el momento de añadir esta acción al flujo. Cualquier formato comunicativo de los existentes en EMMA puede ser usado para rutas de clientes.
-   **Enviar Email:** Seleccionando esta opción se enviará un email a los usuarios en el momento adecuado del flujo. El email en cuestión se selecciona en el momento de añadir esta opción al flujo.
- **Enviar Postback:** Permite enviar postbacks a un sistema de un tercero. Originalmente están pensadas para ser configuradas después de una comunicación (inapp o push), pero se pueden configurar también en otro momento. Para ello EMMA permite el envío de dos tipos de postbacks: 
	-	**HTTPS**: Permite enviar postbacks a terceros usando el protocolo https. 
	-	**Integración Adobe**: Permite enviar postbacks a Adobe. Para poder usar este tipo de postback es necesario tener realizada una [integración con Adobe.](https://docs.emma.io/es/adobe)
  
### **Postbacks HTTPS** 
Para configurar una postback HTTPS tan solo hay que cubrir los pasos del formulario de configuración.</br><p style="width:700px">![rutas_iv.png](/rutas_iv.png)</p>
- **Tipo de postback**: Seleccionamos HTTPS
- **URL para el Postback**: Establece la URL sin parámetros a la que haya que enviar la notificación de postback. Por ejemplo https://emma.io:postbacks/rutas.
- Método del Postback: Selecciona el método a través del cual se enviará la postback entre: 
	-	GET
	-	POST
	-	PUT
	-	DELETE
	-	PATCH
> Si no sabes qué método del postback elegir, debes contactar con el sistema que vaya a recibir la postback para que confirmen qué método se debe emplear. {.is-info}
- **Cuerpo del postback**: Establece el parámetro que se quiere enviar en la postback y establece el valor que se debe vincular a ese parámetro. 
	-	**customerId**: ID de cliente que tiene vinculado el dispositivo que ha realizado la acción en BBDD.
	-	**campaignId**: ID de la campaña de comunicación con la que se ha interactuado.
	-	**campaignType**: Tipo de campaña con la que se ha interactuado. Puede ser push, email, adball, banner, cupón, native ad, prisma, startview o strip. 
	-	**journeyId**: ID de la ruta de cliente en la que se ha ejecutado la postback.
	- **unixTime**: Hora actual en formato unix.
	- **isoDate**: Fecha actual en formato iso. 
	-	**uuidV4**: Identificador aleatorio. 
	-	**Valores personalizados**: Se puede configurar un valor personalizado fijo, por ejemplo 3. 
- **Cabecera del Postback**: Se puede configurar el valor de las cabeceras del postbacks con un valor fijo concreto. Las cabeceras que se pueden configurar son: 
	-	Authorization
  -	Content-Type
  -	X-Requested-With

### Postbacks Integración Adobe
Para configurar una postback de *Integración Adobe* tan solo hay que cubrir los campos que se hayan determinado cuando se realizó la [integración con Adobe](https://docs.emma.io/es/adobe). Por ejemlplo: 
- **InteractionResult**: En este campo se estabelece el valor de la interacción. Por ejemplo si se ha abierto la notificación push o no, se podría establecer el valor si o no según corresponda.
- **InteractionType**: En este caso se establecería el tipo de intreacción llevada a cabo. En una notificación push la interacción es la apertura, en el caso de una inapp puede ser impresión o clic. 

Por ejemplo, si vamos a enviar una postback después de una comunicación inapp, el valor de ***InteractionType*** podría ser *impression* y el valor de ***interactionResult*** podría ser *yes* en caso de que se haya realizado la impresión o *no* en caso de que no se haya relaizado la impresión.
En el caso de enviar una postback después de una notificación push, el valor de ***InteractionType*** podría ser *open* y el valor de ***interactionResult*** podría ser *yes* en caso de que se haya abierto la notificación push o *no* en caso de que no se haya abierto la notificación push.



## Control del flujo

Dentro del bloque **Control del flujo** tenemos varias opciones disponibles para poder realizar un control del flujo del usuario según nuestras necesidades específicas en cada caso. De esta manera, podemos establecer los siguientes controles del flujo: 

![rutas_vii.png](/rutas_vii.png)

-   **Interacción:** Se utiliza inmediatamente después de una acción (enviar un push, email o In-App) para poder controlar si los usuarios han realizado o no alguna interacción con dicha comunicación.  
    En el caso de añadir la ***interacción*** tras un email o una notificación push, la acción que se podrá comprobar si se ha realizado es la ***apertura*** del mismo.   
    En el caso de una comunicación In-App se podrá comprobar si se ha visualizado ***(impresión)*** o sí se ha interactuado ***(clic)*** con la comunicación en cuestión.
   > Al añadir una interacción después de una comunicación inapp, tendremos la opción de que la comunicación deje de mostrarse al llegar a este punto del flujo. Si habilitamos esta opción, la comunicación dejará de mostrarse a todos los usuarios que se encuentren en este paso, independientemente de que hayan realizado o no la interacción establecida (clic, impresión o apertura)
![rutas_2.png](/rutas_2.png)
    {.is-info}
-   **Esperar:** Nos permite añadir un espera entre acciones o controles de flujo para poder cumplir con los tiempos de acción requeridos en caso. La espera puede ir desde horas, hasta meses en función de lo que sea necesario y se adapte para cada caso concreto.   
    Las opciones existentes son:   
    -	*Horas:* desde 1 hasta 48  
    -	*Días:* desde 1 hasta 90
    -	*Semanas:* desde 1 hasta 18
    -	*Meses:* desde 1 hasta 36
    ![](/rutas_espera_tiempo.png)

-   **División aleatoria:** Añadiendo una *división* *aleatoria* nos permite dividir la audiencia que llegue a este paso para unos usuarios sigan un flujo de comunicación y otros usuarios otro de manera totalmente aleatoria.
-   **Evento:** Añadiendo un control de flujo de *evento*, podremos comprobar si, llegados a un punto específico de la ruta, los usuarios han realizado o no X evento clave dentro de nuestra app o no. Con el control de flujo tipo *evento* simplemente se comprueba si el usuario ha realizado el evento o no una vez que está dentro de la ruta, no cuando ni cuantas veces ni hace cuanto tiempo.
-   **Decisión:** Al añadir una *decisión* se podrá ir construyendo el flujo de comunicación en base a cierta información que cumplan los usuarios, como por ejemplo, que en la etiqueta de usuario A tengan X información o que el atributo de sistema operativo sea X (iOS por ejemplo). De esta manera, gracias a este control de flujo, podremos ir acotando los usuarios que queremos que avancen por un flujo de comunicación y otro en función de la información que exista en las etiquetas de usuario para cada uno de ellos.
- **Esperar interacción:** Se utiliza inmediatamente después de una acción (enviar push, email o inapp) y permite establecer un flujo directo a través de una interacción con la acción, pudiendo revisar si el usuario ha realizado una interacción con la comunicación en cuestión en el tiempo máximo configurado. </br>
La espera máxima se puede configurar en un rango de 1 hora hasta 36 meses. </br>
En el caso de añadir ***Esperar interacción*** tras una notificación **push** o un email, la **interacción** que se podrá comprobar es saber si se ha relizado la **apertura** del mismo. 
En el caso de añadir ***Esperar interacción*** tras una comunicación **inapp** se podrá comprobar si se ha visualizado dicha comunicación (**impresión**) o si se ha interactuado con ella (**clic**). </br><p style="width:700px">![rutas_ii.png](/rutas_ii.png)</p>
    
> **EJEMPLO**
Si se añade un ***Esperar interacción*** después de un push con una espera máxima de 1 hora, si el usuario abre el push antes de que finalice el tiempo de espera establecido avanzará de manera inmediata a través del flujo. 
Si no realiza la apertura en el tiempo de espera establecido, una vez transcurrido este tiempo seguirá el camino de “no se ha realizado la interacción”.<p style="width:900px">![rutas_iii.png](/rutas_iii.png)</p>{.is-info}



# Cómo crear comunicaciones para rutas de clientes

Crear comunicaciones para rutas de clientes es igual de sencillo que crear otro tipo de comunicaciones, tan solo nos tenemos que asegurar de seleccionar *Tipo automática para rutas de clientes* a la hora de crear la comunicación. Para ver más detalladamente cómo dar de alta nuevas comunicaciones, visita nuestra guía sobre [Cómo crear una nueva comunicación](https://docs.emma.io/es/mensajes-inapp). 

# Cómo crear rutas de clientes

Crear rutas de clientes es muy sencillo. Una vez que hayas creado todas las comunicaciones que quieras vincular a una ruta, tan solo debes ir a ***Comportamiento > Rutas de cliente (beta)** * y dar de alta una nueva ruta. 

Para ello, tan solo tienes que hacer clic en el botón verde situado a la derecha de la pantalla que dice ***\+ Nueva ruta de cliente*** y se abrirá el formulario de creación. 

![](/rutas_clientes_formulario_creación_i.png)

Veamos a continuación cada uno de los diferentes campos y acciones que se pueden realizar a la hora de dar de alta una nueva ruta de cliente. 

1.  **Nombre:** Configura un nombre para la ruta. Si no se establece ninguno, por defecto será *Nueva Ruta de cliente.* El sistema permite usar un nombre ya existente, con lo cual, si usas un nombre existente, será responsabilidad tuya saber identificar posteriormente las rutas.
2.  **Fuentes de entrada:** Tras configurar un nombre deberemos establecer una fuente de entrada, bien sea un evento o una audiencia.
3.  **Acciones y Control de flujo:** Una vez que hemos añadido una *fuente de entrada*, la sección de *Acciones* y *Control del flujo* se desbloqueará y podremos añadir las acciones y controles de flujo que necesitemos para ir dando forma a nuestra ruta.
4.  **Zoom in / Zoom out:** Existe una opción para poder acercar y alejar el zoom de la ruta para poder ver el detalle de la misma o poder visualizarla de un modo global cuando se trata de una ruta compleja con muchos flujos diferentes.
5.  **Deshacer / Rehacer:** Con los botones de deshacer / rehacer podemos deshacer la última acción que hayamos realizado o volverla a rehacer.
6.  **Corregir ruta:** Si creamos un flujo incorrecto, este botón se marcará en rojo. Haciendo clic en él, automáticamente corregirá la ruta añadiendo el elemento(s) que falte(n) o eliminando lo(s) elemento(s) que sobre(n).
7.  **Lanzar / Guardar borrador /Cancelar:** Una vez que tenemos creada y configurada nuestra ruta podemos *Cancelar* la ruta, *Guardar  borrador* (lo que nos permitirá tenerla precreada para poderla lanzar cuando necesitemos) o *Lanzar.* Con esta última opción nuestra ruta entrará en el proceso para activarse y que los usuarios empiecen a acceder a la misma.

# Editar, pausar, eliminar, clonar y ver rutas de clientes

Una vez tenemos creadas nuestras rutas de clientes podemos realizar varias acciones sobre las mismas a través del menú al que se puede acceder desde los … que se muestran a la izquierda de la tabla de resumen. 

![](/rutas_editar.png)

Una vez que tenemos una ruta activa, las acciones que podemos hacer sobre la misma son: 

-   **Ver:** Con esta acción podemos previsualizar la configuración que tiene la ruta y los usuarios que hay en cada uno de los pasos de la misma.
-   **Clonar:** Al clonar una ruta, se clonará toda la configuración de la misma.
-   **Asignar:** Podemos asignar un nuevo propietario a la ruta. Por defecto el propietario es el usuario que la da de alta en la plataforma.
-   **Parar:** En caso de ser necesario se puede pausar la ruta. Una vez pausada, su estado cambiará de Activa a Borrador.

Si queremos Editar o Eliminar una ruta, es necesario  pararla antes de poder realizar ninguna de estas acciones. Una vez parada, en el menú aparecerán dos nuevas opciones: 

![](/rutas_editar_eliminar.png)

-   **Editar:** Nos permite editar la ruta y realizar las modificaciones que sean necesarias sobre la misma.
-   **Eliminar:** Podemos eliminar una ruta que ya no queramos usar. Las rutas eliminadas solo permiten las acciones de **Ver** y **Clonar.** 

# Reporting 

En rutas de clientes tenemos dos tipos de reporting diferenciados. Por un lado, tenemos un reporting específico del rendimiento de la ruta en la propia pantalla de rutas de clientes y, por otro lado, tenemos un reporting de cada una de las comunicaciones dentro de la ruta que podemos consultar desde la sección Comunicación > Informe de comunicaciones. 

>Los reportings de rutas de clientes se basan en el estado “actual” de la ruta. </br>
Cuando pausamos una ruta, ésta pasa al estado borrador y se reinicia. En ese momento, pasa a tener 0 usuarios de audiencia y todos los reportings (tanto el de *Rutas de cliente* como el  *Informe de comunicaciones*) pasan a no tener información. </br>
Por tanto, si pausamos una ruta de cliente, perderemos la información del reporting que había hasta ese momento.
{.is-warning}


## Reporting rutas de cliente

En el reporting de rutas de clientes vamos a ver la siguiente información: 

![](/rutas_reporting_i.png)

-   **ID:** ID interno de EMMA para cada ruta.
-   **Nombre:** Nombre dado a cada una de las rutas en el momento de la creación.
-   **Estado:** Estado en el que se encuentra la ruta. Los estados posibles son:
    -   ***En proceso***: Una ruta estará en proceso inmediatamente después de lanzarla durante unos minutos (5 aporx) hasta que pasa al estado *Activa.*
    -   ***Activa***: Cuando una ruta está *Activa* significa que ya está lista para que los usuarios entren en ella y sigan los diferentes flujos de comunicación creados.
    -   ***Eliminada***: La ruta ha sido eliminada.
    -   ***Borrador***: La ruta ha sido guardada como *Borrador* y no está disponible para los usuarios.
-   **Audiencia**: Bajo el KPI audiencia podremos ver todos los usuarios que actualmente están en algún paso de la ruta. Es decir, veremos los usuarios que están dentro de la ruta y los que ya han salido. En el caso de rutas con una fuente de entrada dinámica, este dato se irá actualizando a medida que nuevos usuarios vayan entrando en la ruta.
-   **Rendimiento:** Porcentaje de usuarios sobre el total de la audiencia que ya han finalizado la ruta.
-   **Última modificación:** Fecha en la que se ha realizado la última modificación sobre la ruta.

Además, si hacemos clic en la opción **Ver** desde el menú contextual, dentro de la propia ruta, podremos ver en detalle cuantos usuarios hay actualmente en cada uno de los pasos de la ruta y el total de usuarios que han pasado por cada uno de los pasos. 

El número en negro representa los usuarios que actualmente están en ese paso en concreto y el número en gris representa los usuarios totales que han pasado por cada uno de los pasos que componen la ruta. <p style="width:1000px">![rutas_reporting_iii.png](/rutas_reporting_iii.png)</p>

## Reporting informe de comunicaciones

Al igual que con el resto de comunicaciones (clásicas o automáticas para reglas), en la sección ***Comunicación > Informe de comunicaciones*** podremos ver las estadísticas individuales de cada una de las comunicaciones que están siendo empleadas en las diferentes rutas. Podremos ver información de *impresiones, impresiones únicas, clics, eventos…* 

![](/rutas_reporting_ii.png)

Visita nuestra guía de [*Reporting de campañas*](https://docs.emma.io/es/comunicacion/campa%C3%B1as/reporting-campa%C3%B1as) para obtener más información al respecto.

# Preguntas frecuentes
**1. ¿Por qué los usuarios pueden tardar hasta 1 hora en entrar en la ruta y avanzar de un paso a otro?**
Para asegurar un correcto funcionamiento de las rutas y evitar colapsos y posibles errores, el sistem de EMMA ejecuta el cálculo de los flujos cada hora. Por eso mismo motivo, las esperas mínimas que se pueden establecer son de 1 hora. 
Pongamos por ejemplo que el cálculo de las rutas se realiza a las en punto. Si un usuario entra en la audiencia de la ruta por ejemplo a las 12:05 y otro a las 12:57, un usuario "tardará" más tiempo que otro en avanzar al segundo paso de la ruta debido a que el sistema no se ejcutaría nuevamente hasta las 13:00.
**2. ¿Cuánto tiempo tarda en lanzarse una ruta?**
Los usuarios pueden tardar hasta 1 hora en entrar en la ruta y en avanzar de un paso a otro cuando ya están dentro de la ruta. 
**3. ¿Se puede programar la hora de envío de las notificaciones push que están dentro de una ruta?**
No, no se puede programar la hora de envío de una notificación push que está dentro de una ruta. La hora de envío de las notificaciones push se verá afectada por el tiempo de transación de los usuarios entre pasos dentro de la ruta así como por las esperas establecidas en el flujo de la ruta.
Se puede trabajar con el controlador de flujo "esperar" para intentar hacer una aproximación a la hora de envío deseada añadiendo esperas en el flujo de la ruta, pero si se desea enviar una notificación push a una hora exacta sólo se podría llevar a cabo mediante las [notificaciones push](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications) clásicas fuera de las rutas. 
**4. ¿Un usuario puede salir en mitad de una ruta porque cumple un criterio en otra, aunque en la ruta 1 no hayamos definido ese punto de fin?**  No. Un usuario no puede salir en mitad de una ruta. Un usuario solo sale de una ruta cuando llega al fin de la misma a través del cualquiera de los caminos establecidos. 
**5. Si un dispositivo tiene varios usuarios (clientes diferentes) ¿en base a qué identificador se mete al usuario en la ruta?** En EMMA siempre trabajamos en base al id de dispositivo. Por tanto entrá en la ruta el dispositivo, siempre y cuando este cumpla las condiciones de la fuente de entrada establecida en la ruta.
