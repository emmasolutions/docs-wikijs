---
title: Resumen ejecutivo
description: Resumen ejecutivo
published: true
date: 2025-03-04T10:27:56.247Z
tags: 
editor: markdown
dateCreated: 2021-05-31T11:43:01.537Z
---

# Introducción

El **resumen ejecutivo** te permite revisar la actividad principal de tu app de manera unificada en una misma pantalla. Es la forma más rápida y sencilla de acceder a los principales KPI's de tu aplicación y poder tener un evolutivo de los mismos. 

> Es importante que tengas en cuenta que se trata de una pantalla de reporting y los datos que se muestran no son en tiempo real, sino que la información se actualiza cada 3 horas.{.is-warning}

Haz login en EMMA y ve a la sección ***General > Resumen Ejecutivo.*** 

![](/r_ejecutivo_5.png)

Los datos que mostrará EMMA serán siempre en función del [rango de fechas seleccionado](https://docs.emma.io/es/barras-de-menu#men%C3%BA-supra-principal) y se visualizarán en las gráficas, según el tipo de agrupación seleccionada (*días*, *semanas* o *meses*).</br><p style="width:650px">![](/r_ejecutivo_1.png)</p>

Los rangos de fecha disponibles para filtrar los datos son:

-   *Hoy*
-   *Ayer*
-   *Últimos 7 días*
-   *Últimos 30 días*
-   *Este mes*
-   *Mes pasado*
-   *Rango personalizado*

Una vez seleccionado el rango de fechas a consultar, verás que los tres KPI's situados en la cabecera de la página han actualizado su información:

![](/r_ejecutivo_6.png)

|     |     |
| --- | --- |
| ***Usuarios activos**** | Usuarios que han abierto la aplicación en los últimos 30 días. Al seleccionar un rango de fechas concreto, se muestra el número máximo de MAUs de ese rango de fechas. Para calcular este dato se usa la información del KPI *last session*.|
| ***Instalaciones*** | Usuarios que han abierto la aplicación por primera vez, en el intervalo de tiempo seleccionado. |
| ***Aperturas*** | Aperturas de la aplicación en el intervalo de fecha seleccionado. |

> *Para poder calcular los MAUs, EMMA tiene la información día a día. Al seleccionar un rango de fechas concreto, EMMA muestra el dato del día que tuvo más MAUs dentro de ese rango.</br></br>**Por ejemplo:** En el rango entre el 3 y el 6 de junio, los MAUs de cada día son:</br>- día 3: 100 MAUs</br>- día 4: 137 MAUs</br>- día 5: 92 MAUs</br>- día 6:108 MAUs</br>
Al seleccionar ese rango de fechas, la información que mostrará el dashbord será 137 MAUs, ya que es el volumen máximo de MAUs del rango de fechas seleccionado.
{.is-info}

De igual modo, cada una de las secciones que componen el resumen ejecutivo, habrán actualizado la información a mostrar en función del rango de fechas seleccionado anteriormente y el tipo de agrupación. A continuación, veremos más en profundidad cada una de las 4 secciones que forman esta pantalla.

-   [Adquisición](https://docs.emma.io/es/resumen-ejecutivo#adquisici%C3%B3n)
-   [Comportamiento](https://docs.emma.io/es/resumen-ejecutivo#comportamiento)
-   [Comunicaciones](https://docs.emma.io/es/resumen-ejecutivo#comunicaciones)
-   [Ventas](https://docs.emma.io/es/resumen-ejecutivo#ventas)

# Adquisición

En la sección de **Adquisición**, podrás consultar los datos de los usuarios provenientes de tus campañas, es decir, usuarios atribuidos en el EMMA Apptracker.

## Instalaciones

En la gráfica de instalaciones se muestra el total de las instalaciones de tu aplicación y un desglose de las mismas en función de su origen, así como el porcentaje de variación respecto al periodo inmediatamente anterior.

![](/r_ejecutivo_2.png)

|     |     |
| --- | --- |
| ***Instalaciones*** | Usuarios que han abierto la aplicación (= instalación de la app) por primera vez en el periodo de fechas seleccionado. |
| ***Instalaciones orgánicas*** | Instalaciones generadas de manera orgánica. |
| ***Instalaciones no orgánicas***| Instalaciones generadas por campañas de pago. |
| ***Instalaciones última versión*** | Usuarios que han abierto la última versión de la aplicación (= instalación de la app) registrada en EMMA por primera vez en el  periodo de fechas seleccionado.<br><br>Esta versión suele coincidir con la que está en las stores, pero si se compila una versión en desarrollo con una numeración mayor, se mostrarán los datos de esta. |

## Fuentes de medios principales

En la siguiente gráfica podemos ver las mejores fuentes de medios y el volumen de instalaciones que ha conseguido cada una de ellas. 

En el centro del gráfico se puede ver el número total de instalaciones generadas por las fuentes de medios en el periodo de fechas seleccionado, así como el % de variación con respecto al periodo anterior. Además, puedes hacer *mouse over* sobre las distintas áreas del gráfico para visualizar el número de instalaciones de cada una de las fuentes. 

> Sólo se muestran las 6 mejores fuentes de medios, es decir, las que han generado un mayor número de instalaciones de la app en el periodo de fechas seleccionado. 
{.is-info}

</br><p style="width:500px">![](/r_ejecutivo_3.png)</p>

En la leyenda de la gráfica, podemos ver listadas cada una de las 6 fuentes de medios, con su nombre, el número de instalaciones y el % que supone ese volumen sobre el total de las instalaciones de estas fuentes de medios. 

## Desinstalaciones

En esta última gráfica, se muestran las desinstalaciones de la aplicación versus las instalaciones. De este modo, se podrá hacer una comparativa rápida entre ambos kpi's en periodos de fecha concretos. En la parte superior se muestra el número total de instalaciones junto con el total de desinstalaciones.</br><p style="width:750px">![](/r_ejecutivo_4.png)</p>

Para poder ver datos de desinstalaciones es necesario: 

1.  En el caso de Android, tener una versión del SDK igual o superior a la 4.2.11. En el caso de iOS no es necesaria una versión mínima del SDK
2.  Tener integrado el módulo de notificaciones push en [Android](https://developer.emma.io/es/android/integracion-sdk#integraci%C3%B3n-notificaciones-push) y en [iOS](https://developer.emma.io/es/ios/integracion-sdk#integraci%C3%B3n-notificaciones-push).

> Debido a los cambios de privacidad que ha llevado a cabo Apple, desde las versión de OS, iOS 14.5 o superior, no comparte información de las desinstalaciones con terceros.{.is-warning}

### Cómo calcula EMMA las desinstalaciones

EMMA calcula las desinstalaciones de tu app en un rango inferior a 24 horas sin necesidad de esperar 7 días, 2 semanas o un mes para establecer que dicho usuario ya no está activamente usando tu app. 

Para ello, nuestro tracking de desinstalaciones envía una notificación silenciosa cada 24 horas a todos los usuarios. Si el usuario ha desinstalado tu app recibiremos un error de Google o de Apple indicando que dicho dispositivo ya no tiene la aplicación. 

En caso de recibir dicho error en iOS, EMMA marcará a este dispositivo con el evento de desinstalación. 

En dispositivos Android, EMMA verificará este estado con una segunda notificación a las 6 y una tercera notificación a las 12 horas. Al tercer KO en Android, EMMA marcará a este dispositivo con el evento de desinstalación. 

Al contrario que con las notificaciones push normales, con las notificaciones silenciosas el usuario no ve ni recibe ningún indicador de que nuestro sistema esté verificando el estado de la app en su dispositivo. Además, las notificaciones silenciosas pueden ser enviadas a todos los usuarios, incluso, aunque estos hayan desactivado las notificaciones push normales. 

> Apple y Google son los encargados de determinar si el token de un usuario está activo o no y el tiempo que debe transcurrir entre que un usuario se desinstala la app y el token deja de estar activo (que puede no ser el mismo en todos los casos). 
Desde EMMA no tenemos ningún tipo de control sobre este flujo ni tampoco conocemos los tiempos internos que maneja ninguna de las dos plataformas.
{.is-info}


# Comportamiento

En la sección de Comportamiento se muestran los datos correspondientes a esta parte de la herramienta que engloba el comportamiento de los usuarios dentro de la App.

## **Usuarios**

El primer gráfico muestra el volumen de usuarios según su comportamiento dentro de la aplicación. Puedes seleccionar las distintas áreas del gráfico para visualizar datos de:
- aperturas
- usuarios activos
- usuarios registrados
- compradores

Este gráfico aparece acompañado de un índice explicativo que refleja el volumen total de los usuarios según su tipo de comportamiento y la variación de los mismos sobre el periodo anterior.

![](/r_ejecutivo_7.png)

|     |     |
| --- | --- |
| ***Aperturas*** | Número de veces que se ha abierto la App en el periodo de tiempo seleccionado. |
| ***Usuarios activos***| Número de usuarios que han abierto la aplicación, al menos un vez, en un periodo de 30 días. Al seleccionar un rango de fechas concreto, se muestra el número máximo de MAUs de ese rango de fechas. Puedes ver más información sobre qué información se muestra [aquí](https://docs.emma.io/es/resumen-ejecutivo#introducci%C3%B3n). |
| ***Usuarios registrados*** | Los usuarios que han hecho un registro directamente, a través de la aplicación o sitio móvil, en el intervalo de fechas seleccionado. |
| ***Compradores***| Usuarios que han hecho un evento de compra a través de la aplicación o sitio móvil, en el intervalo de fechas seleccionado. |


> **TIP:** Usa los [eventos por defecto](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto) de EMMA para enviar la actividad relacionada con los registros y las ventas y que estos KPI's se visualicen en el resumen ejecutivo. {.is-info}

## **Apertura por horas**

La gráfica de barras muestra el volumen de aperturas de la aplicación por momento del día. Para conocer la cantidad de sesiones por hora es necesario deslizar el cursor por la gráfica.

En la parte inferior aparece reflejada la duración media de la sesión, es decir, el tiempo que un usuario pasa con la aplicación en primer plano en su dispositivo en el periodo.

![](/r_ejecutivo_8.png)

## **Usuarios por aperturas**

El gráfico situado a la derecha muestra el volumen de usuarios por número de aperturas. En la parte inferior aparece el número medio de aperturas por usuario en el periodo.

![](/r_ejecutivo_9.png)

> Cuando es la media de varios días, se realiza cogiendo la media individual de cada día y se usan las sesiones para hacer una media ponderada. {.is-info}

# Comunicaciones

En el apartado de Comunicación se muestran los resultados de las campañas realizadas durante el período de tiempo seleccionado, tanto de notificaciones Push (reflejado en la primera gráfica) como de mensajes inapp (segunda gráfica de la pantalla).

## **Notificaciones Push e Ingresos**

El primer gráfico muestra información sobre Notificaciones *Push* e Ingresos. En concreto, el volumen de Notificaciones Push enviadas contrapuestas a los ingresos generados. Aparece junto a un índice en el que se recogen breves explicaciones sobre la tipología de las notificaciones push mostradas en el gráfico y las ventas e ingresos asociados.

Se trata de una gráfica interactiva por lo que, para visualizar las distintas áreas puedes deslizar el cursor sobre este. Se mostrarán los datos concretos según el periodo seleccionado.

![](/r_ejecutivo_10.png)

|     |     |
| --- | --- |
| ***Push enviadas*** | Total de campañas de notificaciones Push enviadas en el intervalo de fechas seleccionado. |
| ***Push entregadas*** | Total de notificaciones Push entregadas a los dispositivos en el intervalo de fechas seleccionado. |
| ***Push abiertas*** | Total de usuario que han abierto una notificación Push en el intervalo de fechas seleccionado. |
| ***Ventas asociadas*** | Ventas totales asignadas a las notificaciones Push en el intervalo de fechas seleccionado. |
| ***Ingresos asociados*** | Valor total generado mediante las transacciones de notificaciones Push, en el intervalo de fechas seleccionado. |

## **Mensajes In-App**

El segundo gráfico, muestra las comunicaciones In-App que has programado en tu aplicación dentro de Comunicación > Mensajes*.* Para visualizar en detalle el número exacto de impresiones, impresiones únicas, *clics* y cupones aplicados correspondientes a un periodo de tiempo determinado, basta con seleccionar las distintas áreas del gráfico. Este gráfico cuenta con un índice explicativo a modo de apoyo, para que una rápida y sencilla comprensión de las acciones sobre Mensajes In-App que se están midiendo.

![](/r_ejecutivo_11.png)

|     |     |
| --- | --- |
| ***Impresiones*** | Total de usuarios impactados por las comunicaciones In-App, en el intervalo de fechas seleccionado. |
| ***Impresiones únicas*** | Número de usuarios únicos que han visualizado los Mensajes In-App, en el intervalo de fechas seleccionado. |
| ***Clics*** | Interacciones totales generadas con las comunicaciones In-App, en el intervalo de fechas seleccionado. |
| ***Cupones aplicados*** | Volumen total de cupones aplicados por los usuarios. |

# Ventas

En la sección de ventas se muestra el volumen total de ingresos generados por los eventos de compra, el número total de ventas y el volumen de venta contrapuesto con el de los ingresos generados por estas.

> **TIP:** Usa el [evento por defecto](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto) de EMMA para enviar la actividad relacionada con las ventas y que este KPI y sus atributos se visualicen en el resumen ejecutivo {.is-info}

## **Ingresos**

En la sección Ingresos, situado en la parte superior izquierda de la pantalla, se muestra un desglose del total de ingresos generados por los eventos de compra, junto con el valor total generado por las ventas (situado a continuación) y el número total de ventas completadas.

![](/r_ejecutivo_12.png)

|     |     |
| --- | --- |
| ***Total generado por los eventos de compra*** | Valor total generado a través de la transacción en el intervalo de fechas seleccionado. |
| ***Valor total generado por ventas*** | Número de pedidos totales realizados en el intervalo de fechas seleccionado. |
| ***Total de ventas completadas*** | Número total de ventas completadas dentro de la aplicación, en el intervalo de fechas seleccionado. |

## **Ventas** 

Esta sección (situada a la derecha de las sección "Ingresos") muestra de manera resumida el número total de ventas en el intervalo de tiempo seleccionado previamente.

![](/r_ejecutivo_13.png)

|     |     |
| --- | --- |
| ***Ingresos por venta*** | Valor promedio por pedido/transacción en el intervalo de fechas seleccionado. |
| ***Ingresos por usuario*** | Corresponde al valor medio de ingresos generados por comprador. |
| ***Primera venta*** | Promedio de días para la primera transacción desde la instalación de la app o visita al sitio móvil por primera vez. Información disponible para un rango de fecha máximo de 180 días. |

## **Ingresos y ventas**

El gráfico de barras muestra la relación entre el volumen de ventas y el de ingresos generados, en el intervalo de tiempo seleccionado.

![](/r_ejecutivo_14.png)