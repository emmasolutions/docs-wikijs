---
title: Eventos
description: Eventos
published: true
date: 2023-05-03T12:10:54.739Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:48:24.781Z
---

En EMMA los eventos se utilizan para identificar el comportamiento y las acciones de los usuarios dentro de una aplicación. Un mismo usuario puede tener un número infinito de repeticiones para cada evento creado. 

Tú decides cómo quieres medir este comportamiento delimitando el número y el tipo de eventos que quieres medir.

En EMMA existen dos tipos de eventos diferenciados. Los eventos **por defecto** y los eventos **personalizados.** 

![e.a.-etiquetas.png](/e.a.-etiquetas.png)

# **Eventos por defecto**

EMMA define como **eventos por defecto** a aquellos eventos básicos que suelen ser comunes en todas las aplicaciones, y que se crean de forma automatica cuando se realiza la integración de EMMA. [Aquí](https://developer.emma.io/es/home) puedes ver como implementar los eventos por defecto de EMMA en tu app. 

A continuación se detalla la lista de los **eventos por defecto** que aparecerán automáticamente en EMMA cuando se usen los métodos correspondientes de la integración:

-   **EMMA Instalación:** genera datos cuando se produce una instalación. Una instalación se produce cuando el usuario descarga la app y la abre por primera vez. 
-   **EMMA Apertura app:** genera datos cuando se produce una sesión. La sesión se lleva a cabo cada vez que el usuario abre la aplicación.
-   **EMMA Registro:** genera datos cuando se produce un registro. El registro normalmente se produce cuando el usuario completa el formulario de alta/registro dentro de la app. 
-   **EMMA Primer login:** genera datos cuando se produce el primer inicio de sesión.
-   **EMMA Login:** genera datos cuando se produce un inicio de sesión. Este inicio de sesión normalmente salta cuando se introducen las credenciales de usuario y contraseña para poder acceder a la app. 
-   **EMMA Conversión:** genera datos cuando se produce una transacción/conversión. Esta transacción/conversión normalmente se produce cuando el usuario realiza una compra dentro de la app.

# Eventos personalizados

Son todas aquellas acciones o secciones dentro de la aplicación que son exclusivas de vuestra aplicación. Hay dos tipos de acciones principales y que darán lugar la mayoria de todos vuestros **eventos personalizados**:

-   **Ver una pantalla o sección de la app.**
-   **Click en un botón de la app.**

Ten en cuenta también es posible que envieis un evento personalizado con atributos o propiedades concretas. 

Por ejemplo, supongamos que en nuestra aplicación tenemos una sección que es "blog", en la que mostramos diferentes posts. Si queremos saber a qué posts acceden los usuarios tendremos que:
1. Enviar a EMMA un **evento** que sea **blog**. Ëste medirá si los usuarios acceden a esa sección específica enviando la información a EMMA cuando ocurra. 
1. Para conocer qué post dentro del blog leen, debemos enviar como **atributo o propiedad** del **evento** **blog** el **nombre** en concreto del **post**, y/o el **id** interno del **post**. 

Para integrar los eventos personalizados en vuestra aplicación y medirlos con EMMA, lo primero que debemos hacer es crear esos eventos en EMMA. Al crear un evento en EMMA obtendremos un token necesario e imprescindible para su implementación en el código de la aplicación. Puedes crear hasta **512 eventos** personalizados por aplicación.

Consulta nuestras [guías de integración](https://developer.emma.io/es/home) para ampliar información sobre cómo integrar los **eventos personalizados.** 

A continuación, veremos con detalle cómo crear, editar y eliminar eventos. 

# Crear eventos

Para poder integrar un evento personalizado en la aplicación y medirlo con EMMA, lo primero que necesitas es darlo de alta en EMMA. Para ello, tienes que: 

-   Inicia sesión en EMMA y ve a ***Gestión > Eventos.***

![](/crear_eventos_1.png)

-   Haz clic en el botón ***\+ Nuevo evento*** para crear un nuevo evento.

![](/crear_eventos_2.png)

-   Completa los datos para la configuración del evento:

![](/crear_eventos_3.png)

-   **Nombre (obligatorio):** selecciona el nombre del evento. Este nombre definirá el evento en los informes asi como en el resto de EMMA.
-   **Token (opcional):** el token del evento es el identificador que debes integrar en el código de la aplicación. Si este espacio se deja vacío, EMMA asignará automáticamente un token para identificar cada uno de los eventos.
-   **¿Activo? (opcional):** indica si queremos "encender" el evento o no, por defecto el switch está activado. En el caso de marcarlo como inactivo, EMMA dejará de registrar datos para dicho evento.
-   **¿Es una pantalla? (opcional):** señala esta opción si el evento se corresponde con la visualización de una pantalla de la aplicación. Es importante que identifiques qué eventos corresponden con la visualización de una pantalla ya que existen [**reglas**](https://docs.emma.io/es/reglas) directamente dependientes de esta elección.
-   **Orden (opcional):** establece un orden para el evento, siendo el primero el número uno. Esto te ayudará a ver los eventos con un orden coherente en Explorador y al generar los reportings.
-   **Postback (opcional):** configura una URL válida para que EMMA notifique cada vez que se realice dicho evento. Puedes ver cómo realizar esta configuración [aquí](https://docs.emma.io/es/primeros-pasos/eventos#configurar-postback-para-notificar-todos-los-eventos-realizados-a-un-tercero)



# Gestionar eventos

Una vez que tenemos creados todos los eventos que queremos medir con EMMA, podemos acceder a la **Gestión > Eventos** tantas veces como necesitemos para crear nuevos eventos, editar, gestionar o eliminar los eventos ya existentes. 

Desde esta pantalla, podemos ver en una tabla todos los eventos que hemos dado de alta. En esta tabla podemos ver la siguiente información:
![tabla_eventos.png](/tabla_eventos.png)

-   **Nombre del Evento:** nombre dado al evento en el momento de su creación. Se puede modificar editando el evento. Esta cambio no tiene afectación más allá del nombre que se muestra en EMMA para localizar ese evento. Más info [aquí](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos). 
-   **Token:** token del evento. Si realizamos cambios sobre este token en EMMA y no se hacen los cambios pertinentes en el código de la aplicación, el evento dejará de medirse. Más info [aquí](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos).
- **Activo:** Si tiene un check nos dice que el evento está activo y por tanto se EMMA está recopilando información del mismo. Si no está activo, aunque el evento esté integrado, no se podrán ver datos de mismo en el dashboard de EMMA. Puedes ver como activar un evento [aquí](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos)
- **configuración:** Si tiene un icono de una pantalla, significa que el evento ha sido marcado como **Pantalla** para poder ser usado en comunicaciones con [reglas](https://docs.emma.io/es/reglas). Lo habitual es que sólo aquellos eventos que queremos usar para comunicaciones con reglas tengan este icono.
-   **Orden:** orden establecido al evento. Esto afecta al orden en el que se muestran los eventos en las pantallas de reporting de EMMA. Más info [aquí](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos).  


## Editar eventos

Puedes editar un evento en cualquier momento volviendo a entrar en ***Gestión > Eventos*** y seleccionando la opción *Editar* en el menú contextual que se encuentra a la izquierda del nombre:  

![editar_evento_1.1.png](/editar_evento_1.1.png)

Los cambios en los campos de tu evento no afectan al *token* del evento que ya se generó. Puedes modificar cualquier campo de la configuración del evento, pero es importante que tengas en cuenta que si se modifica el token es necesario cambiarlo también en el código de la aplicación. Nuestra recomendación es no modificar el campo de *token* una vez el evento esté implementado en la aplicación. 

![editar_evento_2.1.png](/editar_evento_2.1.png)

Una vez hagamos clic sobre el botón ***Editar evento*** los cambios se aplicarán al evento en cuestión. 

Cuando los eventos se hayan implementado en el código de la aplicación, EMMA registrará la información y te la mostrará en la pantalla de Explorador. 

## Eliminar eventos

Para eliminar un evento, selecciona la opción ***Eliminar*** que aparece en el menú contextual:

![eliminar_evento_1.png](/eliminar_evento_1.png)

EMMA te preguntará si estás seguro de eliminar el evento y una vez confirmado, se eliminará automáticamente de tu cuenta:

![eliminar_evento_2.png](/eliminar_evento_2.png)

# Buscar eventos
Si es necesario localizar un evento para realizar alguna modificación o simplemente revisar la configuración de dicho evento, se puede realizar una búsqueda del mismo tanto por el nombre del evento como por el token del evento. 

Para ello tan solo hay que seguir estos pasos: 
1. Ve a la sección Gestión > Eventos. 
2. Localiza el buscador en la parte superior derecha de la tabla de eventos. ![buscar_evento_1.png](/buscar_evento_1.png)
3. Escribe el nombre o el token del evento a localizar.
![buscar_evento_2.png](/buscar_evento_2.png)
![buscar_evento_3.png](/buscar_evento_3.png)

# Configurar postback para notificar todos los eventos realizados a un tercero
Si se necesita enviar a un tercero una notificación cada vez que se realiza un determinado evento dentro de la app (independientemente de que sea un evento atribuido a una campaña u orgánico), es necesario configurar el campo de postback que aparece al crear/editar un evento.

> Es importante que tengas en cuenta que con esta configuración se notificarán todos los eventos que se realicen dentro de la app, independientemente de si es de un usuario orgánico o de un usuario atribuido a una campaña en concreto. [Aquí](https://docs.emma.io/es/adquisicion/apptracker) puedes ver más información sobre las campañas de EMMA. 
{.is-warning}

La configuración es muy sencilla, simplemente hay que establecer una URL de notificación válida en el campo de postback y añadir parámetros dinámicos y/o fijos en caso de ser necesario. 

> La URL que se debe configurar y a la que se va a notificar, la debe determinar el tercero que va a recibir la información de los eventos. No es responsabilidad de EMMA saber que Postback URL se debe configurar en cada caso.
{.is-info}

Teniendo esto en cuenta, para configurar una postback para notificar un evento, tenemos que seguir estos pasos: 
1. Ir a la sección **Gestión > Eventos**.
![postback_evento_v.png](/postback_evento_v.png)
2. Buscar el evento en cuestión y hacer clic en **editar**. ![editar_evento_1.1.png](/editar_evento_1.1.png)
3. Hacer scroll down hasta la parte inferior de la pantalla donde se encuentra la **sección** de **Postback**.![postback_evento_vii.png](/postback_evento_vii.png)
4. Establecer la postback url en el input **URL de postback**. Por ejemplo https://estaEsMiPostback/event![postback_evento_viii.png](/postback_evento_viii.png)
5. Si la postback tiene paráemtros fijos se deben añadir en el input URL de postback. Se añaden anexados a la URL base ya configurada y se pueden añadir tantos como se necesiten separados entre ellos por un **&**. Por ejemplo http://estaEsMiPostback/event?paramfijo=Test&tracker=EMMA&control=123
![postback_evento_i.png](/postback_evento_i.png)
6. Si la postback tiene parámetro dinámicos se deben seguir estos pasos: 
6.1. Haz click en el botón **+ Añadir parámetro** que aparece justo debajo del input de **URL de postback**. ![postback_eventos_ii.png](/postback_eventos_ii.png)
6.2. A continuación, en la columna **Valor EMMA** selecciona una de las macros disponibles (las macros disponibles están listadas a continuación de estos pasos). Importante que tengas en cuenta que las únicas macros que puedes seleccionar de entre las disponibles son las que hemos listado en este artículo. ![postback_eventos_iii.png](/postback_eventos_iii.png)
6.3. Por último, en la columna **Parámetro URL** debes escribir el parámetro del tercero tal cual te digan ellos con el que se va a vincular la macro de EMMA.![postback_eventos_iv.png](/postback_eventos_iv.png)
6.4. Repetiremos este proceso tantas veces como parámetros dinámicos vayamos a añadir.
> **EJEMPLO**
Configuramos en el campo **URL de postback** la siguiente URL con parámetros fijos: http://estaEsMiPostback/event?paramfijo=Test&tracker=EMMA&control=123 </br>
A continuación, añadimos dos parámetros dinámicos con la siguiente configuración: </br>
Añadimos el primer parámetro seleccionando en el campo **Valor EMMA** la macro **udid** y como **Parámetro URL** configuramos *id_dispositivo*.
 Añadimos el segundo parámetro seleccionado en el campo **Valor EMMA** la macro **event_id** y como **Parámetro URL** configuramos *evento*. </br>
Con esta configuración, se enviará una notificación como la siguiente: 
http://estaEsMiPostback/event?paramfijo=Test&tracker=EMMA&control=123&id_dispositivo=ad6af18a-b9c4-4f27-bfa1-b5d651c268e6&evento=ajustes
{.is-info}

Los parámetros dinámicos permitidos son los siguientes: 
- **customer_id**: Es el id de cliente que se envia desde la app a EMMA.
- **idfa**: El identificador de publicidad de iOS. Con iOS 14 y su política de privacidad en muchos casos, si el usuario no da permiso para usar el IDFA lo que se usa es el IDFV.
- **idfv**: Identificador aleatorio de iOS cuando el usuario no da permiso para usar el IDFA. 
- **aaid**: Identificador de publicidad de Android.
- **udid**: Identificador de publicidad del dispositivo, independientemente del sistema operativo. Enviará el que corresponda en cada caso, idfa, idfv o aaid.
- **event_id**: Identificador del evento en EMMA.
- **random**: Valor aleatorio
- **order_id**: Id de la transacción. Sólo aplica si se tienen ventas dentro de la app y estas se miden con los eventos por defecto de EMMA para dicho propósito. 
- **amount**: Cantidad gastada en el pedido/compra. Sólo aplica si se usan los eventos pode defecto de EMMA para medir las compras dentro de la app.


Un **parámetro fijo** es aquel en el que el valor no varia y siempre va a ser el mismo cada vez que se ejecute la acción concreta. Un **parámetro variable** es aquel que varia cada vez que se ejecuta la acción. 
Por ejemplo un parámetro fijo puede ser la herramienta desde la que se recibe la información. Este parámetro va a ser siempre el mismo, cada vez que se ejecute la acción deseada, ya que la herramienta es siempre la misma, en este caso EMMA. Un parámetro variable puede ser, por ejemplo, el id interno del cliente o el id del dispositivo que ha realizado la acción en cada momento. 
