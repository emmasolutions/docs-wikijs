---
title: Barras menú
description: 
published: true
date: 2025-03-04T11:27:11.292Z
tags: 
editor: markdown
dateCreated: 2021-05-27T13:47:19.133Z
---

Dentro de EMMA vas a encontrar tres niveles diferentes de menús en los que podrás realizar una configuración general, acceder a las diferentes secciones de EMMA o aplicar filtros específicos a los datos que se muestran en determinadas secciones. 

Estos tres menús que componen EMMA son: 

-   Menú supra principal
-   Menú principal
-   Menú de filtrado

A continuación veremos más detalladamente cada uno de ellos y el tipo de acciones que se pueden llevar a cabo. 

# Menú supra principal

https://youtu.be/MABj4wks1MI

El menú supra principal es el menú en el que podemos seleccionar nuestra app, aplicar rangos de fechas y acceder al menú de configuración general de la cuenta y la app. 

![](/menu_supra.png)

Desde este menú podemos seleccionar, para las secciones de EMMA en las que aplique como en Resumen Ejecutivo o Instalaciones, cómo queremos que estén agrupados los datos que se muestran. Podemos elegir entre las opciones días, semanas o meses. </br><p style="width:350px">![](/menu_supra_ii.png)</p>

Según la agrupación seleccionado los datos de los gráficos se mostrarán desglosados por día, semana o mes. 

Además, en este menú también es donde establecemos el rango de fechas para le cuál EMMA nos está devolviendo datos. Este valor, afecta a los datos que se muestran en cualquier sección de EMMA. Por defecto, EMMA muestra datos de los último 7 días, pero podemos establecer otros rangos de fechas según nuestras necesidades entre los disponibles: </br><p style="width:700px">![](/menu_supra_iii.png)

-   **Hoy:** EMMA muestra datos del día en curso.
-   **Ayer:** EMMA muestra datos del día de ayer.
-   **Últimos 7 días:** EMMA muestra datos de los últimos 7 días desde el día en curso.
-   **Últimos 30 días:** EMMA muestra datos de los últimos 30 días desde el día en curso.
-   **Mes pasado:** EMMA muestra datos del mes natural inmediatamente anterior al actual.
-   **Este mes:** EMMA muestra datos del mes en curso.
-   **Rango personalizado:** EMMA muestra datos del rango de fechas seleccionado, puede ser un día concreto, una semana, 15 días o 1 mes y 3 días. En caso de existir algún límite de fechas a la hora de mostrar los datos, el dashboard avisará de dicho límite.

Ya por último, desde este menú, tenemos acceso a las diferentes secciones de configuración de la cuenta y la app haciendo *mouse over* sobre el nombre de usuario que se muestra al final de este menú. 

![](/base.png)

Puedes ver más detalles sobre cada una de las opciones que componen este menú de configuración, desde esta [guía](https://docs.emma.io/es/configuracion). 

# Menú principal

https://youtu.be/qf35MMk88w8

El menú principal es el menú al que accedemos a las diferentes secciones y funcionalidades que componen el dashboard de EMMA. 

![](/menu_princi.png)

Desde este menú podemos acceder a las siguientes secciones: 

-   General:
    -   [Resumen Ejecutivo](https://docs.emma.io/es/resumen-ejecutivo)
    -   [Informes](https://docs.emma.io/es/informes)
-   Captación:
    -   [Instalaciones](https://docs.emma.io/es/instalaciones)
    -   [Apptracker](https://docs.emma.io/es/adquisicion/apptracker)
    -   [Fuentes de medios](https://docs.emma.io/es/adquisicion/fuentes-de-medios#notificar-eventos-a-redes-publicitarias)
    -   [Enlaces cortos](https://docs.emma.io/es/adquisicion/enlaces-cortos)
-   Comportamiento:
    -   [Explorador](https://docs.emma.io/es/analisis-de-comportamiento/segmentos)
    -   [Embudos](https://docs.emma.io/es/analisis-de-comportamiento/embudos)
    -   [Audiencias](https://docs.emma.io/es/audiencias)
-   Comunicación:
    -   [Mensajes](https://docs.emma.io/es/mensajes-inapp)
    -   [Informe de comunicaciones](https://docs.emma.io/es/comunicacion/campa%C3%B1as/reporting-campa%C3%B1as)
    -   [Reglas](https://docs.emma.io/es/reglas)
    -   [Rutas de clientes](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys)
-   Gestión:
    -   [Plantillas nativas](https://docs.emma.io/es/mensajes-inapp#crear-plantillas)
    -   [Proveedores de email](https://docs.emma.io/es/comunicacion/mensajes-out-app/email)
    -   [Integraciones](https://docs.emma.io/es/guias-de-integracion/salesforce)
    -   [Eventos](https://docs.emma.io/es/primeros-pasos/eventos)
    -   [Etiquetas](https://docs.emma.io/es/etiquetas)
    -   [Landings](https://docs.emma.io/es/landings)
    -   [Categorías](https://docs.emma.io/es/categorias)

Puedes ver información más detalla de cada una de las secciones haciendo clic en ellas. 

# Menú de filtrado

https://youtu.be/T4Qf2p9DksU

El último menú que compone EMMA, es el menú de filtrado. Podrás identificar este menú por su color grisáceo. Este menú no está disponible para todas las secciones de EMMA. 

![](/menu_filtrado_i.png)

Dependiendo de la sección de EMMA en la que nos encontremos, habrá unos u otros filtros aplicados por defecto y podremos aplicar unos filtros específicos referentes a la sección en la que nos encontramos. 

Veamos los filtros disponibles, según la sección de EMMA en la que nos encontramos: 

## Filtros en Captación > Apptracker

En la sección de Apptracker de EMMA hay aplicadas varias agrupaciones/filtros por defecto: 

![filtro_apptracker_1.png](/filtro_apptracker_1.png)
- **Agrupación por campañas o canales:**  Por defecto se muestra la agrupación por campañas, es decir se mostrarán todas las campañas creadas por su nombre. Pero se puede cambiar a agrupación por canales. De este modo, se mostrará la información en base a los diferentes canales establecidos. Puedes ver más información sobre cómo crear y vincular canales [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios).
> La vista de agrupación por canales es **solo de lectura**. Si se quieren editar o crear nuevas [campañas](https://docs.emma.io/es/adquisicion/Campa%C3%B1as)/[fuentes](https://docs.emma.io/es/adquisicion/fuentes-de-medios), se debe realizar en la vista de agrupación por campaña.
{.is-warning}
- **Fitlro de todas las campañas o campañas favoritas:** Por defecto se muestra la información de todas las campañas, pero se puede cambiar el filtro para que se muestren sólo aquellas campañas que hayan sido previamete marcadas como favoritas. Puedes ver más información sobre cómo marcar campañas como favoritas [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as).
- **Fitlro de campañas básicas o campañas de Retargeting:** Es un filtro fijo y obligatorio que por defecto muestra las campañas **Básicas**, o sea, de captación. Se puede cambiar a **Retargeting** para que muestre esta modalidad de campañas o seleccionar ambas opciones a la vez. De esta manera, podremos ver todos los datos de un vistazo. En este enlace se puede consultar información más detallada sobre los modelos de atribución disponibles en EMMA.

Además, de los filtros/agrupaciones por defecto podemos añadir dos filtros a mayores haciendo clic en el botón **\+ Añadir filtro:** 

-   **Países:** Selecciona entre el listado de países disponibles para que EMMA muestre datos en base a tu filtro.
-   **Sistema operativo:** Selecciona el sistema operativo entre Android e iOS para que EMMA muestre datos en base a tu filtro.

> Los filtros y agrupaciones son combinables entre sí. Es decir, podemos aplicar múltiples filtros y agrupaciones entre sí para obtener la información deseada. {.is-info}

## Filtros en Comportamiento > Gente
En la sección de *Gente* tenemos un filtro sencillo por *Usuarios activos* o *Usuarios inactivos*.

  ![filtro_gente_1.png](/filtro_gente_1.png)

Por defecto, la opción seleccionada es *Usuarios activos*, pero podemos cambiar a la opción que más nos encaje en función del análisis de datos que queramos llevar a cabo. 
- **Usuarios activos:** Al seleccionar este filtro, EMMA mostrará los dispositivos activos, es decir, aquellos que en el momento de la consulta (en el día en curso) han realizado actividad para considerarse activos.
- **Usuarios inactivos:** Al seleccionar este filtro, EMMA mostrará **todos** los dispositivos que se han instalado la app. Es decir, toda la base de datos.

## Filtros en Comportamiento > Embudos

En la sección de *Embudos* tenemos un filtrado sencillo por sistema operativos. </br><p style="width:350px">![](/filtro_embudos.png)</p>

Por defecto, la opción seleccionada es *“Todos los sistemas operativos”*, por lo que EMMA muestra datos conjuntos de ambos OS. Sin embargo podemos cambiar la selección entre: 

-   **Android:** Selecciona esta opción para ver datos solo de dispositivos Android.
-   **iOS:** Selecciona esta opción para ver datos solo de dispositivos iOS.

## Filtros en Comportamiento > Audiencias

En la sección de Audiencias también tenemos este menú de filtros. En este caso, los filtros que podemos aplicar son los siguientes: </br><p style="width:500px">![](/filtro_audiencias.png)</p>

-   **Todas las audiencias:** es el filtro por defecto y devuelve todas las audiencias existentes para la app. Si seleccionamos la opción ***“Mis audiencias”*** veremos únicamente las audiencias que hemos creado.
-   **Estado:** Filtra las audiencias en función del estado en el que se encuentran (*En progreso*, *Error*, *Inactiva*, *Lista* y *Nueva*).
-   **Tipo:** Selecciona el tipo de audiencia entre las opciones:
	-	CSV (dinámico)
	-	CSV (estático)
	-	FTP/SFTP
	-	Fuente externa
	-	HTTPS
	-	Informe de rendimiento
	-	S3
	- Fuente externa
  Puedes ver más información sobre cada origen [aquí](https://docs.emma.io/es/audiencias#crear-audiencias). 

## Filtros en Comunicación > Mensajes

En la sección Mensajes, puedes aplicar filtros para que EMMA te devuelva unas u otras comunicaciones en función a los filtros que hayas aplicado. </br><p style="width:700px">![](/filtro_mensajes.png)</p>

-   **Aplicación actual:** Es un filtro por defecto. Se puede cambiar por ***“Todas las aplicaciones”.*** Según la opción seleccionada, mostrara sólo las comunicaciones que han sido creadas en la app actual o las comunicaciones de todas las aplicaciones que estén bajo nuestra cuenta.
-   **Todos los mensajes:** También es un filtro por defecto. Se puede cambiar por ***“Mis mensajes”.*** En función de la opción seleccionada, mostrará todas las comunicaciones creadas en la app, independientemente del usuario que las ha creado o mostrará solo las comunicaciones creadas por el usuario en cuestión.
-   **Formato:** Puedes seleccionar uno o varios formatos entre [Adball](https://docs.emma.io/es/mensajes-inapp#adball), [Banner](https://docs.emma.io/es/mensajes-inapp#banner), [Cupón](https://docs.emma.io/es/mensajes-inapp#cup%C3%B3n), [Dynamic Tab](https://docs.emma.io/es/mensajes-inapp#tab-din%C3%A1mico), [Email](https://docs.emma.io/es/comunicacion/mensajes-out-app/email), [Native Ad](https://docs.emma.io/es/mensajes-inapp#native-ad), [Push](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications), [Startview](https://docs.emma.io/es/mensajes-inapp#startview) y [Strip](https://docs.emma.io/es/mensajes-inapp#strip) para que EMMA te devuelva las comunicaciones que coincidan con el o los formato(s) seleccionado(s).
-   **Estado:** Puedes seleccionar uno o varios estados de las comunicaciones para que EMMA te devuelva las comunicaciones que se encuentren en ese estado en concreto. Puedes seleccionar entre: *Activa*, *Aprobada*, *Eliminada*, *Borrador*, *Finalizada*, *Pausada*, *En proceso*, *Lista*, *Programada*, *Testing* y *Para validar*. Puedes ver más información sobre los estados [aquí](https://docs.emma.io/es/mensajes-inapp).
-   **Test A/B:** Filtra por aquellas comunicaciones que tengan establecidas variaciones de Test A/B o por las que no las tengan. Puedes ver más información sobre el Test A/B [aquí](https://docs.emma.io/es/test-a-b).

## Filtros en Comunicación > Informe de comunicaciones

Los filtros disponibles en la sección de Informe de comunicaciones son: </br><p style="width:450px">![](/filtro_informe_cmunicaciones.png)</p>
-   **In-App:** Por defecto se muestran los resultados de las comunicaciones In-App. Pero podemos cambiar la opción por **Out-App** para que se muestren datos de las comunicaciones Out-App.
-   **Test A/B:** Filtra por aquellas comunicaciones que tengan establecidas variaciones de Test A/B o por las que no las tengan. Puedes ver más información sobre el Test A/B [aquí](https://docs.emma.io/es/test-a-b).
-   **Sistema Operativo:** Selecciona el sistema operativo entre *Android* e *iOS* para que EMMA muestre datos en base a tu filtro.
-   **Formato:** Puedes seleccionar uno o varios formatos entre [Adball](https://docs.emma.io/es/mensajes-inapp#adball), [Banner](https://docs.emma.io/es/mensajes-inapp#banner), [Cupón](https://docs.emma.io/es/mensajes-inapp#cup%C3%B3n), [Dynamic Tab](https://docs.emma.io/es/mensajes-inapp#tab-din%C3%A1mico), [Email](https://docs.emma.io/es/comunicacion/mensajes-out-app/email), [Native Ad](https://docs.emma.io/es/mensajes-inapp#native-ad), [Push](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications), [Startview](https://docs.emma.io/es/mensajes-inapp#startview) y [Strip](https://docs.emma.io/es/mensajes-inapp#strip) para que EMMA te devuelva las comunicaciones que coincidan con el o los formato(s) seleccionado(s).
-   **Campañas:** Selecciona una campaña en concreto de las existentes para que se muestren datos en base a este criterio.

Todos los filtros son combinables entre sí, se pueden aplicar varios a la vez. 

## Filtros en Comunicación > Rutas de cliente

Los filtros disponibles en Rutas de cliente son: </br><p style="width:500px">![](/filtro_rutas.png)</p>

-   **Todas las rutas:** Es un filtro por defecto y devuelve todas las rutas creadas. Se puede cambiar por ***“Propias”*** para que solo se muestren las rutas creadas por uno mismo.
-   **Estado:** Selecciona entre las opciones *Activa*,*Borrador*, *Eliminada* o *En proceso*, para que se filtren las rutas que cumplan los criterios de filtrado.

## Filtros en Gestión > Plantillas nativas

Los filtros disponibles en la sección de Plantillas nativas son: </br><p style="width:350px">![](/menu_plantilas_nativas.png)</p>

-   **Sólo privadas:** Es un filtro por defecto. Devuelve aquellas plantillas categorizadas como privadas. Si seleccionamos la opción ***“Privadas y públicas”*** veremos tanto las plantillas privadas como las públicas. Puedes ver más información al respecto en esta [guía](https://docs.emma.io/es/mensajes-inapp#crear-plantillas).

## Filtros en Gestión > Eventos

En la sección de Eventos podemos aplicar un filtro para que EMMA nos devuelva unos u otros eventos: </br><p style="width:250px">![](/menu_eventos.png)</p>

-   **Personalizados:** Se muestran los eventos personalizados en el listado de eventos. Puedes ver más info sobre los eventos personalizados [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados).
-   **Por defecto:** Se muestran los eventos por defecto en el listado de eventos. Puedes ver más información sobre los eventos por defecto [aquí](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto).
-   **Pantallas:** Se muestran las pantallas detectadas en el listado de eventos.