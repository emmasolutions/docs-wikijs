---
title: Configuración
description: Configuración
published: true
date: 2023-12-18T13:25:41.618Z
tags: 
editor: markdown
dateCreated: 2020-11-25T23:11:59.209Z
---

# Preferencias de la cuenta
https://youtu.be/ac3YH3pxNWo

En la sección *Preferencias cuenta* podrás revisar y modificar los datos fiscales y de facturación de tu empresa que has facilitado a EMMA. Para acceder a esta sección tan solo tienes que hacer *mouse over* sobre tu nombre de usuario en la parte superior derecha de la pantalla. En el desplegable, selecciona la opción *Preferencias cuenta.* <p style="width:400px !important;">![pref_cuenta.png](/pref_cuenta.png)</p>

Una vez dentro de esta sección podemos ver la información desglosada en tres grupos: *General, Facturación y Pagos.* 

En el apartado ***General*** podemos ver el nombre de empresa que hemos configurado en el momento de darnos de alta en EMMA. Este nombre será el nombre de tu app dentro de EMMA. 

En el apartado ***Facturación*** podemos ver la información referente a la facturación, los datos fiscales de la empresa como el *nombre,* el *CIF, dirección, código postal…* <p style="width:900px !important;">![](/pref_cuenta_iii.png)</p>

Por último, en el apartado ***Pagos*** podremos ver la información sobre el método de pago introducido para hacer frente a los pagos de EMMA. El número de tarjeta de crédito (codificado por seguridad), fecha de caducidad y CVC (codificado también). 

Toda la información de esta página se puede editar en cualquier momento para poder llevar a cabo modificaciones en los datos facilitados a EMMA en caso de ser necesario. 

# Preferencias de perfil

En la sección *Preferencias perfil* podrás realizar cambios básicos sobre la información de tu perfil así como hacer modificaciones en la clave de acceso a EMMA. Para acceder a esta sección tan sólo tienes que hacer *mouse over* sobre tu nombre de usuario en la parte superior derecha y seleccionar la opción *Preferencias perfil* desde el desplegable. <p style="width:400px">![pref_perfil.png](/pref_perfil.png)</p>

En el apartado **General** podrás ver tu nombre y apellidos (que son los que ves en tu nombre de usuarios arriba a la derecha de la pantalla) y el email con el que has sido dado de alta en EMMA. Puedes realizar las modificaciones pertinentes sobre estos campos en caso de ser necesario haciendo clic en el botón **Editar**. 

Si quieres realizar una modificación sobre tu clave de acceso a EMMA tan solo tienes que hacer clic en **Cambiar contraseña** para acceder al formulario de cambio de contraseña. <p style="width:600px">![cambiar_contraseña.png](/cambiar_contraseña.png)</p>

# Preferencias de la app

https://youtu.be/MYXM_1XV2iM

En la sección de EMMA *Preferencias app* podrás realizar diversas configuraciones del funcionamiento de EMMA o realizar la configuración pertinente para la implementación de algunas funcionalidades. 

En esta guía, veremos qué configuración podemos realizar desde esta sección y a qué afectan y para qué sirven. 

Para acceder a la sección de preferencias de la app haz *mouse over* sobre tu nombre de usuario en la parte superior derecha de la pantalla. En ese momento, se desplegará un menú en el que podrás seleccionar la opción *Preferencias app.* <p style="width:1000px">![pref_app.png](/pref_app.png)</p>

## General (EMMA Key y API Key)

En la sección General podrás y modificar el nombre de la app así como la dirección web. Además, también tendrás acceso al EMMA Key, necesario para empezar a enviar datos desde la app a EMMA, así como al API Key, necesario en caso de usar la API. 

El EMMA Key es necesario para la implementación del SDK de EMMA. Se trata de un identificador único que vincula tu app con el dashboard de EMMA para poder obtener toda la información de manera correcta. 

El EMMA Key se crea automáticamente en el momento de dar de alta una app (más info sobre cómo dar de alta apps [aquí](https://docs.emma.io/es/configuracion#apps-de-la-cuenta)). 

Puedes consultar el EMMA Key siempre que lo necesites desde la sección Preferencias app en la sección *General*. Si haces clic sobre el icono de *clipboard* que se muestra a la derecha del EMMA Key podrás copiarlo directamente al portapapeles. 

![](/pref_app_emma_key_ii.png)

En caso de que hayas contratado el uso de la API, en esta sección, también podrás acceder al API Key, necesario para poder conectarte a nuestra API para enviar y/u obtener información. En esta guía puedes ver [cómo conectarte a nuestra API](https://developer.emma.io/es/api/emma-rest-api) y en esta [otra](https://ws.emma.io/swagger/) todo el detalle de las llamadas que se pueden realizar. 

![](/api_key.png)

## Preferencias de iOS

En la sección de preferencias de iOS podemos llevar a cabo la configuración básica para este sistema operativo y la configuración necesaria para que algunas de las funcionalidades de EMMA funcionen. Como por ejemplo el uso de powlink o notificaciones push. <p style="width:850px">![](/preferencias_app_ios_ii.png)</p>

La configuración que podemos hacer en preferencias de iOS es la siguiente: 

-   **AppStore:** URL de la app en la App Store. EMMA usará por defecto esta URL como url de redirección de iOS al crear campaña de [Apptracker](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). También es necesario para la configuración del [powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).
-   **AppStore Bundle ID:** Este campo es necesario para configurar el [powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) en iOS.
-   **AppStore ID:** Es el id de la app en la store. Este ID se puede obtener de la propia URL de la store (https://apps.apple.com/es/app/emma-mobile/**id943061465**). Es necesario por ejemplo para lanzar campañas con [Facebook](https://docs.emma.io/es/guias-de-integracion/facebook) o para lanzar campañas con [TikTok](https://docs.emma.io/es/tiktok). 
-   **Apple Team ID:** Este identificador también es necesario para integrar el [powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) en iOS.
-   **Mínima versión iOS soportada:** Establece la versión mínima del sistema operativo iOS con la que tu app es compatible. Los usuarios que tengan un versión inferior en lugar de ser redireccionados a descargar la app serán llevados a la URL alternativa que se haya configurado al crear la campaña de [Apptracker](https://docs.emma.io/es/adquisicion/Campa%C3%B1as#redirecciones-en-apptracker).
-   **Apple Push Auth Key:** Es el certificado tipo .p8 para poder hacer envío de [notificaciones push](https://developer.emma.io/es/ios/integracion-sdk#push-auth-key) al sistema operativo iOS.
-   **Apple Push Auth Key ID:** Es el identificado asociado al certificado .p8 generado. Es necesario para poder hacer envío de [notificaciones push](https://developer.emma.io/es/ios/integracion-sdk#push-auth-key) al sistema operativo iOS.

## Preferencias de Android

En la sección de preferencias de Android podemos llevar a cabo la configuración básica para este sistema operativo y la configuración necesaria para que algunas de las funcionalidades de EMMA funcionen. Como por ejemplo el uso de powlink o notificaciones push. <p style="width:850px">![](/preferencias_app_android.png)</p>

La configuración que podemos hacer en preferencias de iOS es la siguiente: 

-   **Google Play:** URL de la app en Google Play. EMMA usará por defecto esta URL como url de redirección de Android al crear campaña de [Apptracker](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). También es necesario para la configuración del [powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).
-   **Google Play ID:** Es el id de la app en la store. Este ID se puede obtener de la propia URL de la store (https://play.google.com/store/apps/details?id=**io.emma.emmaandroid**). Es necesario por ejemplo para lanzar campañas con [Facebook](https://docs.emma.io/es/guias-de-integracion/facebook) o para lanzar campañas con[TikTok](https://docs.emma.io/es/tiktok)
-   **Certificate fingerprints:** Este identificador también es necesario para integrar el [powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) en Android.
-   **Mínima versión Android soportada:** Establece la versión mínima del sistema operativo Android con la que tu app es compatible. Los usuarios que tengan un versión inferior en lugar de ser redireccionados a descargar la app serán llevados a la URL alternativa que se haya configurado al crear la campaña de [Apptracker](https://docs.emma.io/es/adquisicion/Campa%C3%B1as#redirecciones-en-apptracker).
-   **Google Push Sender ID:** Sender ID en Firebase Console. Es necesario para realizar el envío de notificaciones push en [Android](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#habilita-fcm-android-para-las-notificaciones-push).
-   **Google Push Server Key:** Server Key en Firebase Console. Es necesario para realizar el envío de notificaciones push en [Android](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#habilita-fcm-android-para-las-notificaciones-push).

## Preferencias de Huawei

En la sección de preferencias de Huawei podemos realizar la configuración de la información de nuestra app para Huawei con HMS en caso de que se necesite.<p style="width:850px;">![](/preferencias_app_huawei.png)</p>

La configuración sería la siguiente: 

-   **Huawei App Gallery:** URL de la app en la App Gallery. EMMA usará por defecto esta URL como url de redirección de Huawei al crear campaña de [Apptracker](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). También es necesario para la configuración del [powlink](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).
-   **HMS App ID:** ID de la app en la consola de Huawei developers necesario para el envío de notificaciones push en dispositivos Huawei con servicios HMS.
-   **HMS App Secret:** App Secret de la app en la consola de Huawei developers necesario para el envío de notificaciones push en dispositivos Huawei con servicios HMS.

## Localización

En la sección de *Localización* puedes configurar la información sobre la zona horario y moneda utilizada por la App. Por defecto la configuración de la zona horaria será *Europe/Madrid* y la divisa Euros.

Ten en cuenta que la configuración de la zona horaria que establezcas aquí se usará en el envío de [notificaciones push](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#programaci%C3%B3https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#programaci%C3%B3n) cuando se seleccione la opción *“Envío planificado Zona horaria del usuario”.*<p style="width:900px;">![](/preferencias_app_localización.png)</p>

## Dominio enlaces cortos

Para poder usar la funcionalidad de EMMA [*Enlaces Cortos*](https://docs.emma.io/es/adquisicion/enlaces-cortos) (Captación > Enlaces cortos) es necesario configurar un dominio. <p style="width:900px;">![](/preferencias_app_dominios_cortos.png)</p>

## Dominio enlaces powlink

Para poder hacer uso de la funcionalidad de Powlink de EMMA es necesario establecer un dominio para el Powlink y, en caso de que se vaya a usar, también para el Powlink corto. En esta [guía](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) puedes ver información más detalla sobre el powlink y su funcionamiento así como información sobre su implementación en la app. <p style="width:900px;">![](/preferencias_app_enlaces_powlink.png)</p>

## Envío de emails

Esta configuración de la app es necesaria para poder realizar el envío de emails a través del formato de EMMA. <p style="width:800px;">![](/preferencias_app_email.png)</p>

-   **Dominio de envío:** En el dominio de envío se debe establecer el dominio desde el que se va a realizar el envío de emails, por ejemplo [***tuempresa.com***](http://tuempresa.com/).
-   **Prefijo de la clave de dominio:** En este campo se debe establecer un nombre o palabra clave (por ejemplo ***EMMA*** o ***tuempresa***).
-   **Clave pública:** Es necesaria la creación de una clave pública. En caso de que ya se tenga una clave pública, existe la posibilidad de cargar un fichero directamente.
-   **Clave privada:** También es necesaria la creación de una clave privada. Esta clave privada puede cargarse en caso de que ya se tenga o se puede generar automáticamente en EMMA.
-   **Nombre del registro DNS:**  Es el nombre del registro DNS que se genera automáticamente tras cubrir los campos *Dominio de envío* y *Prefijo de la clave de dominio.*
-   **Contenido del registro DNS:** Es el contenido del registro DNS de tipo ***TXT*** que también se genera automáticamente tras haber cubierto el campo *Clave pública.*

Revisa nuestra guía específica sobre la [configuración del email](https://docs.emma.io/es/comunicacion/mensajes-out-app/email) para ver más detalles sobre la configuración. 

## Limitar los impactos de las notificaciones Push

Si se tiene pensado impactar a varios segmentos de usuarios o envíar un gran número de notificaciones push es recomendable limitar el capping de frecuencia para no sobreimpactar a los usuarios.

Haz *scroll down* hasta localizar la sección ***Límite de impactos de push.*** Por defecto el capping de las notificaciones push es ilimitado, ya que el switch está habilitado, por lo tanto EMMA interpretará que el usuario será susceptible de recibir notificaciones push ilimitadas.   
Si deshabilitamos el switch podremos seleccionar el número de veces que se quiere impactar a un dispositivo por día, semana y/o mes.<p style="width:600px;">![](/pref_app_capping_push_ii.png)</p>

Cuando vayas a programar la notificación Push, si el límite de impactos no es ilimitado, se indicará el número de dispositivos que pese a pertenecer al segmento o filtro seleccionado, ya han superado el límite de recepción de notificaciones establecido.<p style="width:650px;">![](/pref_app_capping_push_iii.png)</p>

## Límite de impactos In-App

Al igual que para las notificaciones Push, EMMA te permite configurar un límite de impactos para los formatos In-App. Por defecto el límite de impactos de las comunicaciones In-App es ilimitado, por lo tanto EMMA interpretará que el usuario será susceptible de recibir impactos ilimitados de los formatos In-App.<p style="width:600px;">![](/pref_app_impactos_inapp.png)</p>

Si deseamos establecer un límite de impactos para que cada dispositivo no pueda recibir más de X impactos al día, semana o mes; tan solo tenemos que deshabilitar el check y establecer el número máximo de impactos por día, semana y/o mes que el usuario puede recibir.  

Además, también podemos establecer un límite adiciona según categorías de campañas. Por defecto, esta configuración está deshabilitada. Puedes ver más información al respecto en nuestra guía sobre [*Control de saturación*](https://docs.emma.io/es/control-de-saturacion)*.*

## Configuración de campañas de comunicación

En esta sección puedes establecer el periodo de tiempo que debe transcurrir para que tus campañas de comunicación cambien automáticamente de estado. Por defecto este periodo es ilimitado, pero podemos establecer que las campañas cambien de estado automáticamente tras X días sin cambios.<p style="width:600px;">![](https://docs.emma.io/preferencias_app_config_campan%CC%83as_inapp_i.png)</p>

De ser así, esta configuración tendrá efecto sobre las campañas en estado **Testing** y **Pausada.** 

-   Las campañas en estado **Testing** cambiarán automáticamente al estado **Eliminada** una vez transcurrido el tiempo establecido.
-   Las campañas en estado **Pausada** cambiarán automáticamente al estado **Finalizada** una vez transcurrido el tiempo establecido.

## Seguimiento de clics en comunicación

En esta configuración se puede definir el tipo de comportamiento por defecto para la medición de clics en las comunicaciones de tipo Adball y Startview. <p style="width:900px;">![por_defecto.png](/por_defecto.png)</p>

Independientemente de la opción por defecto configurada aquí, se podrá cambiar el modelo para cada comunicación en concreto en el momento de su creación. 
Los tipos de medición disponibles son: 
- Tap
- Cambio URL
- Deeplink
- Clic en URL

Puedes ver más información sobre cada uno de los diferentes modos [aquí](https://docs.emma.io/es/mensajes-inapp#adball). 

## Re-Targeting

Selecciona el tipo de modalidad para tus campañas de Re-Targeting. Por defecto tus campañas de Re-Targeting usarán la modalidad *Último clic*, pero puedes cambiar la modalidad a *Clic único* habilitando el check. Puedes ver más información sobre los distintos modelos de atribución de campañas de Re-Targeting [aquí](https://docs.emma.io/es/adquisicion/apptracker#campa%C3%B1as-re-targeting). <p style="width:700px;">![](/pref_app_rt.png)</p>

## Seguridad

Si quieres establecer el sistema de aprobación de campañas de comunicación activa está opción. Por defecto EMMA no usa el sistema de aprobación de campañas de comunicación y esta opción está desactivada. Puedes ver más información sobre esta funcionalidad [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as). <p style="width:700px;">![](/pref_app_seguridad.png)</p>

## Informes

Establece el formato de todos los informes y exportaciones que realices desde EMMA. Podemos seleccionar el formato entre: 

-   ZIP
-   CSV

Por defecto, el formato de descarga de EMMA es ZIP.

![](/preferencias_app_informes.png)

# Equipo y apps de la cuenta

En EMMA puedes dar acceso a tu cuenta a tantos usuarios diferente como necesites controlando en todo momento el tipo de visibilidad que tiene cada uno de ellos a los datos y las funciones que cada uno de ellos tiene habilitada. 

## Equipo de la cuenta

### Crear nuevos usuarios

Para ello solo tienes que seguir estos pasos: 

-   Haz [login](https://ng.emma.io/) en tu cuenta de EMMA y ve a la parte superior derecha donde aparece tu nombre de usuario.</br><p style="width:500px;">![usuario.png](/usuario.png)</p>

Selecciona tu usuario para desplegar el menú

-   En el menú desplegable selecciona la opción ***Equipo de la cuenta*** y a continuación haz clic en el botón ***Nuevo miembro del equipo.*** </br><div style="width:400px;">![equipo_cuenta.png](/equipo_cuenta.png)</div></br><p style="width:600px;">![](https://docs.emma.io/crear_usuario_3a.png)</p>

-   Cubre la siguiente información del formulario de alta de usuarios:</br><p style="width:900px;">![](https://docs.emma.io/crear_usuario_4.png)</p>

-   **Nombre:** Nombre del nuevo usuario
-   **Apellido:** Apellido del nuevo usuario
-   **Email:** Email del usuario con el que va a tener acceso a EMMA.
-   **Rol:** Seleccionar un rol entre los disponibles de EMMA:
    -   **Administrador:** Tiene acceso ilimitado a EMMA a datos y funciones. Es el único usuario que puede realizad modificaciones de configuración de la app en la sección *Preferencias app.* 
    -   **Agencia:** Puede acceder y operar (crear, editar, eliminar) en cualquier sección de EMMA. Puede crear y editar las aplicaciones y usuarios de una cuenta pero no podrá eliminarlos.
    -   **Analista:** Este rol solo aparecerá disponible si se tienen habilitada la funcionalidad de *Pre-validación de campañas.* Para ver información más detalla sobre qué puede o no hacer este usuario, consulta nuestra guía sobre [Pre-validación de campañas](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as).
    -   **Autor:** Este rol solo aparecerá disponible si se tienen habilitada la funcionalidad de *Pre-validación de campañas.* Para ver información más detalla sobre qué puede o no hacer este usuario, consulta nuestra guía sobre [Pre-validación de campañas](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as).
    -   **Cliente:** Puede acceder y operar (crear, editar, eliminar) en cualquier sección de EMMA. Puede crear y editar las aplicaciones y usuarios de una cuenta pero no podrá eliminarlos.
    -   **Gestor:** Este rol solo aparecerá disponible si se tienen habilitada la funcionalidad de *Pre-validación de campañas.* Para ver información más detalla sobre qué puede o no hacer este usuario, consulta nuestra guía sobre [Pre-validación de campañas](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as).
    -   **Proveedor:** Tan solo tiene acceso de lectura a la sección de *Captación > Apptracker* de EMMA única y exclusivamente a las campañas y fuentes a las que se le de acceso posteriormente. Continúa leyendo esta guía para ver como dar acceso a unas fuentes específicas.
-   **Departamento:** Selecciona al departamento que pertenece ese usuario dentro de tu empresa de entre las opciones disponibles:
    -   Dirección
    -   Finanzas
    -   Marketing
    -   Otro
    -   Producto
    -   Soporte
    -   Tecnología
    -   Ventas
-   **Aplicaciones:** Selecciona las apps dentro de tu cuenta a las que tiene que tener acceso. Si solo tienes app dentro de tu cuenta, se dará acceso al usuario a esa única app. En caso de tener varias apps, por defecto se dará acceso a todas a no ser que selecciones lo contrario.
-   Por último, haz clic en el botón ***Guardar Usuario*** para crearlo. En ese momento, el usuario recibirá un correo con los pasos a seguir para validar su usuario.<p style="width:900px;">![](https://docs.emma.io/crear_usuario_7.png)</p>

### Configurar acceso de proveedor a fuentes

Una vez que hemos creado un nuevo usuario con rol *Proveedor* tan solo nos queda darle acceso a las fuentes que queremos que visualice. 

Para ello, tan solo tenemos que seguir estos pasos: 

-   Realizar los pasos previos en esta guía para crear un nuevo usuario con rol *Proveedor.* 
-   Ir a la sección ***Captación > Fuentes de medios.*** 

![](https://docs.emma.io/crear_usuario_10.png)

-   Buscar y editar la fuente de medios en cuestión a la que queremos dar acceso al proveedor.
-   En el formulario de edición, nos deslizamos hasta la sección que pone ***Permisos.*** Activamos los permisos y añadimos el email del usuario que hemos creado previamente. Podemos añadir tantos usuarios como sean necesarios haciendo clic en el botón *\+ Añadir acceso.* 

![](https://docs.emma.io/crear_usuario_11.png)

-   Guardar los cambios y listo, el proveedor solo visualizará aquellas fuentes que en *Apptracker* tengan configurado como proveedor la fuente a la que se le ha dado desde esta sección.

### Editar o eliminar usuarios existentes

Para editar o eliminar un usuario existente, tan solo tenemos que hacer clic en el menú lateral a la derecha del nombre del usuario y seleccionar la opción adecuada, *Editar o Eliminar.* 

![](https://docs.emma.io/crear_usuario_8.png)

Si seleccionamos la opción *Editar*, se abrirá la pantalla de edición del usuario y tan solo tenemos que hacer las modificaciones pertinentes y guardar los cambios realizados. 

Si seleccionamos la opción *Eliminar*, se abrirá un alerta para confirmar si realmente queremos eliminar ese usuario o no. 

![](https://docs.emma.io/crear_usuario_9.png)

## Apps de la cuenta

La primera app de la cuenta se crea automáticamente en el momento en el que te registras en EMMA y [creas tu cuenta](https://docs.emma.io/es/home#empezando). Si necesitas crear más apps dentro de tu cuenta, tan solo tienes que seguir estos pasos: 

-   Dirígete al menú de usuario en la parte superior derecha de la pantalla y selecciona **Apps de la cuenta.** 
![apps_cuenta.png](/apps_cuenta.png)

-   Haz click en el botón **Nueva App**

![](/apps_cuenta_3.png)

-   Rellena el formulario de creación de app:
    -   **Nombre**: Introduce el nombre de tu nueva aplicación.
    -   **Web:** Introduce el sitio web de tu app.
    -   **AppStore**: Introduce la URL de tu app iOS en el App Store.
    -   **Google Play:** Introduce la URL de tu app Android en Google Play.
    -   **Huawei App Gallery:** Introduce la URL de tu app Huawei en App Gallery.
    -   **Zona horaria:** Configura la zona horaria usada por tu app. Por defecto es Europe/Madrid (UTC +01:00/+02:00)
    -   **Divisa:** Configura la divisa empleada por tu app. Por defecto es EUR.
-   Una vez configurados todos los campos haz clic en el botón **Guardar App** para que se cree.
-   Una vez que hayas guardado tu app, se generará un *EMMA Key* propio para la aplicación. Este *EMMA Key* se utiliza para conectar el SDK de  EMMA integrado en tu aplicación con la cuenta creada en EMMA.

![](/apps_cuenta_5.png)

-   Envía el SDK de EMMA junto a las [guías de integración](https://developer.emma.io/es/home) y el *EMMA Key* a tu equipo de desarrollo.
-   Consulta con nuestro equipo de Soporte para revisar que todas las funcionalidades del SDK de EMMA estén bien implementados antes de actualizar tu aplicación en la Store.
-   Actualiza una nueva versión de tu aplicación con el SDK de EMMA integrado en la Store.

# Dispositivos de test

Para poder llevar a cabo múltiples pruebas de atribución sin necesidad de tener que emplear dispositivos que nunca hayan tenido la app y por ende no estén en nuestra base de datos, ha nacido la funcionalidad de ***Dispositivos de test.*** 

Gracias a esta nueva funcionalidad, podremos añadir hasta **40** **dispositivos** de test por **cuenta** para poder realizar tantas pruebas de atribución con el mismo dispositivo como sea necesario. 

A continuación veremos con más detalle cómo dar de alta nuevos dispositivos de test, el funcionamiento y algunas consideraciones a tener en cuenta para su uso. 

## Cómo añadir dispositivos de test

Añadir nuevos dispositivos de test es muy sencillo. Para ello, tan solo tienes que seguir estos pasos: 

-   Inicia sesión en [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex).
-   Ve a la sección **Comportamiento > Explorador** y busca tu dispositivo en base de datos. Haz clic en el botón **Exportación Raw**, copia el identificador que aparece en la columna ***Device ID*** y guárdalo, ya que lo necesitaremos más adelante.

![](https://docs.emma.io/dispositivos_test_iii.png)

-   Haz *mouse over* sobre tu nombre de usuario, a la derecha de la pantalla, y en el menú desplegable selecciona la opción **Dispositivos de test.**<p style="widht:400px">![dispositivos_test.png](/dispositivos_test.png)</p>

-   Haz clic en el botón **\+ Nuevo dispositivo de test.**

![](https://docs.emma.io/dispositivos_test_ii.png)

-   Cubre los campos del formulario de alta:
    -   **Nombre:** Establece un nombre identificativo para ese dispositivo de test.
    -   **Sistema operativo:** Selecciona el sistema operativo correspondiente entre las opciones disponibles (**iOS, Android, Otro).**
    -   **Identificador:** Pega el identificador obtenido en la sección Explorador anteriormente.
-   Guarda el dispositivo.

Repite este proceso tantas veces como dispositivos de test necesites añadir hasta un máximo de **40** **dispositivos** por **cuenta**. 

## Editar y eliminar dispositivos de test

Una vez que hemos dado de alta nuestros dispositivos de test, eliminaros o editarlos de ser necesario es muy sencillo. Tan solo tenemos que hacer clic en el menú contextual que se muestra al lado del nombre del dispositivo de test y seleccionar la opción que deseemos. 

![](https://docs.emma.io/dispositivos_test_v.png)

-   **Editar:** Se abrirá el menú de edición del dispositivo y podremos modificar cualquier campo: **nombre, sistema operativo e identificador.** 
-   **Eliminar:** Si hacemos clic en eliminar, el dispositivo de test será eliminado de inmediato (sin confirmación). Eso significa que ya no podrá ser empleado como un dispositivo “nuevo” para medir la atribución.

## Cómo funcionan los dispositivos de test

Cuando damos de alta dispositivos de test, lo que estamos haciendo es decirle a EMMA que trate esos dispositivos como si fuesen nuevas instalaciones en nuestra base de datos pero manteniendo el identificador. 

De esta manera, podremos realizar tantas pruebas de atribución con powlink de EMMA como sean necesarias, ya que con cada nuevo clic y posterior instalación de la app se actualizará tanto la fecha de instalación, como la campaña y fuente, si así corresponde, a la que está atribuido el usuario. 

## Cómo hacer pruebas de atribución y ver los datos 

Una vez que tenemos nuestro dispositivo dado de alta como dispositivo de test, el procedimiento para hacer pruebas de atribución y revisar los datos es el siguiente: 

-   Desinstalar la aplicación de nuestro dispositivo.
-   Hacer clic en un powlink de EMMA previamente creado. Si no sabes cómo crear un powlink, visita nuestro artículo sobre el [Apptracker](https://docs.emma.io/es/adquisicion/apptracker) para ver como hacerlo.
-   Instalar la app y la abrirla por primera vez.
-   A continuación, vamos a la sección **Comportamiento > Explorador** y buscamos nuestro dispositivo en base de datos.
-   Una vez localizado nuestro dispositivo hacemos una **Exportación Raw** y seleccionaremos las columnas ***Install\_at***, ***From\_campaign*** y ***From\_source*** para revisar la nueva atribución del dispositivo.  

![](https://docs.emma.io/dispositivos_test_vi.png)

## Qué información del dispositivo se actualizará

La única información recogida del dispositivo que se verá afectada cuando tengamos un dispositivo de test será: 

-   Fecha y hora de instalación.
-   Campaña a la que el usuario está atribuido.
-   Fuente a la que el usuario está atribuido.

Toda la información relativa a **eventos** no sufrirá cambios y tendremos el histórico del usuario. Así mismo, las **etiquetas de usuario** funcionarán como siempre y también se mantendrá está información. 

## Consideraciones extra para iOS 14

Con iOS 14 el funcionamiento de dispositivos de test es un poco especial debido a las limitaciones que está sufriendo esta versión del OS con respecto al uso del IDFA. 

Si el dispositivo en cuestión no ha permitido la recopilación del IDFA, lo que se va a recuperar es un identificador vendor del dispositivo. Este identificador, a diferencia del IDFA, no es único por dispositivo, sino que es único por grupo de apps del desarrollador. 

Es decir, si en nuestra cuenta de App Store o Google Play, tenemos bajo el mismo desarrollador 3 aplicaciones, siempre que tengamos al menos una de esas 3 apps instaladas en el dispositivo, el vendor se podrá compartir entre las 3 apps y será el mismo.

Sin embargo, si solo tenemos una app, cada vez que instalemos la app, se generará un nuevo vendor diferente asociado a nuestro dispositivo. 

Teniendo estos dos casos en cuenta: 

-   Cuando tenemos varias apps del mismo desarrollador, podemos buscar nuestro vendor en base de datos y añadirlo como un dispositivo de test, ya que el funcionamiento será como en Android o iOS inferiores a iOS 14.   
    **IMPORTANTE:** Eso sí, ten en cuenta que si desinstalas todas las apps del desarrollador del dispositivo, cuando vuelvas a instalar tu vendor será completamente diferente.
-   Cuando el desarrollador solo tiene una app o solo tenemos instalada una de sus apps, no es necesario añadir el dispositivo como un dispositivo de test, ya que con cada nueva instalación, se generará un nuevo vendor distinto.

# Facturación

## Información de facturación

En EMMA trabajamos con dos tipos de licencias diferenciadas entre sí, mensual y anual que se diferencian básicamente en el modo de pago tal y como su nombre indica (mes a mes o anualmente). A continuación podrás conocer las características de cada una de las licencias vigentes.

### Licencias mensuales

-   Todas las licencias se facturarán al **inicio del plan** y corresponden a un **mes** **completo**.
-   La emisión de la **factura** se realizará a los datos fiscales informados en su cuenta de EMMA, en la fecha de inicio del plan y se remitirá por correo electrónico a la/s direcciones de email informadas a tal efecto. Esta factura será recurrente durante todo el periodo de duración del acuerdo. Puedes revisar los cuáles serán los datos empleados para la facturación en tu perfil en la sección [Preferencias de la cuenta.](https://docs.emma.io/es/configuracion#preferencias-de-la-cuenta)
-   Todos aquellos **conceptos** **extras** o no incluidos en su licencia o plan mensual, se facturarán al **cierre** del **mes** en curso en el mismo modo y forma que su licencia vigente.

### **Licencias de pago único anual**

-   Las licencias de pago único anual se facturarán igualmente al **inicio** **del** **plan** y corresponden a un periodo de **12 meses**.
-   La emisión de la **factura** se realizará a los datos fiscales informados en su cuenta de EMMA, en la fecha de inicio del plan y se remitirá por correo electrónico a la/s direcciones de email informadas a tal efecto. Puedes revisar los cuáles serán los datos empleados para la facturación en tu perfil en la sección [Preferencias de la cuenta.](https://docs.emma.io/es/configuracion#preferencias-de-la-cuenta)
-   Todos aquellos **conceptos** **extras** o no incluidos en su licencia o plan anual, se facturarán al **cierre** de cada **mes**, con la emisión de una factura que será enviada por correo electrónico y deberá ser regularizada mediante **transferencia** **bancaria** **net30**.

### **Métodos de pago**

En EMMA ofrecemos dos métodos de pago en función del importe concreto de la licencia. 

-   El método de pago para **licencias** de **importe** **<1.000€** (impuestos no incl) será **tarjeta** de crédito **net0.**
-   El método de pago para **licencias** de **importe** **\>1.000€** (impuestos no incl) será **transferencia** **bancaria** al número de cuenta designado en la factura de venta en modalidad **Net30.**

Toda nuestra facturación se realiza de forma digital, por lo que EMMA no envía facturas en papel ni otros formatos.

## Cancelación de la cuenta

Para cancelar tu cuenta debes escribe un email a **support@emma.io** y a **finance@emma.io** con una antelación mínima de 10 días naturales antes de la fecha de la factura del siguiente periodo para que procedamos a dar de baja tu cuenta.
Puedes consultar información más detallada en nuestra página de [Términos de uso](https://emma.io/terminos-de-uso/). 
