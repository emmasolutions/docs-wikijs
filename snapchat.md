---
title: Snapchat
description: Snapchat
published: true
date: 2025-03-10T14:05:55.978Z
tags: 
editor: markdown
dateCreated: 2021-05-31T12:01:28.440Z
---

# Introducción

Snapchat App Install es el formato publicitario que ha desarrollado la propia plataforma de Snapchat App Install, orientado a conseguir nuevos usuarios para las apps móviles. Al igual que con otras fuentes publicitarias, no tendrás ningún problema para medir tus campañas de Snapchat con EMMA. Para ello, tan solo tienes que seguir los pasos listados a continuación. 

> Antes de lanzar campañas con Snapchat, ponte en contacto con tu account asignado o a través de [support](mailto:support@emma.io) para que podamos darte de alta en el sistema para que puedas ver los datos de las campañas de Snapchat sin ningún problema.   
Asimismo, también es importante que sigas los pasos de esta guía antes de lanzar cualquier campaña. {.is-warning}

# Configuración en EMMA

Para empezar a medir campañas de Snapchat App Installs en EMMA, lo primero de todo es realizar una pequeña configuración en el dashboard de EMMA. 

-   Haz login en tu cuenta de EMMA y ve a ***Preferencias App.*** 

![](/pref_app.png)

-   Edita la configuración de la app.
-   En la configuración de iOS, configura la URL de la app en App Store y el AppStore ID.

![](/snapchat_2.png)

Puedes obtener el AppStore ID de la URL de la store. 

![](/snapchat_5.png)

-   En la configuración de Android, configura la URL de la app en Google Play y el Google Play ID.

![](/snapchat_3.png)

-   Puedes obtener el Google Play ID de la URL de la store.

![](/snapchat_4.png)

# Activar la fuente de medios Snapchat

Una vez realizado el paso anterior, solo queda activar la fuente de medios Snapchat. 

-   Ve a **Captación > Fuentes de medios**.

![](/snapchat_6.png)

-   Busca la fuente de medios **Snapchat**  y dale a ***Editar*** desde el menú lateral izquierdo.

![](/snapchat_8.png)

-   Activa la fuente de medios, haciendo clic en el botón ***Activar*** del mensaje de aviso.

![](/snapchat_9.png)

-   El resto de campos de la configuración no son obligatorios a no ser que se use algún intermediario para lanzar las campañas de Snapchat.

Y ¡listo! con esto tenemos la integración con Snapchat realizada. Una vez se empiecen a medir instalaciones en Snapchat, aparecerá una nueva campaña en el Apptracker de Snapchat con todos los datos de la misma.

# Preguntas fercuentes
**¿Qué detalle de desglose de campaña ofrece EMMA?**
En el apptracker de EMMA, se va a poder ver el desglose de información por campaña y grupo de anuncios configurado en Snapchat. De esta manera, veremos la siguiente información: 
- Nombre de campaña en Snapchat = Nombre de campaña en EMMA
- Nombre de grupo de anuncios en Snapchat = Nombre de fuente en EMMA

**¿Por qué veo en EMMA una campaña que se llama *Snapchat Restricted*?**
Hay ciertas circunstancias en las que Snapchat no comparte la información del nombre de la campaña, como por ejemplo, cuando reporta las instalaciones post impresión.

La campaña de EMMA **Snapchat Restricted** aglutina todas las instalaciones que han sido notificadas por Snapchat, pero de las cuales no sabemos la campaña exacta porque no se ha compartido esa información con EMMA. 

**¿Si cambio el nombre de la campaña en el panel de Snapchat, se actualiza en EMMA?**
Sí, cualquier cambio realizado sobre la nomenclatura de la campaña se verá reflejado en EMMA. De esta manera, se podrá ver una nomenclatura unificada entre ambas plataformas.