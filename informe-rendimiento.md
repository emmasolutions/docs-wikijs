---
title: Informe de rendimiento
description: Informe de rendimiento
published: true
date: 2023-09-26T12:25:14.112Z
tags: 
editor: markdown
dateCreated: 2022-04-28T17:59:50.131Z
---

En el informe de redimiento de EMMA podrás consultar información de tus campañas de comunicación, ya sean In-App o Out-App, pudiendo aplicar los [parámetros](https://docs.emma.io/es/parametros-comunicacion) y otros valores para agrupar o filtrar las campañas para poder obtener la información. 

# Informe de rendimiento

Para acceder al informe de rendimiento, tienes que hacer [login](https://ng.emma.io/es/login?returnUrl=%2Findex) en tu cuenta de EMMA y a continuación ir a la sección ***Comunicación > Informe de rendimiento.*** <p style="width:700px;">![](/informe_rendimiento_1_1.png)</p>

Una vez en esta sección, lo primero que se puede hacer es determinar en base a qué KPI se quiere agrupar la información y los filtros que se quieren aplicar para filtrar los datos entre los siguientes: <p style="width:300px">![](/informe_rendimiento_3.png)</p><p style="width:300px;margin-top:-407px; margin-left:350px">![](/informe_rendimiento_4.png)</p>

-   **Campaña:** Muestra el listado de todas las campañas que hayan tenido datos en el rango de fechas seleccionado. Si filtramos por este KPI nos permite seleccionar una campaña en concreto.
-   **Formato:** Agrupa y/o filtra las campañas y los datos en función de su formato:
    -   Adball
    -   Banner
    -   Cupón
    -   Tab dinámico
    -   Email
    -   Native Ad
    -   Prisma
    -   Push
    -   Startview
    -   Strip
-   **Sistema operativo:** Agrupa y/o filtra las campañas y los datos en función del sistema operativo:
    -   Android
    -   iOS
-   **Versión de la aplicación:** Agrupa y/o filtra las campañas por la versión de la aplicación desde la que se han visualizado.
-   **Tipo de mensaje:** Agrupa y/o filtra las campañas por el tipo de mensaje
    -   In-App
    -   Out-App
-   **Tipo de campaña:** Agrupa y/o filtra las campañas en función del tipo de campaña:
    -   Manual
    -   Automática para reglas
    -   Automática para rutas de cliente
-   **Categoría:** Agrupa y/o filtra las campañas en función de la [categoría](https://docs.emma.io/es/categorias) que tenía vinculada cada campaña.
-   **Test A/B:** Filtra las comunicaciones en función de si tienen configurado test a/b o no.
-   **Parámetros de comunicación:** Agrupa y/o filtra las campañas en función de los [parámetros de comunicación](https://docs.emma.io/es/parametros-comunicacion) que se hayan vinculado a cada campaña.

La primera gráfica que visualizamos al acceder a este informe es una gráfica que nos permite ver la evolución de las campañas en el rango de fechas seleccionado en función del KPI seleccionado. 

-   Visualizaciones
-   Clics

Esta gráfica muestra por defecto el volumen de visualizaciones de las comunicaciones, pero haciendo clic en el selector de la parte superior derecha de la gráfica podemos cambiar el KPI a mostrar en la gráfica. Además los datos se pueden visualizar agrupados por día, semana o mes. Puedes ver más info al respecto de la agrupación temporal [aquí](https://docs.emma.io/es/barras-de-menu#men%C3%BA-supra-principal). Ten en cuenta también que en función de la agrupación que hayamos seleccionado veremos la información de una u otra manera.

![](/informe_rendimiento_2.png)

En esta gráfica se mostrarán por defecto las 10 campañas con mayor volumen en el rango de fechas seleccionado. El resto de campañas aparecerán agrupadas bajo *“Otras”*.

En la segunda gráfica podemos ver el número total de visualizaciones de cada campaña en el rango de fechas seleccionado. Esta tabla mostrará la información en función de la agrupación seleccionada previamente. 

![](/informe_rendimiento_5.png)

> Ambas gráficas muestran información de visualizaciones o clics totales conseguidos por las comunicaciones.
{.is-info}

Ya por último podemos ver la tabla resumen de campañas. En esta tabla, al igual que en el resto de la pantalla, se mostrará la información de una manera u otra en función de la agrupación y/o filtros seleccionados. 

![](/informe_rendimiento_6.png)

Al igual que en el resto de tablas de EMMA, podemos seleccionar las columnas que queremos visualizar y podemos realizar una búsqueda de un valor específico a través del buscador situado en la parte superior derecha de la tabla. 

En esta tabla veremos la siguiente información: 

-   **Campaña:** Este KPI varia en función de la agrupación seleccionada mostrando la información correspondiente en base a la agrupación seleccionada.
-   **Nº de campañas:** Cuando tenemos una agrupación diferente a *“Campaña”*, por ejemplo *“Formato”* nos dice el número de camapñas de cada formato.
-   **Visualizaciones:** Visualizaciones totales realizadas en cada comunicación. En el caso de las comunicaciones Out-App, los envíos se contabilizan como impresiones. 
-	**Visualizaciones únicas:** Ids de clientes únicos que han visualizado cada comunicación. 
-   **Visualizaciones sobre el total:** Muestra el % de visualizaciones sobre el total de visaulizaciones. El calculo sería visualizaciones de la comunicación en cuestión entre el total de visualizaciones por 100.
-   **Clics:** Clics totales realizados en cada comunicación. En el caso de las comunicaciones Out-App, las aperturas se contabilizan como clics.
-   **Clics sobre el total:** Muestra el % de clics sobre el total de visaulizaciones. El calculo sería visualizaciones de la comunicación en cuestión entre el total de visualizaciones por 100.
-   **CTR:** Porcentaje de dispositivos que han hecho clic en la comunicación entre las impresiones totales de dicha comunicación.

# Crear audiencias desde el informe de rendimiento

Desde el informe de rendimiento se pueden crear audiencias en base a los dispositivos que han sido impactados por la(s) comunicación(es) (visualizaciones) o en base a los dispositivos que han interactuado con la(s) comunicación(es) (clics). 

Para crear una audiencia, hay que seguir estos pasos: 

-   Ir a la tabla resumen de las comunicaciones.
-   Hacer *mouse over* sobre el menú contextual en concreto.
-   Hacer clic en la opción ***Crear audiencia.***<p style="width:800px">![](/informe_rendimiento_7.png)</p>

-   En el formulario de creación, en el selector ***A partir de*** selecciona la opción deseada: 
    -   ***Impactados:*** Crea la audiencia en base a los dispositivos impactados por la(s) comunicación(es) en el rango de fechas seleccionado. Esta audiencia es estática y sélo tiene en cuenta los usuarios que cumplen la condición en el momento de la creación.
    -   ***Conversiones:*** Crea la audiencia en base a los dispositivos que han realizado clic en la(s) comunicación(es) en el rango de fechas seleccionado. Esta audiencia es estática y sólo tiene en cuenta los usuarios que cumplen la condición en el momento de la creación.<p style="width:450px">![](/informe_rendimiento_8.png)</p>
-   Establece un nombre identificativo a la audiencia y haz clic en el botón **Crear audiencia**.

Y ¡listo! tu audiencia a partir de los datos de las comunicaciones ha sido creada. Puedes consultar la audiencia desde la sección [**Comportamiento > Audiencias**](https://docs.emma.io/es/audiencias) y usarla para crear nuevas comunicaciones tanto [In-App](https://docs.emma.io/es/mensajes-inapp) como [Out-App](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications). 

# Histogramas
Con el informe de histogramas de EMMA  podremos ver la distribución de usuarios que han hecho clic o han visualizado un determinado número de campañas en un periodo específico. 

En la barra de filtrado podremos aplicar los diferentes filtros para obtener la información de uno u otro mes y compararlo o no con algún otro mes determinado. 
![histograma_6.png](/histograma_6.png)

> Los histogramas muestran información a día cerrado. No se muestra información del día en curso. </br> Además, no podremos seleccionar un rango de fechas custom, solo se podrán ver datos mes a mes. 
{.is-info}

Podremos ver información de dos histograma diferentes. Histograma de clics e histograma de visualizaciones. 

## Histograma de clics
En el histograma de clics podremos ver la distribución de usuarios (%) que han hecho clic en un determinado número de campañas en un periodo específico (meses). 

Por defecto, se muestra la información del mes en curso, pero podemos seleccionar otro mes a través de la barra de filtrado, así como seleccionar un mes con el que comparar los datos o no. En caso de haber habilitado parámetros de comunicación para que sean usados en los histogramas, también se podrán usar para filtrar la información que muestra el histograma. Puedes ver más info sobre los parámetros de comunicación aquí. </br>
![histograma_1.png](/histograma_1.png)

> El % son usuarios (id de cliente) distintos que han hecho clic en x número de campañas diferentes.
{.is-info}
## Histograma de visualizaciones
En el histograma de visualizaciones podremos ver la distribución de usuarios (%) que han visualizado un determinado número de campañas en un periodo específico (meses). 

Por defecto, se muestra la información del mes en curso, pero podemos seleccionar otro mes a través de la barra de filtrado, así como seleccionar un mes con el que comparar los datos o no. 
histogramas, también se podrán usar para filtrar la información que muestra el histograma. Puedes ver más info sobre los parámetros de comunicación aquí.</br>
![histograma_2.png](/histograma_2.png)

> El % son usuarios (id de cliente) distintos que han visualizado x número de campañas diferentes.
{.is-info}