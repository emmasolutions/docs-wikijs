---
title: SKAN Perspectiva
description: SKAN Perspectiva
published: true
date: 2025-03-03T11:16:22.793Z
tags: 
editor: markdown
dateCreated: 2023-09-22T13:02:51.518Z
---

# Introducción

La pantalla de SKAN perspectiva, es una pantalla pura de análisis que nos va a permitir llevar a cabo una evaluación de los datos de instalaciones, reinstalaciones y eventos provenientes de las redes de SKAD Network. 

SKAD Network es una solución de atribución de aplicaciones móviles desarrollada por Apple basada en la privacidad del usuario.  Es decir, SKAD Network da prioridad a la privacidad del usuario y no comparte información privada, como puede ser por ejempo el IDFA y basa su atribución en la realización de eventos, por lo que a través de las campañas de SKAD Network no podemos identificar qué usuario se ha instalado la app, sino que simplemente podemos saber que se ha realizado una instalación a través de una campaña determinada, pero nunca podremos establecer una vinculación entre instalación y usuario que ha realizado dicha instalación. 

Es decir, SKAD Network nos da información agregada de las instalaciones y los eventos atribuidos a las campañas orientadas a dispositivos iOS con versión del sistema operativo superior a 14.5 y que no hayan aceptado el seguimiento publicitario.

Para que EMMA pueda mostrar datos en esta pantalla, es necesario tener habilitado el seguimiento de campañas de SKAD Network con EMMA. Actualmente, se puede obtener información de SKAD Network de las siguientes redes: 

-   [Google](https://docs.emma.io/es/google#campa%C3%B1as-de-skadnetwork)
-   [Meta](https://docs.emma.io/es/guias-de-integracion/facebook#campa%C3%B1as-de-skadnetwork)
-   [TikTok](https://docs.emma.io/es/tiktok#campa%C3%B1as-de-skadnetwork)

# Filtros

Lo primero que debemos saber es que, como todas las pantallas de reporting de EMMA, nos muestra datos del rango de fechas seleccionado. Por defecto, este rango de fechas son los últimos 7 días, pero podremos seleccionar el rango de fechas deseado en cada caso. Puedes ver más información sobre el calendario y los rangos de fechas desde [aquí](https://docs.emma.io/es/barras-de-menu#men%C3%BA-supra-principal). 

Además, desde la barra de filtrado también podremos aplicar filtros a nivel de campaña para seleccionar las que sean de interés en cada momento y a nivel de fuentes de medios. 

![](/skan_perspectiva_2.png)

> Ten en cuenta que los filtros son independientes y no se condicionan uno al otro. Es decir, si selecciono el filtro campaña SKAdNetwork TikTok y después añado un filtro de fuentes de medios, aparecerán todas las fuentes de medios para ser seleccionadas, no solo las de la campaña de TikTok.{.is-info}

# SKAN perspectiva

En el módulo principal de la pantalla, podemos ver por un lado una tabla y a su lado una gráfica que nos va a permitir poder ver la información de un modo más visual y directo. 

![](/skan_perspectiva_1.png)

En ambos casos, la información que podemos ver es: 

-   **Instalaciones:** Volumen de instalaciones totales generadas en el rango de fechas seleccionado. Incluye nuevas instalaciones y reinstalaciones.
-   **Reinstalaciones:** Volumen de reinstalaciones totales generadas en el rango de fechas seleccionado. Ten en cuenta que no todas las fuentes dan información de las reinstalaciones. Actualmente,las redes que dan información de las reinstalaciones son:
    -   Google
    -   Meta
    -   TikTok
-   **Eventos In-App:** Volumen de eventos in-app realizados, a partir  de campañas de SKAD.
-   **Ratio de reinstalaciones:** Porcentaje de dispositivos que han reinstalado respecto a las instalaciones totales.
-   **Ratio de evento:** Porcentaje de eventos realizados respecto a las instalaciones totales.

> En esta pantalla, se muestra el desglose de las instalaciones de SKAD Network, pero también se tienen en cuenta estas instalaciones en el volumen total de instalaciones y en las instalaciones no-orgánicas mostradas en la pantalla de Resumen Ejecutivo.</br>
Sin embargo, no podemos consultar el volumen de instalaciones de SKAD desde* Explorador o Gente, ya que al no ser instalaciones vinculadas a un usuario, no se puede mostrar esa vinculación desde estas secciones. {.is-info}