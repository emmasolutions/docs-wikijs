---
title: Apple Search Ads
description: Apple Search Ads
published: true
date: 2023-08-30T13:00:31.163Z
tags: 
editor: markdown
dateCreated: 2021-05-31T12:00:34.624Z
---

Con Apple Search Ads puedes promocionar tu aplicación móvil de iOS para que aparezca en los primeros resultados de búsqueda de AppStore.

EMMA, partner integrado de Apple, te permite medir y atribuir todas las conversiones que provengan de este formato.

Para ello, sigue las instrucciones que se detallan a lo largo de esta guía. 

### Importante

Para poder medir tus campañas de Apple Search Ads en EMMA debes tener la versión del EMMA iOS SDK 4.2.13 o superior. Para ver cómo integrar el SDK de EMMA haz click [aquí](https://developer.emma.io/es/ios/integracion-sdk).

![apple_search_ads.png](/apple_search_ads.png)

https://youtu.be/Ss64jv55AOk

# Obtener tu id de grupo de campaña y generar claves

Para poder llevar a cabo al conexión entre Apple Search Ads y EMMA es necesario obtener el ***ID de grupo de campaña.***

Para poder relizar esta conexión, es necesario que el usuario que la lleve a cabo tenga un acceso de ***API Acco***

-   Haz login en tu cuenta de Apple Search Ads.
-   Obtén tu **ID de grupo de campaña (organization ID)**. Para ello tienes que ir a la parte superior izquierda, donde aparece el nombre del grupo de campaña seleccionado y copiar el ID del grupo de campaña correspondiente.   
     

![](/asa_1.png)

-   Haz login en EMMA y ve a la sección **Captación > Fuentes de medios.**  
     

![](/asa_11.png)

-   Busca la fuente Apple Search Ads y desde el menú contextual haz clic en la opción ***Editar.***   
     

![](/asa_8.png)

-   Haz scroll hasta la sección ***Impresiones, clics y coste***, introduce el el ID de grupo de campaña que has obtenido anteriormente en el campo ***Organization ID*** y haz clic en el botón ***Generar claves.***   
     

![](/asa_10.png)

-   Copia la clave pública tal y como se muestra en la captura.  
     

![](/asa_12.png)

-   Un usuario con acceso de ***API Account Manager*** debe hacer login en el dashboard de Apple Search Ads (si no hay ningún usuario con este rol se puede hacer un cambio sobre uno existente o crear un nuevo usuario específico con este acceso) e ir a la sección ***Settings*** desde el menú desplegable en la parte superior derecha de la pantalla (donde se muestra el nombre de usuario).   
     

![](/asa_2.png)

-   Pega la clave pública copiada de EMMA en el campo ***Client Credentials > Public Key*** y haz clic en el botón ***Generate API Client.***   
     

![](/asa_13.png)

-   Automáticamente se generará un ***clientId, teamId y KeyId.*** Copia los 3 identificadores y dirígete nuevamente al dashboard de EMMA.   
     

![](/asa_14.png)

# Configura Apple Search Ads en EMMA

Lo último que queda, para empezar a medir tus campañas de Apple Search Ads en EMMA, es realizar la conexión de la consola con el apptracker de EMMA siguiendo estos sencillos pasos.

-   Haz login en EMMA y ve a la sección **Captación > Fuentes de medios** y edita la fuente de medios de Apple Search Ads.
-   Cubre los campos de **Client ID, Key ID** y **Team ID** con la información obtenida de la consola de Apple Search Ads.   
     

![](/asa_15.png)

-   Haz clic en **Guardar** para guardar la configuración realizada.

Una vez completados todos los pasos de esta guía podrás empezar a ver los resultados de tus campañas de Apple Search Ads en el Apptracker de EMMA.

# Principales causas de discrepancia

En este apartado podrás encontrar de manera más detallada las principales causas de discrepancia que pueden existir en los datos entre las plataformas de EMMA y Apple Search Ads (de ahora en adelante ASA).

1.  **Install vs open**  
    ASA cuenta todas las descargas cómo instalaciones/conversiones mientras que EMMA sólo cuenta la instalación como tal una vez que el usuario se descarga la app y realiza la primera apertura de la misma (que además debe ser una sesión mínima de 4 segundos, que es el tiempo que se tarda en realizar al conexión con Apple para que nos envíe la información).
2.  **Limitación de seguimiento publicitario**  
    Si los usuarios tienen la limitación de seguimiento publicitario habilitada para no recibir anuncios basados en sus intereses, serán atribuidos en ASA pero no en EMMA. Esto es debido a que, a raíz de la política de privacidad, ASA no puede compartir la información de esos usuarios específicos con EMMA.
3.  **Ventana de atribución**  
    La ventana de atribución de ASA es de 30 días mientras que la de EMMA es de 2 días. Esta es una de las principales causas de discrepancia entre ASA y EMMA.  
      
    Si un usuario hace click en el anuncio hoy y se instala la app dos días después, ASA atribuye ese usuario a alguna de sus campañas. Sin embargo, en EMMA, si un usuario hace click hoy en el anuncio, se instala la app mañana pero no realiza la primera apertura hasta tres días después del click, esa instalación en EMMA no es atribuida a la campaña de ASA.
4.  **Re-Installs**  
    ASA contabiliza las re-instalaciones. Es decir, aquellos usuarios que han tenido la app en el pasado, la han desinstalado y la han vuelto a instalar a través de un anuncio de ASA, son atribuidos como re-instalaciones en ASA. Por tanto, bajo la columna de instalaciones de ASA, se están contabilizando tanto las nuevas instalaciones como las re-instalaciones.   
      
    En EMMA, si un usuario ya tuvo la app anteriormente y se la vuelve a instalar a través de un anuncio de ASA no es contabilizado como una nueva instalación atribuida a esa campaña ya que ese usuario ya estaba en nuestra base de datos. Con lo cual el dato correcto para comparar ambas plataformas sería nuevas instalaciones de ASA vs instalaciones de EMMA. 

# Preguntas frecuentes

**¿Qué nivel de desglose voy a tener?**

En EMMA podrás encontrar tu campaña por el mismo nombre que le has dado a la campaña en la consola de Apple. Además, podrás diferenciarla fácilmente porque tendrá el icono de Apple delante del nombre.

En cuanto al desglose de la campaña, cada fuente, tomará el nombre del Ad Group que le hayas dado en Apple Search Ads.

**¿Qué datos voy a poder ver en EMMA?**

Desde el Apptracker de EMMA podrás ver los datos de *clicks, coste, instalaciones* y *eventos in-app* realizados tras la instalación.

**¿Porqué veo en EMMA una campaña con el nombre** ***CampaignName*** **que no está en el Dashboard de ASA?**

La campaña de ASA, *CampaignName*, que ves en el AppTracker de EMMA es una prueba que realiza EMMA para comprobar que la conexión es correcta. Por eso es normal que veas algunas instalaciones asociadas a esta campaña.