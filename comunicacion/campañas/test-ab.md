---
title: Test AB
description: Test AB
published: true
date: 2022-02-15T10:28:00.612Z
tags: 
editor: markdown
dateCreated: 2020-11-25T23:06:11.096Z
---

Una vez que las comunicaciones de Test A/B están lanzadas y generando datos, es necesario saber cuál es la que mejor está funcionando en base a los objetivos marcados para esa comunicación. 

Para ello, se podrán analizar los datos y ver la comunicación con mejor rendimiento desde la sección de ***Comunicación > Informe de comunicaciones***.

![](/reporting_test_ab_1.png)

Para ver los datos en concreto de las comunicaciones de Test A/B deberemos bajar hasta la tabla con todas las comunicaciones y buscar la comunicación deseada. Al tratarse de una comunicación de Test A/B podremos ver, al lado de la columna del ***Estado,*** tres puntitos verticales a forma de menú. Es desde ese menú contextual desde donde se accederá al informe concreto de Test A/B. 

![](/reporting_test_ab_2.png)

# Informe Test A/B

Una vez que se accede al informe, nos encontraremos con tres secciones diferenciadas. Por un lado, está el resumen con datos generales, a continuación se pueden ver los datos de cada una de las campañas que forman el AB testing y han obtenido datos y por último nos encontramos una tabla como en el informe clásico con toda la actividad recogida por cada campaña. 

## Datos generales

Antes de nada, es necesario seleccionar el evento objetivo de la campaña en el desplegable de la parte superior izquierda de la pantalla. Este evento será el KPI objetivo en base al cual se decidirá la campaña que mejor funciona.

![](/reporting_test_ab_3.png)

En esta primera parte del informe se podrán ver datos generales (totales) de las campañas, como impresiones, impresiones únicas, clics (o el evento objetivo seleccionado como objetivo) y ETR. Además, se podrá ver una gráfica en la que se muestran datos del evento objetivo de cada una de las campañas. 



> El **ETR** es el ***Event Through Ratio*** y es el KPI que se emplea para determinar la campaña "vencedora" del Test A/B. Este KPI se calcula dividiendo los ***eventos*** entre las ***impresiones*** de cada versión. La versión que tenga un mayor ETR, un mejor porcentaje de conversión, será la versión ganadora. 
{.is-info}

## Versiones

![](/reporting_test_ab_4.png)

La segunda gráfica nos mostrará el ganador del test A/B en base al ETR. La campaña ganadora se mostrará con un icono de una copa antes del nombre. 

En esta gráfica se verán única y exclusivamente los KPIs empleados para seleccionar el vencedor. Por lo tanto sólo se podrán ver datos de impresiones, impresiones únicas, evento objetivo y ETR. 

-   **Versión:** Nombre de la versión. Las posibilidades son *Original, Versión A, Versión B y Versión C.* 
-   **Impresiones:** Número de veces que la comunicación ha sido mostrada (impresiones totales).
-   **Impresiones únicas:** Número de dispositivos únicos en los que se han mostrado las comunicaciones.
-   **Evento objetivo**: La columna ***clics*** irá cambiando de nombre en función del evento objetivo seleccionado. Se muestra el número de veces que el usuario realiza el evento objetivo.
-   **ETR:** Event Through Ratio. Ratio de evento frente a impresiones.

> El evento objetivo irá cambiando en la columna en función del evento seleccionado previamente (ver apartado *Datos Generales* mas arriba). En este ejemplo concreto el evento objetivo son clics. {.is-info}

## Estadísticas individuales

![](/reporting_test_ab_5.png)

Asimismo, en la última gráfica de *Estadísticas individuales* se podrá consultar toda la actividad asociada a las campañas al igual que en el informe de comunicaciones clásico con cualquier otra comunicación. 

|     |     |
| --- | --- |
| *Versión* | Nombre de la versión de la comunicación. Las opciones son *Original, Versión A, Versión B y Versión C.* |
| *Impresiones* | Número de veces que la comunicación ha sido mostrada (impresiones totales). |
| *Impresiones únicas* | Número de dispositivos únicos en los que se han mostrado las comunicaciones. |
| *Clics* | Número de veces que el usuario interactúa con la comunicación. |
| Eventos por defecto y personalizados | Medición de los eventos que tengas definidos para tu aplicación. |


Con este nuevo **Informe de comunicaciones,** tienes que tener en cuenta:

-   El Informe de comunicaciones se actualizan cada 2 horas. Ten en cuenta que el tiempo puede ser mayor o menor dependiendo del volumen de datos.
-   Los datos se muestran según las fechas que tengas seleccionadas. NO son acumulativos.
-   Aunque una campaña esté activa, si en las fechas que tú seleccionas no ha recogido datos, esta no se mostrará en la tabla.
-   Las campañas que se hayan eliminado antes de recopilar datos no se mostrarán.
-   Si se realiza una exportación de la tabla *Estadísticas individuales,* esta se hará como lo ves en la propia tabla, solo aparecerán las columnas de los KPI y campañas que estaban seleccionadas.
-   El Informe de comunicaciones se actualiza cada 2h. Ten en cuenta que el tiempo de actualización puede variar en función del volumen de datos que se estén recogiendo.