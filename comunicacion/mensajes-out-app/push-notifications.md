---
title: Push Notifications
description: Push Notifications
published: true
date: 2023-09-04T07:35:41.965Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:58:08.989Z
---

# Crear notificaciones push

![push_enrich.png](/push_enrich.png)

EMMA da la posibilidad de enviar notificaciones push a aquellos dispositivos que hayan aceptado las notificaciones. Crear estas notificaciones es muy sencillo. Tan solo tienes que seguir estos pasos: 

-   Haz [login](http://ng.emma.io) en EMMA y dirígete a ***Comunicación > Mensajes***

![](/push_01.png)

-   En el botón ***\+ Nuevo mensaje*** selecciona el formato ***Push*** en el listado.

![](/push_02_ok.png)

La configuración de la notificación push se divide en 5 pasos, que veremos detallados a continuación: 

1.  Propiedades
2.  Contenido
3.  Programación
4.  Objetivo
5.  Confirmación

## Propiedades

Lo primero es la configuración general de la campaña. Donde estableceremos un nombre y seleccionaremos el tipo de campaña. 

![](/push_03.png)

-   **Nombre (obligatorio):** Establece un nombre para tu campaña push. Es un nombre a nivel interno (no es visible para los usuarios) para identificar fácilmente la comunicación una vez creada.
-   **Tipo de campaña:** Selecciona el tipo de campaña:
    -   **Manual (clásica):** La notificación se enviará en la fecha establecida a los usuarios establecidos.
    -   **Automática para rutas de cliente:** La notificación se enviará únicamente cuando forme parte de un flujo de rutas de cliente. Puedes ver más información sobre las rutas de clientes en este [artículo](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys).
-   **Propietario:** Este campo se cubre automáticamente con el usuario que está creando la comunicación en caso que tu cuenta use la prevalidación de campaña. De no usarlo, no verás este campo. Puedes ver más información sobre la prevalidación de campañas [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as).
-   **Responsable:** Selecciona, en caso de que tu cuenta use prevalidación de campañas, un usuario responsable del listado para que reciba las notificaciones con los cambios que se realicen en la comunicación. Puedes ver más información sobre la prevalidación de campañas [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as).

-  **Parámetros**: Se pueden configurar dos tipos de parámetros:
	-	**Parámetros para ser recogidos por el SDK:** Haciendo clic en el botón **+Añadir** podemos configurar los parámetros necesarios para que sean recogidos por el SDK para ponerlos a disposición de la app para su posterior tratamiento. Añadir estos parámetros es totalmente opcional.
	-	**Parámetros para filtrar en el informe de rendimiento:** Si se han creado [parámetros de comunicación](https://docs.emma.io/es/parametros-comunicacion) espcíficos se pueden añadir para que puedan tener efecto en las comunicaciones para poder aplicar flitros y agrupaciones en el [informe de rendimiento](https://docs.emma.io/es/informe-rendimiento).

## Contenido

Ahora ha llegado el momento de establece el contenido que va a tener nuestra notificación push. 

![push_27.png](/push_27.png)

-   **Añadir variación de Test A/B:** Si quieres añadir variaciones de test a/b a la notificación push para tener varias versiones, puedes hacerlo haciendo clic en el botón **Añadir variación de Test A/B.** Para más info sobre Test A/B haz clic [aquí](https://docs.emma.io/es/test-a-b).
-		**Título:** Establece un título para tu notificación push. Puede tener una extensión de hasta 50 caracteres incluyendo emojis y etiquetas. 
-   **Mensaje (obligatorio):** Escribe el copy de tu push en el cuadro de ***Mensaje.*** Puede tener una extensión máxima de 144 caracteres y permite el uso de emojis. Para insertar los emojis, basta con hacer clic en el botón secundario del ratón, seleccionar la opción ***emojis y símbolos*** e insertar el emoji deseado, o bien se pueden copiar de cualquier librería de emojis y pegarlo en el campo de mensaje.   
> Es importante que tengas en cuenta que no todos los emojis se pueden visualizar en todas las versiones de los sistemas operativos. Si algún emoji se ha introducido por primera vez por ejemplo en la versión de Android 9, los dispositivos con versiones anteriores no visualizarán ese emoji. 
{.is-info}

También puedes usar nuestro ***motor de sustitución de textos*** para insertar el valor individual de las etiquetas (*tags)* atribuidos a tus usuarios. Para ello solo tienes que hacer clic en el selector ***Etiquetas*** que aparece encima del cuadro de mensaje. El listado de *Etiquetas* que aparezca dependerá de las propiedades que envíe tu aplicación a EMMA. Conoce como enviar *Etiquetas* a través de nuestro SDK de [iOS](https://developer.emma.io/es/ios/integracion-sdk#propiedades-del-usuario-tags) y de [Android](https://developer.emma.io/es/android/integracion-sdk#completar-el-perfil-del-usuario-mediante-tags).   
    Aquellos usuarios que reciban una comunicación con un texto compuesto por las variables del motor de sustitución, pero no tengan asignado ningún valor para esa *Etiqueta*, verán únicamente el texto sin las variables.</br>![](/push_05.png)</br>![](/push_06.png)

-   **Ventana de atribución:** Configura la ventana de atribución Push que se activará. La ventana de la atribución establece las veces que se puede asignar una campaña *Push* si el usuario interactúa con el *Push* (lo abre) y realiza alguna acción dentro de la app. Por **defecto** son **6** **horas**, pero se puede modificar este campo según las necesidades específicas de cada comunicación push.
-   **Ratio mensajes por segundo:** Permite delimitar el número de notificaciones push que se quieren enviar por segundo. Si está limitado se enviará a la velocidad máxima permitida.
-   **Rich push URL:** Permite personalizar la redirección que se hace en la apertura del push. El uso del Rich Push URL no es compatible con el uso en una misma campaña del *Push Tag.* También se puede usar esta opción para hacer URL's dinámicas, usando el motor de sustitución*.*
    -   ***Rich Push http/https URL*** (http://www.yoururl.com/whatever): muestra una *interstitial* con contenido web establecido por la dirección URL introducida cuando el usuario abre el *Push*. Sigue nuestras [guías de integración](https://developer.emma.io/es/home) (*Push Notifications > Rich Push URL*) para integrar este método en tu App.
    -   ***Rich Push DeepLink URL*** (scheme://page1): redirecciona a una sección específica dentro de la app cuando el usuario abre el *Push*.  Toda la estructura del DeppLink URL es personalizable.
-   **Etiqueta push:** Permite la interacción después de que el usuario abra el *Push* (ej. redirigir al usuario a cierta pantalla o enlace de producto o aplicar una oferta de descuento). Sigue nuestras [guías de integración](https://developer.emma.io/es/home) (*Push Notifications > Push Tag*) para integrar este método en tu App.
-   **Sonido del push:** Permite la elección de sonidos personalizados de las notificaciones push a través de los sonidos integrados en la aplicación. Requiere integración específica.
-   **Imagen:** Puedes adjuntar una imagen junto al texto en dispositivos iOS & Android. Elige el archivo, selecciona la imagen (*jpg, png* o *gif)* que vas a enviar en la comunicación *Push*. El tamaño mínimo recomendado es de 450x225px.  
    Por motivos del sistema operativo Android ajenos a EMMA, aconsejamos dejar también un pequeño margen en los bordes para evitar cortes que puedan comprometer el entendimiento del mensaje gráfico, sobre todo si este incluye texto. Es importante incluir el contenido relevante de la imagen dentro de esa zona de seguridad para que los Android de pantalla más pequeña no recorte la imagen:</br><p style="width:500px;">![push_23.png](/push_23.png)</p> </br> Si quieres modificar la imagen, tienes que eliminar la imagen actual haciendo clic en el botón **Eliminar** y posteriormente cargar la nueva imagen deseada.</br><p style="width:350px;">![](/push_16.png)</p>
-  **Botones de acción:** Haz clic en el botón **+ Añadir botón** para añadir hasta 3 botones de acción para cada notificación push. De esta manera podrás llevar al usuario a uno u otro sitio dentro de tu app en función del botón en el que haga clic. Para ello tan solo tienes que configurar los siguientes pasos. </br>![push_acciones_ii.png](/push_acciones_ii.png)
    -   **ID de acción**: Establece un identificador único de la acción. Este identificador servirá para poder medir los clics realizados en este botón. Para ello es importante crear un evento en la sección Gestión > Eventos (más info [aquí](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos)) y establecer en este campo el token de dicho evento. </br>Por ejemplo, podemos crear un evento que sea Botón Aceptar con el token boton-aceptar. Así, cada vez que vayamos a incluir un botón de aceptar, pondremos en este campo de ID de acción el token boton-aceptar.
    -   **Texto del botón**: Establece el texto que se mostrará en el botón. Por ejemplo, Aceptar.
    -   **Contenido de la acción**: Configura el **deeplink** al que quieres que sea redireccionado el usuario o la **URL** que quieres que se le muestre al usuario cuando haga clic en el botón. También puedes dejar este campo sin cubrir y, en este caso, simplemente se abrirá la app.
    -   **Ruta del icono**: Establece la ruta del icono en caso de que la app tenga iconos. Por limitaciones del sistema operativo Android, no es posible el uso de iconos en dicho sistema operativo.
    -   **Icono del sistema**: Por defecto está desactivo. En caso de que vayas a usar un icono del sistema (sólo disponible para iOS) habilita este check. 

> Para poder usar las **acciones** de **push** es necesario tener actualizada la versión del SDK. En el caso de **Android** la **versión** **mínima** necesaria es la **4.10** y en el caso de **iOS** la **versión mínima** es la **4.10.1**
{.is-info}

> Para poder usar **GIFs** **animados** en las notificaciones push es necesario tener la versión mínima del **SDK** de **iOS 4.6.2** para que este formato sea soportado. Debido a limitaciones del sistema operativo Android, actualmente no es posible el uso de GIFs animados en este sistema operativo.
{.is-info}


## **Programación**

Ha llegado el momento de establecer cuando se enviará la notificación. Puede enviarse inmediatamente una vez finalicemos la creación de la misma, o podemos realizar una programación para que se realice el envío en una fecha y hora concreta. 

![](/push_07.png)

**Fecha de envío**

![](/push_09.png)

-   **Enviar ahora:** Si el switch está habilitado, el envío se realizará inmediatamente tras finalizar la creación de la notificación push. Si está deshabilitado permitirá la programación del envío
-   **Programar envío:** Establece una fecha y hora determinado para el envío de la notificación push. La fecha de envío no puede ser anterior a la fecha actual.
-   **Envío planificado:** Configura la opción horaria. Puedes seleccionar entre *Hora local del dispositivo* y *Zona horaria del usuario.* 
    -   **Hora local del dispositivo:** La comunicación se enviará en diferentes momentos, en función del intervalo de tiempo UTC de cada país.
    -   **Zona horaria del usuario:** La comunicación se enviará al mismo tiempo a todos los usuarios, independientemente de los intervalos de tiempo de UTC. Por zona horaria del usuario se entiende por defecto CEST (UTC+1 en invierno y UTC+2 en verano)

**Envío recurrente**

![](/push_10.png)

-   **Repetir:** Si el switch está deshabilitado será un envío único. Si por el contrario queremos que sea un envío repetitivo en el tiempo, debemos habilitar el switch.
-   **Enviar:** Cuando es un envío repetitivo, podemos seleccionar que tipo de repetición deseamos:
    -   **Diariamente:** El push se enviará todos los días a la hora establecida a partir del primer envío.
    -   **Semanalmente:** El push se enviará semanalmente los días seleccionados a la hora establecida a parir del primer envío.
    -   **Mensualmente:** El push se enviará mensualmente un día determinado a la hora establecida a partir del primer envío.
-   **Día(s) del mes/semana a enviar:** Si hemos seleccionado un envío *Semanal o Mensual* deberemos establecer los días que queremos que se envíe.
    -   **Días de la semana:** En el caso de un envío semanal, podremos elegir uno o varios días entre las opciones Lunes, Martes, Miércoles, Jueves, Viernes, Sábado y Domingo.
    -   **Día del mes:** En el caso de un envío mensual, podremos seleccionar un día entre el 1 y el 31.

## Objetivo

Ya por último, debemos establecer el público objetivo que queremos que reciba la notificación push. Para ello debemos seleccionar del listado una audiencia de las disponibles. 
Si el listado de audiencias está vacío es porque no hay ninguna audiencia disponible que se pueda usar en las notificaciones push o que no has creado ninguna audiencia todavía. En esta [**guía**](https://docs.emma.io/es/audiencias) puedes ver más información sobre cómo crear audiencias. 

Una vez seleccionada la audiencia deseada, haz clic en el botón **Siguiente** para poder finalizar la creación de la notificación push. 

![](/push_22.png)

> Ten en cuenta que en las notificaciones push **sólo** podrás usar audiencias que estén 100% procesadas. 
{.is-danger}


En caso de haber creado variaciones de test ab, automáticamente, debajo de la segmentación se habilitará una sección para configurar el % de usuarios que queremos que visualicen una u otra pieza. Además, también podremos configurar un grupo de control, son perfectamente compatibles. Tan solo deberemos asignar un % de usuarios que queremos para cada una de las opciones disponibles. 

![](/push_13.png)

En caso de no tener variaciones de test ab, podemos igualmente establecer un grupo de control, para ello, tan solo tenemos que habilitar el check de grupo de control y establecer el % de usuarios que NO queremos que reciban la comunicación. Para más información sobre Grupo de control, visita nuestra guía al respecto. 

![](/push_15.png)

## Confirmación

Ya por último, llegamos a la pantalla de confirmación de la comunicación, donde podremos ver el número de dispositivos activos para el envío de notificaciones push. 
EMMA te mostrará la siguiente información como resultado de la audiencia: 

-   **Dispositivos:** dispositivos totales que tienen o han tenido en algún momento la app instalada.
-   **Dispositivos totales:** dispositivos incluidos en el criterio de segmentación que en algún momento han trackeado un token válido. Esto no implica, que en el momento del envío del push los dispositivos activos para el push tengan las notificaciones activas.
-   **Usuarios que ya han sido impactados:** dispositivos activos para el push pero que no serán impactados por tener un limite de impactos aplicado. Esta información hace referencia al momento en el que se está creando la comunicación, pero esto no implica que si es un push programado, en el momento del envío del mismo, no lo vayan a recibir.  Para personalizar tu limite de impactos sigue [estas instrucciones](https://docs.emma.io/es/configuracion#limitar-los-impactos-de-las-notificaciones-push).

Además también se podrá ver un resumen de toda la configuración realizada en los pasos previos. 
Una vez revisada toda la configuración podemos proceder a enviar el mensaje haciendo clic en el botón ***Enviar mensaje.*** 

![](/push_19.png)

# Estados de las campañas
Una vez que has creado tus campañas pueden encontrarse en uno u otro estado, dependiendo de las caracterísitcas que cumpla. Los posibles estados en los que se puede encontrar una comunicación son: 
- **Activa:** Tu campaña se ha iniciado, esta activa y está siendo mostrada a los usuarios que corresponda.
- **Aprobada:** Este estado solo aplica si está en uso el sistema de prevalidación de campañas. Más info al respecto [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as). Significa que la campaña ha sido aprobada y está a la espera de que sea lanzada definitivamente. Mientras la campaña se encuentra en este estado no está siendo visible para ningún usuario. 
- **Eliminada:** La campaña ha sido eliminada.
- **Borrador:** La campaña ha sido guardada como borrador. En este estado no se muestra a ningún usuario y puede ser recuperada en cualquier momento para lanzarla. 
- **Finalizada:** La campaña ha finalizado porque se ha cumplido la fecha de fin establecida. En este estado ningún usuario puede visualizarla. 
- **Pausada:** La campaña esta pausada y no se está mostrando a ningún usuario. 
- **Lista:** Este estado solo aplica para campañas automáticas y significa que la campaña está lista para ser usada en una [regla](https://docs.emma.io/es/reglas) o en una [ruta de cliente](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys). En este estado la comunicación no se está mostrando a ningún usuario.
- **Programada:** La comunicación está a la espera de cumplir la fecha de inicio establecida para activarse y empezar a mostrarse a los usuarios. 
- **Testing:** Este estado solo aplica si está en uso el sistema de prevalidación de campañas. Más info al respecto [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as). Significa que la campaña esta en modo pruebas y se está mostrando a los usuarios que cumplen el segmento establecido. 
- **Para validar:** Este estado solo aplica si está en uso el sistema de prevalidación de campañas. Más info al respecto [aquí](https://docs.emma.io/es/prevalidacion-de-campa%C3%B1as). Significa que la campaña ha sido creado y está a la espera de que sea validad y aprobada para posteriormente ser lanzada. Mientras la campaña se encuentra en este estado no está siendo visible para ningún usuario. 

# Editar, pausar, clonar o eliminar notificaciones Push
## Editar notificaciones push

> Podrás editar todas las notificaciones push salvo las que se encuentren en los estados: 
Finalizada
Eliminada
{.is-info}

Para editar tus campañas push tienes que: 

-   Ve a la tabla resumen de la pantalla **Comunicación > Mensajes,** haz *mouse over* en el menú contextual de la comunicación deseada y haz clic en la opción **Editar.** 

![](/push_i.png)

-   Modifica los campos de la campaña que desees modificar y finaliza el proceso de edición de la comunicación. Haz clic en el botón **Enviar mensaje.** 

![](/push_ii.png)

-   A continuación acepta la edición haciendo clic en el botón **Guardar** del pop up de confirmación para que se apliquen los cambios realizados en tu comunicación. 

![](/push_iii.png)

En la siguiente tabla, podrás consultar qué campos se pueden modificar según el estado en el que se encuentre tu comunicación:

|     |     |
| --- | --- |
| **Estado** | **Qué puedo modificar** |
| *Repeat Mode* | Se pueden modificar todos los campos menos el *Control group* |
| *Rules Mode* | Sólo se pueden modificar los campos de *Message* y *Rich push URL* |
| *Waiting* | Se pueden modificar todos los campos |

## Pausar notificaciones Push

Es importante que tengas en cuenta que sólo vas a poder pausar aquellas notificaciones Push que tengan como estado *" Activa".* Para poder pausarlas tendrás que hacer lo siguiente:

-   Dirígete a la tabla resumen de la pantalla **Comunicación > Mensajes**, haz *mouse over* en el menú contextual de la comunicación deseada y ha clic en la opción **Pausar**. La notificación push cambiará al estado Pausado de inmediato.

![](/push_iv.png)

Si quieres volver a activar la campaña deberás hacer clic en el botón de **Lanzar** desde el menú contextual otra vez.

![](/push_v.png)

## Clonar notificaciones Push

Podrás clonar cualquier notificación push independientemente del estado en el que se encuentre. Ten en cuenta que una vez clones la campaña, el formulario de push se mostrará con la configuración de la campaña clonada a excepción de los campos de fecha de programación.

Para clonar una notificación push sólo tienes que seguir estos pasos:

-   Haz clic en **Clonar** desde el menú contextual de la notificación push que deseas clonar.

![](/push_vi.png)

-   Modifica la información que necesites y finaliza el proceso de creación de la notificación push. Haz clic en el botón **Enviar mensaje.** 
-   A continuación acepta el pop up de confirmación de envío de la notificación push haciendo clic en el botón **Guardar mensaje**

Y listo, tu notificación push ha sido clonada y posteriormente enviada. 

## Eliminar notificaciones Push

Si quieres eliminar una notificación Push que ya hayas programado tienes que:

-   Ve a la tabla resumen de la pantalla **Comunicación > Mensajes,** haz *mouse over* sobre el menú contextual de la comunicación deseada y haz clic en la opción **Eliminar.**

![](/push_vii.png)

-   Aprueba el mensaje para confirmar la acción de eliminar.

![](/push_viii.png)

> Sólo se pueden eliminar las notificaciones Push que todavía no han iniciado el envío o aquellas que están en modo repetición. 
{.is-warning}


# Limitar los impactos para notificaciones push

Si se tiene pensado impactar a varios segmentos de usuarios o enviar un gran número de notificaciones push es recomendable establecer un límite de frecuencia para no sobreimpactar a los usuarios. Para ver más información sobre como configurar este límite de impactos haz clic [aquí](https://docs.emma.io/es/configuracion#limitar-los-impactos-de-las-notificaciones-push). 

# Sobre el texto de las notificaciones Push en Android

Por defecto, los dispositivos Android muestran el texto completo programado en EMMA: </br><p style="width:250px;">![push_24.png](/push_24.png)</p>
Pero si en el listado de notificaciones pendientes del dispositivo hay más de una notificación pendiente, el texto de la notificación Push se reduce:  </br><p style="width:250px;">![push_25.png](/push_25.png)</p>
Si quieres mostrar una ventana emergente con el texto completo cuando el usuario es redirigido a la App través de una notificación Push, debes integrar el valor "false" en el *Custom Dialog*p>para habilitarlo. </br><p style="width:250px;">![push_26.png](/push_26.png)</p>

Además, en el caso de configurar una notificación push con una imagen, debido a limitaciones del sistema operativo, si el texto es demasiado "largo" y no "cabe" en el hueco disponible, este no se muestra entero, sino reducido. </br><p style="width:350px;">![push_imagen_android_1.png](/push_imagen_android_1.png)</p> </br><p style="width:350px;">![push_imagen_android_2.png](/push_imagen_android_2.png)</p>


# Doble Notificación en dispositivos iOS

¿Tienes problemas con la recepción de notificaciones Push en tus dispositivos iOS?

Si este es tu caso y recibes la notificación duplicada en todos tus dispositivos iOS, probablemente se debe a que en tu AppDelegate se están registrando más de una vez las notificaciones. Para ello se deben hacer las modificaciones pertinentes para que exista un único registro de notificaciones, que puede ser el de EMMA o bien uno custom.

**Opción 1. Utilizar SDK de EMMA:**

Existen dos formas de hacerlo:

A) Añade \[eMMa startPushSystem:launchOptions\] en el AppDelegate.

B) Añade \[eMMa starteMMaSession:@”key” withOptions:launchOptions\] en el AppDelegate. Una vez hecho esto añade \[eMMa startPushSystem:NULL\] en cualquier lugar de la App.

**Opción 2. No utilizar SDK de EMMA:**

A) Añade \[eMMa starteMMaSession:@”key” withOptions:launchOptions\] en el AppDelegate.

B) Realiza un registro de notificaciones custom.

**_IMPORTANTE:_** 

Si usas la ***Opción 2. No utilizar EMMA SDK*** recuerda revisar tu código y verificar que **no estás utilizando el método de eMMa startPushSystem**. Es necesario eliminarlo en caso de estar siendo utilizado para esta opción 2.

# Habilita FCM Android para las Notificaciones Push

-   Primero creamos un proyecto en Firebase o añadimos uno ya existente de Google Cloud. Una vez creado aprecerá la siguiente página.

![fcm_1.png](/fcm_1.png)
-   Hacemos click en Android. Añadimos el application Id y le damos a registrar app.

![fcm_2.png](/fcm_2.png)

-   Una vez registrada descargamos el google-services.json y lo añadimos a /app dentro del proyecto.

![fcm_3.png](/fcm_3.png)

-   Al completar estos dos pasos es suficiente para activar las notificaciones. Salimos del proceso y vamos al apartado de "Project settings".

![fcm_4.png](/fcm_4.png)

-   En "Project Settings" cogemos el Server Key y el Sender Id.

![fcm_5.png](/fcm_5.png)
![fcm_8.png](/fcm_8.png)
-   Haz [login](https://ng.emma.io/login?returnUrl=%2Findex) en EMMA, dirígete a *Preferencias App.* </br><p>![pref_app.png](/pref_app.png)</p>
-   Haz clic en el botón *Editar* y busca la configuración de *Android.* Cubre la información de *Google Push* *Sender ID* y *Google Push Server Key* obtenida anteriormente.

![fcm_7.png](/fcm_7.png)

-   Haz clic en *Guardar configuración.*

# Cómo probar las notificaciones push
Para poder llevar a cabo pruebas de notificaciones push tan solo debes seguir los siguientes pasos: 
- Verificar en el dispositivo que las notificaciones push están activadas. Puedes ver más sobre cómo hacer esta revisión [aquí](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#activaci%C3%B3n-de-las-notificaciones-push-en-el-dispositivo).
- Validar en EMMA que efectivamente el dispositivo está activo para recibir notificaciones push. Puedes ver más detalladamente cómo hacerlo [aquí](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#validaci%C3%B3n-en-emma-que-el-dispositivo-est%C3%A1-activo-para-recibir-notificaciones-push).
- Crear una audiencia interna de test. Puedes ver más información [aquí](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#crear-audiencia-interna-de-test).
- Crear una notificación push. Si no sabes cómo hacerlo puedes ver el paso a paso en esta [guía](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#crear-notificaciones-push).

## Permitir notificaciones push en el dispositivo
Para poder revisar que las notificaciones push están permitidas en el dispositivo en cuestión y poder activarlas en caso de ser necesario, tan solo hay que seguir estos pasos. 

### Android
En Android las notificaciones push se activan por defecto al instalar la app por primera vez, pero es posible deshabilitarlas. Para asegurarnos de que están activas debemos seguir estos pasos: 

- Ir a los ajustes del dispositivo. 
- Buscar el apartado de aplicaciones.</br><p style="width: 300px">![android_push_1.jpg](/android_push_1.jpg)</p>
- Localizar la aplicación en cuestión y acceder a los detalles.
</br><p style="width: 300px">![android_push_2.png](/android_push_2.png)</p>
- Hacer clic en la aplicación para acceder a los detalles de la misma.
- Ir al bloque de Notificaciones y ver que están permitidas.</br><p style="width: 300px">![android_push_3.jpg](/android_push_3.jpg)</p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p style="width: 300px">![android_push_4.jpg](/android_push_4.jpg)</p>
	

### iOS
En iOS, al instalar la app, se muestra una ventana emergente para pedir permiso para enviar notificaciones push. Es necesario aceptar este permiso para que el dispositivo pueda recibir dichas notificaciones. En caso de no haberlo aceptado, se puede cambiar la configuración en los ajustes del dispositivo. Para ello debemos seguir estos pasos:
- Ir a los ajustes del dispositivo.</br><p style="width: 300px">![ios_push_1.png](/ios_push_1.png)</p>
- Hacer scroll down hasta llegar al listado de las aplicaciones instaladas.
- Hacer clic en la aplicación en cuestión.</br><p style="width: 300px">![ios_push_2.png](/ios_push_2.png)</p>
- Hacer clic en la opción Notificaciones.</br><p style="width: 300px">![ios_push_3.png](/ios_push_3.png)</p>
- Habilitar el check Permitir Notificaciones.</br><p style="width: 300px">![ios_push_4.png](/ios_push_4.png)</p>

## Validar estado del dispositivo en EMMA
Una vez activadas las notificaciones push en el dispostivo es importante validar en EMMA el estado de los dispositivos que se van a usar para enviar notificaciones push de test. 

Para que se pueda hacer el envío de notificaciones push desde EMMA, es necesario que desde la app se envíe un identificador único de push de cada dispositivo que se llama token. Si ese token no se envía a EMMA, el dispositivo nunca constará en nuestro sistema como activo para el envío de notificaciones push, por mucho que estás hayan sido activadas en el dispositivo en cuestión. 

Para verificar si un dispositivo consta en EMMA como activo para el envío de notificaciones push, tenemos dos opciones: 

- **Audiencias**: En el momento en el que hemos creado la audiencia (Comportamiento > Audiencias) en la tabla de las audiencias podemos habilitar una columna que se llama ***Con Push***. En esta columna, de los dispositivos totales que cumplen la audiencia, nos dirá cuantos tienen las notificaciones push activas.![pruebas_push_4.png](/pruebas_push_4.png)![pruebas_push_3.png](/pruebas_push_3.png)
- **Explorador/gente**: Desde explorador también es posible ver a través de un segmento simple si un dispositivo tiene las notificaciones push activadas o no. 
Para ello deberemos hacer un segmento con algún identificador único del dispositivo en cuestión (id dispositivo, id cliente o cualquier etiqueta, tal y como comentábamos en el punto 1 de esta guía) y añadir un filtro específico de las notificaciones push (notificación push activada).
![pruebas_push_5.png](/pruebas_push_5.png)Si no sabes cómo crear segmentos en explorador, puedes ver más información al respecto aquí.  

> Si eres desarrollador, en el caso de iOS, si compilas la app con cable deberás escribir un email a tu contacto en EMMA o a soporte@emma.io para que bajemos la app de EMMA a desarollo. Esto es necesario ya que por defecto EMMA siempre usa los certificados de push de producción, con lo cual inicialmente está preparada para que las pruebas se lancen desde Testflight o desde la store directamente y no desde Sandbox. Con este cambio, EMMA sabrá que tiene que usar los certificados de desarrollo y se podrá llevar a cabo el match del token de la app compilada con cable con el certificado cargado en EMMA. 
{.is-info}

## Mi dispositivo está marcado como inactivo para push

Cómo comentamos anteriormente, para poder enviar notificaciones push con EMMA es necesario que desde la app se esté enviando el token de push a EMMA. 

Lo primero es asegurarse de que las notificaciones push estén habilitadas en el dispositivo móvil (puedes ver aquí más info al resepecto). En caso de no estar activas, deberemos activarlas y volver a comprobar el estado del dispositivo en EMMA. Es recomendable dar un marge de unos 15 minutos aproximadamente para volver a hacer la revisión. 

Si por el contrario, en el dispositivo están activas las notificaciones push, pero EMMA dice que ese dispositivo no está activo es necesario contactar con el desarrollador de la app para que verifique que este token de push se está enviando a EMMA. 

En caso de que el token de push esté siendo enviado de manera correcta a EMMA, deberás escribir un email a tu persona de contacto en EMMA o a soporte@emma.io para que te podamos ayudar. 

Además, también es importante revisar que la configuración de push en el panel de EMMA es correcta tanto para Android como para iOS. 

## Crear audiencia interna de test
Para hacer pruebas de push a una audiencia de test cerrada, deberemos crear una audiencia en EMMA que impacte sólo a los dispositivos que queremos en cuestión. Si no sabes cómo crear audiencias puedes ver más información aquí. 

Para poder crear esta audiencia lo primero es identificar en base de datos los dispositivos a los que queremos impactar. Para ello hay varias opciones, pero todas ellas implican usar algún identificador o valor único de cada dispositivo.
En el caso de Android se puede usar el id de publicidad del dispositivo. En esta guía puedes ver cómo localizar ese id. En el caso de iOS, tras las modificaciones de privacidad llevadas a cabo por apple no es posible acceder a este identificador. 
Usar información de id de cliente o alguna etiqueta que se envíe a EMMA con información única de cada dispositivo, como puede ser nombre, número de teléfono, email…

Una vez que sabemos cómo localizar a los dispositivos en bbdd crearemos la audiencia realizando el filtro correspondiente para localizar a esos dispositivos únicos. 
Por ejemplo, supongamos que para localizar los dispositivos Android vamos a usar el id de publicidad y para localizar el iOS vamos a usar un id de cliente interno. La audiencia para impactar a esos dispositivos debería ser: 
Id dispositivo es igual a afc94ed4-2987-47b1-9388-af760c0f4e17, e4062baf-b75e-4bc9-b38f-328910e484e9
O id de cliente es igual a 393399939
Si por ejemplo vamos a usar para localizar todos los dispositivos el id de cliente, la audiencia sería: 
Id de cliente es igual a 123456789, 111333555, 999567442

Se deben incluir separados por comas todos los id de cliente a los que queramos impactar con esa comunicación.


## Preguntas frecuentes
- ¿Qué significa el mensaje vas a impactar a 0 de X dispositivos?
Al crear la notificación push desde Comunicación > Mensajes, en el paso 5. Confirmación se mostrará un mensaje de aviso informado a cuantos usuarios de los que componen la audiencia seleccionada, se va a impactar con la notificación push.![pruebas_push_1.png](/pruebas_push_1.png)
En el ejemplo de la captura, la audiencia seleccionada está compuesta por 1 dispositivo y ese dispositivo no va a ser impactado por la notificación push ya que no tiene las notificaciones push activadas en el dispositivo. 
> Recuerda que NO todos los usuarios que componen una audiencia son usuarios válidos para el envío de notificaciones push. Solo lo son aquellos que han permitido el envío de notificaciones push. 
{.is-info}

Si el o los dispositivos que componen la audiencia tienen las notificaciones push activadas el mensaje que veremos será el siguiente:![pruebas_push_2.png](/pruebas_push_2.png)
En este caso, los dispositivos que componen la audiencia tienen las notificaciones push activadas y se le enviará la notificación push.

En caso de que tu dispositivo no tenga las notificaciones push activadas, debes seguir los pasos del apartado ***[Permitir notificaciones push en el dispositivo](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#permitir-notificaciones-push-en-el-dispositivo)*** de esta guía para poder activar las notificaciones.
En caso de que hayas confirmado que las notificaciones push están activas en el dispositivo, debes seguir los pasos detallados en el apartado ***[Mi dispositivo está marcado como inactivo para push
](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#mi-dispositivo-est%C3%A1-marcado-como-inactivo-para-push)***.