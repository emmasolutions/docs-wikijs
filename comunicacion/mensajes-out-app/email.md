---
title: 
description: 
published: true
date: 2022-04-18T07:00:08.220Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:58:51.948Z
---

# Entregabilidad del email

Para poder usar la funcionalidad de envío de emails de EMMA es necesario realizar una pequeña configuración previa.

> Antes de avanzar con esta configuración es importante que tengas en cuenta que el email de EMMA sólo podrá ser usado si al usar el [***login*** o ***register***](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto) por defecto de EMMA se envía la información del email del usuario. Puedes comprobar si tu app está enviando esta información revisando el campo EMAIL al hacer una [*Exportación RAW*](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#exporta-tus-datos).
{.is-danger}

Para asegurar la máxima entregabilidad de los emails, en EMMA trabajamos con el protocolo ***DKIM*** (DomainKeys Identified Mail) y **SPF** (Sender Policy Framework). DKIM es de un protocolo de autenticación de correo electrónico, y, gracias a él, es posible asumir al remitente como responsable del envío de un determinado email.

¿Cómo? Muy sencillo. El proveedor de correo electrónico recibe el email y realiza una verificación a través de una clave pública y una clave privada para poder verificar la autenticidad de ese email mediante el match de ambas claves y poder atribuir el email al remitente. De este modo, se evita, en la medida de lo posible, que los email lleguen a la carpeta de SPAM del usuario sin necesidad.

Para configurar el ***DKIM*** en EMMA, una vez realizado el log in, es necesario ir a la sección ***Preferencias de la app*** y configurar la sección especifica de ***Envío de emails.***</br><p style="width:500px;">![pref_app.png](/pref_app.png)</p>

La configuración específica del *emailing* se compone de los siguientes puntos:

![email_2.png](/email_2.png)

-   **Dominio de envío:** En el dominio de envío se debe establecer el dominio desde el que se va a realizar el envío de emails, por ejemplo ***[tuempresa.com](http://tuempresa.com)***. Esta configuración se realiza a nivel de aplicación, no de cuenta. De este modo si hay varias aplicaciones bajo una misma cuenta, se podrán establecer dominios de envío diferentes en caso de ser necesario.
-   **Prefijo de la clave de dominio:** En este campo se debe establecer un nombre o palabra clave (por ejemplo ***EMMA*** o ***tuempresa***) que servirá para identificar que quién envía el email es quien dice ser.
-   **Nombre del registro DNS:** Es el nombre del registro DNS que se genera automáticamente tras cubrir los campos *Dominio de envío* y *Prefijo de la clave de dominio.* Este registro debe ser configurado en tu DNS para el correcto funcionamiento del email con el protocolo ***DKIM***. Para más información sobre como crear este registro debes ponerte en contacto con tu proveedor de DNS.
-   **Clave pública:** Es necesaria la creación de una clave pública. En caso de que ya se tenga una clave pública, existe la posibilidad de cargar un fichero directamente, o, si no se dispone de una clave pública, EMMA permite generar una automáticamente.
-   **Clave privada:** También es necesaria la creación de una clave privada. Esta clave privada puede cargarse en caso de que ya se tenga o se puede generar automáticamente en EMMA. Esta será la calve que EMMA empleará para firmar ciertas partes del email y poder realizar el match con la clave pública y verificar la autoría/propiedad del email.
-   **Contenido del registro DNS:** Es el contenido del registro DNS de tipo ***TXT*** que también se genera automáticamente tras haber cubierto el campo *Clave pública.* Este contenido del registro debe ser configurado en el momento en el que se crea el *Registro DNS* del paso anterior.

SPF también es un protocolo de autenticación de correo electrónico que permite autorizar a  servidores SMTP para el transporte de mensajes.

¿Cómo autorizo a los servidores SMTP de EMMA? Muy sencillo, necesitas incluir en el registro DNS de tipo SPF de EMMA: ***[smtp.emma.io](http://smtp.emma.io)***.

En el caso en que no tuvieras un registro de este tipo ya creado, deberías crearlo.

> En el caso de no tener una clave pública y una clave privada, EMMA da la posibilidad de generarlas automáticamente, pero se generan ambas a la vez. Es decir, no se puede generar solo la clave pública o solo la clave privada. Se generan las dos a la vez.
{.is-info}


# Crear proveedores de email

Para hacer el envío de emails desde EMMA se necesita un proveedor de email. Por defecto, en caso de no configurar ninguno, EMMA usará un proveedor por defecto para el envío de estos emails.

> Desde EMMA se recomienda el uso de un SMTP propio para evitar problemas de entregabilidad de los emails. Además, si qiueres hacer envíos de email desde un dominio GMAIL lo mejor es que configures el SMTP de Gmail para asegurar la entregabilidad de estos emails. 
{.is-warning}

Para establecer tu propio proveedor de email (SMTP) tan solo tienes que seguir estos pasos:

1.  Haz [login](https://ng.emma.io/login) en EMMA y dirígete a la sección ***Gestión > Proveedores de email.***
![email_3.png](/email_3.png)
2.  Haz clic en el botón ***Nuevo proveedor*** y cubre todos los campos requeridos en el formulario.
3.  El formulario para dar de alta un nuevo proveedor se divide en dos secciones diferenciadas. Por un lado, tenemos la configuración general del proveedor y, por otro lado, tenemos la configuración de la conexión SMTP del proveedor de email. ![email_4.png](/email_4.png)

-   **Nombre:** Establece un nombre para identificar al proveedor de email.
-   **Tipo:** El tipo es SMTP. Se trata de un input no editable, por lo que no se podrán realizar cambios en el tipo.
-   **Host SMTP:** Establece el nombre del host del servidor SMTP saliente. Por ejemplo [smtp.dominio.com](http://smtp.dominio.com).
-   **Puerto SMTP:** Establece el número de puerto que usa el servidor de correo saliente. Por defecto el puerto es 465, pero se puedes modificar por el puerto que corresponda.
-   **¿Conexión segura?**
-   **Usuario:** Establece tu dirección de correo electrónico completa del proveedor de email que se está configurando.
-   **Contraseña:** Establece la clave con la accedes al correo electrónico establecido en el punto anterior.

4\. Guarda la configuración establecida y, ¡listo! ya tienes creado tu proveedor de email.

# Crear un email
Una vez que tenemos toda la configuración previa del email realizada, llega el momento de lanzar nuestro primer email. Si no has realizado la configuración explicada anteriormente en esta guía, el email no se enviará.
Para poder crear un email en EMMA es necesario hacer [login](https://ng.emma.io/es/login?returnUrl=%2Findex) en la plataforma e ir a la sección ***Comunicación > Mensajes.***

![](/email_i.png)

Una vez dentro de la sección de ***Mensajes*** deberemos hacer clic en el botón ***\+ Nuevo Mensaje*** para poder seleccionar el formato comunicativo entre los disponibles. En este caso, ***Email.*** <p style="width:175px;">![](/captura_de_pantalla_2021-06-30_a_las_9.09.16.png)</p>

La configuración de la comunicación se divide en cinco pasos:

1.  Propiedades
2.  Contenido
3.  Objetivo
4.  Programación
5.  Confirmación

## Propiedades

En la sección de propiedades podremos llevar a cabo la configuración más básica de la comunicación. Es aquí donde se establecerá un nombre (se trata de un nombre a nivel interno) y dónde se seleccionará el tipo de campaña (clásica o automática).

![email_5.png](/email_5.png)

-   **Nombre:** Se trata de un input obligatorio para poder continuar con la configuración de la comunicación. El nombre va a ayudar a localizar la comunicación posteriormente. Será de gran utilidad tener la comunicación localizada por un nombre único para buscarla fácilmente en caso de ser necesarios cambios en la configuración o de cara a analizar el reporting de la misma. El nombre es un identificador interno que el usuario de la aplicación no visualizará en ningún momento.
-   **Tipo de campaña:** Existen dos tipos de campaña diferentes para seleccionar. Por un lado están las ***Campañas Clásicas Manuales*** y por otro lado se encuentran las ***Campaña Automática para [***Rutas de Cliente***](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys)***.

## Contenido

En esta sección de contenido es donde se llevará a cabo la configuración del mensaje en sí. De tal manera que será aquí donde se establezca información como el *remitente, destinatarios, asunto...* que conformarán el email.

![email_6.png](/email_6.png)

-   **Nombre del remitente:** En este input se debe establecer un nombre que identifique al remitente. Este nombre será el que visualice el destinatario del email en su bandeja de entrada. </br><p style="width:400px;">![email_7.png](/email_7.png)</p>


-   **Email del remitente:** Se debe establecer el email que va a realizar el envío del email. El dominio de este email debe ser el mismo que se ha usado para la configuración del [***DKIM***](https://docs.emma.io/es/comunicacion/mensajes-out-app/email#entregabilidad-del-email). Este email será el que visualice el destinatario en los detalles del email recibido.</br><p style="width:500px;">![email_8.png](/email_8.png)</p>
-   **BCC:** El *Black Carbon Copy* o *Con Copia Oculta(CCO)* es un campo en el que se pueden añadir emails de usuarios que vayan a recibir la comunicación de los clientes, pero los destinatarios no podrán ver a aquellas personas a las que se les ha enviado la copia oculta. Por ejemplo, se pueden añadir emails del equipo para que reciban el email y comprobar que se envía correctamente.
-   **Asunto:** Este es el input específico para establecer el titular que recoge lo que el usuario va a encontrarse cuando abra el email. Los mejores ejemplos de asuntos de email muestran el resumen del mensaje y recogen en una frase el contenido del mismo. Nunca aportes información falsa en el email o información que no se corresponda con tu asunto.
-   **Usar plantilla HTML:** Switch para habilitar o inhabilitar el uso de una plantilla HTML.
-   **Plantilla HTML:** Si se ha habilitado el uso de una plantilla HTML, se debe introducir su contenido en este input.

> Si la plantilla de HTML contiene imágenes es importante que éstas estén alojadas en algún servidor.
{.is-warning}

- **Cuerpo:** En caso de no usar una plantilla HTML, este será el input en el que se debe detallar el contenido del email. A lo hora de redactar el cuerpo del email el funcionamiento es el mismo que en la redacción de email comunes, pudiendo establecer una tipografía determinada, tamaño de fuente, justificación del texto, listas... permitiendo personalizar el email.
- **Test A/B:** Al igual que en cualquier otra comunicación, es posible generar versiones de Test A/B del email. Es posible crear hasta 3 variaciones en las que se pueden realizar los cambios pertinentes en cada una de ellas. Puedes ver más información sobre el Test A/B [***aquí***](https://docs.emma.io/es/test-a-b). 

> Las imágenes que se inserten en un email deben estar alojadas en algún servidor externo a EMMA, ya que EMMA no almacena la imagen y es la única manera en la que se puedan visualizar de manera correcta en el email. Si cargamos una imagen que esté guardada en nuestro ordenador, es imagen no se visualizará en el email. 
{.is-danger}

## Programación

Una vez que tenemos establecido el mensaje a comunicar en el email y el público objetivo de ese mensaje, llegó la hora de programar la comunicación. Es decir, establecer cuándo se va a enviar la comunicación, si tendrá repetición y el límite de impactos diarios, semanales y mensuales de la misma.

La configuración de la programación está dividida en ***fecha de envío***, ***envío recurrente*** y ***límite de impactos.***

- ***Fecha de envío:*** Lo primero de todo es decidir si el envío se realizará inmediatamente o si se va a realizar una programación. Si el envío no se va a realizar en el momento el switch debe estar deshabilitado y se deberá seleccionar una fecha y una hora para programar el envío.
![email_9.png](/email_9.png)

## Objetivo

En el apartado de objetivo es dónde se establecerá, valga la redundancia, el público objetivo de la comunicación. Para ello debemos seleccionar del listado una audiencia de las disponibles o crear una nueva.
Si el listado de audiencias está vacío es porque no hay ninguna audiencia disponible para usar o que no has creado ninguna audiencia todavía. En esta [guía](https://docs.emma.io/es/audiencias) puedes ver más información sobre cómo crear audiencias.

![inapp_22.png](/inapp_22.png)

En la primera mitad de la pantalla se podrá cargar o crear el segmento que será el público objetivo que recibirá la comunicación. Por otro lado, en la segunda mitad de la pantalla se podrá establecer un grupo de control en caso de así desearlo. Para ver más información sobre Grupo de Control haz clic [***aquí***](https://docs.emma.io/es/grupo-de-control).

> Para hacer el envío de emails EMMA necesita coger la información del email del usuario. Es por ello que el resultado del filtro, solo mostrará aquellos usuarios que, además de cumplir el filtro, tengan un email asociado.
> 
> En caso de establecer un grupo de control, se podrá determinar qué porcentaje de usuarios será impactado por la comunicación y que porcentaje estará dentro del grupo de control y por consiguiente **no** será impactado por la comunicación. En este artículo puedes consultar información más detalla acerca del grupo de control y su funcionamiento. 
{.is-warning}


## Confirmación

Ya por último, y para finalizar con la programación del email, veremos la sección de confirmación, en la que se puede revisar el detalle de todo lo configurado anteriormente y, en caso de detectar algún error, poder corregirlo antes de lanzar la comunicación.

Podremos ver con el detalle de la información configurada en cada sección como *tipo, nombre, nombre del remitente, email del remitente, bcc, asunto, objetivo, grupo de control, enviar ahora, repetir, impactos diarios, impactos semanales e impactos mensuales.*

Una vez revisada toda a configuración realizada tan solo tenemos que hacer clic en el botón **Enviar mensaje** para enviarlo.

# Preguntas frecuentes
**1. ¿Puedo sustituir Mailchimp si uso el email de EMMA?**
No, EMMA no pretende sustituir el uso de Mailchimp. No somos una plataforma para envío masivo de emails. 
El email de EMMA es un formato comunicactivo más como el resto que ofrece la plataforma y está pensado como un complemento para impactar a los usuarios que no tienen activas las notificaciones push. 
**2. ¿Se puede sincornizar el email de EMMA con Mailchimp?**
No, no se puede realizar una sincronización entre ambas tecnologías. Recuerda que el email de EMMA está pensado como un formato comunicativo más y no como una plataforma para el envío masivo de emails.
**3. ¿Cómo impacto a los usuarios?**
Para poder enviar emails con EMMA, además de la configuración descrita en esta misma guía, es necesario que se recoja la información del email del usuario y se envíe a EMMA. Está información debe ser envíada en el campo específico de email que tiene EMMA que se envía con el registro y/o login del usuario en la app (puedes ver más información al respecto [aquí](https://developer.emma.io/es/home)). Si no se tiene esa información de los usuarios no se puede hacer el envío de email.
**4. ¿Puedo usar la información del campo email para negativizar una audiencia?**
No, no se pueden realizar filtros ni audiencias en base al kpi email de EMMA, ni para incluir ni para excluir usuarios de este tipo de comunicaciones. Si se quieren negativizar usuarios, será necesario crear una audiencia en base a los eventos y etiquetas que la app envía a EMMA. 
**5. Si un usuario quiere darse de baja de los emails, ¿cómo se gestiona con EMMA?**
EMMA no tiene ninguna manera de gestionar la baja de los emails de manera automática. Es cada cliente el que configura un HTML o contenido concreto para esos emails, por lo que es responsabilidad del cliente en caso de así quererlo, establecer un botón en ese HTML para gestionar la baja de los emails y enviar automáticamente esa información a EMMA mediante una etiqueta de usuario o cargarla de forma manual mediante una etiqueta de usuario. 
Puedes ver más info sobre como enviar etiquetas desde la app [aquí](https://developer.emma.io/es/home).
Para ver como hacer una ingesta de inforación en EMMA de manera manual revisa este [artículo](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#importaci%C3%B3n-de-datos-a-emma). 
**6. ¿Qué es el SMTP?**
El SMTP es el servidor que envía el email. Además es el que se encarga de llevar a cabo las validaciones correspondientes para identificar la autenticidad del emisor y receptor. Es por eso que es importante crear un proveedor de email / SMTP en EMMA para asegurar un correcto funcionamiento y envio de emails. 
**7. Si un usuario se desintala la app de su dispositivo, ¿seguirá recibiendo emails?**
Sí. EMMA seguirá teniendo la información del email vinculada al dispositivo del usuario y por eso seguirá enviando emails en los casos en los que el dispositivo cumpla las concidiones estbalecidas para el envío de emails (ya sean emails clásicos o a traves de rutas de clientes). EMMA no dispone de ningún sistema para dejar de enviar emails a los usuarios que se hayan desinstalado la app.