---
title: Otros
description: 
published: true
date: 2021-12-22T10:20:09.761Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:38:00.835Z
---

# Configuración de la política de privacidad en App Store

Con la actualización del sistema operativo de Apple a iOS 14, las aplicaciones deben informar a los usuarios de los datos que son recogidos por la aplicación, incluyendo la que se recoge a través de terceros, como puede ser el SDK de EMMA.

Esto se requerirá para todas las nuevas aplicaciones y para las versiones actualizadas que se suban a la App Store a partir del 8 de Diciembre. 

Para ayudarte a cumplir con los requisitos de la Política de Privacidad en la App Store, en la siguiente lista puedes ver los datos que se recogen en EMMA por defecto y que por tanto se han de indicar en App Store Connect:

-   Precise Location
-   Coarse Location
-   User ID
-   Device ID
-   Product Interaction
-   Advertising Data
-   Other Usage Data

Por otro lado, cualquier evento o etiqueta adicional que tu app esté midiendo con EMMA y se requiera informar al usuario de ello, debe ser incluido también. 

En la siguiente imagen se puede ver con mayor detalle el tipo de datos tal y como aparecerá en App Store Connect una vez seleccionados:

![](/captura_de_pantalla_2021-07-15_a_las_12.07.16.png)

# ¿Cómo etiqueto mi app? 

Antes de empezar con la integración del EMMA SDK es importante definir qué se va a medir y como se etiquetará la app ya que de ello dependerá el reporting y las opciones de segmentación que te dará nuestra tecnología. 

Con el fin de cubrir todas las métricas posibles, **EMMA ofrece tres niveles de etiquetado** que son tratados como opciones complementarias.

-   [Etiquetas](https://docs.emma.io/es/etiquetas)
-   [Eventos por defecto](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto)
-   [Eventos personalizados](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados)

# Obtener el ID de tu dispositivo (AAID/IDFA)

Puedes utilizar tu ID de seguimiento publicitario de tu dispositivo para “[eliminarlo](https://docs.emma.io/es/configuracion#dispositivos-de-test)” de nuestra base de datos y testar tu aplicación como si fueras un nuevo usuario (nueva instalación):

## Android

Para saber cual es el ID de tu dispositivo Android, sigue estos pasos: 

Ve a *Ajustes* del dispositivo y busca la opción ***Google (Ajustes de Google)**.*

![](/id_android_i.jpg)

-   Una vez en los ajustes de Google, haz clic en la opción **Anuncios.**

![](/id_android_ii.jpg)

-   Al final de la pantalla de Anuncios, podrás ver el ID de publicidad de tu dispositivo.

![](/id_android_iii.jpg)

## iOS

Descárgate esta [app](https://itunes.apple.com/us/app/the-identifiers/id564618183?mt=8) y copia el identificador de publicidad (IDFA - Advertising Identifier) que aparece en la pantalla.

![](/id_ios.jpg)

> Si estás usando TestFlight tu ID puede cambiar. Revisa este punto si estás teniendo problemas para encontrar tu dispositivo en la BB.DD o para la recepción de comunicaciones.