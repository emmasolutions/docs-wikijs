---
title: Instalaciones
description: Instalaciones
published: true
date: 2025-03-03T09:48:51.439Z
tags: 
editor: markdown
dateCreated: 2021-06-07T12:24:43.174Z
---

# Instalaciones

En la pantalla de **Instalaciones** podemos consultar los datos generales sobre las **instalaciones de campaña** obtenidas por la app en un periodo de fechas determinado. De este modo, podremos realizar un análisis rápido para ver cómo evolucionan las **campañas** a través del volumen de instalaciones de la App. 

Para poder ver e interpretar todos estos datos, tienes que acceder a la sección **Captación > Instalaciones.** </br><p style="width:300px">![](/instalaciones_i_.png)</p>

> *Ten en cuenta que todos los datos mostrados en esta pantalla irán en función del rango de fechas seleccionado. Además, también podemos seleccionar que los datos de los gráficos se muestren agrupados por días, semanas o meses. Para ver más información sobre la agrupación y los rangos de fechas haz clic* [*aquí*](https://docs.emma.io/es/barras-de-menu)*.* 
{.is-info}


En la primera sección de la pantalla podemos ver una tabla con información de los KPIs básicos: </br><p style="width:450px">![](/instalaciones_iii.png)</p>

-   **Instalaciones:** Número de usuarios que han instalado la app en el periodo de fechas seleccionado. 
-   **Retención:** Porcentaje de retención (últimos 30 días) de los usuarios que han descargado la app en el periodo de fechas seleccionado. 
-   **Coste:** Presupuesto gastado en las campañas realizadas. 
-   **Beneficio:** Beneficio total obtenido de las campañas realizadas. 

# Instalaciones por SO (Sistema Operativo)

En el primer gráfico, que podemos ver en la parte superior derecha de la pantalla, podemos ver la información de las instalaciones desglosadas por sistema operativo en el rango de fechas seleccionado. 

Si hacemos *mouse over* sobre el gráfico, podremos ver información más detallada del número de instalaciones de cada sistema operativo. </br><p style="width:900px">![](/instalaciones_v_b.png)</p>

# Instalaciones por retención

En el siguiente gráfico podemos ver en detalle el desglose de las instalaciones de cada día, semana o mes y el ratio de retención que tienen esas instalaciones respecto a los últimos 30 días. Es decir, podemos ver el porcentaje de usuarios que instalaron la app en una fecha determinada y qu han seguido abriéndola durante los últimos 30 días, contaods desde el día actula. De esta manera, podremos llevar a cabo un análisis de la calidad del tráfico que estamos generando en nuestra app. 

Si hacemos mouse over sobre el gráfico, podemos ver información detalla del número de instalaciones y el ratio de retención de esas instalaciones en concreto. Es normal que las instalaciones de los últimos 30 días muestren un ratio de retención del 100%. </br><p style="width:1200px">![](/instalaciones_vi_b.png)</p>

# Instalaciones en el mundo

En el penúltimo gráfico de esta pantalla podemos ver las instalaciones por el mundo. Es decir, podemos ver de qué países provienen las instalaciones de nuestra app. 

Por un lado podremos ver en un mapa los países que nos están generando instalaciones (marcados con color verde) y por otro lado un gráfico en el que podemos ver más claramente los principales países de los que provienen nuestras instalaciones. 

> Si tu app está enfocada solo a mercado español, pero ves instalaciones de EE.UU no te preocupes, es normal. Ten en cuenta que, tanto Apple como Google realizan una revisión de todas las apps que se suben a las respectivas stores para asegurarse de que el contenido de la app no es inapropiado. Para ello, tienen que realizar descargas de la app que se ven reflejadas en EMMA y en este gráfico. 
{.is-info}

</br><p style="width:1200px">![](/instalaciones_vii.png)</p>

# Instalaciones por proveedor

En este último gráfico, podemos ver qué fuentes de tráfico están generando más instalaciones para tu app. Así, podemos saber y localizar, de un modo sencillo, los proveedores que están trayendo un mayor número de conversiones a la app. 

Si hacemos *mouse over* sobre el gráfico podemos ver la información específica de los proveedores que han generado instalaciones en ese día en concreto.</br><p style="width:1200px">![](/instalaciones_viii_b.png)</p>

# Preguntas frecuentes
**1. ¿Por qué veo instalaciones de EE.UU. si mi app sólo está para mercado español?**
Es normal que veas instalaciones de EE.UU. aunque tu app no esté disponible en ese mercado. Estas son las causas más comunes de estas instalaciones: 
- Cuando se sube una app a las stores, estas pasan un proceso de validación por parte de Google y de Apple. Estas empresas se encuentran en EEUU, por lo en ese proceso de validación se generan instalaciones de EEUU. 
- Durante el proceso de desarrollo de la app, los desarrolladores usan simuladores para lanzar pruebas que simulan una geolocalización en EEUU.
- Por la traducción de la dirección IP. Puede que la traducción de la dirección IP, haya dado como resultado una localización de EEUU. La dirección IP no es tan fiable como la geolocalización de un dispositivo.
- Es posible que haya usuarios que hayan viajado y hayan hecho la conexión desde EEUU por tanto, aunque tengan la store española configurado en la app, la traducción de la IP da como resultado EEUU.
