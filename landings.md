---
title: Landings
description: Landings
published: true
date: 2023-04-27T10:51:12.835Z
tags: 
editor: markdown
dateCreated: 2021-06-07T12:24:13.188Z
---

# Introducción

EMMA permite el uso de landings para aquellos casos que así lo requieren como en una campaña de Google que no sea con objetivo a instalación (más info [aquí](https://docs.emma.io/es/google#gesti%C3%B3n-de-landings-para-campa%C3%B1as-con-powlinks)) o en aquellos casos en los que queremos que el powlink tenga un redireccionamiento concreto cuando el usuario tiene un configurado un navegador que no es el navegador por defecto del sistema operativo (más info [aquí](https://docs.emma.io/es/adquisicion/apptracker#redireccionamiento-para-navegadores-no-nativos)).

Además de ofrecer dos landings por defecto, una para cada casuística, también se permite la creación de landings personalizadas para cada uno de los dos casos de uso. 

# Crear landings

https://youtu.be/Vln13FOr9Ms

Como explicábamos anteriormente, EMMA permite el uso de landing para determinadas casuísticas. En ambos casos, tenemos dos opciones a la hora de usar una landing:

1.  Usar la landing por defecto que ofrece EMMA.
2.  Crear nuevas landings personalizadas con el contenido deseado.

## Landing por defecto EMMA

Si empleamos la landing por defecto que ofrece EMMA, se mostrará al usuario final una landing muy sencilla, con el logo de la store correspondiente (App Store en el caso de que un usuario iOS haga clic en el anuncio, PlayStore en el caso de que un usuario Android haga clic en el anuncio), el nombre de la app y un enlace que redireccionará al usuario a las URLs establecidas en el powlink con el que se haya vinculado la landing

## Landings personalizadas

Si prefieres crear tu propia landing personalizada, tan solo tienes que ir a la sección ***Gestión > Landings.*** Desde aquí, podrás dar de alta tantas landings como necesites. 

![](/_landing_1.png)

Crear una landing nueva es muy sencillo, tan solo deberemos hacer clic en el botón ***Nueva landing*** y cubrir el sencillo formulario de creación compuesto por:

-   **Nombre**: en este campo se establece un nombre identificativo para la landing. Es un nombre para uso interno, el usuario final nunca visualizará este nombre.
-   **Tipo:** Selecciona el tipo de landing que quieres crear. Selecciona **Web** si es una landing para anuncios de Google Ads (más info [aquí](https://docs.emma.io/es/google#gesti%C3%B3n-de-landings-para-campa%C3%B1as-con-powlinks)) o **Redirección (Deeplink)** si es una landing para redirección en navegadores no nativos (más info [aquí](https://docs.emma.io/es/adquisicion/apptracker#redireccionamiento-para-navegadores-no-nativos)).
-   **Landing por defecto:** si queremos que esta sea la landing que se vincule por defecto a los powlinks, deberemos habilitar este check.

> Ten en cuenta que sólo puede haber una landing de Redirección (Deeplink) por defecto. La landing de deeplink que esté configurada como landing por defecto, será la que aplique para todos los casos en los que se use. En un inicio, la landing de EMMA estará marcada como la landing por defecto, pero puedes establecer que tu landing personalizada sea la landing por defecto. 

-   **Contenido HTML de la landing:** aquí es donde se establece el HTML deseado. Puedes copiar el HTML de una landing que ya tengas creada y pegarlo aquí. En caso de que en campo **Tipo** hayas seleccionar la opción **Redirección (Deeplink)** deberás descargar nuestra landing de deeplink por defecto desde [aquí](https://emma.io/assets/landing/default_landing_deeplink.html) para poder adaptar el html a vuestras necesidades especificas.

![](/landing_2.png)

Posteriormente podrás editar las landings creadas o desactivarlas para no usarlas más (se pueden volver a activar si vuelve a ser necesario su uso). 

![](/landing_7.png)

### Personalizar la landing web por defecto de EMMA
EMMA pone a disposición de los usuarios el código empleado en la landing web por defecto de EMMA para que se pueda hacer uso de ella realizando las modificaciones pertinentes que apliquen a cada caso de uso si así fuese necesario.

Este es el código empleado en la landing web por defecto de EMMA:
```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Download App</title>
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <style>
    html {
      font-family: 'Roboto', sans-serif;
    }
    body {
      display: flex;
      position: absolute;
      font-size: 16px;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
    }
    #containerStore {
      margin: auto;
      max-width: 450px;
      width: 100%;
      text-align: center;
      padding: 3rem 2rem;
    }
    #containerStore.showios .iositem,
    #containerStore.showandroid .androiditem {
      display: block;
    }
    .appstoreIcon img {
      max-width: 180px;
      width: 100%;
      display: block;
      margin: 0 auto;
    }
    .appName {
      font-size: 2rem;
    }
    .appName > p {
      margin-top: .8rem;
    }
    .appBtn img {
      max-width: 180px;
      width: 100%;
      display: block;
      margin: 0 auto;
    }
  </style>
</head>
<body>
  <div id="containerStore" class="showios">
    {% if not redirect %}
    <div class="appstoreIcon">
      {{ ICON }}
    </div>
    <div class="appName">
      <p>{{ APP_NAME }}</p>
    </div>
    <div class="appBtn">
      {{ LINK_STORE }}
    </div>
    {% else %}
    <span>Redirecting...</span>
    <script>window.location = '{{redirect}}' </script>
    {% end %}
  </div>
</body>
</html>
```

Descripción de marcos de la plantilla a tener en cuenta: 
- **APP_NAME:** Nombre de la app en EMMA.
- **ICON:** Contenido HTML con el icono que corresponde con la plataforma.
Ejemplo:
```<img class='iositem' src='/static/img/iosIcon.png'>```
- **LINK_STORE:** Contenido HTML que contiene una imagen y un link a la store correspondiente.
Ejemplo:
```<a class='iositem' href='https://apps.apple.com/es/app/emma-mobile/id943061465' target='_self'><img src='/static/img/iosBtn.png' ></a>```
- **URL_STORE:** URL de la store que corresponde con la plataforma (Android, iOS, Huawei).
- **redirect:** URL de redirección javascript que aplica cuando no es una plataforma Android o iOS.
- **OS:** 1 para Android, 0 para iOS.
- **DEVICE_TYPE:** Android, iOS, Huawei, web.

# Vincular un Powlink a una Landing

https://youtu.be/k_QEJE3uzdQ

Una vez que ya tenemos la(s) landing(s) creada(s) solo queda vincularla(s) con uno o varios powlinks. Para ello tenemos que ir a **Captación > Apptracker** y crear una nueva fuente en una campaña ya existente o crear una nueva campaña con su correspondiente fuente. 

## Vincular un powlink a una landing Web

Para vincular un powlink a una landing Web, es importante que en el momento de crear la nueva fuente, en el campo ***Proveedor*** seleccionemos ***Google Ads*** para que se muestre el correspondiente selector que nos permitirá elegir la landing intermedia que se quiere vincular con ese powlink. 

![](/landing_5.png)

Una vez creada la fuente para el Proveedor de Google Ads, tan solo queda configurar la campaña en Google Ads y establecer el powlink en el campo de URL final.

## Vincular un powlink a una landing de Redirección (Deeplink)

La landing de Redirección (Deeplink) se habilita a nivel de fuente (powlink). Es decir, unas fuentes (powlinks) pueden tener habilitado el uso de la landing de deeplink y otros no en función de nuestras necesidades. 

Para habilitar el uso de la landing con deeplink, en el momento de creación o edición de la fuente (powlink) debemos ir a la parte final de configuración al apartado de ***Deeplink*** y habilitar el check de *¿Utilizar Landing Deeplink?.* 

![](https://docs.emma.io/configurar_lading_deeplink.png)

Habilitando ese check, nuestro powlink ya usará la landing de deeplink por defecto de EMMA. Si quieres usar tu propia landing de deeplink, puedes ver como hacerlo en el apartado *Crear nuevas landings personalizadas*.