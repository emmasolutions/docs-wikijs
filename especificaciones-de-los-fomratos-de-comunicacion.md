---
title: Especificaciones de los formatos de comunicación
description: Especificaciones de los formatos de comunicación
published: true
date: 2023-09-28T06:43:35.014Z
tags: 
editor: markdown
dateCreated: 2021-05-31T11:58:43.712Z
---

A continuación se detallan las especificaciones requeridas para el desarrollo de la creación de las creatividades en los distintos formatos de comunicación de EMMA:

|     |     |
| --- | --- |
| ![](/01_push.png) | **Rich Push**<br><br>Display rectangular que aparece junto a una Notificación Push cuando se recibe en el dispositivo.<br><br>Se recomienda dejar un [pequeño margen de seguridad](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications) debido a los problemas de adaptación de dispositivos Android ajenos a EMMA. |
| ![](/02_banner.png) | **Banner**<br><br>Display rectangular que aparece en la parte superior o inferior de la pantalla del dispositivo.<br><br>Se recomienda que la imagen tenga unas dimensiones de al menos de 640x100px para asegurar la nitidez de la imagen. Lo importante es manter la proporción, por lo que un formato de 1280x200px tambíen seriviría. <br><br>Hay que tener en cuenta que una imagen inferior a las dimensiones mínimas recomendadas (640x100px) podría verse pixelada y con unas dimensiones superiores la imagen podría pesar demasiado y no llegar a cargarse. <br><br>Se podrá usar una imagen de cualquier otro tamaño y dimensiones; pero automáticamente se ajustará el contenido para que se mantenga la proporción. |
| ![](/03_adball.png) | **AdBall**<br><br>Display circular cuyo contenido aparece cuando el usuario pincha sobre él.<br><br>Se recomienda que la imagen circular tenga unas dimensiones de al menos de 100x100px para asegurar la nitidez de la imagen.<br><br>Se podrá usar una imagen de cualquier otro tamaño y dimensiones; posteriormente EMMA facilitará elegir una sección de la imagen que cumpla las mismas proporciones.<br><br>El contenido tiene unas dimensiones aproximadas de 640x760px, pero esto varía en función del dispositivo, la mejor opción es crear una página responsive que adapte el contenido a las dimensiones disponibles de la pantalla. Con esto se cubrirían todos los diferentes dispositivos del mercado resultando en una mejor experiencia para el usuario. |
| ![](/04_dynamic_tab.png) | **Dynamic Tab**<br><br>Pestaña que se muestra en el menú inferior de la App. Exclusivo de iOS.<br><br>El icono que se muestra en la pestaña es de 16x16px. Un tamaño superior podría descuadrar su posicionamiento. Se recomienda utilizar un PNG con fondo transparente. |
| ![](/05_startview.png) | **StartView**<br><br>Interstitial que aparece cubriendo por completo la pantalla del dispositivo.<br><br>El contenido tiene unas dimensiones mínimas de 500x750px y unas dimensiones recomendables de 640x960px, pero esto varía en función del dispositivo, la mejor opción es crear una página responsive que adapte el contenido a las dimensiones disponibles de la pantalla. Con esto se cubrirían todos los diferentes dispositivos del mercado resultando en una mejor experiencia para el usuario. |
| ![](/06_prisma.png) | **Prisma**<br><br>Interstitial que se muestra en ¾ de la pantalla centrado. Se comporta como un slider y tiene une estilo en 3D. <br><br>Se recomienda un tamaño aproximado de 250-300x550-600px (ancho x alto) para el contenido, pero dependerá un poco del tamaño de la pantalla del dispositivo en cuestión. Además recomendamos que el  contenido de la creatividad esté centrado en la imagen y no vaya pegado a los bordes superiores y laterales. Es decir, se recomienda dejar un borde de seguridad para asegurar que la creatividad se vea bien en la mayor parte de los dispositivos. <br><br>Si la imagen es superior o inferior a las dimensiones recomendadas se realizará un crop center de la imagen para ajustarla al espacio disponible en la pantalla. |

> Para cualquier formato que permita el uso de imágenes recomendamos que estas tengan un peso máximo de 500KB para que un funcionamiento óptimo en el momento de la carga de la imagen en el dispositivo móvil. 
{.is-info}
