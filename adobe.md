---
title: Adobe
description: Integración con Adobe
published: true
date: 2022-03-15T20:45:09.099Z
tags: 
editor: markdown
dateCreated: 2022-03-08T11:28:24.495Z
---

Llevar a cabo una conexión con Adobe permite establecer la configuración necesaria para que posteriormente se pueda realizar el envío postbacks desde EMMA a Adobe con la información pertinente. 

> Recuerda que las postbacks que se envían a Adobe son única y exclusivamente las que se configuran en los flujos de [Rutas de cliente](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys) y se enviarán en base a la configuración que se realice en esta configuración de integración. 
{.is-info}


Para realizar la conexión con Adobe, tan solo tienes que seguir estos pasos: 
1. Ve a la sección **Gestión > Integraciones**.</br><p style="width:400px !important;">![adobbe_i.png](/adobbe_i.png)</p>
2. Edita la integración específica de Adobe y cubre el formulario de conexión. Es el propio Adobe el que debe aportar la información necesaria para realizar esta conexión. En este caso la configuración se divide en 3 secciones:</br><p style="width:600px!important">![adobe_ii.png](/adobe_ii.png) <p/>

### Autenticación

Los campos que debemos configurar en este apartado de Autenticación son: 
- URL de Autenticación
- Client ID 
- Cllient Secret
- Certificado PKEY

### Parámetros de integración

Los campos que se deben configurar en este apartado de Parámetros de integración son: 
- imsHost
- imsOrgId
- techId
- metaScopes

### Configuración de peticiones

- API URL
- Esquema del cuerpo: Los parámetros del esquema tendrán el formato {{nombreParametro}}. Los parámetros conocidos serán reemplazados automáticamente, mientras que el resto de parámetros aparecerán como variables en la configuración de cada postback. Cada cliente tiene un cuerpo del mensaje distinto, este dato es proporcionado por Adobe. Un ejemplo de este cuerpo de mensaje sería:

```json
{
    "header": {
        "schemaRef": {
            "id": "<<SCHEMA ENDPOINT>>",
            "contentType": "application/vnd.adobe.xed-full+json;version=1.0"
        },
        "imsOrgId": "<<IMS ORG ID ADDED IN CONFIG>>",
        "source": {
            "name": "<<SOURCE CODE NAME>>"
        },
        "datasetId": "<<DATA_SET_ID>>"
    },
    "body": {
        "xdmMeta": {
            "schemaRef": {
                "id": "<<SCHEMA URL ENDPOINT>>",
                "contentType": "application/vnd.adobe.xed-full+json;version=1.0"
            }
        },
         "xdmEntity": {"model_name":{"my_company":{"campaignId":{{campaignId}},"campaignType":"{{campaignType}}","interactionResult":"{{interactionResult}}","interactionType":"{{interactionType}}"},"identification":{"id":"{{customerId}}"}},"_id":"{{uuidV4}}","eventMergeId":"Sample value","eventType":"emma.notification","producedBy":"self","timestamp":"{{isoDate}}"}
        }
	}
```
>  Notese que los campos que contienen <<>> significa que son datos dinámicos y tienen que ser reemplazados en el cuerpo del mensaje.
{.is-info}

El contenido dentro de `xdmEntity` depende del modelo de datos que se configura en la plataforma de Adobe, este simplemente es un ejemplo usado para mostrar como sería el cuerpo del mensaje.

Cualquier campo dentro del cuerpo del mensaje que sea configurado con {{}} y no entre dentro de los parametros por defecto de EMMA, saldrá como opción rellenable a la hora de ser configurado en una acción de postback en los Customer Journeys. Todos los parámetros por defecto de EMMA son reemplazados a la hora de realizar el envio de postback. 

En esta [guía](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys) puedes ver más info sobre cómo configurar las postbacks en las rutas para notificar a Adobe y los parámetros que se pueden recoger.