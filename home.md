---
title: Primeros pasos
description: Aprende a configurar tu cuenta y empieza a trabajar con EMMA
published: true
date: 2023-04-27T09:46:10.597Z
tags: 
editor: markdown
dateCreated: 2020-11-26T19:00:21.823Z
---

# Introducción

EMMA es una plataforma de app marketing para la adquisición y retención de clientes que proporciona la funcionalidad para integrar análisis y marketing de aplicaciones. La plataforma ayuda a los clientes a incrementar la participación de los usuarios de tres formas:

-   Analiza el origen de las descargas de tu app mediante la atribución de instalaciones a fuentes de medios.
-   Realiza un seguimiento de las acciones que realizan los usuarios y analiza cómo las personas utilizan el producto.
-   Segmenta a los usuarios según sus acciones y ejecuta campañas dirigidas a estos segmentos.

# Características de la plataforma

La plataforma EMMA tiene 5 partes:

La interfaz de EMMA proporciona la funcionalidad para atribuir el origen de las descargas de tu app para optimizar tus campañas de adquisición, segmentar a los usuarios según sus acciones y propiedades de perfil y ejecutar campañas de comunicación dirigidas a estos segmentos analizando el rendimiento de cada campaña.

El SDK realiza un seguimiento de las acciones de los usuarios dentro de las aplicaciones móviles. También brinda la capacidad de personalizar aplicaciones móviles y su contenido al proporcionar a los clientes acceso a los datos del perfil del usuario.

La API envía datos de eventos o perfiles de usuario desde cualquier fuente a EMMA. Nuestra API también exporta datos de usuario de EMMA para su análisis en herramientas de BI y enriquece la información del cliente en sistemas CRM.

Integraciones con plataformas de publicidad como Google y Facebook, plataformas de analítica como Salesforce y Adobe y plataformas de remarketing como Smadex y Appreciate.

Los postbacks desencadenan flujos de trabajo en los sistemas backend tan pronto como ocurren los eventos de calificación.

# Conceptos principales

Los usuarios, eventos, segmentos, campañas e informes son fundamentales para EMMA, por lo que es importante comprender cada función.

**Usuarios**: después de integrar EMMA SDK, se crea un perfil de usuario para cada persona que inicia la aplicación. El perfil de usuario de EMMA tiene un conjunto de campos predeterminados, como dispositivo(s) y ubicación. También puede ampliar el modelo de datos de perfil de usuario predeterminado agregando atributos personalizados específicos para su negocio.

**Eventos**: el SDK de EMMA realiza un seguimiento de las acciones que realizan los usuarios en su aplicación o sitio web, como un usuario que ve un producto, escucha una canción o realiza una compra. Los eventos están asociados con un perfil de usuario.

**Segmentos**: EMMA ofrece a los usuarios la capacidad de crear segmentos. Un segmento es un grupo de usuarios cuyas acciones o propiedades de perfil de usuario coinciden con un conjunto de criterios que ha definido. Una vez que un usuario ha creado un segmento, puede orientarlos con una campaña o crear un informe para analizarlos.

**Campañas de adquisición:** las campañas de adquisición de EMMA analizan el origen de cada descarga de la app por cada usuario para poder optimizar la adquisición de nuevos usuarios.

**Campañas de comunicación**: las campañas de comunicación de EMMA permiten a los clientes realizar una comunicación a gran escala, pudiendo personalizar y automatizar cada mensaje. EMMA ofrece 10 formatos de mensajería diferentes para llegar a sus usuarios en el canal óptimo.

**Informes**: EMMA brinda la capacidad de crear informes para comprender el efecto de las campañas en sus usuarios. Puede utilizar estos informes para analizar la participación de los usuarios y orientar las decisiones sobre productos.

# Usuarios previos al SDK de EMMA: Nuevos usuarios

https://youtu.be/0K1Z_0aIWuQ

En el caso que ya tengas lista la App para iniciar y decidas empezar a medir tus estadísticas con EMMA, ¿qué pasa con los usuarios que previamente ya utilizan la App? ¿Cuándo iniciará EMMA el seguimiento de estos? ¿Cómo podrás identificar a esos usuarios en tus estadísticas de la App?<p style="width:700px">![](/usuarios_previos_sdk_emma.png)</p>

Al instalar un nuevo SDK **todos los usuarios de la versión anterior aparecerán como nuevos usuarios una vez actualicen la aplicación.** Este es un proceso común e inevitable que permite detectar y guardar todos los usuarios medidos, los que realmente serán los nuevos usuarios y los que no.

En base a eso, debes tener en cuenta que los primeros días de análisis en EMMA el porcentaje de nuevos usuarios será mayor que en períodos futuros.

Como se puede ver en el ejemplo visual de una aplicación (representado en la imagen previa) si tenemos un número de usuarios existentes antes de realizar la integración del SDK EMMA, estos se contarán como nuevos usuarios cuando abran la aplicación una vez actualizada a la nueva versión. Así, el tráfico existente, generado a través de los markets, se sumarán como nuevos usuarios.

Por desgracia, la separación real entre los nuevos usuarios y los ya existentes no es posible con las tecnologías actuales así, a fin de evitar datos devaluados, se recomienda esperar al menos 15 días a partir de la actualización de la versión de la aplicación para empezar a considerar el nuevo tráfico.

# Empezando

Lo primero que necesitas para poder empezar a usar EMMA es crear una cuenta. A continuación te explicamos detalladamente cómo hacerlo.

-   Haz clic [**aquí**](https://ng.emma.io/es/register) y completa el formulario de registro para crear tu cuenta.
- Selecciona el plan contratado con EMMA y el volumen de usuarios activos mensuales correspondientes de tu plan.<p style="width: 400px">![registro_11.png](/registro_11.png)</p>

-   Establece el correo electrónico con el que quieres usar EMMA y una clave de acceso. A continuación haz clic en el botón **Crear cuenta**. <p style="width: 400px">![registro_12.png](/registro_12.png)</p>

-   Establece la información que se pide para poder configurar tu cuenta. Una vez configurado haz clic en el botón **Continuar**.
    -   **Nombre:** Introduce tu nombre. Será usado como tu nombre de Usuario.
    -   **Apellidos:** Introduce tus apellidos. Serán usandos para configurar tu perfil.
    -   **Empresa:** Introduce el nombre de tu empresa. Esta información será usada para la facturación.
    -   **Nombre de la app:** Establece el nombre que tendrá la app en EMMA.
    -   **Dirección:** Establece la dirección fiscal de la empresa. Esta información será usada para la facturación.
    -   **Código postal:** Establece el código postal de la empresa. Esta información será usada para la facturación.
    -   **Localidad:** Introduce la localidad en la que tiene sede la empresa. Esta información será usada para la facturación.
    -   **País:** Establece el país en el que tiene sede la empresa. Esta información será usada para la facturación. </br><p style="width: 400px">![registro_14.png](/registro_14.png)</p>

-   Introduce la información de una tarjeta de crédito. La facturación de EMMA se realizará contra la tarjeta configurada en esta apartado. También deberás aceptar los Términos de uso y la Política de privacidad de EMMA para poder contunuar con el proceso de creación de tu cuenta.<p style="width: 500px">![registro_15.png](/registro_15.png)</p> 
> EMMA permite la configuración de tarjetas VISA y MasterCard compuestas por 16 dígitos (4-4-4-4) y un CVC o CVV de 3 dígitos. Asímismo, también se permite configurar una tarjeta American Express (AMEX) con el formato de 15 dígitos (4-5-6) y un CVC o CVV de 4 dígitos. {.is-info}

-   Si todos los datos han sido introducidos de manera correcta, podrás ver un mensaje de validación en tu pantalla. A continuación te enviaremos un email para validar tu cuenta. <p style="width: 400px">![registro_16.png](/registro_16.png)</p>

-   Valida tu cuenta de EMMA con el enlace que se envía en el email. Este punto es muy importante para evitar problemas de acceso a EMMA.  
     ![registro_6-blur.png](/registro_6-blur.png)

-    Una vez hayas validado tu cuenta, recibirás otro email de Bienvenida. Sigue las instrucciones del email y ¡listo!. Enhorabuena, has creado tu cuenta de EMMA y puedes empezar a hacer uso de ella en cuanto quieras.  
     ![registro_7.png](/registro_7.png)