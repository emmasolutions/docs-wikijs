---
title: Perfil de usuario
description: Perfil de usuario
published: true
date: 2023-03-08T09:43:03.742Z
tags: 
editor: markdown
dateCreated: 2022-11-28T12:29:18.331Z
---

Con la nueva pantalla de perfil de usuario de EMMA, podrás ver información de los dispositivos en base al id de cliente de estos. 

Para acceder al perfil del usuario, tienes que ir a Comportamiento > Gente y realizar una consulta con total normalidad.<p style="width:300px">![perfil_usuario_1.png](/perfil_usuario_1.png)</p>

Una vez realizada la consulta, en la tabla de resultados, podrás ver el id de cliente de cada dispositivo en la columna con el mismo nombre. Tan solo tendrás que hacer clic en el id de cliente deseado para acceder al perfil del usuario.<p style="width:800px">![perfil_usuario_2.png](/perfil_usuario_2.png)</p>

En este perfil del usuario, vamos a poder ver información relevante del id de cliente así como todos los dispositivos vinculados a ese id. En esta pantalla no vamos a poder seleccionar el rango de fechas del que queremos consultar los datos.

En primer lugar podemos ver información de tres KPIs relevantes:<p style="width:1000px">![perfil_usuario_3.png](/perfil_usuario_3.png)</p>
- **ID de cliente:** ID de cliente que se está consultando
- **Visto por última vez:** Fecha en la que se realizó el KPI visto por última vez. Este KPI refleja actividad del usuario en la app, pero no tiene que corresponder con un login o una apertura, sino que puede corresponderse con eliminar la app de background por ejemplo y se coge la información del dispositivo con actividad más reciente. 
- **Nº de dispositivos:** Este KPI muestra el número de dispositivos totales en base de datos vinculados al ID de cliente que estamos consultando. 

> En caso de ser necesario, haciendo clic en el botón ***eliminar usuario*** que se ve junto al id de cliente, se podrá eliminar el usuario. Es decir, se eliminará de base de datos todos los dispositivos vinculados al id de cliente en cuestión.<p style="margin:0 auto; width:272px">![perfil_usuario_3.png](/perfil_usuario_11.png)</p>
{.is-info}

La siguiente información que podemos ver es, por un lado *Datos de usuario* y por otro lado una gráfica de *Eventos realizados durante los últimos 7 días*. <p style="width:350px">![perfil_usuario_4.png](/perfil_usuario_4.png)</p>
En los datos de usuario vamos a poder ver información de determinado eventos por defecto de EMMA. 
- **Email:** Muestra el email vinculado al último dispositivo que ha tenido actividad en la app. 
- **Localización:** Muestra la localización del último dispositivo que ha tenido actividad en la app. 
- **Latitud:** Muestra la latitud del último dispositivo que ha tenido actividad en la app.
- **Longitud:** Muestra la longitud del último dispositivo que ha tenido actividad en la app.
- **Registrado:** Muestra la fecha de registro más antigua. Es decir, el primer dispositivo que se registro con el id de cliente consultado. 
- **Primer login:** Muestra la fecha del primer login más antiguo. Es decir, la fecha del primer dispositivo que hizo primer login con el id de cliente consultado. 
- **Último login:** Muestra la fecha del último login más reciente. Es decir, la fecha del último dispositivo que hizo login con el id de cliente consultado.
- **Días 1ª conversión:** Muestra el tiempo transcurrido desde la instalación hasta que se ha realizado la primera conversión. Si no se mide el evento conversión por defecto de EMMA, este KPI aparecerá vacío. 
- **Primera conversión:** Muestra la fecha de la conversión más antigua. Es decir, la fecha del primer dispositivo que hizo una conversión. Si no se mide el evento conversión por defecto de EMMA, este KPI aparecerá vacío. 
- **Última conversión:** Muestra la fecha de la conversión más antigua. Es decir, la fecha del último dispositivo que hizo una conversión. Si no se mide el evento conversión por defecto de EMMA, este KPI aparecerá vacío. 

En la gráfica de Eventos realizados, vamos a poder ver los eventos realizados por TODOS los dispositivos vinculados al id de cliente consultado en los últimos 7 días. 
> El rango de fechas no se puede modificar, la información que mostrará esta tabla será siempre de los últimos 7 días y datos de todos los dispositivos. 
{.is-info}

En el selector, podremos seleccionar hasta 5 eventos simultáneamente de los que queremos consultar la actividad reciente para poder ver la evolución de los mismos en la gráfica. <p style="width:800px">![perfil_usuario_5.png](/perfil_usuario_5.png)</p>

Inmediatamente a continuación, podremos ver dos tablas, una con información de las etiquetas del último dispositivo con actividad en la app y la otra con información de todas las comunicaciones que han recibido/visto/clicado TODOS los dispositivos en los últimos 7 días. <p style="width:400px">![perfil_usuario_6.png](/perfil_usuario_6.png)</p>

En la tabla de Etiquetas, podremos ver los valores de cada etiqueta existente para el **último dispositivo que ha accedido a la app**. Cada vez que un dispositivo tenga actividad en la app, la información de las etiquetas se actualizará a la información del dispositivo más reciente. 
En caso de ser necesario, se podrá realizar una búsqueda de alguna etiqueta en concreto que se quiera analizar. <p style="width:800px">![perfil_usuario_7.png](/perfil_usuario_7.png)</p>

Mientras que en la tabla de Comunicaciones podremos ver el listado de todas las comunicaciones que han recibido entre TODOS los dispositivos vinculados al id de cliente analizado los últimos 7 días. Se podrá saber que se ha recibido una comunicación en concreto, pero no que dispositivo en particular. 
En caso de ser necesario, se puede navegar por la paginación hasta llegar a la comunicación deseada o directamente buscar esta por nombre desde la barra de buscador. 

Ya por último, en la parte final de la pantalla podemos ver información relativa a cada dispositivo vinculado o bien ver todos los dispositivos en una tabla para poder ver información relevante de eventos y etiquetas de cada uno y poder descargar la información. 
Por defecto, al acceder a esta sección, aparece seleccionado el último dispositivo con actividad, pero podemos cambiar en el selector para seleccionar otro o simplemente ver todos los dispositivos en una tabla. <p style="width:850px">![perfil_usuario_8.png](/perfil_usuario_8.png)</p>

La información que podemos ver de cada dispositivo es la siguiente: <p style="width:850px">![perfil_usuario_9.png](/perfil_usuario_9.png)</p>
- Modelo dispositivo y versión del sistema operativo. 
- Identificado dispositivo. GAID en el caso de Android (Google Advertising ID) o IDFA o IDFV en el caso de iOS(Identifier for advertisment).  
- Compañía de telefonía, operador que tenga el dispositivo (proveedor).
- Red, ya sea 3G, 4G, WIFI…
- IP del dispositivo en el momento de la última conexión. 
- ID EMMA generado aleatoriamente. 
- EMMA Build instalada en la app (versión del SDK de EMMA).
- Nombre de la aplicación.  
- Versión de la aplicación. 
- Idioma dispositivo.
- Fecha de instalación de la app en el dispositivo en cuestión. 
- En caso de ser una instalación de campaña del Apptracker de EMMA, se mostrará el nombre de la campaña desde la que ha instalado la aplicación.
- En caso de ser una instalación de campaña del Apptracker de EMMA, se mostrará el nombre de la fuente desde la que se ha instalado la aplicación. 

> Con el botón ***Eliminar dispositivo*** que se muestra debajo del selector de dispositivo, podemos eliminar un dispositivo concreto de la base da datos y toda la información vinculada a este. 
{.is-info}

Ya por último, si seleccionamos la vista de tabla, podremos ver todos los dispositivos vinculados y toda la información existente en base de datos (eventos, etiquetas e información del dispositivo) tal y como se puede hacer desde explorador/gente. Puedes ver más detalles sobre la información que se visualiza aquí en este [artículo](https://docs.emma.io/es/analisis-de-comportamiento/segmentos#exporta-tus-datos). <p style="width:850">![perfil_usuario_10.png](/perfil_usuario_10.png)</p>
Además, en caso de ser necesario, se puede realizar una descarga de la información que se está mostrando en la tabla en un formato tipo .csv. 



