---
title: Parámetros de comunicación
description: Parámetros de comunicación
published: true
date: 2022-06-17T12:09:58.711Z
tags: 
editor: markdown
dateCreated: 2022-04-28T18:12:58.748Z
---

Los parámetros de comunicación de EMMA te permitirán crear los parámetros que necesites para poder vincularlos posteriormente a las comunicaciones, tanto [In-App](https://docs.emma.io/es/mensajes-inapp#propiedades) como [Out-App](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications#propiedades), que crees para poder usar la información de estos para aplicar agrupaciones y filtros en el [informe de rendimiento](/es/informe-rendimiento) de EMMA.

# Crear parámetros de comunicación

Crear parámetros de comunicación es muy sencillo, tan solo tienes que seguir los pasos listados a continuación:

- Haz login en tu cuenta de [EMMA.](https://ng.emma.io/es/login?returnUrl=%2Findex)

- Ve a la sección ***Gestión*** > ***Parámetros de comunicación.*** <p style="width:700px;">![](/parametros_comunicacion_i.png)</p>

- Haz clic en el botón ***+ Nuevo Parámetro.*** <p style="width:700px;">![](/parametros_comunicacion_ii.png)</p>
- Cubre el formulario de creación de parámetros.<p stlye="width:900px">![parametros_comunicacion_7.png](/parametros_comunicacion_7.png)</p>

	-   **Nombre:** Establece un nombre identificativo para tu parámetro. Este nombre será el que aparece en el selector al [crear una nueva comunicación](/es/mensajes-inapp).
	-   **Descripción:** Opcionalmente puedes establecer uns descripción para tu parámetros. Está descripción aparecerá en la tabla de resumen de los parámetros y te ayudará a saber a qué hace referencia dicho parámetro.
	-   **Obligatorio en la comunicación:** Establece si el parámetro va a ser de obligatorio uso en la comunicación u opcional. Si marcamos que un parámetro sea obligatorio, estaremos obligados a cubrirlo cada vez que creamos una nueva comunicación.
	-   **Tipo:** Establece la tipología de tu parámetro. Los parámetros puede ser de tipología:
        -   **Campo abierto:** En el momento de seleccionar el parámetro al crear la comunicación, el usuario podrá establecer el valor deseado. 
        -   **Selección de valores:** Al crear el parámetros, se deben establecer los valores del parámetros. De esa manera, en el momento de seleccionar el parámetro al crear la comunicación, el usuario solo podrá seleccionar uno de los valores añadidos al crear el parámetro.
-   **\+ Añadir valor:** Añade los valores disponibles para el parámetro. 
- **Usable para histogramas:** Si habilitamos el check nos permitirá habilitar un parámetro para usarlo para filtrar en los histogramas. Sólo se podrá habilitar este check en aquellos parámetros de tipo selector y obligatorios. Así mismo, sólo se podrán habilitar un máximo de 3 parámetros para ser usados en histogramas.![parametros_comunicacion_6.png](/parametros_comunicacion_6.png)

Una vez hayamos habilitado un parámetro para ser usado en el histograma, este empezará a mostrar datos desde el momento en el que se ha habilitado, no es retroactivo. 
Si deshabilitamos un parámetro de su uso para histogramas y en el futuro lo volvemos a habilitar, no perderemos el histórico, pero el tiempo que ha estado “pausado” no tendremos información. 

En la tabla resumen de los parámetros podemos ver la siguiente información:
![parametros_comunicacion_8.png](/parametros_comunicacion_8.png)

|     |     |
| --- | --- |
| **Nombre** | Nombre que se le ha dado al parámetro en el momento de su creación. |
| **Descripción** | Descripción del parámetro establecida en el momento de su creación. Es un campo opcional, en caso de que no se haya cubierto, no se mostrará nada. |
| **Tipo** | Tipo establecido para el parámetro en cuestión. Puede ser “Selección de valores” o “Campo abierto”. |
| **Obligatorio** | Si el parámetro ha sido marcado como obligatorio o no. En caso de ser obligatorio veremos un check y en caso de no ser obligatorio no se verá nada. |
| **Para histogramas** | Si el parámetro ha sido marcado para histogramas o no. En caso de ser para histogramas veremos un check y en caso de no ser para histogramas no se verá nada. |
| **Valores posibles** | En el caso de los parámetros de tipo “Selección de valores” se mostrarán los valores configurados. |

        
# Editar parámetros
En caso de necesitar realizar modificaciones en alguno o varios parámetros, tan solo tenemos que seguir estos pasos para proceder a su edición: 
- Haz login en EMMA. 
- Ve a la sección ***Gestión > Parámetros de comunicación***.
- Haz *mouse over* sobre el menú contextual y selecciona la opción ***Editar***.![parametros_comunicacion_iv.png](/parametros_comunicacion_iv.png)
- Desde la pantalla de edición del parámetro, haz las modificaciones que necesites. 
- Haz clic en ***Guardar Parámetro***.

# Borrar parámetros
En caso de necesitar eliminar alguno o varios parámetros, tan solo tenemos que seguir estos pasos para proceder a su eliminación: 
- Haz login en EMMA. 
- Ve a la sección ***Gestión > Parámetros de comunicación***.
- Haz *mouse over* sobre el menú contextual y selecciona la opción ***Borrar***. Ten en cuenta, que una vez que hagas clic en este bbotón el parámetro será eliminado al momento.</br>![parametros_comunicacion_v.png](/parametros_comunicacion_v.png)

