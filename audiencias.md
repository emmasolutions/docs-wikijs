---
title: Audiencias
description: audiencias
published: true
date: 2023-08-07T10:45:54.316Z
tags: 
editor: markdown
dateCreated: 2021-06-29T10:54:10.054Z
---

![e.a.-audiencias.png](/e.a.-audiencias.png)

Con la funcionalidad de Audiencias de EMMA podrás crear segmentos de usuarios para usarlos posteriormente en tus comunicaciones In-App y Out-App y poder impactar a los usuarios deseados. Con Audiencias tendrás la ventaja de poder crear primero todos los segmentos de usuarios que necesites y vincularlos posteriormente a una comunicación y poder lanzar esta sin necesidad de esperar a que el segmento esté 100% procesado. Mucho más rápido y dinámico.

# Crear audiencias

Para crear una nueva audiencia tan solo tienes que seguir estos pasos:

-   Accede a la sección **Comportamiento > Audiencias.**

![](/audiencias_i.png)

-   Haz clic en el botón **\+ Nueva Audiencia.**

![](/audiencias_ii.png)

-   Cubre el formulario de creación de audiencia.

![](/audiencias_iii.png)

-   **Nombre:** Establece un nombre identificativo para tu audiencia.
-   **¿Audiencia de prueba?:** Habilita este check si usas [prevalidación de campañas](/es/prevalidacion-de-campa%C3%B1as) y quieres que un usuario con rol *Autor* pueda usar esta audiencia para crear campañas.
-   **Tipo:** Selecciona la tipología de la audiencia. Puede ser **dinámica o estática.** Una audiencia **dinámica** está basada en filtros y los usuarios irán entrando y saliendo de la misma a medida que cumplan o dejen de cumplir los filtros establecidos. Una audiencia **estática** está basada en un archivo CSV y los usuarios que componen la audiencia son los que cumplen los criterios en el momento de la creación de la Audiencia y no podrán entrar ni salir usuario de la misma.
-   **Origen:** Selecciona el origen de la audiencia.   
    Si has seleccionad una audiencia de tipo **dinámica** podrás seleccionar entre los siguientes orígentes:
    -   **Segmento de EMMA:** Está basada en los filtros de EMMA y los usuarios irán entrando y saliendo de la audiencia a medida que cumplan o dejen de cumplir los filtros establecidos. Añade los filtros necesarios para completar tu audiencia. La funcionalidad de Audiencias no permite el uso de subfiltros en la creación de los segmentos. En está [guía](/es/analisis-de-comportamiento/segmentos) puedes ver más al respecto sobre como usar los filtros.
    -   **S3:** Para poder usar la audiencia dinámica de tipo S3 es necesario tener una conexión con S3 previamente realizada. [Aquí](https://docs.emma.io/es/audiencias#integraci%C3%B3n-con-s3) puedes ver cómo llevar a cabo dicha conexión. Puedes alojar un CSV de tu audiencia en un S3 y vincularlo en EMMA con un campo de identificación. Los campos de identificación pueden ser:
        -   ID Cliente
        -   ID dispositivo
        -   EMMA ID
        -   Etiquetas personalizadas que tengan formato de texto o numérico. Las etiquetas formatadas como fecha no podrás ser empleadas.  
            Puedes ver más información sobre qué significan los distintos campos de identificación [aquí](/es/analisis-de-comportamiento/segmentos#filtros-disponibles).
    -   **HTTPS:** Puedes alojar un CSV de tu audiencia en un servidor y vincularlo en EMMA con un campo de identificación. Los campos de identificación pueden ser:
        -   ID Cliente
        -   ID dispositivo
        -   EMMA ID
        -   Etiquetas personalizadas que tengan formato de texto o numérico. Las etiquetas formatadas como fecha no podrás ser empleadas.  
            Puedes ver más información sobre qué significan los distintos campos de identificación [aquí](/es/analisis-de-comportamiento/segmentos#filtros-disponibles).
    -   **CSV:** Permite crear una audiencia dinámica en base a un CSV cargado. La audiencia dinámica de tipo CSV está pensada única y exclusivamente para usarse con el tipo de identificador de **ID Cliente**. De esta manera, por ejemplo si un id de cliente está en el CSV cargado, y dentro de 3 meses instala la app en otro dispositivo e inicia sesión con sus credenciales (mantiene el mismo id de cliente), a diferencia de las audiencias estáticas, el usuario visualizará las comunicaciones pertinentes en ese nuevo dispositivo.
     -   **Fuente externa:** Permite crear una audiencia a la que se envían usuarios a través de la API. En este tipo de audiencias los datos se reciben en tiempo real. Además, estas audiencias están disponibles única y exclusivamente para ser empleadas como fuente de entrada en [rutas de cliente](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys#fuentes-de-entrada). Para alimentar una audiencia externa es necesario hacerlo conectandose a través de un endpoint de nuestra API, añadiendo el customer ID y en el caso que fuese necesario los tags correspondientes. Puedes consultar la documentación de nuestro endpoint [aquí](https://ws.emma.io/swagger/#/Audiences/put_audiences__id__add).
    -   **FTP:** Puedes enlazar un CSV alojado via FTP, para ello tan solo tienes que cubrir la siguiente información:
        -   **Campo de identificación:** Selecciona el campo de identificación deseado entre:
            -   ID Cliente
            -   ID dispositivo
            -   EMMA ID
            -   Etiquetas personalizadas que tengan formato de texto o numérico. Las etiquetas formatadas como fecha no podrás ser empleadas.  
                Puedes ver más información sobre qué significan los distintos campos de identificación [aquí](/es/analisis-de-comportamiento/segmentos#filtros-disponibles).
        -   **Host:** Establece el dominio en el que está alojado el CSV (por ejemplo [**ftp.dominio.com**](http://ftp.dominio.com)).
        -   **Puerto:** Establece el número de puerto que usa el servidor de FTP. Por defecto el puerto es 21, pero se puede modificar por el puerto que corresponda.
        -   **Usuario:** Establece el usuario que tiene acceso al FTP donde se encuentra alojado el CSV para que EMMA pueda acceder al archivo.
        -   **Contraseña:** Establece la contraseña de acceso al FTP donde se encuentra alojado el CSV para que EMMA pueda acceder al archivo.
        -   **Ruta del fichero:** Establece la ruta del archivo CSV alojado en el FTP para que EMMA pueda acceder a ese archivo en concreto. Ejemplo: **/miCarpeta/misArchivos/fichero.csv** (es importante añadir la extensión del archivo, .csv)

> Es importante que nunca se elimine el archivo alojado en el FTP que está vinculado a una audiencia, ya que sino, la audiencia dará error y se tendrá que volver a crear una audiencia nueva cuando se vuelva a alojar el archivo. </br>   
En lugar de eliminar el archivo, se debe sustituir por uno que tenga el mismo nombre y que esté vacío, de esta menera, la audiencia no da error y cuando se cargue el archivo con datos nuevamente se podrá seguir empleando la misma audiencia. {.is-danger}



-   Si has seleccionado una audiencia de tipo **estática** deberás seleccionar como **origen** de la audiencia la opción CSV. Recuerda que la audiencia estática se calcula una única vez en el momento de su creación y se hace una traducción de los ids cargados al número de dispositivos que cumplen esa condición en el momento exacto en el que se ha cargado. Si 2 días después de cargar el CSV un usuario cumple con uno de los ids cargados no visualizará la comunicación ya que la audiencia estática se calcula sólo en el momento de su carga y devuelve los dispositivos que cumplen en ese momento exacto. Deberás seleccionar el campo de identificación en el que está basado el archivo CSV que vas a cargar entre:
    -   ID cliente
    -   ID dispositivo
    -   EMMA ID  
        Por último deberás cargar el fichero CSV que se desea emplear en la audiencia en cuestión.
-   El último paso para guardar la audiencia creada es hacer clic en el botón **Guardar Audiencia.**

Es importante que tengas en cuenta que una vez creadas las audiencias no podrán ser editadas ni clonadas.

> La carga de archivos **CSV** está permitida en las audiencias **dinámicas** y **estáticas**, pero en el caso de las audiencias **dinamicas** esta se ve **limitada** a CSVs de máximo **5.000 líneas**.  </br>
Si tu CSV es **superior**, deberás crear una audiencia **estática** con ese CSV. Recuerda que en el caso de las audiencias **estáticas**, el archivo CSV esta limitado a 500Mb. </br> 
Los filtros **ID Cliente**, **ID Dispositivo** y **EMMA ID** están disponibles para su uso tanto en audiencias **dinámicas** como en audiencias **estáticas**. {.is-info}

> El archivo CSV que subas debe tener un formato adecuado para que sea procesado. Es decir, tiene que ser un archivo **.CSV** que cuente con una columna de datos identificativos. El archivo que vayas a subir a Audiencias de EMMA deberá contener tan sólo una columna de datos. {.is-warning}

# Vincular audiencias

Si tienes un CSV de más de 5.000 líneas y has tenido que crear una audiencia estática pero quieres usar ese CSV junto a unos filtros para obtener el público objetivo, tan solo tienes que seguir estos pasos:

-   Crea una audiencia de tipo **estática** como se explica en el paso anterior.
-   Crea una audiencia de tipo **dinámica** y selecciona el filtro **“Usuarios”.**

![](/audiencias_xi.png)

-   Selecciona el operador que se ajuste al tipo de audiencia que queremos crear entre **“están en la audiencia”** o **“no están en la audiencia”.**
-   Selecciona el CSV que quieres usar para crear tu nueva audiencia dinámica.

![](/audiencias_xii.png)

-   Añade los filtros que consideres oportunos para segmentar a los usuarios.

![](/audiencias_xiii.png)

Y listo, ya tienes tu audiencia estática vinculada a una audiencia dinámica con filtros.

# Usar audiencias en las comunicaciones

Una vez que hemos creado las audiencias ya solo queda usarlas en las comunicaciones. Para ello, basta con seguir el flujo de creación de comunicaciones habitual (más info al respecto [aquí](/es/mensajes-inapp)) y en el paso **Objetivo** seleccionaremos una de las audiencias creadas previamente. Podemos usar indistintamente una audiencia dinámica o estática siempre que esté en un estado apto para ser usada.

> Ten en cuenta que en las comunicaciones OutApp (Push e Email) sólo está permitido el uso de audiencias procesadas al 100%. Si tu audiencia no aparece en el listado espera hasta que este 100% cargada. {.is-warning}

En el selector de **Segmentación** tendremos un listado con todas aquellas audiencias que están listas para ser usadas en una comunicación. Las audiencias que estén en estado **Nueva** no aparecerán en este listado. Sólo podremos usar aquellas audiencias que estén en los estados **Lista** o **En** **progreso**.

> Las audiencias marcadas como **Test** para que puedan ser usadas por usuarios *Autor* aparecen marcadas con el texto **\[Test\]** delante del nombre de la audiencia. </br> 
Las audiencias que se encuentren **en progreso** aparecen marcadas con un **icono** de un **reloj de arena**, pero podemos usarlas sin ningún problema para lanzar nuestras comunicaciones. {.is-info}

![](/audiencias_xiv.png)

Si usamos una audiencia en estado **Lista**, en el paso de **Confirmación** tendremos un mensaje que nos dirá el número de dispositivos a los que impactaremos. Ten en cuenta que si se trata de una audiencia dinámica este dato puede ir variando a medida que los usuarios cumplan o dejen de cumplir las condiciones de la audiencia dinámica seleccionada.

![](/audiencias_xvii.png)

Si seleccionamos una audiencia en estado **En progreso**, es decir, que se está calculando, en el paso **Confirmación** tendremos un mensaje que nos avisará de esta casuística.

![](/audiencias_xv.png)

Si hacemos clic en el botón **Enviar mensaje**, inmediatamente saltará una ventana emergente que nos avisa que la audencia está siendo procesada. En este momento tenemos dos opciones:

1.  Seleccionar la opción **Esperar** y cancelar la comunicación hasta que la audiencia esté totalmente calculada.
2.  Seleccionar la opción **Lanzar**. Con esta opción no tendremos que esperar y la campaña se lanzará inmediatamente impactando a los usuarios que ya estén en la audiencia e irá impactando al resto de usuarios confirme aparezcan en la audiencia.

![](/audiencias_xvi.png)

# Pantalla Audiencias

En la pantalla de audiencias, además de crear nuevas audiencas, podremos ver una tabla con todas las audiencias creadas hasta el momento.  
Por defecto se muestran todas las audiencias creadas, pero si así lo queremos, podemos añadir un filtro que muestre solo las audiencias que hemos creado nosotros mismos.

![audiencias_viii.png](/audiencias_viii.png)

Asimismo, podemos añadir otro tipo de filtrado como puede ser el estado o el origen de la audiencia.

![origen_1.png](/origen_1.png)

Al filtrar por el orgien, podemos elegir entre los siguientes valores:

-   **CSV (estático)**: Filtra las audiencias cuyo origen sea un archivo CSV estático.
-	**CSV (dinámico)**: Filtra las audiencias de tipo dinámica cuyo origen sea un archivo CSV dinámico.
-   **Segmento de EMMA**: Filtra las audiencias de tipo dinámica cuyo origen sea un segmento de EMMA.
-   **FTP**: Filtra las audiencias de tipo dinámica cuyo origen sea un archivo alojado via FTP.
-	**HTTPS**: Filtra las audiencias cuyo origen sea una url HTTPS. 
-   **S3**: Filtra las audiencias de tipo dinámica cuyo origen sea un archivo alojado en un S3.
-	**Fuente externa**: Filtra las audiencias de tipo dinámica cuyo origen sea una fuente externa.

Al filtrar por el estado de la audiencia, podemos elegir entre las siguientes opciones:

-   **Nueva**: Filtras las audiencias que tengan como estado nueva. Este estado se da cuando la audiencia es nueva y todavía no está en progreso.
-   **En progeso**: Filtra las audiencias que todavía están siendo procesadas por el sistema.
-   **Lista**: Filtra las audiencias que ya han temrinado de procesarse y están listas para usarse en las comunciaciones.
-   **Inactiva**: Filtras las audiencias que se han inactivado debido a que no están en uso. Puedes ver más información sobre las audiencias inactivas [aquí](/es/audiencias#audiencias-inactivas).
-   **Error**: Filtra las audiencias que han dado error al intentar procesarlas. Podría ser porque se ha metido mal la url del FTP, por el archivo CSV cargado no tenga un formato válido...

En la tabla resumen de las audiencias podremos ver la siguiente información referente a las mismas:

![audiencias_v.png](/audiencias_v.png)

-   **Origen:** Muestra el origen de la audiencia con un icono. Las audiencias de origen estático (CSV) estarán marcadas con el icono de CSV. Las audiencias con origen dinámico de tipo segmento (filtros) estarán marcadas con el icono de EMMA. Las audiencias con origen dinámica de tipo FTP o S3 también estarán marcadas con un icono específico.
-   **Compartido:** Si la audiencia está siendo compartida con Facebook o con Google, en esta columna se mostrará el icono correspondiente a cada red para poder identificar qué audiencias se están compartiendo y en qué plataforma. Puedes ver más información sobre como compartir audiencias con Facebook [aquí](/es/guias-de-integracion/facebook#compartir-audiencias-con-facebook-opcional) y con Google [aquí](/es/google#compartir-audiencias-con-google-opcional).
-   **ID:** ID interno de EMMA para identificar cada una de las audiencias creadas.
-   **Fecha de creación:** Fecha en la que la audiencia ha sido creada.
-   **Fecha de actualización:** Fecha en la que la audiencia se ha actualizado. Las audiencias se actualizan una vez al día.
-   **Audiencias:** Nombre que se le ha dado a la audiencia en el momento de la creación.
-   **Test:** Si la audiencia ha sido marcada como audiencia de test, en esta columna aparecerá marcada con un check.
-   **Estado:** Estado en el que se encuentra la audiencia. Puede ser Nueva, En progreso, Lista, Inactiva, Error.
-   **Proceso:** Estado el procesamiento de la audiencia. Mientras no sea 100% la audiencia no pasará al estado Lista.
-   **Dispositivos:** Número de dispositivos que cumplen los criterios de la audiencia.
-   **Usuarios:** Número de usuarios únicos (ids de cliente únicos) que cumplen los criterios de la audiencia.
-   **Con push:** Número de dispositivos que están activos para el envío de notificaciones push.
-   **Con email:** Número de dispositivos de los que tenemos información del email para poder hacer envío de emails desde EMMA.
-   **Exceso de push:** Número de dispositivos que han excedido el límite de push. Solo aplica si se ha establecido un límite de push [configuracion](/es/configuracion).

En caso de necesitarlo, podremos descargar el contenido de la tabla haciendo clic en el botón **Descargar datos**. Una vez hagamos clic se descargará automáticamente la tabla con las columnas que tengamos seleccionadas en ese momento.

Además, si queremos ver la configuración concreta que tiene una audiencia o directamente eliminarla también lo haremos desde esta sección.  
Para ello tendremos que hacer mouse over sobre el menú contextual que se muestra a la izquierda de la tabla y seleccionar la opción deseada.

![audiencias_xx.png](/audiencias_xx.png)

# Audiencias inactivas

Para evitar que el sistema de audiencias se sature y asegurar un mejor funcionamiento y estabilidad a tus comunicaciones, EMMA procederá a inactivar aquellas audiencias que no estén siendo usadas en una comunicación o no estén siendo compartidas con Google y/o Facebook.

> Solo se marcarán como **inactivas** aquellas audiencias que no estén vinculadas a ninguna comunicación activa o programada siempre y cuando hayan transcurrido **14** **días** desde su **creación**. </br>  
Las audiencias marcadas como **Test** siempre estarán **activas**, independientemente de que no estén vinculadas a una comunicación activa o programada. </br>  
Asimismo, las **audiencas** compartidas con **Google** y/o **Facebook** tampoco serán marcadas como inactivas. {.is-info}

Las audiencias se mantendrán como audiencias activas mientras cumplan una de estas condiciones:

-   La audiencia está vinculada a una comunicación automática para reglas y tiene vinculada una regla activa o programada en el futuro.
-   La audiencia está en una comunicación clásica activa actualmente o programada para un futuro.
-   La audiencia está vinculada a una notificación push o email que esté activo o programado para un futuro.
-   La audiencia está siendo compartida con Google o Facebook. En este caso, la audiencia nunca se marcará como inactiva mientras esté siendo compartida.
-   La audiencia está vinculada en otra audiencia de tipo dinámica de segmento que se considera activa, es decir, cumple alguna de las anteriores condiciones de este listado.
-   Las audiencias de Test siempre serán audiencias activas.

Que una audiencia se marque como inactiva no implica que no se pueda volver a activar. Recuerda que las audiencias se marcan como inactivas cuando ya no están siendo usadas de manera directa, pero si queremos volver a activar una audiencia inactiva tan solo tenemos que hacer clic en el botón Reactivar desde el menú contextual de la audiencia en cuestión.

La audiencia tardará en reactivarse lo que tarde en calcularse la audiencia nuevamente. Este tiempo de espera puede variar dependiendo del volumen de cada audiencia.

![audiencia_inactiva.png](/audiencia_inactiva.png)

Además podremos saber qué audiencias están marcadas como inactivas ya que en la propia pantalla de audiencias, en la columna ***Estado***, estas estarán marcadas como ***Inactiva***.

![inactivas_1.png](/inactivas_1.png)

A mayores, también podremos establecer un filtro para que el dashboard nos muestre únicamente las audiencias inactivas o aquellas que se encuentren en otro estado.  
Puedes ver más información sobre como funcionan los filtros [aquí](/es/barras-de-menu#filtros-en-comportamiento-audiencias).

![inactivas_2.png](/inactivas_2.png)

# Integración con S3
Para añadir una conexión con S3 y poder emplear las audiencias dinámicas de tipo S3 es necesario que sigas estos pasos: 
- Haz login en EMMA.
- Ve a la sección **Gestión > Integraciones**.</br><p style="width:350px!important;">![s3_i.png](/s3_i.png)</p>
- En el apartado de S3, haz clic en el botón **Añadir conexión**.</br><p style="width:600px!important;">![s3_ii.png](/s3_ii.png)</p>
- Cubre el formulario de conexión:</br><p style="width:650px !important;">![s3_iii.png](/s3_iii.png)</p>
    -   **Nombre del Bucket:** Nombre establecido al Bucket cuando se creó. 
    -   **Región:** Región del Bucket.
    -   **Host:** Dominio del S3 en el que se encuentra el Bucket.
    -   **Clave de acceso:** Clave de acceso o Key ID del S3.  
    -   **Contraseña clave de acceso:** Contraseña de acceso o Key secret del S3 vinculada a la clave de acceso anterior. 
- Haz clic en el botón **Guardar integración** para guardar la configuración realizada. </br><p style="width:350px !important;">![s3_iv.png](/s3_iv.png)</p>
- Si la configuración realizada es correcta, verás un check verde al lado del nombre S3 confirmando la correcta integración.</br><p style="width:600px !important;">![s3_v.png](/s3_v.png)</p>
- Si la configuración realizada no es correcta, verás una x roja al lado del nombre S3 confirmando que integración tiene algún error. En ese caso, deberás editar la integración y realizar la modificación pertinente en función del error que nos muestre EMMA una vez hacemos clic en el botón **Editar integración**.</br><p style="width:700px !important;">![s3_vi.png](/s3_vi.png)</p>
> Si eliminamos una integración con S3 y tenemos audiencias dinámicas de tipo S3 que además están siendo usadas en comunicaciones, cuando la audiencia se recalcule (lo hace cada 24 horas) esta dará error y, en el momento en el que la audiencia de error, dejará de mostrarse la comunicación correspondiente a los usuarios.
{.is-warning}

# Compartir audiencias con terceros
Desde EMMA es posible compartir audiencias con diferentes sistemas con los que estamos integrados. 
De este modo, se aprovecha todo el potencial de EMMA para crear la audiencia necesaria en base a acciones que han realizado o no dentro de la app y demás filtros y se comparte la información de los dispositivos que cumplen las condiciones con plataformas terceras. 

## Compartir audiencias con Facebook
Gracias a la conexión entre EMMA y Facebook es posible enviar audiencias a Facebook. Puedes ver más información sobre cómo compartir audiencias con Facebook [aquí](https://docs.emma.io/es/guias-de-integracion/facebook#compartir-audiencias-con-facebook-opcional). 

## Compartir audiencias con Google Ads
Gracias a la conexión entre EMMA y Google Ads es posible enviar audiencias a Google Ads. Puedes ver más información sobre cómo compartir audiencias con Google Ads [aquí](https://docs.emma.io/es/google#compartir-audiencias-con-google-opcional). 

## Compartir audiencias con The Trade Desk (TTD)
Gracias a la integración entr EMMA y The Trade Desk, es posible realizad el envío de audiencias a TTD para poder aprovechar toda la información en la plataforma. 

Para poder compartir audiencias con The Trade Desk tan solo debemos seguir estos pasaos: 
- Hacer [login](https://ng.emma.io/es/login?returnUrl=%2Findex) en EMMA con nuestras credenciales. 
- Ir a la sección Captación > Apptracker. </br>![audiencias_1.png](/audiencias_1.png)
- Buscar y editar el proveedor The Trade Desk. </br>![audiencias_2.png](/audiencias_2.png) 
- Configurar los campos de **ID de anunciante** y **Clave secreta**. </br> ![audiencias_3.png](/audiencias_3.png)
- Dentro de la edición, ir al apartado **Audiencias** y seleccionar la audiencias que se desea compartir con TTD. </br>![audiencias_4.png](/audiencias_4.png)
- Haz clic en el botón **Guardar** situado en la parte superior derecha de la pantalla para que se apliquen los cambios.

Y ¡listo! Tu audiencia se empezará a compartir con The Trade Desk. 

Por defecto, en las audiencias, EMMA comparte únicamente el ID de dispositivo, por lo que, en caso de desear compartir también el email del usuario, se debe habilitar el check de **Compartir EMAIL con The Trade Desk**.
![audiencias_5.png](/audiencias_5.png)

> La información de email se envía codificada mediante sha256.{.is-info}


