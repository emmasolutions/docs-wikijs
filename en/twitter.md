---
title: Twitter
description: 
published: true
date: 2024-12-10T11:31:51.487Z
tags: 
editor: markdown
dateCreated: 2024-09-02T12:23:27.115Z
---

# Tracking with Twitter App Promotion
[Twitter App Promotion](https://business.x.com/es) is Twitter's targeted format for reaching new app users on mobile devices.</br><p style="width:700px">![twitter_5.png](/twitter_5.png)</p>

## Set up in Twitter
Follow the guidelines below to start measuring *Twitter App Promotion* campaigns: 
- Go to “Edit account access”:</br><p style="width:1600px">![twitter_6.png](/twitter_6.png)</p>
- Select “Add access” and enter the EMMA @emmaIO account with account administrator access: </br><p style="width:1600px">![twitter_7.png](/twitter_7.png)</p>
- Save the changes: </br><p style="width:1600px">![twitter_8.png](/twitter_8.png)</p>

## Set up in EMMA
Once you have configured Twitter in the previous step you must go to EMMA and follow the steps listed below: 
- Go to **App Preferences** and make sure that the App Store URL and/or Google Play URL fields are correctly configured. </br><p style="width:300px">![twitter_en_1.png](/twitter_en_1.png)</p>
- Go to the **Acquisition > Media Sources** section. </br><p style="width:700px">![twitter_en_2.png](/twitter_en_2.png)</p>
- Search and edit the Twitter feed. </br><p style="width:1200px">![twitter_en_3.png](/twitter_en_3.png)</p>
- No need to do anything to save the changes. </br><p style="width:1200px">![twitter_en_4.png](/twitter_en_4.png)</p>
- After saving the Twitter media feed, the team will receive an email with your request to measure Twitter App Promotion.
- The team has to perform a backend configuration and will notify you as soon as everything is ready and you will be able to launch Twitter campaigns.
- Once the team confirms that everything is ready, you should check on Twitter, under **Tools > Event Manager** that the conversion tag has been generated for the installation for both Android and iOS and that it is active.
- Once activated, you can launch your campaign. As soon as you start generating installations, a campaign called **Twitter Campaign** will automatically appear in EMMA where you will be able to see all the campaign data.

## Inapp event measurement (Optional)
In addition to measuring Twitter campaigns in EMMA , you can also send any inapp events you are measuring in EMMA to Twitter to measure them. Follow these steps to set up events for Twitter:
- [Log in](https://ng.emma.io/en/login?returnUrl=%2Findex) to the EMMA website and go to **Acquisition > Media Sources**.</br><p style="width:700px">![twitter_en_2.png](/twitter_en_2.png)</p>
- Edit **Twitter** media source.
- In the **Events** section, click the **add event** button to add the events you want to send to Twitter. It is important that in the **Event Identifier column for the Media Source** you select the Twitter event to which the event sent from EMMA will be linked. </br><p style="width:900px">![twitter_en_5.png](/twitter_en_5.png)</p>
- Decide which Twitter event you will want to link the event sent from EMMA to.
	-	PURCHASE
	- SIGN_UP
	- RE_ENGAGE
	- UPDATE
	- TUTORIAL_COMPLETE
	- RESERVATION
	- ADD_TO_CART
	- ADD_TO_WISHLIST
	- LOGIN
	- CHECKOUT_INITIATED
	- SEARCH
	- LEVEL_ACHIEVED
	- ACHIEVEMENT_UNLOCKED
	- CONTENT_VIEW
	- SHARE
	- INVITE
	- ADDED_PAYMENT_INFO
	- SPENT_CREDITS
	- RATED

# SKAdNetwork campaigns
EMMA allows measuring Twitter App Promotion campaigns oriented to iOS devices with an operating system version higher than iOS 14.5, known as SkAdNetowork campaigns. By allowing the measurement of this type of campaigns, we can obtain in EMMA the aggregated data of the installations and events carried out through these campaigns.

> Es importante que tengas en cuenta que debido a la política de privacidad de Apple, a través de las campañas de SkAdNetwork no podemos obtener información del usuario que se ha instalado la app o realizado eventos inapp, ya que Apple no comparte esta información con terceros. La única información que comparte con terceros es que se ha realizado una instalación o un evento vinculado a una determinada campaña. </br></br>That is to say, we will only be able to obtain aggregated data of installations and inapp events, without being able to know under any circumstances which user has performed these actions.
 {.is-info}

## Enabling SkAdNetowrkt in EMMA for installs
In order to start measuring this type of campaign in EMMA, the first step is to validate these two steps:
- [Log in](https://ng.emma.io/en/login?returnUrl=%2Findex) to your EMMA account.
- Go to the app preferences and make sure that in the iOS settings section, the App Store ID field is configured. If not, just get this id from the url of the store of your app and configure it. </br><p style="width:900px">![twitter_en_7.png](/twitter_en_7.png)</p>
- Make sure you have followed the previous steps in this guide.

Once the above steps have been validated, it is necessary to do this configuration in order to measure SKAdNetwork from Twitter App Promotion.
- Go to the **Acquisition > Media Sources** section. </br><p style="width:700px">![twitter_en_2.png](/twitter_en_2.png)</p>
- Search and edit the Twitter provider. </br><p style="width:1200px">![twitter_en_3.png](/twitter_en_3.png)</p>
- Enable the SKAdNetwork check. </br><p style="width:700px">![twitter_en_6.png](/twitter_en_6.png)</p>

> In addition to this configuration, it is recommended to integrate the EMMA SDK (version 4.12 or later) to be able to track the user at the event level and also to be able to receive a direct copy of the SkAdNetwork postback for further information in EMMA.
{.is-info}

As soon as data from SKAdNetwork campaigns starts to be received, a new campaign called **SkAdNetwork Twitter** will automatically appear in EMMA showing all the aggregated information.
</br><p style="width:900px">![twitter_en_13.png](/twitter_en_13.png)</p>

### Considerations to be taken into account
The connection for SKAdNetwork data is made via the API, this means that the following must be taken into account:
- SKAdNetwork does not transmit real-time data to Twitter and may experience delays of a minimum of 24 hours. Therefore, results will be recorded based on the time they were sent to Twitter and not the time they actually occurred.

## SKAdNetwork event measurement
To be able to perform SKAdNetwork event attribution, it is necessary to have EMMA SDK version 4.12 or later installed.
To configure the events we must follow these steps:
- Log in to your EMMA account.
- Go to the **Management > Events** section. </br><p style="width:700px">![twitter_en_8.png](/twitter_en_8.png)</p> 
- Search and edit the desired event. </br><p style="width:900px">![twitter_en_9.png](/twitter_en_9.png)</p> 
- At the bottom of the screen you must enable the SKAdNetwork check and add a conversion value between 1 and 63.</br><p style="width:900px">![twitter_en_10.png](/twitter_en_10.png)</p> 

> The conversion value configured for an event, will be the same for all the platforms where SKAdNetwork campaigns work, so if we configure the value 1 for event X, that will be the value of that event for the SKAdNetowork campaigns of Twitter, Google, Meta and TikTok. {.is-info}

Conversion values must be unique, the same value cannot be used in multiple events. In case of selecting a value in use, EMMA will warn us and show us the closest available value for its configuration. </br><p style="width:700px">![twitter_en_11.png](/twitter_en_11.png)</p> 

> Keep in mind that SKAdNetwork only notifies us of the last conversion, the last event performed within the attribution window, so if a user performs 3 events consecutively within the same attribution window, we will only receive notification of the last one and this will be the one that appears in EMMA.
{.is-info}

# Twitter Re-Targeting Campaigns
Twitter campaigns can generate both actions for new users (recruitment) and old users (re-engagement) depending on the segment established. Therefore, if you want to create a campaign that targets only new users, it is important to exclude in the campaign a custom audience with users who already have the app or had it before.

Re-engagement actions are generated by users who already had the app previously installed. On the other hand, recruitment actions come from those users who install the app for the first time.

In addition, if this is the first time you are going to measure Twitter with EMMA, you must first follow the steps in the section Twitter Tracking in order to correctly connect to the social platform and be able to measure all your campaigns.

## Re-Targeting with Twitter
To be able to measure Twitter Re-Targeting campaigns through EMMA, in addition to the configuration explained in the guide mentioned above, it is necessary to enable the events you want to send to Twitter to use in your Re-Targeting campaigns. To do this you have to follow these steps: 

- [Log in](https://ng.emma.io/en/login?returnUrl=%2Findex) to your EMMA account and go to the **Acquisition > Media Sources** section. </br><p style="width:700px">![twitter_en_2.png](/twitter_en_2.png)</p>
- Edit **Twitter** media source.
- In the **Events** section, click the **add event** button to add the events you want to send to Twitter. It is important that in the **Event Identifier column for the Media Source** you select the Twitter event to which the event sent from EMMA will be linked. </br><p style="width:900px">![twitter_en_5.png](/twitter_en_5.png)</p>
- Decide which Twitter event you will want to link the event sent from EMMA to.
	-	PURCHASE
	- SIGN_UP
	- RE_ENGAGE
	- UPDATE
	- TUTORIAL_COMPLETE
	- RESERVATION
	- ADD_TO_CART
	- ADD_TO_WISHLIST
	- LOGIN
	- CHECKOUT_INITIATED
	- SEARCH
	- LEVEL_ACHIEVED
	- ACHIEVEMENT_UNLOCKED
	- CONTENT_VIEW
	- SHARE
	- INVITE
	- ADDED_PAYMENT_INFO
	- SPENT_CREDITS
	- RATED
- In the **Retargeting events** section select the events that will be used for Retargeting campaigns. Note that you will only be able to enable as retargeting events the events you have previously configured in the **Events** section. </br><p style="width:900px">![twitter_en_12.png](/twitter_en_12.png)</p>

# Frequently asked questions

**Can I see the clicks of Twitter App Promotion campaigns in EMMA?**
The 3rd party integration with official Twitter partners includes conversion and engagement data only. We do not have access to click-through data. 

**Can I see the App installations in Twitter ads?**
Yes, with the integration you will be able to see conversions on both platforms, both EMMA and Twitter.

**How can I test my Twitter App Promotion configuration?**
Twitter does not allow testing the configuration in a test environment so we recommend launching a test campaign, with a budget limit, to verify if the first installations are registering correctly in EMMA.

