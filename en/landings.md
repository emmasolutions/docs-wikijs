---
title: Landings
description: 
published: true
date: 2021-11-05T11:45:38.878Z
tags: 
editor: markdown
dateCreated: 2021-11-05T11:45:37.162Z
---

# Introduction
EMMA allows the use of landings for those cases that require it, such as in a Google campaign that does not target installation (more info [here](https://docs.emma.io/es/google#gesti%C3%B3n-de-landings-para-campa%C3%B1as-con-powlinks)) or in those cases in which we want the powlink to have a specific redirection when the user has configured a browser that is not the default browser of the operating system (more info [here](https://docs.emma.io/es/adquisicion/apptracker#redireccionamiento-para-navegadores-no-nativos)).

In addition to offering two default landings, one for each case, it also allows the creation of custom landings for each of the two use cases.
# Create landings

As explained above, EMMA allows the use of landing pages for certain cases. In both cases, we have two options when using a landing:

1. Use the default landing offered by EMMA.
2. Create new customized landings with the desired content.

## EMMA default landing
If we use the default landing offered by EMMA, the end user will be shown a very simple landing, with the logo of the corresponding store (App Store in case an iOS user clicks on the ad, PlayStore in case an Android user clicks on the ad), the name of the app and a link that will redirect the user to the URLs set in the powlink with which the landing is linked.

## Customized landings
If you prefer to create your own customized landing page, just go to the **Management &gt; Landings** section. From here, you can create as many landings as you need.

![landing_1.png](/landing_1.png)

Creating a new landing is very simple, just click on the **New landing** button and fill in the simple creation form:

- **Name**: in this field you set an identifying name for the landing. It is a name for internal use, the end user will never see this name.
- **Type**: Select the type of landing you want to create. Select **Web** if it is a landing for Google Ads (more info [here](https://docs.emma.io/es/google#gesti%C3%B3n-de-landings-para-campa%C3%B1as-con-powlinks)) or **Redirect (Deeplink)** if it is a landing for redirection in non-native browsers (more info [here](https://docs.emma.io/es/adquisicion/apptracker#redireccionamiento-para-navegadores-no-nativos)).
- **Landing by default**: if we want this to be the default landing to be linked to the powlinks, we must enable this check.
> Note that there can only be one Deeplink landing by default. The deeplink landing that is
> set as the default landing will be the one that applies for all cases where it is used.
> Initially, the EMMA landing will be marked as the default landing, but you can set your
> custom landing to be the default landing.
{.is-info}


- **HTML content of the landing**: this is where you set the desired HTML. You can copy the HTML from a landing page you have already created and paste it here. In case you have selected the **Redirect (Deeplink)** option in the **Type** field, you will have to download our default deeplink landing from [here](https://emma.io/assets/landing/default_landing_deeplink.html) in order to adapt the html to your specific needs.
![landing_2.png](/landing_2.png)
You can then edit the created landings or deactivate them to stop using them (they can be reactivated if you need to use them again).
![landing_7.png](/landing_7.png)
# Linking a Powlink to a Landing
Once we have the landing(s) created, we only have to link it (them) with one or more powlinks. To do this we have to go to **Recruitment &gt; Apptracker** and create a new source in an existing campaign or create a new campaign with its corresponding source.
## Linking a powlink to a Web landing page
To link a powlink to a Web landing, it is important that at the moment of creating the new source, in the **Provider** field we select **Google Ads** so that the corresponding selector is displayed, which will allow us to choose the intermediate landing that we want to link to that powlink.
![landing_5.png](/landing_5.png)

Once the source for the Google Ads Provider has been created, all that remains is to set up the campaign in Google Ads and set the powlink in the final URL field.

## Linking a powlink to a Redirect landing page (Deeplink)
Deeplink landing is enabled at the powlink level. That is to say, some powlinks can have the use of deeplink landing enabled and others not, depending on our needs. To enable the use of deeplink landing, when creating or editing the source (powlink) we must go to the final part of configuration to the **Deeplink** section and enable the check box
*Use Landing Deeplink*.
![configurar_lading_deeplink.png](/configurar_lading_deeplink.png)
By enabling this check, our powlink will already use EMMA&#39;s default deeplink landing. If you want to use your own deeplink landing, you can see how to do it in the section Create new custom landings.