---
title: Rules
description: 
published: true
date: 2025-03-05T10:57:36.714Z
tags: 
editor: markdown
dateCreated: 2024-09-02T10:46:46.943Z
---

*EMMA RULES* is EMMA's technology that allows you to set rules for automating communications with your users.  

Based on the tagging you have implemented in your app, *EMMA RULES* will use this information to automatically trigger communications to users who meet the criteria of your rule.  

# How to create your first EMMA RULE  

- Log in to EMMA and go to **Communication > Rules**.
</br><p style="width:300px">![journey19.png](/journey19.png)
  
- Click on **+ New Rule**.
  
![journey22.png](/journey22.png)
  
- Complete the rule creation form. 
</br><p style="width:680px">![journey23.png](/journey23.png)
  
- **Name**: Set a name for your rule so you can later identify it in the list of scheduled rules.  
  
- **Recipe**: Recipes are the condition for the behavior that will trigger EMMA to impact certain users. Select one of the available recipes from the dropdown. Learn in detail how each recipe works by clicking [here](https://docs.emma.io/en/reglas#available-recipes). If your recipe is not available, you can suggest adding it by following these [instructions](https://docs.emma.io/es/reglas#a%C3%B1adir-nuevas-recetas).  
  
- **Action**: Once you’ve selected a recipe, the section to select an action will appear. Depending on the type of recipe selected, we will have different communication campaign options available to impact users, but these are all the existing ones.  
  - **Send push message**: Sends a push notification to users who meet the recipe condition. Complete the configuration for the push notification that will be sent to users. Set the message they will receive, the time the push notification will be sent, and optionally, you can add a URL or a deep link route so that your users are redirected to a specific section of the app when they open the push notification. 
  - **Send an automatic campaign**: Allows you to apply an automatic in-app campaign previously created under **Communication > Messages** to the recipe.  
If the selector is blank, it means you haven’t created an in-app campaign yet or the one you created isn’t scheduled in **Automatic mode for rules**. Follow these [instructions](https://docs.emma.io/en/mensajes-inapp) to create your first automatic in-app campaign to later apply to EMMA RULES.  
</br> Once the communication is selected, you can define the communication **priority** by selecting between values 1 and 9 in the ***priority tree***, where 1 is the lowest, 5 is medium, and 9 is the highest. This priority will be used by EMMA if several in-app message formats coincide for the same users on the same screens of the app. In this way, if a *Startview* with High priority and an *Adball* with Medium priority coincide in time, screen, and user, only the *Startview* will be shown. If two message formats with the same priority coincide, only the most recently created one will be shown. With this priority scale, you can have 9 simultaneous campaigns.  
</br> **Important note:** If multiple Rules are active, and the one with the highest priority has a campaign that, for reasons such as segmentation or frequency, doesn’t show, EMMA WILL NOT show the next Rule with the next highest priority instead. The user will simply not see anything. EMMA first chooses the Rule with the highest priority and second, whether the selected Rule has a campaign that should be shown to that user.
	- **Create automatic coupon**: Allows you to apply an automatic coupon to the recipe that was previously created under **Communication > Messages**.  
If the selector is blank, it means you haven’t created a coupon yet or the coupon you created isn’t scheduled in **Automatic mode for rules**. Follow these [instructions](https://docs.emma.io/en/mensajes-inapp) to create your first automatic coupon to later apply to EMMA RULES.  

- **Scheduling**: Set the start date and time from which you want the rule to begin operating. By default, rules remain active over time until you delete them, but you can set an end date by unchecking the **No end** option and selecting a specific end date and time.
  
![rules32.png](/rules32.png)
  
- Click the ***Save rule*** button located at the top right of the rule creation screen to launch your rule.  

Once the campaign is linked to a rule, its status will change from ***Ready*** to ***Active*** in the campaign table within the ***Communication > Messages*** section.
  
![journey27.png](/journey27.png)

![journey28.png](/journey28.png)

# View, edit, clone, or delete rules

Once you have created your first EMMA RULE, you will be able to view, edit, clone, delete, or export active rules.  

The rules summary table will display the rule name, applied recipe, action executed by EMMA, and the start and end dates for the rule.  

You can use the **Search** field to easily locate all your rules.

![journey29.png](/journey29.png)

To edit, clone, or delete any of your rules, simply hover over the context menu located to the left of the Name column and select the desired option.  

- **Edit:** Selecting this option will open the rule editor, allowing you to make the necessary changes. Once finished, click *Save Rule* to apply the modifications.  
- **Clone:** This option opens the rule creation screen with all the settings from the cloned rule. You can modify any field as needed and then click *Save rule* to store the new version.  
- **Delete:** Clicking this option will immediately delete the rule. **IMPORTANT:** The rule will be permanently deleted upon clicking *Delete*, with no confirmation popup.

![journey30.png](/journey30.png)

You can export this table by clicking the **Download data** button.  

# Available recipes

EMMA currently offers a set of predefined recipes based on three different user behaviors:  

- **On their birthday:**  
  Triggers an interaction with the user on their birthday. For EMMA to recognize the user's birth date, a **tag** must be sent following the specified format for [**iOS**](https://developer.emma.io/en/ios/integracion-sdk#user-properties-tags) and [**Android**](https://developer.emma.io/en/android/integracion-sdk#complete-the-users-profile-by-means-of-tags).  

- **User Registered Yesterday:**  
  Targets users the day after they register.  

- **User on a Specific Screen:**  
  Triggers when a user enters a particular screen in the app. This allows communication rules based on app screen interactions.  If the screen list appears empty, EMMA has not detected any events categorized as screens. You can manually define screen events by following these [**instructions**](https://docs.emma.io/en/primeros-pasos/eventos#create-events).  

- **User Redeemed a Coupon:**  
  Displays a coupon when a user has redeemed another available coupon. You need to select the redeemed coupon and the new coupon to be shown once the action occurs.  

- **User Spent at Least a Certain Amount:**  
  Triggers interactions for users who have spent at least "X" amount in the app.  

- **User Purchased a Product:**  
  Targets users who buy a particular product. If the product list appears empty, EMMA has not detected any products in the app. Review the transaction submission documentation for [**iOS**](https://developer.emma.io/en/ios/integracion-sdk#event-measurement) and [**Android**](https://developer.emma.io/en/android/integracion-sdk#event-measurement) to ensure products are correctly registered.  

- **Tag Has a Specific Value:**  
  Triggers interactions based on the value of a specific tag EMMA automatically generates options for selecting TAGS based on the available data.  
  
|     |     |     |
| --- | --- | --- |
| Date of first conversion | Date of the first transaction event                 | [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#event-measurement) |
| Date of first login      | Date of the first login event                        | [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#event-measurement) |
| Date of installation     | Date of app installation on the device              | [Android](https://developer.emma.io/en/android/integracion-sdk#download-emma-android-sdk) & [iOS](https://developer.emma.io/es/ios/integracion-sdk#descarga-emma-ios-sdk) |
| Date of last conversion  | Date of the last transaction event                   | [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#event-measurement) |
| Date of last login       | Date of the last login event                         | [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#event-measurement) |
| Date of last session     | Date of the last app open event                      | [Android](https://developer.emma.io/en/android/integracion-sdk#download-emma-android-sdk) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#download-emma-ios-sdk) |
| Date of loyal buyer      | Date of the second transaction event                 | [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#event-measurement) |
| Date of loyal user       | Date of the fifth app open event                     | [Android](https://developer.emma.io/en/android/integracion-sdk#download-emma-android-sdk) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#download-emma-ios-sdk) |
| Date of register         | Date of the registration event                       | [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) & [iOS](https://developer.emma.io/en/ios/integracion-sdk#medici%C3%B3n-de-eventos) |

You can create new custom TAGS by following the documentation for [iOS](https://developer.emma.io/en/ios/integracion-sdk#user-properties-tags) and [Android](https://developer.emma.io/en/android/integracion-sdk#complete-the-users-profile-by-means-of-tags). 

You can customize how you want to apply the recipe to your TAG based on a text field, number, or date by following these [steps](https://docs.emma.io/en/etiquetas).  

- **User performed an event:**  
Impacts the user X time after performing a specific event within the app.  
</br>If this list appears empty, it means that no custom events are being measured in EMMA. You can check this documentation to learn [how to create custom events](https://docs.emma.io/en/primeros-pasos/eventos#create-events), and in this other one, you can see the event integration process for both [Android](https://developer.emma.io/en/android/integracion-sdk#event-measurement) and [iOS](https://developer.emma.io/en/ios/integracion-sdk#event-measurement).  

In the following table, you can see what types of communications you can carry out with each type of recipe:

![journey31.png](/journey31.png)

# Add new recipes

It is possible that the option you need to automate your communications is not available in the list of *recipes*.

Each month, EMMA will add new automation formulas to the list of recipes, and we will take all the suggestions you send to support@emma.io into account. Your help and experience are essential to ensure that we create the most powerful list of actions for **Automated Mobile Marketing**.

