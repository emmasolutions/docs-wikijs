---
title: Attribution Windows
description: 
published: true
date: 2025-03-05T11:54:40.306Z
tags: 
editor: markdown
dateCreated: 2025-03-05T11:54:13.560Z
---

With EMMA, it is possible to customize attribution windows for click-to-install and install-to-in-app events for each of the different networks you work with.  

What are attribution windows?  

- Click attribution window, determines the maximum time that can pass between a click and the app installation on the device for it to be attributed to a specific source.  
- In-app event attribution window, determines the maximum time that can pass between an installation and the occurrence of in-app events for them to be attributed to the installation source.  

Below, we will explore the different types of attribution and the possible configuration of attribution windows in detail.

# Deterministic attribution  

Deterministic attribution is the primary attribution methodology in EMMA. **This type is based on the use of unique device identifiers, such as the advertising identifier, making it the most accurate attribution method.**  

The deterministic attribution window can be customized. Depending on each network, different values can be set.  

The following table shows the level of customization allowed for each network, as well as the default configuration for each one:

| **Network** | **Click attribution window** | **Default click in EMMA** | **Event attribution window** | **Default event in EMMA** |  
| --- | --- | --- | --- | --- |  
| ***Apple Search Ads*** | 30 days | 30 days | Between 1 day and 12 months | 30 days |  
| ***Google*** | Between 1 and 90 days | 7 days | Between 1 day and 12 months | 30 days |  
| ***Meta (Facebook)*** | Between 1 and 28 days | 7 days | Between 1 day and 12 months | 30 days |  
| ***Microsoft Ads (Bing)*** | Between 1 and 30 days | 7 days | Between 1 day and 12 months | 30 days |  
| ***Snapchat*** | Between 1 and 28 days | 7 days | Between 1 day and 12 months | 30 days |  
| ***TikTok*** | Between 1 and 28 days | 7 days | Between 1 day and 12 months | 30 days |  
| ***Twitter*** | Between 1 and 30 days | 7 days | Between 1 day and 12 months | 30 days |  
| ***Other networks*** | Between 1 and 30 days | 7 days | Between 1 day and 12 months | 30 days |

> From EMMA, we recommend that the attribution window configured on the platform matches the attribution window set in EMMA. This will help avoid potential discrepancies.
{.is-warning}

# Probabilistic attribution  

Probabilistic attribution does not rely on unique device identifiers but instead uses a probabilistic distribution to assign attribution.  

Since it cannot be based on unique device identifiers, attribution is performed through an internal algorithm based on statistics and matches.  

> The attribution window for probabilistic attribution cannot be modified and is set to 24 hours.  
> {.is-info}

# Customizing attribution windows in EMMA  
To customize attribution windows, simply follow these steps:  

- [Log in](https://ng.emma.io/en/login) to your EMMA account.  
2. Go to **Acquisition > Media Sources**. 
</br><p style="width:280px">![attribution_windows.png](/attribution_windows.png)
  
- Find and edit the provider you want to configure.  
- Scroll down to the section named **Attribution**.
  
![attribution_windows2.png](/attribution_windows2.png)
  
- Set the desired attribution window within the allowed intervals for that specific network.
  
# Customizing the retargeting attribution window in EMMA  

In **retargeting** campaigns, it is also possible to configure the click-to-in-app event attribution window for each powlink.  

The **retargeting** attribution window determines the maximum time that can pass between the click and the in-app action.  

This configuration is set when creating the **powlink**, and a different window can be assigned to each retargeting powlink. You can find more information on how to create powlinks [here](https://docs.emma.io/en/adquisicion/fuentes-de-medios).  

> The **retargeting** attribution window can be customized between **1 and 90 days**. If no value is selected, EMMA’s default window of 30 days will be used.
> {.is-info}
