---
title: Others
description: 
published: true
date: 2022-07-08T08:12:58.743Z
tags: 
editor: markdown
dateCreated: 2021-11-05T12:55:43.891Z
---

# Privacy policy settings in App Store
With Apple&#39;s operating system update to iOS 14, apps must inform users of the data that is collected by the app, including that which is collected through third parties, such as the EMMA SDK.

This will be required for all new apps and for updated versions uploaded to the App Store
on or after December 8.

To help you comply with the Privacy Policy requirements in the App Store, in the following list you can see the data that is collected by EMMA by default and therefore has to be indicated in App Store Connect:

- Precise Location
- Coarse Location
- User ID
- Device ID
- Product Interaction
- Advertising Data
- Other Usage Data

On the other hand, any additional event or tag that your app is measuring with EMMA and is required to inform the user about, must be included as well.

In the following image you can see in more detail the type of data as it will appear in App Store Connect once selected:
![captura_de_pantalla_2021-07-15_a_las_12.07.16.png](/captura_de_pantalla_2021-07-15_a_las_12.07.16.png)
# How do I tag my app?
Before starting with the EMMA SDK integration it is important to define what is going to be measured and how the app will be tagged, as this will depend on the reporting and segmentation options that our technology will give you.

In order to cover all possible metrics, **EMMA offers three levels of tagging** that are treated as complementary options.
- [Tags](https://docs.emma.io/es/etiquetas)
- [Default events](https://docs.emma.io/es/primeros-pasos/eventos#eventos-por-defecto)
- [Custom events](https://docs.emma.io/es/primeros-pasos/eventos#eventos-personalizados)

# Get your device ID (AAID/IDFA)
You can use your ad tracking ID from your device to [&quot;remove&quot;](https://docs.emma.io/es/configuracion#dispositivos-de-test) it from our database and test your application as if you were a new user (new installation):

## Android
To find out what is the ID of your Android device, follow these steps:
Go to *Device Settings* and look for the ***Google option (Google Settings)***.

![id_android_i.jpg](/id_android_i.jpg)
- Once in Google settings, click on the Ads option.
![id_android_ii.jpg](/id_android_ii.jpg)
- At the bottom of the Ads screen, you will see the Ad ID of your device.
![id_android_iii.jpg](/id_android_iii.jpg)


