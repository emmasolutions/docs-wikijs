---
title: Menu Bars
description: 
published: true
date: 2021-11-05T12:01:24.690Z
tags: 
editor: markdown
dateCreated: 2021-11-04T11:01:24.753Z
---

Within EMMA you will find three different levels of menus in which you can perform a general configuration, access the different sections of EMMA or apply specific filters to the data displayed in certain sections.

These three menus that make up EMMA are:
- Supra main menú
- Main menú
- Filter menu

Next we will see in more detail each one of them and the type of actions that can be carried out.
# Main menu above
The main supra menu is the menu where we can select our app, apply date ranges and access the general account and app settings menu.

![menu_supra_i.png](/menu_supra_i.png)
From this menu we can select, for the EMMA sections in which it applies such as Executive Summary or Facilities, how we want the data displayed to be grouped. We can choose between the options days, weeks or months.

![menu_supra_ii.png](/menu_supra_ii.png)
Depending on the grouping selected, the graph data will be displayed broken down by day, week or month.

In addition, this menu is also where we set the date range for which EMMA is returning data. This value affects the data displayed in any section of EMMA. By default, EMMA displays data for the last 7 days, but we can set other date ranges according to our needs among those available:

![menu_supra_iii.png](/menu_supra_iii.png)

- **Today**: EMMA displays data for the current day.
- **Yesterday**: EMMA displays data from yesterday.
- **Last 7 days**: EMMA displays data for the last 7 days since the current day.
- **Last 30 days**: EMMA displays data for the last 30 days since the current day.
- **Last month**: EMMA displays data for the calendar month immediately preceding the current month.
- **This month**: EMMA displays data for the current month.
- **Custom range**: EMMA shows data for the selected date range, it can be a specific day, a week, 15 days or 1 month and 3 days. In case there is a date limit when displaying data, the dashboard will warn of this limit.

Finally, from this menu, we have access to the different sections of the account and app configuration by clicking on the user name shown at the end of this menu.

![menu_supra_iv.png](/menu_supra_iv.png)

You can see more details about each of the options that compose this configuration menu from this guide.

# Main menu
The main menu is the menu to access the different sections and functionalities that make up the EMMA dashboard.
![menu_principal_i.png](/menu_principal_i.png)

From this menu we can access the following sections:
- General:
  - [Executive Summary](https://docs.emma.io/es/resumen-ejecutivo)
  - [Reports](https://docs.emma.io/es/informes)
- Capturing:
  - [Installations](https://docs.emma.io/es/instalaciones)
  - [Apptracker](https://docs.emma.io/es/adquisicion/apptracker)
  - [Media Sources](https://docs.emma.io/es/adquisicion/fuentes-de-medios#notificar-eventos-a-redes-publicitarias)
  - [Short links](https://docs.emma.io/es/adquisicion/enlaces-cortos)
- Behavior:
  - [Browser](https://docs.emma.io/es/analisis-de-comportamiento/segmentos)
  - [Funnels](https://docs.emma.io/es/analisis-de-comportamiento/embudos)
  - [Audiences](https://docs.emma.io/es/audiencias)
- Communication:
  - [Messages](https://docs.emma.io/es/mensajes-inapp)
  - [Communication Report](https://docs.emma.io/es/comunicacion/campa%C3%B1as/reporting-campa%C3%B1as)
  - [Rules](https://docs.emma.io/es/reglas)
  - [Customer routes](https://docs.emma.io/es/analisis-de-comportamiento/customer-journeys)
- Management:
  - [Native templates](https://docs.emma.io/es/mensajes-inapp#crear-plantillas)
  - [Email providers](https://docs.emma.io/es/comunicacion/mensajes-out-app/email)
  - [Integrations](https://docs.emma.io/es/guias-de-integracion/salesforce)
  - [Events](https://docs.emma.io/es/primeros-pasos/eventos)
  - [Tags](https://docs.emma.io/es/etiquetas)
  - [Landings](https://docs.emma.io/es/landings)
  - [Categories](https://docs.emma.io/es/categorias)

You can see more detailed information about each of the sections by clicking on them.
# Filter menu

The last menu that makes up EMMA is the filter menu. You can identify this menu by its grayish color. This menu is not available for all sections of EMMA.

![menu_filtrado_i.png](/menu_filtrado_i.png)
Depending on the EMMA section we are in, there will be some filters applied by default and we will be able to apply specific filters related to the section we are in.
## Filters in Capture &gt; Apptracker
Let&#39;s see the available filters according to the EMMA section we are in:

In the Apptracker section of EMMA a Basic filter is applied by default. With this default filter EMMA returns the recruitment campaigns. If we change it to Re-Targeting EMMA will show the campaigns in Re-Targeting mode. You can see more information about EMMA&#39;s attribution models here.
![filtro_apptracker.png](/filtro_apptracker.png)
In addition to the default filter we can add two additional filters by clicking on the + **Add filter** button:
- **Countries**: Select from the list of available countries for EMMA to display data based on your filter.
- **Operating system**: Select the operating system between Android and iOS for EMMA to display data based on your filter.

The filters are combinable. That is, we can apply both filters at the same time.

## Filters in Behavior &gt; Funnels
In the funnels section we have a simple filtering by operating system.
![filtro_embudos.png](/filtro_embudos.png)
By default the selected option is &quot;All operating systems&quot; so in this section by default EMMA shows joint data from both OS, but we can change the selection between:
- **Android**: Select this option to view data only from Android devices.
- **iOS**: Select this option to view data only from iOS devices.
## Filters in Behavior &gt; Audiences
In the Audiences section we also have this filter menu. In this case, the filters that we can apply are the following:

![filtro_audiencias.png](/filtro_audiencias.png)

- **All audiences**: this is the default filter and returns all existing audiences for the app. If we select the option &quot;My audiences&quot; we will see only the audiences we have created.
- **Status**: Filters the audiences according to their current status (New, In progress, List, Error).
- **Type**: Select the type of audience from the Static or Dynamic options.
## Filters in Communication &gt; Messages
In the Messages section you can apply filters so that EMMA will return one or another communication depending on the filters you have applied.

![filtro_mensajes.png](/filtro_mensajes.png)
- **Current application**: This is a default filter. It can be changed to **&quot;All applications&quot;**. Depending on the selected option it will show only the communications that have been created in the current app or the communications of all the applications that are under our account.
- **All messages**: This is also a default filter. It can be changed to **&quot;My messages&quot;**. Depending on the selected option it will show all communications created in the app, regardless of the user who created them or it will show only the communications created by my user.
- **Format**: You can select one or several formats among [Adball](https://docs.emma.io/es/mensajes-inapp#adball), [Banner](https://docs.emma.io/es/mensajes-inapp#banner), [Coupon](https://docs.emma.io/es/mensajes-inapp#cup%C3%B3n), [Dynamic Tab](https://docs.emma.io/es/mensajes-inapp#tab-din%C3%A1mico), [Email](https://docs.emma.io/es/comunicacion/mensajes-out-app/email), [Native Ad](https://docs.emma.io/es/mensajes-inapp#native-ad), [Push](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications), [Startview](https://docs.emma.io/es/mensajes-inapp#startview) and [Strip](https://docs.emma.io/es/mensajes-inapp#strip) so that EMMA will return the communications that match the selected format(s).
- **Status**: you can select one or more communication statuses so that EMMA will return to you communications that are in that particular status. You can select from: *Active, Approved, Deleted, Draft, Finished, Paused, In process, Ready, Scheduled, Testing and To validate*. You can see more information about the statuses [here](https://docs.emma.io/es/mensajes-inapp).
- **A/B Test**: Filter by those communications that have established A/B Test variations or those that do not. You can see more information about the A/B Test [here](https://docs.emma.io/es/test-a-b).
## Filters in Communication &gt; Communications report
The filters available in the Communications Report section are:
![filtro_informe_cmunicaciones.png](/filtro_informe_cmunicaciones.png)
- **In-App**: By default, the results of In-App communications are shown. But we can change the option to **Out-App** to show data from Out-App communications.
- **A/B Test**: Filter by those communications that have established A/B Test variations or by those that do not. You can see more information about A/B Test [here](https://docs.emma.io/es/test-a-b).
- **Operating System**: Select the operating system between Android and iOS for EMMA to display data based on your filter.
- **Format**: You can select one or several formats between [Adball](https://docs.emma.io/es/mensajes-inapp#adball),[Banner](https://docs.emma.io/es/mensajes-inapp#banner), [Coupon](https://docs.emma.io/es/mensajes-inapp#cup%C3%B3n), [Dynamic Tab](https://docs.emma.io/es/mensajes-inapp#tab-din%C3%A1mico), [Email](https://docs.emma.io/es/comunicacion/mensajes-out-app/email), [Native Ad](https://docs.emma.io/es/mensajes-inapp#native-ad), [Push](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications), [Startview](https://docs.emma.io/es/mensajes-inapp#startview) and [Strip](https://docs.emma.io/es/mensajes-inapp#strip) for EMMA to return the communications that match the selected format(s).
- **Campaigns**: Select a specific campaign from the existing ones to display data based on this criteria.

All filters can be combined, several can be applied at the same time.
## Filters in Communication &gt; Customer routes
The filters available in Customer Routes are:

![filtro_rutas.png](/filtro_rutas.png)
- **All routes**: This is a default filter and returns all routes created. It can be changed to **&quot;Custom&quot;** so that only routes created by yourself are displayed.

- **Status**: Select from the options *Draft, In Process, Active or Deleted* to filter the routes that meet the filtering criteria. 
## Filters in Management &gt; Native Templates
The filters available in the Native Templates section are:

![menu_plantilas_nativas.png](/menu_plantilas_nativas.png)
- **Private only**: This is a default filter. It returns those templates categorized as private. If we select the ***&quot;Private and public&quot;*** option we will see both private and public templates. You can see more information about this in this [guide](https://docs.emma.io/es/mensajes-inapp#crear-plantillas).
## Filters in Management &gt; Events
In the Events section we can apply a filter so that EMMA returns one or other events:
![menu_eventos.png](/menu_eventos.png)
- **Custom**: Custom events are shown in the event list. You can see more info about customized events here.
- **Default**: The default events are shown in the event list. You can see more information about the default events here.
- **Screens**: Detected screens are shown in the event list.