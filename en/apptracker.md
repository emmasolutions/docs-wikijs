---
title: Apptracker
description: 
published: true
date: 2021-11-05T13:51:32.135Z
tags: 
editor: markdown
dateCreated: 2021-11-05T13:51:30.529Z
---

### Manages acquisition campaigns and media sources
# Introduction
In EMMA&#39;s apptracker, you can manage all your campaigns and sources. This way, you will be able to know the exact origin of each of your app&#39;s users.

When the campaigns are active and generating data, you will be able to see in a general way how the campaigns are working from this same section.

The first thing to do before consulting the data is to select the date period for which you want to process the data. By default, data from the last 7 days are shown. To select a date period, use the date selector at the top right of the screen.
![r_ejecutivo_1.png](/r_ejecutivo_1.png)

Filters can then be added depending on the data to be queried. The available filters are:

- **Basic / Re-Targeting**: this is a fixed and mandatory filter that by default shows Basic campaigns. More detailed information on the attribution models available in EMMA can be found at this [link](https://docs.emma.io/es/adquisicion/apptracker#campa%C3%B1as-b%C3%A1sicas-y-re-targeting).
- **Country**: this filter allows you to split the data according to the country of origin of the installations.
- **Operating system**: with this filter we can query the data based on a specific operating system (Android/iOS).

Once the campaigns have generated activity, it will be possible to see, always based on the selected date range and the established filters, the general data of all the campaigns in the main graph.
![apptracker_2.png](/apptracker_2.png)
This graph shows the following information:
