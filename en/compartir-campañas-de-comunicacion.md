---
title: Share communication campaigns
description: 
published: true
date: 2025-03-04T12:24:57.018Z
tags: 
editor: markdown
dateCreated: 2024-09-02T11:28:12.421Z
---

With the *campaign-sharing functionality* (Communication > Messages), users will be able to view all campaigns created within the same account and clone them, regardless of the app in which the campaign was created and the app from which they are accessing (as long as they have access to more than one app within the same account).

# Share messaging campaigns

If you have access to multiple apps within the same account, you will see that in the **Messages** section there is a button from which you can select whether you want to view only the campaigns belonging to the current app or if you want to view the campaigns from all applications. </br><p style="width:400px">![share_communications.png](/share_communications.png)

In the case of selecting the option **All applications**, the table with the scheduled campaigns will look as follows:
  
![share_communications2.png](/share_communications2.png)
  
In the **Campaign Name** column, we will have a cloud icon for those campaigns that do not belong to the app we are in, to make their identification easier and faster. Additionally, we will also have a new column called **App**, where we can see which app each campaign belongs to.

Campaigns created in other apps can be previewed or cloned by hovering over the contextual menu to the left of the table.

### Important  
Campaigns from app A **CANNOT** be edited, deleted, launched, or paused from app B or app C; they can only be previewed or cloned.

# Clone campaigns
  
When cloning campaigns from another app, we simply need to *hover over* the contextual menu and select the **Clone** option. </br><p style="width:560px">![share_communications3.png](/share_communications3.png)
  
This option will open the communication setup screen with all the settings from the cloned original campaign. In this way, we only need to make modifications to the fields that are required, such as the start and end dates of the communication and the target audience, as well as any fields we wish to modify.