---
title: Segment updates
description: 
published: true
date: 2025-03-04T11:59:15.488Z
tags: 
editor: markdown
dateCreated: 2024-09-02T11:26:16.057Z
---

When you have created your communications in EMMA aimed at specific established segments, it is very important to keep the following in mind.  

Once a segment is created in a communication, a group of users is being defined that must meet a series of specific characteristics in order to be impacted by the given communication.

When the campaign starts being displayed, meaning when it is active, EMMA calculates the segment at the beginning to determine which users meet the conditions (the segment) to be impacted by the campaign and which do not. Keep in mind that segmentation is **not real-time**; instead, the filters are recalculated periodically.  

Regardless of whether users meet the segment criteria at the time the campaign is launched, they may perform actions that cause them to enter or exit the segment.  

To determine which users enter and/or leave the segment, EMMA periodically refreshes the segment to recalculate which users should be included. This segment **update** can take anywhere from **0 to 30 minutes**.  

### **Segment Recalculation**  
Some factors that influence the time required for segment recalculation include the segment size, the tags used, device properties, etc.