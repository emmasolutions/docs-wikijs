---
title: A/B Testing
description: 
published: true
date: 2025-03-03T12:09:25.781Z
tags: 
editor: markdown
dateCreated: 2024-09-02T11:24:42.966Z
---

With EMMA's **A/B Test**, you can create different variations of **In-App** communication campaigns and determine which one best suits your end users and performs best based on your established objectives. This allows you to identify the most effective communication for each scenario and launch it to all users. You can find more information about A/B Test reporting [here](https://docs.emma.io/en/comunicacion/campa%C3%B1as/test-ab).  

Below, we'll go into detail on how to create A/B Test variations for a communication campaign.  

- First, [log in](https://ng.emma.io/en/login) to the EMMA platform using your usual credentials.  
- Navigate to the ***Communication*** section in the top menu and select ***Messaging*** from the dropdown options. </br><p style="width:300px">![test_ab.png](/test_ab.png)
  
- Click on New Message and choose the desired format from the available options. </br><p style="width:400px">![test_ab2.png](/test_ab2.png)
  
> Keep in mind that the A/B Test option is not available for customer journey communications.
>{.is-info}

- In step ***2. Content***, configure the communication details such as the *destination URL* and *creative elements*. This setup will correspond to the ***Original Version***. </br><p style="width:400px">![test_ab3.png](/test_ab3.png)
  
- Once the ***Original Version*** has been configured, you can add different versions for the A/B Test, with a maximum of three variations (*Version A, Version B, and Version C*). To do this, click the ***Add A/B Test Variation*** button. </br><p style="width:400px">![test_ab4.png](/test_ab4.png)
  
- Once the desired A/B Test variations have been added, you only need to make the necessary modifications. By default, the settings from the Original Version are cloned, so you only need to change the fields you want while keeping the rest as they are.  

### **Note**  
If you want to delete any of the created A/B Test variations, simply click on the trash can icon next to the name of each variation. </br><p style="width:400px">![test_ab5.png](/test_ab5.png)
  
- Finally, it is necessary to set a percentage of users who will view each communication. To do this, in step ***3. Target***, in addition to selecting a **segment**, the percentage of users that should see each version of the communication must be defined.  
</br> By default, the **Original Version** will be set to 100% of users, so if this is not modified, users will only see this version.

![test_ab6.png](/test_ab6.png)

Keep in mind that, depending on the selected communication format, different inputs can be modified in the A/B Test variations.  

The following table shows which values can be modified according to each communication format.
</br>
| **Format**       | **Inputs** |
|-------------|--------|
| **Adball**  | - Destination URL  <br> - Vertical position  <br> - Horizontal position  <br> - Image |
| **Banner**  | - Destination URL  <br> - How to open URL  <br> - Screen position  <br> - Display settings  <br> - Image |
| **Coupon**  | - Message  <br> - Promotion code  <br> - Total redemption limit  <br> - Daily redemption limit  <br> - Hide limit  <br> - Image |
| **Dynamic Tab**  | - Destination URL  <br> - Tab title  <br> - Tab position  <br> - Image |
| **Email**  | - Sender name  <br> - Sender email  <br> - BCC  <br> - Subject  <br> - Use HTML template?  <br> - HTML template or message body if not using a template |
| **Native Ad**  | - How to open the URL  <br> - Template  <br> - All fields included in the template |
| **Startview**  | - Destination URL  <br> - Close button |
| **Strip**  | - Message  <br> - Display settings  <br> - Text color  <br> - Background color |  
