---
title: Categories
description: 
published: true
date: 2021-11-04T13:26:06.425Z
tags: 
editor: markdown
dateCreated: 2021-11-04T13:26:04.724Z
---

Categories are used in saturation control in order to establish a hierarchy of impact limits at the product or vertical level. You can see more information about saturation control and the use of categories in this [guide](https://docs.emma.io/es/control-de-saturacion#impactos-por-categor%C3%ADa).

Creating a new category is very simple, just follow the steps below.
- Go to the ***Management &gt; Categories section.***

![categoria_1.png](/categoria_1.png)
- Click on ***New category*** and fill in the creation form:
![categoria_2.png](/categoria_2.png)

Establish an identifying name for the category so that it can be easily located later and linked to the correct communication (here you can see more information on how to link a category to a communication). And set the desired impact limits for each of the categories By default they are unlimited, but you can set the limit you need, for example 1 daily, 3 weekly and 10 monthly impacts.

Once the category is created, if necessary, it can be edited or deleted at any time. All we have to do is click on the menu of the specific category and click on the *Edit* or *Delete* option depending on the action we need to carry out.

![categoria_3.png](/categoria_3.png)