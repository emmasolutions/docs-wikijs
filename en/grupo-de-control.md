---
title: Control group
description: 
published: true
date: 2025-03-04T09:56:39.434Z
tags: 
editor: markdown
dateCreated: 2024-09-02T11:25:22.239Z
---

Control Group is a functionality that allows us to see the differences in behavior between users who view/receive a communication and users who do not view/receive it. This way, it will be possible to later analyze the in-app behavior linked to the communication of users who have seen/received the communication versus those who have not. This will allow us to determine whether that communication is achieving the desired results or not and to make the necessary changes to achieve the established results.  

**How do we configure the Control Group?**  

Very simple. In all EMMA communication formats, in step **4. Target**, we can enable or disable this functionality.  

If we do not want to establish a **Control Group**, we will simply leave this option disabled, just as it is by default. If, on the other hand, we want to establish a Control Group, we must enable this option.

![control_group.png](/control_group.png)

If we have enabled the Control Group, we only need to select the % of users who will see/receive the communication and the % who will not see/receive the communication. The percentage set in the Control Group bar will be the percentage of users who **will NOT receive the communication**.

![control_group2.png](/control_group2.png)

Once the Control Group is activated, you can select the percentage of users who will **not** see the communication in a completely random manner. At the bottom, you will see a warning message informing you of the defined Control Group percentage.  

To view the results of a communication with an established control group, go to **Behavior > Funnels**. More information [here](https://docs.emma.io/en/analisis-de-comportamiento/embudos).