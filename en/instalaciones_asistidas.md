---
title: Assisted installs
description: 
published: true
date: 2025-03-05T10:22:37.723Z
tags: 
editor: markdown
dateCreated: 2025-02-06T09:57:41.038Z
---

# Assisted installs

With the Assisted Installs feature, we can have exhaustive control over the installs from one channel that have been assisted by other channels, as well as determine how many installs a channel has assisted.  

To access the *Assisted Installations* screen, simply go to *Acquisition > Assisted Installs.* </br><p style="width:300px">![assisted-installations.png](/assisted-installations.png)

This screen consists of three filters, allowing you to refine the information according to your needs and achieve the desired data granularity. Additionally, the data displayed on the screen will also be affected by the **selected date range**.</br><p style="width:280px">![assisted-installations1.png](/assisted-installations1.png)
  
> It is important to note that both the selected date range, as well as the grouping and applied filters, will affect all the data displayed on this screen.
{.is-info}

The available filters are:

- **Campaigns**: Allows selecting one or multiple campaigns. Once selected, only data from the chosen campaigns will be displayed.  
- **Channels**: Allows selecting one or multiple channels. Once selected, only data from the chosen channels will be displayed.  
- **Operating System**: Allows selecting a specific operating system. Once selected, only data from the chosen OS will be displayed.  

The **Assisted Installs** screen is divided into four main sections:  

1. [Summary](https://docs.emma.io/en/instalaciones_asistidas#summary)
2. [Assisted and non-assisted installs](https://docs.emma.io/en/instalaciones_asistidas#assisted-and-non-assisted-installations)
3. [Winning and assisted installs by channel](https://docs.emma.io/en/instalaciones_asistidas#winning-and-assisted-installations-by-channel)
4. [Detailed table by channel](https://docs.emma.io/en/instalaciones_asistidas#detailed-table-by-channel)
  
  
## Summary 
  
In this first section, you will be able to see summary and general data for all channels or based on the applied filter.
  
![assisted-installations-2.png](/assisted-installations-2.png)
  
The information available is as follows:  

- **Total installs**: All generated installs. It is the sum of non-assisted and assisted installs.  
- **Non-assisted installs**: Installs from the total that have not been assisted by other channels.  
- **Assisted installs**: Installs from the total that have been assisted by other channels.  
- **% of assisted installs**: Displays the percentage of assisted installs over the total.
  
## Assisted and non-assisted installs
  
The first chart on the screen displays a bar graph comparing assisted vs. non-assisted installs per day. This allows for an easy visualization of the trend and comparison over time.
  
![assisted-installs3.png](/assisted-installs3.png)
  
## Winning and assisted installs by channel
  
The graph of winning and assisted installs by channel allows you to perform an analysis by channel of the installs attributed to that channel (winning) and the installs that the channel has assisted.
  
![assisted-installs4.png](/assisted-installs4.png)
  
In this graph, we can see the following information:

- **Winning Installs:** Final installs attributed to a specific channel.
- **Assisted Installs:** Installs that a specific channel has assisted but are attributed to another winning channel.
  
For example, if the Google Ads - ACI channel has 67 winning installs and 23 assisted, it means the following:

- The Google Ads - ACI channel has 67 winning installs (attributed).
- The Google Ads - ACI channel has assisted 23 installs that are attributed to other channels.
  
By default, this graph shows a top 5 of channels, but it can be modified to display a top 10 or top 15.
  
![assisted-installs7.png](/assisted-installs7.png)
  
## Detailed table by channel
  
Finally, in the channel detail table, you can see detailed data for each of the different channels.
  
![assisted-installs6.png](/assisted-installs6.png)

- **Channel**: The name of the channel configured in EMMA. You can find more information about channels [here].  
- **Winning**: Installs attributed to the specific channel.  
- **% Winning**: The percentage of winning installs from the channel relative to the total installs (the sum of all channels).  
- **Assisted**: Installs that the channel has assisted but are attributed to another winning channel.  
- **% Assisted**: The percentage of assisted installs from the channel relative to the total assists (the sum of all channels).  

> By clicking on the name of each channel, you can access the details of that channel to see more specific information. You can find more info about the details [here](https://docs.emma.io/en/instalaciones_asistidas#assisted-install-details).
>   {.is-info}

# Assisted install details
  
The Assisted Install Detail screen will allow us to perform a detailed analysis for each of the different channels, where we can see specific information related to each channel.

To access the Assisted Install Detail screen, simply click on the name of a channel from the [main Assisted installs screen](https://docs.emma.io/en/instalaciones_asistidas#detailed-table-by-channel).

This screen consists of 2 filters that will allow us to filter the information according to our needs, enabling us to reach the desired level of data granularity.</br><p style="width:250px">![assisted_installs13.png](/assisted_installs13.png)

The available filters are:

- **Campaigns**: Allows you to select one or more campaigns. After selecting the campaigns, only the data for the chosen campaigns will be displayed.  
- **Operating System**: Allows you to select a specific operating system. After selecting the operating system, only the data for the selected system will be shown.  

The Assisted Install Detail screen is divided into four main sections:

1. [Summary](https://docs.emma.io/en/instalaciones_asistidas#summary-1)
2. [Volume and evolution by install type](https://docs.emma.io/en/instalaciones_asistidas#volume-and-evolution-by-install-type)
3. [Assistance volume from main sources](https://docs.emma.io/en/instalaciones_asistidas#assistance-volume-from-main-sources)
4. [Attribution detail table](https://docs.emma.io/en/instalaciones_asistidas#attribution-detail-table)
  
## Summary
  
In the first section, we will be able to see summary and general data for the specific channel that has been accessed.
  
![assisted-installs9.png](/assisted-installs9.png)
  
The information we can see is as follows:

- **Winning Installs**: These are all the installs attributed to the specific channel.  
- **Assisted Installs**: Winning installs from the channel that have been assisted by other channels.  
- **Assists to Installs**: Installs to which the specific channel has contributed (assisted). These installs are winning (attributed) to other channels.
  
## Volume and evolution by install type
  
In the *Volume and evolution by install type* graph, you can analyze the volume of the channel's winning installs and the volume of the installs the channel has assisted on a daily, weekly, or monthly basis.
  
![assisted-installs10.png](/assisted-installs10.png)
  
In this chart, you can see the following information:  

- **Winning installs**: Installs attributed to the channel.  
- **Assisted installs**: Installs attributed (won) by another channel but assisted by the analyzed channel.
  
## Assistance volume from main sources
  
In the *Assistance Volume from Main Sources* chart, you can analyze the contribution of each specific source in assisting installs for other channels. In other words, you can see a breakdown of the assists generated by each source. </br><p style="width:450px">![assisted-installs11.png](/assisted-installs11.png)
  
Taking this into account, the graph in the screenshot shows the following information:

- Source 1 has assisted 1,075 installs that are attributed to other channels.  
- Source 2 has assisted 532 installs that are attributed to other channels.  
- Source 3 has assisted 275 installs that are attributed to other channels.  
- Source 4 has assisted 253 installs that are attributed to other channels.  
- Source 5 has assisted 28 installs that are attributed to other channels.  
- The remaining sources of the channel have assisted 7 installs that are attributed to other channels.
  
> This graph shows a top 5 of the sources that have generated the most assists to installs from other channels, along with a sixth option that represents the assists generated by the remaining sources of the channel.
>{.is-info}
  
## Attribution detail table
  
Finally, in the Attribution Table, you can see detailed data for each of the different sources and campaigns that have generated winning installs and assists to installs from other channels.
  
![assisted-installs12.png](/assisted-installs12.png)
  
- **Source**: The name of the source in EMMA.  
- **Campaign**: The name of the campaign in EMMA.  
- **Number of Winning Installs**: Installs attributed to the specific source.  
- **% Winning Installs**: The percentage of winning installs from the source relative to the total installs (the sum of all sources).  
- **Number of Assists**: Installs that each source has assisted and are attributed to another winning channel.  
- **% Assisted Installs**: The percentage of assisted installs from the source relative to the total assists (the sum of all sources).
  
  
  

