---
title: Snapchat
description: 
published: true
date: 2025-03-11T12:10:28.926Z
tags: 
editor: markdown
dateCreated: 2024-09-02T12:15:32.440Z
---

# Introduction
Snapchat App Install is the Snapchat App Install advertising format developed by the Snapchat App Install platform itself to drive new users to mobile apps. As with other ad sources, you will have no problem measuring your Snapchat campaigns with EMMA. To do so, simply follow the steps listed below. 

> Before launching Snapchat campaigns, please contact your assigned account or through [support](mailto:support@emma.io) so that we can sign you up to the system so that you can view Snapchat campaign data without any problems. 
It is also important that you follow the steps in this guide before launching any campaign.{.is-warning}
# Set up in EMMA
To start measuring Snapchat App Installs campaigns in EMMA, the first thing to do is to set up a small configuration in the EMMA dashboard.

- Log in to your EMMA account and go to **App Preferences**. </br><p style="width: 300px">![tiktok_en_1.png](/tiktok_en_1.png)</p>
- Edit app settings.
- In iOS settings, configure the app's App Store URL and AppStore ID.</br><p style="width: 700px">![tiktok_en_2.png](/tiktok_en_2.png)</p>
You can get the AppStore ID from the store URL. </br><p style="width: 700px">![snapchat_5.png](/snapchat_5.png)</p>
- In the Android settings, configure the app URL in Google Play and the Google Play ID.</br><p style="width: 700px">![tiktok_en_3.png](/tiktok_en_3.png)</p>
You can get the Google Play ID from the store URL. </br><p style="width: 700px">![snapchat_4.png](/snapchat_4.png)</p>

# Activate the Snapchat media source
Once the previous step has been completed, all that remains is to activate the Snapchat media feed.

- Go to **Acquisition > Media sources**. </br><p style="width: 700px">![tiktok_en_4.png](/tiktok_en_4.png)</p>
- Find the **Snapchat** media source and click **Edit** from the left side menu.</br><p style="width: 600px">![snapchat_en_1.png](/snapchat_en_1.png)</p>
- Activate the media source by clicking the **Activate** button in the warning message.
</br><p style="width: 800px">![snapchat_en_2.png](/snapchat_en_2.png)</p>
- The other fields in the settings are not mandatory unless you are using an intermediary to launch Snapchat campaigns.

And that's it, the Snapchat integration is done. Once you start measuring Snapchat installs, a new campaign will appear in the Snapchat apptracker with all the campaign data.

# Frequently Asked Questions
**What campaign breakdown details does EMMA offer?**
In EMMA's Apptracker, you can view the breakdown of information by campaign and ad group configured in Snapchat. This allows us to see the following details:

- Snapchat campaign name = Campaign name in EMMA
- Snapchat ad group name = Source name in EMMA

**Why do I see a campaign called *Snapchat Restricted* in EMMA?**
There are certain circumstances where Snapchat does not share the campaign name information, such as when it reports post-impression installs.

The **Snapchat Restricted** campaign in EMMA gathers all installations reported by Snapchat, but for which we do not have the exact campaign name because that information was not shared with EMMA.

**If I change the campaign name in the Snapchat panel, is it updated in EMMA?**
Yes, any changes made to the campaign naming will be reflected in EMMA. This way, a unified naming convention will be visible across both platforms.

