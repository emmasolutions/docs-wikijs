---
title: Google
description: 
published: true
date: 2024-12-11T12:59:16.562Z
tags: 
editor: markdown
dateCreated: 2024-09-02T12:03:54.619Z
---

# Tracking campaigns with App Promotion objective
To set up campaigns for mobile Apps under the [App Campaign (AC)](https://support.google.com/google-ads/answer/6247380?hl=en) mode you should first start reviewing the following points.
1. For the attribution of any Google Ads campaign it is necessary that your application is sending us the device's advertising identifier (IDFA/AAID). Visit our [integration guides](https://developer.emma.io/en/home) for more information.
2. Go to [App Preferences](https://ng.emma.io/es/login?returnUrl=%2Fuser%2Fapp) and make sure that the iOS BUNDLE ID and Android GOOGLEPLAY ID fields are set correctly.

If you have everything ready... Let's get started!

## Create the Google Ads LINK ID
> This action can only be performed by the Google Ads account admin.
{.is-info}

- Log in to your Google Ads account and go to **Tools**.
- Select **Linked Accounts**. </br><p style="width: 1000px">![google_1.png](/google_1.png)</p>
- In the **Third-party app analytics** box click on **DETAILS**.</br><p style="width: 800px">![google_2.png](/google_2.png)
- Add a new account by clicking on the “**+**” button. </br><p style="width: 1000px">![google_3.png](/google_3.png)
- Select the **Other Provider** option in the drop-down menu. </br><p style="width: 700px">![google_4.png](/google_4.png)
- Enter the following **Provider ID: 5623804413**. </br><p style="width: 700px">![google_5.png](/google_5.png)
- Copy the new LINK ID that identifies your mobile app in Google Ads. Remember to generate a LINKID for each of your apps (Android and iOS).

## Set up Google Ads in EMMA
- Login to EMMA and select your app. Go to **Acquisition > Media sources**. </br>![google_en_1.png](/google_en_1.png)
- Find the **Google Ads** source and in the side menu click on the Edit option. </br>![google_en_2.png](/google_en_2.png)
- Enter the LINK ID generated in Google Ads previously for Android and/or iOS. Click **Save**. </br>![google_en_3.png](/google_en_3.png)



## Consents

Due to the new **Digital Markets Act (DMA)** (also known as *Regulation 2022/1925*) that came into effect on March 6, 2024, additional requirements have been introduced for companies like Google and other large platforms designated as "*gatekeepers.*" These "*gatekeepers*" are now specifically responsible for obtaining user consent.

In this context, the new consents to notify Google are:

- **eea:** If the user belongs to the European Economic Area.
- **ad_user_data:** If the user allows data sharing with Google.
- **ad_personalization:** If the user allows personalized ads.

To comply with this new law, EMMA has added a new section in the Google Ads provider details called **Consents**, enabling their configuration.

### How these consents affect apps inside and outside the EEA

It is important to note that, although consents are not mandatory if your app is outside the European Economic Area (EEA), Google recommends implementing them for all users, as their use may likely expand to other regulations in the future.

In that case, here is an example of consent configuration in EMMA:

- **eea:** false
- **ad_user_data:** true
- **ad_personalization:** true

However, if your app is within the European Economic Area, the use of these consents is **mandatory**, and you can configure them following the steps detailed [here](https://docs.emma.io/en/google#consent-configuration).

In this case, here is an example of consent configuration in EMMA:

- **eea:** default value set to true.
- **ad_user_data:** default value set to false and linked to the tag collecting consent, such as **ALLOW_USER_DATA**.
- **ad_personalization:** default value set to false and linked to the tag collecting consent, such as *ALLOW_AD_PERSONALIZATION.*

You can find more information about frequently asked questions [here](https://docs.emma.io/en/google#digital-markets-act).

### Consent Configuration

To configure consents for the application, follow these steps:

- Log in to your [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex) account.
- Go to the **Acquisition > Media Sources** section.
- Find and edit the **Google Ads** provider.
- In the provider editing screen, navigate to the **Consents** section.
- Set the tag sent to EMMA that captures each of the requested consents. **If you don’t have a specific tag, leave this field blank or [create a tag](https://docs.emma.io/en/etiquetas) that collects the permission to include it and ensure compliance with the law.** If multiple consents are collected in a single tag, you can apply the same tag to all fields.
- Set the tag sent to EMMA that captures each of the requested consents. **If you don’t have a specific tag, leave this field blank or create a tag that collects the permission to include it and ensure compliance with the law.*
- Set the default value. Choose between **True** and **False**. The configured default value will be sent to Google for these permissions if no tag captures that information or if the tag has no value for a particular user.
</br><p style="width: 1000px">![consents.png](/consents.png)</p>

> Note that the allowed values for the tags used for these permissions are: **true/false, 0/1, yes/no, true/false**.
> These values are not case-sensitive, so, for example, the value *True* is also accepted.
{.is-warning}


## Enable In-App event notification (optional)

If you want to send in-app events to Google Ads in addition to installs, go to ***Acquisition > Media Sources***, find Google Ads, and click the **Edit** option in the side menu.

In the Events section, click the **Add Event** button and select the events to be sent.

> It is important to note that, due to a Google limitation, when configuring events to notify Google Ads, the name set in the **Event Identifier column for the Media Source** must contain between 1 and 64 characters (using UTF-8 encoding). Otherwise, Google will not be able to receive the event.{.is-warning}

</br><p style="width:500px">![google_ads_consents.png](/google_ads_consents.png)

  - Once all events have been selected, click **Save**.

> For Google Ads to start tracking conversions, you need to open your app and perform the events you want to send at least once. After doing so, it may take up to 6 hours for new conversions to change their status from “No recent conversion” to “Recording conversion.”
> {.is-info}

## Add your conversions to Google Ads
  
- Log back into your *Google Ads* account and go to **Tools**.
- Select "**Conversions**".
  
![google_9.png](/google_9.png)
- Click the **"+"** button to add a new conversion tag.
  
![google_10.png](/google_10.png)
- Select the "**APP**" option from the list of types.
  
![google_11.png](/google_11.png)
- Select the "**Third-party app analytics**" option and click Continue.
  
![google_12.png](/google_12.png)
- Select the **"first-open"** event for each app (mandatory for tracking installs). Optionally, select the in-app events you want to attribute in Google Ads.

![google_13.png](/google_13.png)
- Click **"Import and Continue."**
- You will now see the list of integrated conversions.
- To view downloads in your Google Ads campaign reporting, make sure the conversion tag you integrated is included in "Conversions." To do this, click on the name of the conversion tag and check that the *Include in "Conversions"* section says *YES*.
  
![google_14.png](/google_14.png)
  
## Global Account (MCC) with sub-accounts
  
In case you have a global account (MCC) with multiple sub-accounts, the configuration needed to measure the data from all sub-accounts in EMMA is as follows:

You need to link the MCC account with EMMA via the link ID.

- Go to the child account (sub-account) and copy the **customer ID**.
  
<p style="color:red"> IMAGEN
  
- Once you have the **customer ID** of the child account, you must return to the global account (**MCC**) and go to **Tools > Linked Accounts > Third-Party App Analytics.**
  
<p style="color:red"> IMAGEN

- Select the option to **share with another account**.

<p style="color:red"> IMAGEN

- Add the **client ID** of the child account and continue with the process as indicated by Google Ads.
  
<p style="color:red"> IMAGEN

These steps must be carried out for each of the child sub-accounts created under the global MCC account. If 3 sub-accounts are created, these steps must be followed 3 times, and so on.
  
# SKAdNetwork campaigns

EMMA allows tracking of TikTok Ads campaigns targeting iOS devices with operating system versions above iOS 14.5, known as SkAdNetwork campaigns. By enabling the measurement of this type of campaign, EMMA can provide aggregated data on installs and events generated through these campaigns.
  
> It is important to note that, due to Apple’s privacy policy, SkAdNetwork campaigns do not provide user-level information for app installs or in-app events, as Apple does not share this data with third parties. The only information shared is that an install or event occurred, linked to a specific campaign.
> 
> In other words, only aggregated data on installs and in-app events can be obtained, without any possibility of identifying which user performed those actions.
{.is-info}
  
> To enable Google's SKAdNetwork, you need a Manager Account (MCC) in Google. You can find more information about this type of account [here](https://ads.google.com/intl/es-419_co/home/tools/manager-accounts/).
> {.is-warning}

  
## Enabling SkAdNetwork in EMMA for installs
  
To start tracking this type of campaign in EMMA, the first step is to validate these two steps:

- Log in to your [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex) account.
- Go to your app preferences and ensure that in the iOS-related settings section, the App Store ID field is configured. If not, simply retrieve this ID from your app's Store URL and configure it.
  
![skadnetwork.png](/skadnetwork.png)

- Make sure you have completed the previous steps in this guide.
Once the previous steps have been validated, you need to complete this configuration to enable Google Ads SKAdNetwork tracking.

- Go to the **Acquisition > Media Sources** section.
</br><p style="width:350px"> ![media_source.png](/media_source.png)
 
- Find and edit the **Google Ads** provider.
  
![mediasource_googleads.png](/mediasource_googleads.png)
  
- If you have not set up the **connection with Google Ads**, you will need to establish it.
  
![googleads_connection.png](/googleads_connection.png)
![skadnetwork_connection.png](/skadnetwork_connection.png)
  
- Once the connection is established, you can enable the SKAdNetwork checkbox.
  
![googleads_established.png](/googleads_established.png)
![skadnetwork_enabled.png](/skadnetwork_enabled.png)
  
- Next, you must select the manager account.
  
![skadnetwork_enabled2.png](/skadnetwork_enabled2.png)
  
- Finally, select the client account from which you want to retrieve data. This selector is multi-select, so if multiple client accounts apply, you can select them.  
  
![skadnetwork_enabled3.png](/skadnetwork_enabled3.png)
  
> In addition to this configuration, it is recommended to integrate the EMMA SDK (version 4.12 or later) to track users at the event level and also receive a direct copy of the SkAdNetwork postback to gather more information in EMMA.
>{.is-info}
  
As soon as data from SkAdNetwork campaigns starts being received, a new campaign called **SkAdNetwork Google Ads** will automatically appear in EMMA, displaying all the aggregated information.
  
![skadnetwork_enabled4.png](/skadnetwork_enabled4.png)

## SKAdNetwork event measurement
  
If you want to receive attribution information for in-app events performed through SkAdNetwork campaigns, you first need to have SDK version 4.12 or later integrated.

Additionally, you need to configure the events you want to track in EMMA. To do this, follow these steps:

- Log in to your EMMA account.
- Go to the **Management > Events** section.
  </br><p style="width:300px">![events.png](/events.png)
  
- Find and edit the desired event.

![edit_event.png](/edit_event.png)
  
- At the bottom of the screen, enable the SKAdNetwork checkbox and assign a conversion value between 1 and 63.
  
![skadnetwork_event.png](/skadnetwork_event.png)
  
Conversion values must be unique and cannot be used for multiple events. If you select a value that is already in use, EMMA will notify you and display the nearest available value for configuration.
  
![skadnetwork_event2.png](/skadnetwork_event2.png)
  
> Keep in mind that SKAdNetwork only notifies us of the last conversion, the most recent event performed within the attribution window. Therefore, if a user performs 3 events consecutively within the same attribution window, only the notification for the last event will be received, and it will be the one displayed in EMMA.
>{.is-warning}

# App Campaign Re-Targeting Campaigns
  
In addition to the classic ACi model from Google for App campaigns with the installation goal, there is also a model for App campaigns with the re-targeting goal (ACe).

With EMMA, you can also measure this type of campaign by following the same configuration detailed in the previous section.
  
# Share Audiences with Google (optional)
  
> To enable click and cost tracking, you need to have a Manager Account (MCC) in Google. You can find more information about this type of account [here](https://ads.google.com/intl/es-419_co/home/tools/manager-accounts/).
>{.is-warning}
  
With EMMA, it is possible to obtain click and cost information from Google to have this data in the EMMA app tracker, along with installation information and in-app activity performed by the user. This allows you to analyze all campaign-related information directly from the interface.

To enable the download of this information, simply follow these steps:

- Log in to your [EMMA](https://ng.emma.io/en/login?returnUrl=%2Findex) account.
- Go to **Acquisition > Media Sources**. Find and edit the Google Ads provider.
- Within the editing screen, scroll down to the **Clicks and Cost** section and enable the switch.
  
![clickandcost.png](/clickandcost.png)
  
- Once enabled, you will be able to select the manager account and the client account respectively to download the corresponding click and cost data.
  
![clickandcost2.png](/clickandcost2.png)
  
And that's it! You will now be able to see the click and cost data for your campaigns in EMMA.
  
> There may be small discrepancies in the clicks and cost displayed in EMMA compared to Google. This is because Google processes and adjusts clicks throughout the life of the campaign, and the cost also changes accordingly. You can find more information about this [here](https://support.google.com/google-ads/answer/11182074?hl=en-GB&sjid=17630802518324537141-NA).
> {.is-warning}

## Sharing audiences with Google (Optional)
  
Creating Google audiences is the best way to target your campaigns to a specific audience based on various segmentation criteria.

Google allows you to create these audiences from its own advertising platform or automate their creation and updating directly from EMMA's segmentation engine.

The method of sharing audiences from EMMA has two clear advantages:

- You can use all the segmentation criteria offered by EMMA **without the need to manually and periodically upload data to Google**. This is faster, simpler, and requires less technical integration.
- You can create **audiences based on internal IDs of your company** that are not compatible with Google’s audience builder. For example, a list of "customer IDs".
  
> Due to Google’s customer list targeting policy, not all advertisers can create customer lists (audiences), and therefore not all advertisers will be able to share audiences with Google directly from EMMA.
> 
> In this [link](https://support.google.com/adspolicy/answer/6299717), you can find Google’s requirements for using customer lists and, therefore, sharing audiences directly from EMMA. In this [other link](https://support.google.com/google-ads/answer/6276125?hl=en-GB&sjid=3510069347677067644-NC), you can see how to create customer lists in Google. If you cannot create a customer list manually, you will not be able to access the audiences shared from EMMA.
{.is-warning}
  
Does it seem interesting to you?

Before you begin, make sure that you have completed the Google setup in EMMA. It is important that you follow the steps in the [Campaign Measurement with App Promotion Objective](https://docs.emma.io/en/google#tracking-campaigns-with-app-promotion-objective) section to properly connect with the social platform and track all your campaigns.
  
### Enable audiences for the advertiser account
  
To share audiences directly with Google, you need to have the connection with the Google API set up. To do this, simply follow these steps:

- Log in to [EMMA](https://ng.emma.io/en/login?returnUrl=%2Findex).
- Go to **Acquisition > Media Sources** and search for the **Google** source.
- Edit the source and establish the connection with Google by clicking the** Sign in to Google Ads** button displayed in the **Google Ads** section.
  
![googleads_connection.png](/googleads_connection.png)
  
- Save the changes.

Once the connection with the Google API is established, the **Audiences** section will be immediately enabled in the Google source (Acquisition > Media Sources).

If the API connection is not set up, the following message will be displayed.
  
![audiencias_googleads.png](/audiencias_googleads.png)
  
Once the connection with the API has been established, you can select the corresponding advertiser account from the list to directly share the audiences.
  
![audiences_googleads2.png](/audiences_googleads2.png)
  
## Sharing audiences with Google

Now it's time to share your audiences with Google, but don't worry, it's a very simple process. Just follow these steps to share them:

- Go to the **Behavior > Audiences** section and create an audience you want to share with Google.
If you already have existing audiences and want to share one of them, there’s no need to create it again.
You can find more information about how to create audiences in this </br><p style="color:red"> guide. 

</br><p style="width: 300px">![behavior_audiences.png](/behavior_audiences.png)

- Once your audience is ready, go back to the **Acquisition > Media Sources** section and edit the **Google** source.

- Next, within the Google source editing screen, go to the **Audiences** section, select the audience you want to share with Google from the list, and save the changes. That's it! Your audience is now being shared automatically with Google.
  
![audiences_googleads3.png](/audiences_googleads3.png)
  
By default, in the audiences, EMMA only shares the device ID. If you wish to also share the user's email, you need to enable the **Share EMAIL with Google Ads** checkbox.
  
![audiences_googleads2.png](/audiences_googleads2.png)
  
> The email information is sent encoded using sha256.
>{.is-info}
  
Once the audience is shared with Google, from the Audiences screen (Behavior > Audiences), you will be able to see which audiences are being shared with Google, as they will display the Google icon in the **Shared** column.
  
![shared_audience.png](/shared_audience.png)
  
> Google reports that audiences may take up to **48 hours** to display data on their own dashboard.  
> 
> You can view your audiences in the Google dashboard by going to **Tools & Settings > Shared Library > Audience Manager**. <p style="color:red"> **IMAGEN**
>   
> Also, keep in mind that the number of users displayed in EMMA's audience may not match the one shown on Google's dashboard, as Google can only process the device IDs it has in its system.
{.is-warning}
  
## Stop sharing audiences with Google
  
If you want to stop updating the audience data with Google, just follow these steps:

- Go to **Acquisition > Media Sources** and edit the **Google** source.  
- In the Audiences section, open the audience selector and click on the ones you want to stop sharing (you can type their names to find them more easily). You will immediately see the checkbox that kept them selected disappear.
  
![audiences_googleads4.png](/audiences_googleads4.png)
![audiences_googleads5.png](/audiences_googleads5.png)
  
- Save the changes to stop the audience from being shared with Google.
  
> Keep in mind that the effect of this action is simply that the audience will stop updating new information in Google, but this outdated audience will still be available in Google and can continue to be used for campaigns unless it is also deleted there.
> 
{.is-info}

# Landing management for campaigns with Powlinks
  
Google Ads has a strict policy regarding redirects in its ads.
  
> *We want consumers to have a good experience when clicking on an ad, so the ad destinations must provide unique value to users and be functional, useful, and straightforward.*  
>   
  
Given this, they have established a set of best practices that advertisers must follow. Among these best practices, one that affects EMMA recommends avoiding:
  
> *Redirects from the final URL that lead the user to a different domain.*
> *For example, the final URL http://example.com redirects to http://example2.com.*
  
This specific case affects all Google Ads campaigns with objectives other than App Installs that allow the use of an EMMA powlink for tracking.

For this reason, and to prevent Google Ads from disapproving ads that use an EMMA powlink for tracking, we have been working on a new feature that allows you to create new landing pages within EMMA, which we will host on our own server, thus avoiding the redirection that Google does not allow.

In this way, one or more different landing pages can be generated, which can later be linked with a specific powlink that complies with all of Google Ads' redirection requirements.
  
## Create langings
 
EMMA has a default landing page for these cases and also allows the creation of custom landing pages for situations where needed. You can find more information on how to create landing pages [here](https://docs.emma.io/en/landings#crear-landings).
 
## Linking a powlink to a landing page
  
Once we have created our landing page, it's time to link it. You can find more information on how to link a landing page to a powlink in our dedicated article on [Landings](https://docs.emma.io/en/landings#linking-a-powlink-to-a-landing).
  
# Re-Targeting campaigns with Google using EMMA Powlink
  
Keep in mind that regardless of the type of Google campaign, if it's not included in AC campaigns (campaigns that don't require a powlink for tracking), Google may reject the ads due to its strict redirection policy. To address this situation, EMMA provides a solution outlined in the previous section of this [guide](https://docs.emma.io/en/google#tracking-campaigns-with-app-promotion-objective).

In order for Google to correctly attribute events in this specific type of campaign, it is necessary to send both the [GCLID](https://support.google.com/google-ads/answer/9744275?hl=en-GB&sjid=14575380678629686613-NC) parameter and the [session start](https://developers.google.com/app-conversion-tracking/api/request-response-specs#conversion_tracking_request) each time there is a Re-Targeting click.

Regarding the GCLID, EMMA is set up to send this parameter to Google, as long as Google first sends this information so that EMMA can collect and process it properly. This parameter is sent with all events.

To send the **session start** information with the Re-Targeting click, a small configuration in the EMMA dashboard is required.

Apologies for the confusion! Here's the translation:

- Go to **Acquisition > Media Sources**.  
- Search for and edit the **Google Ads** source.  
In the **Events** section, add the **Retargeting Open** event and in the *‘Event Identifier for the Media Source’* column, enter **session_start**.
  
![events_googleads.png](/events_googleads.png)
  
- Click the Save button to apply the changes.

With this configuration, the session_start will be sent with each retargeting open event generated in the app through an EMMA powlink being used in Google campaigns.

This configuration does not affect other events; you can configure events to notify Google as usual. You can find more information about this [here](https://docs.emma.io/en/google#enable-in-app-event-notification-optional).
  
# Frequently asked questions
## Atribution

**Why can't I see the clicks in EMMA?**

Google Ads UAC campaigns do not allow the use of tracking links or sending them to third parties, so you can only view this data in Google Ads.

**Why can't I see the cost of my Google Ads campaigns in EMMA?**

The new integration with the API only allows EMMA to show installation data and in-app event data for these campaigns.

**Where can I see the results of my Google Ads campaigns in EMMA?**

In the **EMMA** section under **Captación > Apptracker** or in **Comportamiento > Explorador**, by creating a segment with the "campaign" KPI.

**What campaign breakdown details does EMMA provide?**

**EMMA Apptracker** will show the results of Google Ads campaigns according to their type. In this case, Universal App Campaigns (UAC) will appear under the name "Adwords UAC." Within the campaign, various sources will be shown, with the campaign name provided by Google Ads. Example:

Campaign: Google Ads App Campaign (AC)  
Sources: Android_Generica_Abril  
iOS_Generica_Abril  
Android_Promo_Junio  
[ .... ]

**If I change the campaign name in the Google dashboard, how will I see it in EMMA?**

If you change the name of a campaign that has already generated data in EMMA, once we receive the new name from Google, we will create a new campaign with the updated name.

**How can I launch a re-engagement campaign for my app?**

In the new Google Ads interface, this option is not available, and as [announced](https://support.google.com/google-ads/answer/9234102?hl=en&visit_id=638695176179236061-3888458979&rd=3), it will no longer be available in the old interface starting from September 17th.

The only alternative they offer to promote a mobile app is to create a "Universal App Campaign" (UAC) and select the "App Actions" goal. This will optimize traffic to achieve the desired event, but it is not considered a re-engagement campaign.
  
## EMMA vs Google Ads discrepancies
  
**Why are there discrepancies between EMMA and Google Ads?**
  
Keep in mind that any discrepancies between the data shown in the Google Ads dashboard and EMMA's Apptracker are mainly due to the deduplication system used by EMMA.

Google Ads is a self-attributed network, but EMMA is not, so an install that Google Ads self-attributes may be attributed in EMMA to any other active campaign in the Apptracker.

There can also be cases where the discrepancy is the other way around, as Google Ads may take up to 72 hours to update reporting information, whereas EMMA performs updates in real time.

Another cause of discrepancy can be the timing of the install attribution. Google Ads attributes the install at the moment the click is made, while EMMA attributes the install when the user performs the first open. For this reason, an install may be attributed to different days in both platforms.
  
## Digital Markets Act
**What happens if an FSS user sends the ad_user_data and ad_personalization to false?**

In this case attribution accuracy decreases as device identifiers cannot be shared with Google and other user data such as email (if active in audiences) cannot be shared.

**What happens if the app operates outside and inside the EEA?**

In this case if an app operates outside and inside the EEA it must send a user tag from the EMMA SDK and link it in Google Ads media source to the consent, this tag must have if the user belongs to the EEA true/false.

**What if I have a consent permission linked to a tag but this user does not send the tag with the consent?**

In this case if there is a user tag linked to a permission and it is not sent, EMMA takes the default value configured in Google Ads media source for that consent.

**What if there is neither an associated default value nor tag binding to these consents?**

EMMA stops sending data to Google for Google Ads campaigns as it is unable to identify consents.

**I have audiences synced with Google Ads, what happens to data sent before March 6, 2024?**

All users synchronized before the change who have not added permissions as of March 6, 2024 or whose permissions are denied will be progressively removed from the lists.

**As of March 6, 2024, what permissions do users have to comply with to share data in Google Ads audiences?**

To share data in the audience users need to have both the ad_user_data permission (share data with Google) and the ad_personalization permission (allow personalized ads) set to true.
If a user does not have these two permissions set to true they will not be added to the audience, even if they meet the targeting.

**What happens if a user is late in submitting their consent labels after installation?**

When EMMA receives a new installation it tries to attribute it to Google Ads as long as there is a previous connection made, if the user still does not give the consents when the installation has been processed the default values are sent to Google Ads, in the case of denying everything by default the permissions would be sent to false. The attribution would not be lost, since 24 hours another attempt would be made with the permissions already updated.


