---
title: Communication formats specifications 
description: 
published: true
date: 2025-03-04T12:28:22.055Z
tags: 
editor: markdown
dateCreated: 2024-09-02T11:29:14.208Z
---

Below are the required specifications for the development of creating creatives in the various communication formats of EMMA:

|     |     |
| --- | --- |
| ![](/01_push.png) | **Rich Push**<br><br>Rectangular display that appears alongside a Push Notification when received on the device.<br><br>It is recommended to leave a [small safety margin](https://docs.emma.io/en/comunicacion/mensajes-out-app/push-notifications) due to adaptation issues with Android devices not using EMMA. |
| ![](/02_banner.png) | **Banner**<br><br>Rectangular display that appears at the top or bottom of the device screen.<br><br>It is recommended that the image have dimensions of at least 640x100px to ensure image sharpness. The key is to maintain the aspect ratio, so a 1280x200px format would also work.<br><br>Keep in mind that an image smaller than the recommended minimum dimensions (640x100px) might appear pixelated, and larger dimensions could make the image too large, causing it not to load properly.<br><br>You can use an image of any other size and dimensions, but it will automatically adjust the content to maintain the aspect ratio. |
| ![](/03_adball.png) | **AdBall**<br><br>Circular display whose content appears when the user clicks on it.<br><br>It is recommended that the circular image have dimensions of at least 100x100px to ensure image sharpness.<br><br>You can use an image of any other size and dimensions; EMMA will later allow you to choose a section of the image that maintains the same proportions.<br><br>The content has approximate dimensions of 640x760px, but this varies depending on the device. The best option is to create a responsive page that adapts the content to the available screen dimensions. This will cover all the different devices on the market, resulting in a better user experience. |
| ![](/04_dynamic_tab.png) | **Dynamic Tab**<br><br>Tab that appears in the bottom menu of the app. iOS exclusive.<br><br>The icon displayed in the tab is 16x16px. A larger size could misalign its positioning. It is recommended to use a PNG with a transparent background. |
| ![](/05_startview.png) | **StartView**<br><br>Interstitial that completely covers the device screen.<br><br>The content has a minimum size of 500x750px and a recommended size of 640x960px, but this varies depending on the device. The best option is to create a responsive page that adapts the content to the available screen dimensions. This will cover all the different devices on the market, resulting in a better user experience. |
| ![](/06_prisma.png) | **Prisma**<br><br>Interstitial displayed in ¾ of the screen centered. It behaves like a slider and has a 3D style.<br><br>We recommend an approximate size of 250-300x550-600px (width x height) for the content, but it will depend somewhat on the screen size of the device. Additionally, we recommend that the creative content be centered in the image and not be too close to the top and side edges. In other words, it is recommended to leave a safety margin to ensure that the creative looks good on most devices.<br><br>If the image is larger or smaller than the recommended dimensions, a center crop will be applied to adjust it to the available screen space. |

> For any format that allows the use of images, we recommend that they have a maximum size of 500KB for optimal performance when loading the image on the mobile device.
{.is-info}