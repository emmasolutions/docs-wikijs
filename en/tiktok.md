---
title: TikTok
description: 
published: true
date: 2025-01-13T08:31:44.470Z
tags: 
editor: markdown
dateCreated: 2024-09-02T12:19:04.060Z
---

# Tracking with TikTok App Installs
TikTok [App Installs](https://ads.tiktok.com/help/article/app-installs-objective?lang=en) is TikTok's advertising format for measuring app installs in order to gain new users.

This guide explains all the setup steps needed to start measuring your campaigns with TikTok App Installs.

## Set up in TikTok Ads
In order to measure TikTok App Install campaigns, the first thing you need to do is go to the TikTok Ads dashboard. If you don't have an account, you will need to create an account and register the Android and iOS apps separately.

If you create your app from scratch, it is important that when registering the app, in step 2 Tracking Settings you select Kochava as the provider. </br><p style="width: 700px">![tiktok_12.png](/tiktok_12.png)</p>

In case you already have the app created, you just need to access the app details and edit the Tracking Settings and select Kochava as the provider (in the steps listed below you can see how to access the details of your app in TikTok). </br><p style="width: 1200px">![tiktok_13.png](/tiktok_13.png)</p>

## Get TikTok App ID
Once you have [created](https://docs.emma.io/en/tiktok#set-up-in-tiktok-ads) your account and registered your apps, just follow these steps:
- Go to your app details from **Assets** > **Events**. </br><p style="width: 1200px">![tiktok_7.png](/tiktok_7.png)</p>
- Click on the **Manage** button. </br><p style="width: 1200px">![tiktok_8.png](/tiktok_8.png)</p>
- Click on your app. </br><p style="width: 800px">![tiktok_9.png](/tiktok_9.png)</p>
- Create and/or copy your TikTok App ID. </br><p style="width: 1250px">![tiktok_10.png](/tiktok_10.png)</p>
- Repeat this process with the Android and iOS app.
- Reserve the obtained TikTok App ID for later configuration in EMMA.





## Set up in EMMA
## Inapp event measurement (Optional)
# SkAdNetwork campaign
## Enabling SkAdNetwork in EMMA for installs
## SkAdNetwork inapp events measurement (optional)
# TikTok API: tracking to measure clicks and cost (optional)
# TikTok Re-Targeting Campaigns
## Set up events for Re-Targeting

