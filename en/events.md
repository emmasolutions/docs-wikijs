---
title: Events
description: 
published: true
date: 2021-11-04T13:05:40.599Z
tags: 
editor: markdown
dateCreated: 2021-11-04T11:26:12.258Z
---

In EMMA, events are used to identify the behavior and actions of users within an application. The same user can have an infinite number of repetitions for each event created.

You decide how you want to measure this behavior by delimiting the number and type of
events you want to measure.

In EMMA there are two different types of events. **Default events** and **custom events**.
# Default events
EMMA defines as **default events** those basic events that are common to all applications, and that are automatically created when EMMA integration is performed.

The following is the list of **default events** that will automatically appear in EMMA when the corresponding integration methods are used.

- **EMMA Install**: generates data when an installation occurs.
- **EMMA Session**: generates data when a session takes place.
- **EMMA Register**: generates data when a registration occurs.
- **EMMA First Login**: generates data when the first login occurs.
- **EMMA Login**: generates data when a login occurs.
- **EMMA Transaction**: generates data when a transaction occurs.
# Customized events
These are all those actions or sections within the application that are unique to your application. There are two main types of actions and they will result in the majority of all your **custom events**:
- **View a screen or section of the app**.
- **Click on a button in the app**.

Note that it is also possible that you send a custom event with attributes or properties.
For example, let&#39;s suppose that in our application we have a section that is a blog in which we show different posts. If we want to know which posts users access we will have to:

1. Send to EMMA an **event** that is a **blog**. This will measure if users access that specific section sending the information to EMMA when it happens.
2. To know which post within the blog they read, we must send as an **attribute or property** of the **blog event** the specific **name** of the **post**, and/or the internal **id of the post**.

To integrate custom events in your application and measure them with EMMA, the first thing to do is to create those events in EMMA. By creating an event in EMMA we will obtain a token that is necessary and essential for its implementation in the application code. You can create up to **512 custom** events per application.

See our [integration guides](https://developer.emma.io/es/home) for more information on how to integrate **custom events**. Next, we will see in detail how to create, edit and delete events.
# Create events
In order to integrate a custom event in the application and measure it with EMMA, the first
thing you need to do is to register it in EMMA. To do this, you need to:
- Log in to EMMA and go to **Management &gt; Events**.
![crear_eventos_1.png](/crear_eventos_1.png)
- Click on the **+ New event** button to create a new event.
![crear_eventos_2.png](/crear_eventos_2.png)
- Fill in the data for the event configuration:
![crear_eventos_3.png](/crear_eventos_3.png)
- **Name (required)**: select the name of the event. This name will define the event in the reports as well as in the rest of EMMA.
- **Token (optional)**: the event token is the identifier that you must integrate in the application code. If this space is left empty, EMMA will automatically assign a token to identify each event.
- **Active? (optional)**: indicates whether we want to &quot;turn on&quot; the event or not, by default the switch is active. In the case of marking it as inactive, EMMA will stop recording data for that event.
- **Is it a screen? (optional)**: mark this option if the event corresponds to the display of an application screen. It is important that you identify which events correspond to the display of a screen as there are [rules](https://docs.emma.io/es/reglas) directly dependent on this choice.
- **Order (optional)**: set an order for the event, the first being number one. This will help you to see the events in a coherent order in Explorer and when generating the reportings.
- **Postback (optional)**: set a valid URL for EMMA to notify every time such an event occurs.
# Manage events
Once we have created all the events we want to measure with EMMA, we can access **Management &gt; Events** as many times as we need to create new events, edit, manage or delete existing events.

From this screen, we can see in a table all the events that we have registered. In this table we can see the following information:

![eventos.png](/eventos.png)

- **Event Name**: name given to the event at the time of its creation. It can be modified by editing the event. This change has no effect beyond the name shown in EMMA to locate the event. More info [here](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos).
- **Token**: token of the event. If we make changes on this token in EMMA and no changes are made in the application code, the event will no longer be measured. More info [here](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos).
- **Order**: order established to the event. This affects the order in which the events are shown in the EMMA reporting screens. More info [here](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos).

In addition, next to the event name we can see some icons.

- If the event has a **stick** icon, it means that the event is active for measurement. The check [Active?](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos) of the event is activated. Normally, all events have this icon.
- If the event has a screen icon, it means that the event has been marked as **Screen** for use in communications with [rules](https://docs.emma.io/es/reglas). Normally, only those events that we want to use for communications with rules have this icon.
- If the event has no icon it means that it is not being measured in EMMA (regardless of whether it is implemented in the application) and is not being used as a screen for rule communications.
# Edit events
You can edit an event at any time by going back to Management &gt; Events and selecting the Edit option in the context menu to the left of the name:

![editar_evento_1.png](/editar_evento_1.png)
Changes in the fields of your event do not affect the event token that has already been generated. You can modify any field in the event configuration, but it is important to note that if the token is modified it is necessary to change it in the application code as well. Ourrecommendation is not to modify the token field once the event is implemented in the application.
![editar_evento_2.png](/editar_evento_2.png)
Once you click on the ***Edit Event*** button the changes will be applied to the event in question.

When the events have been implemented in the application code, EMMA will record the information and show it to you in the Explorer screen.

## Delete events
To delete an event, select the *Delete* option from the context menu:
![eliminar_eventos_1.png](/eliminar_eventos_1.png)
EMMA will ask you if you are sure to delete the event and once confirmed, it will be automatically deleted from your account:
![eliminar_eventos_2.png](/eliminar_eventos_2.png)