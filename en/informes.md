---
title: Reports
description: 
published: true
date: 2025-01-10T13:27:00.020Z
tags: 
editor: markdown
dateCreated: 2024-09-02T10:40:34.946Z
---

# Introduction

EMMA provides a dedicated screen for downloading reports whenever needed.  

To access this reports screen, simply navigate to the main menu and look for the section **General > Reports**. </br><p style="width:350px"> ![reports.png](/reports.png)
  
It is important to note that the data downloaded in any of the available reports will always correspond to the previously selected date range. </br><p style="width:500px">![reports2.png](/reports2.png)
  
> Downloading the report is only allowed with a maximum date range of 90 days.
{.is-info}

All reports have two download options:
  
1. **Download Report** > This option initiates a direct download of the report in the browser. Depending on the amount of data to process, it may take a bit longer to generate.  
2. **Send by Email** > This option processes the report in the backend and sends it via email once it has been generated. Depending on the data load, it may take slightly longer to complete.
  
> The email report will be sent to the email address used to log in to the EMMA platform.
{.is-info}

Additionally, there are multiple reports that allow scheduling to be sent automatically or even via SFTP.

Regardless of the option, the report download frequency can be selected from the following:

- **Hourly**: Receive a report every hour with data from the previous hour.
- **Daily**: Receive a report every day with data from the previous day.
- **Weekly**: Receive a report every week with data from the previous week.
- **Monthly**: Receive a report every month with data from the previous month.

You can find more information about how to schedule your communication reports [here]().
  
> The reports generated are stored for a specific period (4 or 24 hours, depending on the selected date range), ensuring that the same report can be downloaded during that period. </br></br> Reports excluding the current day are stored for 24 hours. This means that if the same file is requested again within those 24 hours for the same date range, the download will be faster since the report has already been generated. </br></br> Reports including the current day are stored for 4 hours. Any downloads for the same date range within these 4 hours will display the same data. After 4 hours from the initial generation, a new report will be generated with updated data.
{.is-info}

# Acquisition reports

Within EMMA's ***Acquisition reports***, several types of reports are available for download. Below is a detailed explanation of each report and its purpose:

- [Assisted Installs Report](https://docs.emma.io/en/informes#assisted-installs-report)
- [Media Source Report](https://docs.emma.io/en/informes#media-source-report)
- [Parameters Report](https://docs.emma.io/en/informes#parameters-report)
- [Installs by Unique Users Report](https://docs.emma.io/en/informes#installs-by-unique-users-report)
- [Attributed Clicks Report](https://docs.emma.io/en/informes#attributed-clicks-report)
- [Unattributed Clicks Report](https://docs.emma.io/en/informes#unattributed-clicks-report)

## Assisted installs report

The *Assisted Installs* Report is related to acquisition campaigns and provides information about the winning campaign for each install. This refers to the campaign that is attributed to each install, as well as the campaigns that assisted in the install. Specifically, it shows the campaigns (up to 3) with which the user interacted (clicked) before installing the app.

> If no campaign, source, or channel is selected, the report will by default download information from all campaigns that have installs within the selected date range.{.is-info}

![assisted_installs.png](/assisted_installs.png)

When downloading the *Assisted Installs* report, you will receive a CSV file with the following information:

|     |     |
| --- | --- |
| **Date** | The date the install was made. |
| **Winning campaign** | Name of the winning campaign, i.e., the one that achieved the attribution of the install. |
| **Winning source** | Name of the winning source, i.e., the source that achieved the attribution of the install. |
| **Winning channel** | Name of the channel linked to the winning source. If no channel has been assigned, *No Channel* will appear by default. |
| **Assisted campaign 1** | Name of the assisted campaign, i.e., the campaign that assisted in the attribution of the install. |
| **Assisted source 1** | Name of the assisted source, that is, the source that assisted in the attribution of the install. |
| **Assisted Channel 1** | Name of the channel linked to the assisted source. If no channel has been assigned, it will default to *No Channel.* |
| **Assisted Campaign 2** | Name of the assisted campaign, that is, the campaign that assisted in the attribution of the install. |
| **Assisted source 2** | Name of the assisted source, i.e., the source that assisted in the attribution of the install. |
| **Assisted Channel 2** | Name of the channel linked to the assisted source. If no channel is assigned, it will default to *No Channel*. |
| **Assisted campaign 3** | Name of the assisted campaign, that is, the campaign that assisted in the attribution of the install. |
| **Assisted source 3** | Name of the assisted source, i.e., the source that assisted in the attribution of the install. |
| **Assisted Channel 3** | Name of the channel linked to the winning source. If no channel has been assigned, it will display *No Channel* by default. |

## Media source report

The *Media Source* report is a performance report for acquisition campaigns that provides information for the selected date range.

Campaign and source data will be broken down by day, and only those sources that have had activity (clicks and/or installations) within the selected date range will be displayed.

Thanks to the available filters, we can select the campaigns, sources, and/or events for which we want to download information.

> If no campaign and/or source is selected, all those with data (installations and/or clicks) within the selected date range will be downloaded by default.
{.is-info}

![media_source.png](/media_source.png)

> It is necessary to select at least one event to download the report. A maximum of 15 events can be selected for each report. If more are selected, the dashboard will display a warning message and will not allow the report to be executed.
{.is-info}

![media_source2.png](/media_source2.png)

In this way, if we download a *Media Source* report, we will obtain a .csv file with the following information:

|     |     |
| --- | --- |
| **Date** | Date the click and/or installation was performed. |
| **O.S** | Operating system. |
| **Channel** | Name of the channel linked to the source in question at the time of its creation. If no channel has been assigned, it defaults to *No Channel*. |
| **Campaign** | Name of the campaign that generated clicks and/or installations. |
| **Campaign type** | Information about the type of campaign, either **Basic** or **Retargeting**. |
| **Source** | Name of the source that generated clicks and/or installations. |
| **Created on** | Date when the source in question was created. |
| **Powlink** | Powlink of the source in question. |
| **Short Powlink** | Short Powlink of the source in question. If it doesn't have one because it's an "old" source, it will show as N/A. |
| **Android URL** | Android redirect URL set in the campaign configuration in question. |
| **iOS URL** | iOS redirect URL set in the campaign configuration in question. |
| **Huawei URL** | Huawei App Gallery redirect URL set in the campaign configuration in question. |
| **Alternative URL** | Redirect URL set in the campaign configuration for all devices that are neither Android, iOS, nor Huawei with App Gallery. |
| **Deeplink** | Deeplink configured in the source in question. If no deeplink is configured, this field will appear empty. |
| **Created by** | Email of the user who created the source in question. |
| **Clicks** | Number of clicks generated that day for that specific source. |
| **Installs** | Number of installations generated that day for that specific source. |
| **Reengagement** | The number of unique interactions for each specific source. This field only applies to Retargeting campaigns. |
| **Event_X** | Total interactions with event_X on that day for the specific source. That is, the total number of events performed. |
| **Event_X Unique*** | Unique interactions with event_x on that day for that specific source. That is, unique events performed. |

> *To obtain this information, EMMA relies on unique customer IDs. If this information (customer ID) is not being sent from the app or if there are users who, for any reason, do not have a customer ID, they will not be considered and a 0 will be shown in the report.{.is-info}

</br><p style="color:red"> IMAGEN
  
## Parameters report
  
The *Parameters* Report is a performance report for acquisition campaigns with parameters per install, where information about the parameters of a source attributed to an install will be obtained.

![parameters.png](/parameters.png)
  
Through the campaign, source, and event selectors, we can configure the relevant settings to download the information. By default, if no campaign and/or source is selected, all those with data (installs) in the selected date range will be downloaded.
  
> It is necessary to select at least one event in order to download the report. A maximum of 15 events can be selected in each report. If more than 15 events are selected, the dashboard will display a warning message and will not allow the report to be generated.
{.is-info}

![parameters2.png](/parameters2.png)
  
The downloaded information will include all campaigns that have generated installs within the previously selected date range.

Thus, when downloading a *Parameters* report, we will obtain a .csv file with the following information:
  
|     |     |
| --- | --- |
| **ID** | User identifier number from the EMMA platform. |
| **Device ID** | Unique identification number for Android (AAID) and iOS (IDFA). |
| **Email** | Email address of the user registered in the mobile app. |
| **Client ID** | Your internal identification number to identify customer users. |
| **Installed on** | Date the user first opened the app after downloading. |
| **App version** | The latest version of the app installed on the user's device. |
| **City** | The city where the user is located while using geolocation in the mobile app. If geolocation is not used, the IP address is translated. |
| **Country** | The country where the user is located while using geolocation in the mobile app. If geolocation is not used, the IP address is translated. |
| **Referrer ID** | Unique identifier of the campaign source that provides information about the user's origin through the campaign. |
| **Channel** | Name of the channel linked to the source in question at the time of its creation. If no channel has been assigned, it defaults to *No Channel*. |
| **Campaign name** | Name of the campaign from which the user originates. |
| **Source name** | Name of the campaign source from which the user originates. |
| **Exid** | Custom network identifier such as click ID. |
| **Camid** | Custom order identifier such as campaign ID. |
| **DP** | Device (iPhone, iPad, Galaxy A1, etc.) |
| **App name** | Name of the app where the click was made. |
| **App categories** | Category of the app where the click occurs. |
| **Eat_sub[NUM]** | Where [NUM] is a number from 1 to 22. Information on extra parameters. |
| **Event_X** | Interactions with event_X on that day for that specific source. |

## Installs by unique users report
 
The *Installs by Unique Users Report* allows us to know, for the installs of the desired campaigns and/or sources and a specified date range, how many come from existing clients (client ID) based on the data. In this way, from the total volume of installs, we can determine how many are from users who were already clients and how many are not.

This report also includes selectors that will allow us to determine the campaigns and/or sources from which we want to obtain the information.

By default, if no campaign and/or source is selected, the information from all campaigns that have had installs within the selected date range will be downloaded.
  
![installs_by_unique_users.png](/installs_by_unique_users.png)
  
When downloading this report, we will obtain a .csv file with the following information:
  
|     |     |
| --- | --- |
| **Device ID** | Device ID of the user who installed the app. |
| **Customer ID** | Customer ID linked to the device that installed the app. If this information is not sent or not available at the time of downloading the report, this field will appear empty. |
| **Channel** | Name of the channel linked to the source at the time of its creation. If no channel has been assigned, it will appear as *No Channel* by default. |
| **Campaign** | Name of the campaign the user comes from. |
| **Source** | Name of the source of the campaign the user comes from. |
| **Is it a new user?** | Verification of whether the customer ID previously existed in the database or not, i.e., verification of whether it is a new customer. |
  
## Attributed clicks report
  
The Attributed Clicks Report is an acquisition report that allows us to download information from campaigns and/or sources that have received clicks, breaking down the number of clicks made by each device in the relevant campaigns within the selected date range.

This report also includes selectors that allow us to determine the campaigns and/or sources from which we want to obtain information.

By default, if no campaign and/or source is selected, information from all campaigns that have received clicks within the selected date range will be downloaded.
  
![attributed_clicks.png](/attributed_clicks.png)
  
When downloading this report, we will receive a .csv file with the following information:
  
|     |     |
| --- | --- |
| **Date** | Date when the device made the click. |
| **Clicks** | Number of clicks made by each device. |
| **Device** | Device model. In some cases, this information may not be available, and in those cases, the literal "undefined" will be shown. |
| **Customer ID** | Customer ID linked to the device that made the clicks. If this information is not provided, this field will appear empty. |
| **O.S** | Operating system of the device from which the click was made. In some cases, this information may not be available, in which case the literal "Unknown" will be shown. |
| **Action** | It will display the literal "install" or "engagement" depending on whether it is a click for install or retargeting. |
| **Device ID** | Device ID that generated the clicks. |
| **Channel** | Name of the channel linked to the source at the time of its creation. If no channel has been assigned, *No Channel* will appear by default. |
| **Campaign** | The campaign to which the clicks are linked. |
| **Source** | The source to which the clicks are linked within the campaign. |
 
 ## Unattributed clicks report
  
The Unattributed Clicks report is an acquisition report that allows us to download information about campaigns that have had clicks, breaking down the number of clicks made by each device in the relevant campaigns within the selected date range.

This report also has selectors that allow us to determine the campaigns and/or sources from which we want to obtain the information.

By default, if no campaign and/or source is selected, the information will be downloaded for all campaigns that have had clicks within the selected date range.
  
![unattributed_clicks.png](/unattributed_clicks.png)
  
> This report only allows downloading information from the last 7 days.
{.is-info}

When downloading this report, we will obtain a .csv file with the following information:
  
|     |     |
| --- | --- |
| **Date** | Date on which the device made the click. |
| **Clicks** | Number of clicks made by each device. |
| **Device** | Device model. In some cases, this information cannot be determined, and in those cases, the literal "undefined" will be displayed. |
| **Customer ID** | As it is an unattributed click, the client ID cannot be determined, so this field will always be empty in this report. |
| **O.S** | The operating system of the device from which the click was made. In some cases, this information cannot be obtained. In those cases, the term "Unknown" will be shown. |
| **Action** | It will display the term "install" or "engagement" depending on whether it is an installation click or a retargeting click. |
| **Device ID** | Since it is an unassigned click, the device ID cannot be determined, so this field will always be empty in this report. |
| **Channel** | Name of the channel linked to the source in question at the time of its creation. If no channel has been assigned, it will appear as *No Channel* by default. |
| **Campaign** | Campaign to which the clicks are linked. |
| **Source** | Source to which the clicks are linked within the campaign. |
  
> We refer to an unattributed click when it has not resulted in an install or engagement.
{.is-info}

# Events report

## Events report

The Events report is a report that will allow us to know the total and unique event volume generated within a specific date range.
  
![events_report.png](/events_report.png)
  
> It is necessary to select at least one event in order to download the report. A maximum of 15 events can be selected in each report. If more are selected, the dashboard will display a warning message and will not allow the report to be generated.
{.is-info}

![events_report2.png](/events_report2.png)
  
Additionally, a schedule can be set for this report by configuring the frequency and the recipient emails for the report. You can find more information about how to schedule reports [here](https://docs.emma.io/en/informes#programming-your-reports).
  
In this report, the information of the previously selected events will be downloaded as follows:
  
|     |     |
| --- | --- |
| **Unique event** | The number of unique times an event was performed within the selected date range. |
| **Total events** | Total number of times an event has occurred in the selected date range. |

> In the report, two columns (unique and total) will be downloaded for each selected event.
>{.is-info}
  
## Detailed event report

The *Detailed events report* is a report that allows us to know in detail the unique and total events performed within a specific date range, broken down by day and operating system.

> To download the report, it is necessary to select at least one event, with a maximum of 15.
> {.is-info}
  
![detailed_event_report.png](/detailed_event_report.png)
  
In this report, the information of the previously selected events will be downloaded as follows:
 
|     |     |
| --- | --- |  
| **Day** | Day x of the selected date range. Each day will generate a new line in the report. |
| **O.S** | Operating system that generated the events on the specific day. |
| **Event name** | Name of each event selected for downloading the report. |
| **Event token** | Token of each event selected to download the report. You can find more information about what a token is [here](https://docs.emma.io/en/primeros-pasos/eventos#customized-events). |
| **Unique** | Unique events performed. |
| **Total** | Total events performed. |
  
## Event properties

The *event properties report* is a report that allows us to understand the concurrency of the properties of one or more specific events. In this report, you can select the desired event(s) as well as the properties you wish to download.

![event_properties.png](/event_properties.png)
  
> It is necessary to select at least 1 event in order to download the report. A maximum of 15 events can be selected in each report. If more than 15 are selected, the dashboard will display a warning message and will not allow the report to be executed.
> 
> There is no limit for the properties, and all existing ones can be selected.
{.is-info}
  
![event_properties2.png](/event_properties2.png)

You can also set up a schedule for this report by configuring the frequency and the recipient emails for the report. You can find more information on how to schedule reports [here](https://docs.emma.io/en/informes#programming-your-reports).
  
> In order to use this report, it is necessary that events with properties are being sent from the app. You can find more information about events with properties [here](https://docs.emma.io/en/primeros-pasos/eventos#customized-events). {.is-warning}
  
In this report, the following information will be obtained:

|     |     |
| --- | --- |
| **Event name** | The name of the downloaded event will be displayed. |
| **Properties x of the event** | A column will be displayed for each of the selected properties. For example, if 4 properties are selected, 4 columns will be shown, one for each property. |
| **Total events** | The total number of times the combination of the event properties has occurred. |
| **Unique events** | The unique number of times the combination of the event properties has occurred. |
  
> A maximum of a 90-day range can be downloaded at once.
{.is-info}
  
## Attributed events report

The attributed events report is a report that allows us to see the devices that have performed a specific event within a given date range and that are also attributed to a specific campaign.
  
![attributed_events.png](/attributed_events.png)
  
|     |     |
| --- | --- |
| **Customer ID** | Client ID linked to the device that performed the event. |
| **Device ID** | Device ID that performed the event. |
| **Channel** | Name of the channel linked to the source in question at the time of its creation. If no channel is assigned, it defaults to *No Channel*. |
| **Campaign** | Name of the campaign to which the event is attributed in the app tracker. |
| **Source** | Name of the media source to which the event is attributed in the app tracker. |
| **O.S** | Operating system of the device that performed the event. |
| **Device** | Device model that performed the event. |
| **Date** | Date when the device performed the event. |
| **Event name** | Name of the event performed. |
  
## Events by customer report
  
The *Events by customer report* is a report that allows us to see the customer ID who performed a specific event and the exact date of the event within a determined date range.
  
> You can select a maximum of 15 events at a time in a single export. The maximum export date range is the last 3 months, as with other reports.
> {.is-info}
  
![event_by_customer.png](/event_by_customer.png)
  
In this report, we will obtain the following information:
  
|     |     |
| --- | --- |
| **Customer ID** | Customer ID linked to the device that performed the event. |
| **Token** | Event token identifier in EMMA. You can see the correlation between the token and the name [here](https://docs.emma.io/en/primeros-pasos/eventos#manage-events). |
| **Date** | Date when the device performed the event. |
  
# Raw Date Report
  
## Installations
  
The *installations report* is a raw data report of all installations, both organic and acquisition-based, within the selected date range.
  
When downloading the installation report, although both options are available, if a large data download is required, we recommend using the email download option.
  
![installations.png](/installations.png)

In this report, we will obtain the following information:
  
|     |     |
| --- | --- |
| **ID** | User identifier number of the EMMA platform. |
| **Device ID** | Unique identification number for Android (AAID) and iOS (IDFA). |
| **Email** | Email address of the user registered in the mobile application. |
| **Customer ID** | Your internal identification number for identifying client users. |
| **Installation on** | Date when the user first opened the application after downloading it. |
| **Registered on** | Date when the user registered for the first time. |
| **First login** | The first time the user has logged in to the mobile application. |
| **Last login** | The last time the user logged into the mobile application. |
| **Device** | The device model of the user through which they used the mobile app. |
| **App version** | The latest version of the app installed on the user's device. |
| **O.S version** | The latest version of the operating system installed on the user's device. |
| **EMMA Build** | The version of the SDK configured in the latest version of the mobile application. |
| **Rated** | Indicates those users who have rated the mobile app (number 1) and those who have not rated it (number 0). |
| **Inactive** | Indicates active users for receiving Push notifications (number 0) and inactive users (number 1). |
| **Latitude** | Latitude coordinates of the user while using geolocation in the mobile app. If geolocation is not used, the IP address is translated. |
| **Longitude** | Coordinates of the user's longitude while using geolocation in the mobile app. If geolocation is not used, the IP address is translated. |
| **City** | City where the user is located while using geolocation in the mobile app. If geolocation is not used, the IP address is translated. |
| **Country** | Country where the user is located while using geolocation in the mobile app. If geolocation is not used, the IP address is translated. |
| **Referrer ID** | Unique identifier of the campaign source that provides information about the origin of the user from the campaign. |
| **IP** | IP address from which the user connected to the app. |
| **Loyal user on** | The moment when the user becomes a loyal user (fifth login of the user). |
| **Loyal buyer since** | Moment when the user becomes a loyal buyer (starting from the user's second purchase). |
| **Last session** | Last time the user opened the mobile app. |
| **Conversions** | Number of purchases the user has made. |
| **Last conversion** | The last time the user made a purchase through the mobile app. |
| **First conversion** | The first time the user made a purchase through the mobile app. |
| **Seconds until the first conversion.** | Seconds elapsed from when the user installs the app to when they make the first purchase through it. |
| **Accepted language** | Device language |
| **Operator** | Mobile phone company contracted by the user. |
| **Connection type** | Indicates whether the user's connection is through WIFI or 3G. |
| **Channel** | Name of the channel linked to the source at the time of its creation. If no channel has been assigned, *No Channel* will appear by default. |
| **From campaign** | The name of the campaign from which the user originates. |
| **From source** | Name of the source of the campaign the user comes from. |
| **Installation postback** | Installation notification sent to the provider. |
| **TAGs** | Values assigned to each of the tags received from the app, if they exist. We can select the tags we want to include in the report from the selector. |
| **Events** | Counter of the number of times each device has performed any of the events sent from the app. We can select the events we want to include in the report from the selector. |

# Communication report
  
## Active campaigns

The *active Campaigns report* is a report related to communication campaigns that are active within a specific time period.

This report allows us to see in a document all the campaigns that are or were active during the selected date range, as well as obtain information related to them, such as impressions, unique impressions, and clicks.
  
![active_campaigns.png](/active_campaigns.png)
  
Similar to the other reports, when downloading this one, we will obtain a .csv file with the following information:
  
|     |     |
| --- | --- |
| **Report ID** | Campaign identifier in EMMA. It corresponds to the ID on the Communications Report screen. |
| **Campaign ID** | Campaign identifier in EMMA. It corresponds to the ID on the Messages screen. |
| **Campaign** | Name of the communication campaign. |
| **Format** | Format of the communication. It can be Adball, Banner, Coupon, Dynamic Tab, Email, Native Ad, Push, Startview, and Strip. |
| **Template** | Name of the Native Ad template used in each of the Native Ad communications. |
| **Screen** | Relative to automatic communications for rules. Name of the screen where the communication is being displayed. |
| **Frequency** | Frequency set for the communication campaign. If it shows 0, it means that no frequency has been set, and it is infinite as long as the campaign is active. |
| **Daily impression limit** | Daily impression limit set for the communication campaign. If it shows 0, it means no limit has been set, and it is infinite as long as the campaign is active. |
| **Weekly impression limit** | Weekly impression limit set for the communication campaign. If 0 is shown, it means no limit has been set, and it is infinite as long as the campaign remains active. |
| **Monthly impression limit** | Monthly impression limit set for the communication campaign. If it shows 0, it means no limit has been set, and it is infinite as long as the campaign is active. |
| **Start** | In classic campaigns, the date and time the campaign starts. In automated campaigns for rules, the start date of the rule. |
| **End** | In classic campaigns, the end date and time of the campaign. In automated campaigns for rules, the end date of the rule (rules may not have an end). |
| **Priority** | Priority set for the rule from 1 to 9. The higher the number, the higher the priority. |
| **Impressions** | Number of times the communication has been shown per campaign (total impressions). |
| **Unique impressions** | Number of devices on which the communication has been shown. |
| **Clicks** | Number of times the user interacts with the communication per campaign. |
| **CTR** | Percentage of devices that clicked on the communication relative to the total impressions per campaign. |
  
## Campaign display

The *campaign display report* is a report that provides information about the users who have viewed each communication, as well as generic information to identify the campaign and its status that the user has viewed.
  
> The download of this report is limited to the last 30 days. It will not be possible to download data with a date range longer than that.
> {.is-warning}
  
This report allows us to view in a document all the users who have viewed X communications, as well as information such as the campaign ID, the status, or the impression date.
 
![campaign_display.png](/campaign_display.png)

As with other reports, when downloading this one, we will obtain a .csv file with the following information:
  
|     |     |
| --- | --- |
| **Campaign** | Name of the communication campaign. |
| **Campaign ID** | Campaign ID in EMMA. It corresponds to the ID on the Messages screen. |
| **Customer ID** | Client ID linked to the device that viewed the communication. |
| **Frequency** | Frequency set for the communication campaign. If it shows 0, it means no frequency has been set, and it is infinite while the campaign is active. |
| **Status** | The status of the campaign. It can be Paused, Active... You can see more information about the different campaign statuses [here]. |
| **Format** | Communication format. It can be Adball, Banner, Coupon, Dynamic Tab, Email, Native Ad, Push, Startview, and Strip. |
| **Type** | Type of communication: Classic or automatic. |
| **First View Date** | Date and time when the user first viewed the communication in question. |
| **Start** | Start date and time of the campaign. In automatic campaigns, this information is not shown as there is no specific start date for the campaign. |
| **End** | End date and time of the campaign. In automatic campaigns, this information is not shown as there is no specific end date for the campaign. |
| **Template ID** | Template ID used in the communication. This field only applies to the Native AD format. In other in-app formats, it will appear empty. You can find more information about the Native Ad format [here]. |
| **Category** | Name of the category configured in the communication (if applicable). You can find more information about categories [here](https://docs.emma.io/en/categorias). |
| **Test A/B** | Whether the communication has A/B Test variations or not. You can find more information about A/B Test [here](https://docs.emma.io/en/test-a-b). |
| **First click** | Date when each user first clicked on the communication in question. |
 
## Out-App campaign view report (Push)

The out-App campaign view report is a report that provides information about users who have received a push notification and whether they opened it or not, as well as more generic information to identify the user, the campaign, and its status.
  
> This report is based on the client IDs that have received a push notification. It is possible that there are devices without a linked client ID but still receive the push notification. These cases will not appear in this report in any way.
>{.is-warning}
  
This report allows us to see in a document all the users (Customer IDs) who have received and opened a specific push notification, as well as information such as the status, and the date of opening and sending the communication.

![push_campaigns_display.png](/push_campaigns_display.png)

As with the other reports, when downloading this, we will obtain a .csv file with the following information:
  
|     |     |
| --- | --- |
| **Campaign** | Name of the push campaign. |
| **Customer ID** | Client ID linked to the device that received the specific push notification. |
| **Status campaign** | The status of the campaign. It can be Paused, Active... You can find more information about the different campaign statuses [here]. |
| **Campaign type** | Type of communication: Classic, Automatic for rules, or Automatic for customer journeys. |
| **Opening date** | Date and time when the user opened the push notification. If the notification has not been opened, this field will be empty. |
| **Send date** | Date and time when the push notification was sent. |
  
# Sales reports

The sales report is a report that will allow us to see a detailed list of purchases made through the application within a specific date range, which will allow us to validate the data with an external source or simply keep track of the sales made.
  
![sales_report.png](/sales_report.png)
  
In this report, the following information will be obtained:
  
|     |     |
| --- | --- |
| **Order ID** | Order ID. This is a unique ID for each purchase made through the app. |
| **Customer ID** | This is the ID of the customer linked to the device that made the purchase. |
| **Quantity** | Total amount of each of the different purchases made. |
| **Currency code** | It shows the currency code selected in EMMA under the [app preferences](https://docs.emma.io/en/configuracion#location). This code follows the international ISO standard. |
| **Created on** | Date and time when the user made the purchase through the application. The information will be displayed according to the time zone selected in the [app preferences](https://docs.emma.io/en/configuracion#location). |
  
# Programming your reports
  
EMMA allows you to schedule multiple reports to be automatically sent to one or more email addresses or directly to a specific SFTP. To schedule your reports for automatic delivery, simply follow these steps:

- Click the **Schedule report sending** button in the communication report you want to schedule.
  
![programming_your_reports.png](/programming_your_reports.png)

- Fill out the configuration form:
  
![programming_your_reports2.png](/programming_your_reports2.png)


- **Period:** Select between the options ***Hourly***, ***Daily***, ***Weekly***, or ***Monthly***. With the ***Hourly*** option, the report will be sent every hour with data from the previous hour. With the ***Daily*** option, the report will be sent every day with data from the previous day. With the ***Weekly*** option, the report will be sent on the selected day of the week (Monday-Sunday) with data from the previous 7 days. With the ***Monthly*** option, the report will be sent on the 1st day of each month with data from the previous month.  

- **Report Recipients:** Set the email addresses of those who should receive the report.  

- Enable the **SFTP** checkbox if you want to send the report to an SFTP and set up the necessary configuration.

![programming_your_reports3.png](/programming_your_reports3.png)
  
- **Host:** Set the exact path of the SFTP where the report will be sent. Example: sftp://host/path  
- **User:** Configure a user with access to the configured SFTP.  
- **Password:** Set the password for the user with access to the SFTP configured in the previous step.
  
> Scheduled reports will be sent at 9:00 UTC in the selected period.  
> Also, keep in mind that they will be sent in .zip or .csv format depending on the configuration set in [App Preferences](https://docs.emma.io/en/configuracion#reports).{.is-info}

# Save the report settings
  
If you create multiple configurations of the same report to get the detailed data you need, you can save that configuration so you don't have to select all the KPIs every time you need to retrieve the information.
  
## Save settings
  
To save a new configuration, simply follow these steps:

- Locate the report in question.
- Set up the desired configuration.
- Click on the save icon. </br><p style="width: 150px">![informes_21.png](/informes_21.png)</p>
- Select the saved configuration you wish to load. </br><p style="width:300px">![save_settings.png](/save_settings.png)
  
> The save icon always appears unlocked, but it only allows you to save a configuration if the mandatory fields of the report have been filled out.
> {.is-warning}

## Load configuration
  
You can save as many configurations as you need, and to load them, simply follow these steps:

- Locate the specific report.
- Click on the save icon. </br><p style="width: 150px">![informes_21.png](/informes_21.png)</p>
- Select the saved configuration you want to load. </br><p style="width:270px">![load_configuration.png](/load_configuration.png)
  
> To delete a configuration, simply click on the trash can icon that appears to the right of the name of each configuration. </br><p style="width:250px">![load_configuration_copia.png](/load_configuration_copia.png)
{.is-info}

