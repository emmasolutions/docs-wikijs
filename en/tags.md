---
title: Tags
description: 
published: true
date: 2021-11-04T13:19:51.393Z
tags: 
editor: markdown
dateCreated: 2021-11-04T13:19:49.695Z
---

In EMMA tags are used **to identify unique characteristics or states of the user**. With this type of tagging a unique value is assigned to each user and will be automatically overwritten if a new value is triggered.

Here are some common examples of tag measurement:

- Country: Spain, Portugal, UK, US,....
- Age: 18, 19, 20, 21...
- Phone number: 650050550, 696362656, 652987541....
- Notifications: On, Off
- Share on social networks: Yes, No.
- Birthday: 1988-11-01,1992-02-26...

See our [iOS](https://developer.emma.io/es/ios/integracion-sdk#propiedades-del-usuario-tags) and [Android](https://developer.emma.io/es/android/integracion-sdk#perfil-del-usuario) integration guides for more information on how to integrate **Tags**.

# Label management

Inside the EMMA **Management** section you will find a screen called **Tags**, where you can configure the tags that your application is sending to EMMA from the SDK. You can see how to use them in [*Push*](https://docs.emma.io/es/comunicacion/mensajes-out-app/push-notifications) Notifications and [*InApp*](https://docs.emma.io/es/mensajes-inapp) communications.

![etiquetas_i.png](/etiquetas_i.png)
In order to view the list of tags in this screen you must have previously [tagged](https://docs.emma.io/es/otros#c%C3%B3mo-etiqueto-mi-app) the application, otherwise this screen will be empty.

Here you will be able to see all the tags that are being sent to EMMA and you will be able to configure the type of information that each one will contain. 

> It is the responsibility of each application to include in the labels the information previously agreed upon. Thus, if in this screen a numeric value field is assigned the text type, EMMA will interpret it as text.
{.is-warning}

![etiquetas_ii.png](/etiquetas_ii.png)

By clicking on the contextual menu of each label, we can edit it to select the type of information that each label contains
- **Text**
- **Number**
- **Date**

![etiquetas_iii.png](/etiquetas_iii.png)
Once the desired value is selected, click on the **+ Save Label** button to apply the changes made.

Remember that depending on the type of information with which the tag is configured (text, number or date), it will affect how these tags can be filtered and segmented in EMMA. 

In the following table you can see which type of operator accepts each type of tag.

- Text tags
  - is equal to
  - is different from
  - contains
  - exists
  - does not exist
- Numerical labels
  - is equal to
  - is different from
  - exists
  - does not exist
  - more than
  - less tan
- Date tags
  - exists
  - does not exist
  - was on
  - was ago
  - more than
  - less than
  - the birthday is
  
> For the time being, these tag customizations will only be applicable to Rules recipes and not to Explorer and Messages segmentation.
{.is-info}
