---
title: Fuentes de medios
description: Fuentes de medios
published: true
date: 2025-03-03T11:07:56.573Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:46:12.437Z
---

# Gestión de fuentes de medios

https://youtu.be/H0l6IZ7od9A

Una vez que se haya creado la campaña, es necesario añadir al menos una source para poder generar el powlink de EMMA.

El powlink de EMMA no es más que un tracking link, un enlace de seguimiento o clic command encargado de realizar las redirecciones pertinentes a las URLs que se hayan configurado previamente en la configuración de la campaña.

A continuación veremos más detalladamente cómo dar de alta nuevas fuentes en EMMA, así cómo editarlas, clonarlas y eliminarlas. 

-   Crear fuentes
-   Editar y clonar fuentes
-   Eliminar fuentes

## Crear fuentes

Para acceder a las fuentes de una campaña, debemos seleccionar la opción **Fuentes** del menú contextual de la tabla resumen de campañas. 

![](/fuentes_i.png)

Dentro de las fuentes, deberemos hacer clic en el botón ***Nueva fuente*** y cubrir el formulario para crear un nuevo tracking link a nuestra campaña.
</br><p style="width: 800px;">![fuentes_6.png](/fuentes_6.png)</p>

|     |     |
| --- | --- |
| ***Nombre*** | Establece un nombre identificativo para la fuente/tracking link. |
| ***Fuente de medios*** | Selecciona un proveedor en el [listado](http://emma.io/partners). EMMA notificará al proveedor todas las instalaciones y eventos in-app provenientes de su tráfico. Se puede seleccionar la opción personalizada *(Custom)* si no se desea notificar a ningún proveedor. Si el proveedor al que se quiere notificar no se encuentra en el listado, envía tu solicitud a [EMMA support](mailto:support@emma.io) y lo integraremos como un nuevo partner. |
| ***Canal**** | Opcionalmente puedes estableer un canal. Esto te servirá para posteriormente poder [agrupar](https://docs.emma.io/es/barras-de-menu#filtros-en-captaci%C3%B3n-apptracker) tus fuentes en base a los canales vinculados. Para crear un canal tan sólo tienes que escribir el nombre de un nuevo canal que deseas crear y hacer clic en **Añadir "Nombre de canal"**.</br><p style="width:600px">![fuentes_7.png](/fuentes_7.png)</p>  En caso de no establecer ningún canal, se categorizará bajo la nomenclatura de canal **Sin canal**. </br></br> La fuente se puede editar en cualquier momento y se permite modificar o eliminar el canal vinculado a dicha fuente.  |
| ***Modelo de pago*** | Selecciona el modelo de pago con el que se quiera medir la compra/venta del tráfico en el informe de la source entre las opciones disponibles: </br> - **Coste fijo** </br> - **CPC** (Coste por clic) </br> - **CPI** (coste por instalación) </br> - **CPA** (coste por acción): En caso de seleccionar la opción CPA, se desplegará un nuevo input llamado **Evento** desde el cual podremos seleccionar el evento en cuestión bajo el que queremos que se calcule el coste total de la campaña. Este evento será el mismo para todas las fuentes de la misma campaña y si lo modificamos en una fuente se modificará automáticamente en todas las demás. |
| ***Coste*** | Rellena la opción de pago. Si tu campaña no tiene coste, rellénalo con un 0. |
| ***Limitar tráfico VPN*** | Por defecto está opción está deshabilitada, pero si la habilitamos los clics que provengan de VPNs serán descartados, con el consiguiente descarte del tráfico. |
| ***Click ID*** | Si se está utilizando un SDK diferente al de EMMA, se deberá cambiar el ID del click. En este [artículo](https://docs.emma.io/es/adquisicion/apptracker#tracking-de-campa%C3%B1as-sin-el-sdk-de-emma) se puede ver más información para saber qué ID de transacción se debe utilizar en cada caso. Si se está utilizando el SDK propio de EMMA, este campo no necesita ningún cambio, se deja con la configuración por defecto. |
| ***Sufijo personalizado en URL corta\***** | Se puede establecer un sufijo personalizado para los short URL. En caso de no querer añadir uno, se deja este campo vacío y EMMA creará uno de manera aleatoria. Recuerda que este sufijo debe estar compuesto únicamente por caracteres alfanuméricos. |
| ***Atribución*** | Si se está dando de alta una fuente de una campaña de Re-Targeting, se ha de configurar la ventana de atribución deseada hasta un máximo de 90 días. Si no se configura ningún valor, se usará la [ventana de atribución de Re-Targeting](https://docs.emma.io/es/ventanas_atribuci%C3%B3n#personalizaci%C3%B3n-de-ventana-de-atribuci%C3%B3n-de-retargeting-en-emma) por defecto de EMMA. |
| ***¿Utilizar landing de deeplink?*** | Activa la landing de deeplink para aquellos casos en los que no sea posible levantar la app automáticamente con un Universal/App Link. Para ver más información acerca de landing de deeplink, consulta nuestro artículo sobre [redirección para navegadores no nativos](https://docs.emma.io/es/adquisicion/apptracker#redireccionamiento-para-navegadores-no-nativos). |
| ***Ruta deeplink*** | Establece tu ruta de deeplink para que los usuarios que ya tengan tu aplicación sean redireccionados a una sección concreta de la app y no a las stores. Visita nuestro artículo sobre [cómo usar el Powlink en EMMA](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) para conocer cómo gestionar esta tecnología en tus campañas. |

> *En el caso de las redes autoatribuidas, como Apple Search Ads, Google, Meta, TikTok, Twitter... el canal se aplica de manera autmática. Estos serían los canales para cada una de las redes: </br> - **Apple Search Ads**: Apple Search Ads </br> - **Google**: Google Ads UAC y Google Ads UACE </br> - **Meta**: Meta </br> - **TikTok**: TikTok </br> - **Twitter**: X (Twitter) </br> - **Snapchat**: Snapchat </br> - **Petal Ads**: Petal Ads - Generic 
{.is-info}

\**EMMA ofrece la posibilidad de establecer un sufijo personalizado a la URL de seguimiento corta, para una mejor identificación de la misma y también de cara a la visibilidad que puedan tener los clientes en caso de que se emplee en un SMS, por ejemplo. De esta manera, se puede establecer un sufijo a la URL corta de hasta **10 caracteres** alfanuméricos en el campo estipulado para ello. 

En caso de no querer personalizar el sufijo, simplemente se debe dejar este campo vacío y EMMA generará un sufijo de manera aleatoria. 

Veámoslo con un ejemplo.

Supongamos que se va a realizar un envío de SMS y queremos trackear ese envío. Establecer en un SMS el powlink clásico de EMMA no es práctico, ya que alarga en exceso el contenido del SMS, por lo que podemos optar por usar el powlink acortado y personalizarlo con el texto *sms* por ejemplo. De esta manera, nuestro enlace corto de EMMA se verá de la siguiente manera: 

[https://emma.pwlnk.io/sms](https://emma.pwlnk.io/sms)

Obtenemos un enlace mucho más manejable y con la misma funcionalidad que el powlink clásico, perfecto para emplear en determinados casos que se necesita un enlace con una extensión más corta. 

> Los sufijos deben ser únicos. Si un sufijo ya está siendo usado en otra fuente, no se podrá volver a usar en una nueva.
{.is-warning}



## Editar y clonar fuentes

Si es necesario realizar alguna modificación en la fuente o crear otra con las mismas características, existe la posibilidad de editar o clonar la fuente de manera rápida y sencilla. 

Tan solo hay que hacer clic en el menú contextual y seleccionar la opción específica de ***Editar*** o ***Clonar.***

![](/fuentes_3.png)

En caso de que se haya **editado** la fuente, se abrirá el formulario de creación de fuentes con la configuración que haya establecida para poder hacer las modificaciones que sean pertinentes. Una vez se hayan realizado los cambios, tan solo deberemos hacer clic en el botón ***Guardar campaña*** para guardar los cambios realizados.

En caso de que se haya **clonado** la fuente, se abrirá el formulario de creación de la fuente copiando la configuración de la fuente clonada. De esta manera, con esta opción podremos ahorrar tiempo en la creación de fuentes que tengan la misma configuración. 

> Ten en cuenta que el proceso de ***Editar*** y ***Clonar*** fuentes se puede llevar a cabo tanto desde la agrupación de campañas como desde la agrupación de canales. Puedes ver más info sobre la agrupación [aquí.](https://docs.emma.io/es/adquisicion/apptracker) {.is-info}

## Eliminar fuentes

Si necesitamos eliminar una fuente, tan solo tenemos que ir al menú contextual y seleccionar la opción ***Eliminar.*** 

Una vez seleccionada la opción ***Eliminar***, la campaña será eliminada inmediatamente del sistema. 

![](/fuentes_4.png)

> El proceso de eliminar una fuente se puede llevar a cabo desde la agrupación por campañas y desde la agrupación por canales. {.is-info}

# Notificar eventos a redes publicitarias

Desde EMMA, estamos integrados con cientos de redes publicitarias diferentes para que puedas gestionar tus campañas de captación o Re-Targeting de manera sencilla desde EMMA y la red pueda recibir toda la información de la campaña en su sistema de manera automática para poder gestionar y optimizar la misma para conseguir un resultado óptimo. 

Para que EMMA pueda notificar a las redes pertinentes, en primer lugar es necesario crear un tracking link desde apptracker y vincular el proveedor (red) adecuado. En esta [documentación](https://docs.emma.io/es/adquisicion/apptracker#gesti%C3%B3n-de-fuentes) puedes ver información sobre cómo realizar esta configuración. 

## Configuración de eventos para notificar a redes publicitarias

Una vez creado el tracking link, deberemos configurar aquellos eventos que queramos notificar a la red en cuestión. Para ello tenemos que seguir estos pasos: 

-   Preguntar a la red si es posible notificarle uno o varios eventos y con qué nombre o ID quiere que se le notifique.
-   Accede a EMMA y ve a la sección Captación > Fuentes de Medios.

![](/notificar_redes_1.png)

-   Busca y edita la Fuente de Medios (proveedor/red) que hayas seleccionado al crear el tracking link.

![](/notificar_redes_2.png)

-   En la pantalla de edición, busca la sección *Eventos* y haz clic sobre el botón ***Añadir evento.*** Realiza este paso tantas veces como eventos quieras notificar a la red.

![](/notificar_redes_3.png)

-   Selecciona los eventos que se van a notificar a la red publicitaria.   
    En el campo ***Identificador del Evento para la Fuente de medios***, configura el nombre con el que la red va a recibir el evento. Puede ser un número, una palabra o una cadena alfanumérica. Si se deja en “blanco”, se notificará exactamente el nombre del evento que se haya puesto en EMMA cuando se creó. En este caso, se podrá ver sombreado cómo se notificará.

![](/notificar_redes_6.png)

> Para poder saber si un proveedor/red publicitaria permite el envío de eventos o no, ponte en contacto con nuestro equipo te integraciones@emma.io para que te verifiquen la configuración realizada en cada caso.
{.is-info}

Una vez se hayan cumplido todos estos pasos, tan solo es necesario guardar los cambios y la red empezará a recibir notificaciones cada vez que un usuario atribuido a su campaña realice los eventos pertinentes. 

## Dejar de notificar eventos a redes publicitarias

Si necesitamos que se deje de notificar un evento a la red, tenemos que seguir estos pasos: 

-   Ve a ***Captación > Fuentes de medios.*** 
-   Busca y edita la fuente de medios (proveedor/red) en cuestión.
-   En la pantalla de edición, ve al apartado de ***Eventos*** y haz clic en el botón ***Eliminar*** del evento que se quiere dejar de notificar.

![](/notificar_redes_7.png)

# Guía de Integración AdNetwork

EMMA te ofrece retroalimentación cuando se  hace una conversión que viene de su tráfico. Cuando nuestro algoritmo de seguimiento coincide con una instalación de un clic, se notifica a tu sistema gracias a una devolución de llamada configurada previamente.

Para configurar nuestra *callback URL* (devolución de llamada) necesitas enviar un e-mail a [integraciones@emma.io](mailto:integraciones@emma.io) con la siguiente información: 

1.  Tu URL base de su api o su **píxel S2S.**
2.  Los **parámetros** y valores dinámicos o fijos a anexar a través de la redirección. Consulta [este artículo](https://docs.emma.io/es/adquisicion/fuentes-de-medios#c%C3%B3mo-a%C3%B1adir-par%C3%A1metros-personalizados) para conocer cómo anexar tus parámetros. Es muy recomendable [anexar un identificador único](https://support.emma.io/hc/es/articles/203391031-Identificadores-%C3%9Anicos-Compatibles) del dispositivo con el fin de mejorar la fiabilidad de descargas.
3.  El **logo de tu compañía** con medidas 225x100 px. Lo añadiremos en nuestra [lista de *Partnerships*](https://emma.io/partners/)*.*

EMMA enviará notificaciones con la estructura de tu URL base y opcionalmente añadirá los siguientes parámetros GET al final, así como: 

*http://yourbaseurl.com/event/70?click=43144397**&UDID=&LATITUDE=&LONGITUDE=&CREATED\_AT=2015-04-20+17%3A08%3A01***

|     |     |
| --- | --- |
| **UDID** | Identificador único de dispositivo |
| **LATITUDE** | Coordenadas del dispositivo (si se permite en la aplicación) |
| **LONGITUDE** | Coordenadas del dispositivo (si se permite en la aplicación) |
| **CREATED\_AT** | Momento exacto de instalación |

En el caso de no querer recibir nuestros parámetros GET, especifícalo en tu e-mail.

Aquí puedes ver las IP's de nuestro servidor en caso de que necesites añadirlas a tu Whitelist.

-   5.9.147.231
-   213.239.208.15

# Cómo añadir parámetros personalizados

Puedes añadir algunos parámetros GET en la URL de *tracking*, con el fin de obtener más información sobre el origen del clic, lo que permite distinguir el nombre de la aplicación o su categoría.

|     |     |
| --- | --- |
| Nombre | Descripción |
| **exid** | Identificador de red personalizado como ID de clic |
| **camid** | Identificador de encargo extra como ID de campaña |
| **dp** | Dispositivo (iPhone, iPad, Galaxy S1, …) |
| **appname** | Nombre de la app donde se hace el clic |
| **appcats** | Categoría de la app donde se hace el clic |
| **eat\_sub\[NUM\]** | Donde \[*NUM*\] es un número entero de 1 a 22. Información de parámetros extra disponibles usando [la función del SDK getUserInfo()](https://developer.emma.io/es/ios/integracion-sdk#informaci%C3%B3n-del-usuario) |

**IMPORTANTE:** el parámetro eat\_sub\[NUM\] podría ser recogido y exportado a través de la API, pero no pueden ser notificadas por las devoluciones de datos del AdNetwork. Si eres un AdNetwork, utiliza el resto de parámetros de la lista para conseguir que se devuelva la información.

A continuación se muestran un par de ejemplos: 

-   http://eat.emmasolutions.net?entw=7b5b7e82e409be76027f5b688de2a67&exid=\[click\_ID\]&dp=\[device\]&eat\_sub1=\[extra\_param1\]&eat\_sub2=\[extra\_param2\] 
-   http://eat.emmasolutions.net?entw=7b5b7e82e409be76027f5b688de2a67&exid=123456&dp=iphone&eat\_sub1=performance&eat\_sub2=zanox