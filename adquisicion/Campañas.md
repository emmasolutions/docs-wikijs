---
title: Campañas de adquisición
description: Campañas de adquisición
published: true
date: 2025-03-03T10:59:51.936Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:45:35.272Z
---

# Gestión de campañas

Para poder conocer en detalle el origen de los usuarios, es necesaria la creación de campañas que nos permitan medir todas las fuentes de tráfico. Posteriormente, se podrá editar, clonar o eliminar cualquiera de las campañas que hayan sido dadas de alta en el sistema.

-   Crear campañas
-   Editar y clonar campañas
-   Eliminar campañas

## Crear campañas

Para crear campañas, tan solo debemos hacer clic en el botón ***Nueva campaña*** que se encuentra en la tabla resumen de las campañas. 

![](/campañas_1.png)

A continuación, es necesario cubrir la información que solicita el formulario para poder dar de alta una nueva campaña en el sistema. Es importante cubrir todos los campos marcados como *Obligatorio*, ya que de otra manera, no se podrán guardar los cambios y no se podrá crear la campaña.</br><p style="width:800px">![campañas_6.png](/campañas_6.png)</p>

|     |     |
| --- | --- |
| ***Nombre*** | Establece un nombre identificativo para tu campaña. Es de uso interno, el usuario final nunca visualizará este nombre |
| ***¿Campaña de Re-Targeting?*** | Si vas a lanzar una campaña en modalidad de atribución de Re-Targeting activa esta opción. En este [artículo](https://docs.emma.io/es/adquisicion/apptracker#campa%C3%B1as-re-targeting), tienes más información sobre los diferentes modos de atribución. |
| ***Inicio de la campaña*** | Establece una fecha de inicio a tu campaña. Esta fecha es importante ya que es el momento en el que se empezará a medir actividad para esa campaña |
| ***Fin de la campaña*** | Establece una fecha de finalización para tu campaña. Esta fecha es importante, ya que cuando una campaña finalice, dejará de atribuir instalaciones/eventos y, si se trabaja con redes publicitarias dejará de notificarse cualquier actividad a las mismas. En caso de que sea una acción que no tiene prevista una fecha de finalización, puedes habilitar el check ***Sin fin*** para que la campaña no tenga fecha de finaliziación. |
| ***Países*** | Selecciona uno o varios países a los que va enfocado tu campaña. Es un campo opcional, pero si lo configuras, el powlink tendrá el comportamiento habitual sólo en aquellos países seleccionados. Si se realiza algún clic desde otro país, el powlink será bloqueado internamente y no realizará las redirecciones pertinentes. De esta manera, se evita tráfico fraudulento en las campañas. |
| ***Eventos atribuidos*** | En caso de que se haya habilitado el check de Re-Targeting y se tenga configurada la atribución de Re-Targeting en modo Unique Click, se deberán seleccionar uno o varios eventos objetivo para que se realice la atribución. Conoce más sobre las campañas de Re-Targeting en modalidad de atribución Unique Click [aquí](https://docs.emma.io/es/adquisicion/apptracker#re-targeting-unique-click). |
| ***iOS URL*** | Establece la URL a la que van a ser redireccionados los usuarios de iOS al hacer clic en el anuncio. Normalmente es la URL de la app en App Store. Si has configurado la URL de la store de iOS en [Preferencias App](https://docs.emma.io/es/configuracion#preferencias-de-ios), por defecto EMMA cubre este campo con la URL configurada. |
| ***Android URL*** | Establece la URL a la que van a ser redireccionados los usuarios de Android al hacer clic en el anuncio. Normalmente es la URL de la app en GPlay. Si has configurado la URL de la store de Android en [Preferencias App](https://docs.emma.io/es/configuracion#preferencias-de-android), por defecto EMMA cubre este campo con la URL configurada. |
| ***Huawei URL*** | Establece la URL a la que van a ser redireccionados los usuarios de Huawei al hacer clic en el anuncio. Normalmente es la URL de la app en App Gallery. Si has configurado la URL de la store de Huawei en [Preferencias App,](https://docs.emma.io/es/configuracion#preferencias-de-huawei) por defecto EMMA cubre este campo con la URL configurada. |
| ***Alternative URL*** | Establece la URL a la que van a ser redireccionados los usuarios de cualquier otro sistema operativo diferente a los anteriores al hacer clic en el anuncio. Si has configurado tu dirección web en [Preferencias App](https://docs.emma.io/es/configuracion#general-emma-key), por defecto EMMA cubre este campo con la URL configurada. |

Si se va a dar de alta una nueva campaña de Re-Targeting en modalidad **unique click** (más info pinchando [aquí](https://docs.emma.io/es/adquisicion/apptracker#re-targeting-unique-click)) es necesario que se establezcan los eventos que se van a querer atribuir a esa campaña en concreto. Cuando habilitemos el check de campaña de Re-Targeting y tengamos como modalidad *unique click,* se habilitará un selector para seleccionar los eventos atribuidos. </br><p style="width:850px">![](/campañas_4.png)</p>

En el selector que se muestra, se deben seleccionar todos aquellos eventos que queremos atribuir. Los eventos disponibles para seleccionar serán los eventos por defecto de EMMA +  todos aquellos eventos personalizados que estén integrados en la app para ser medidos con el SDK de EMMA. 

De este modo, se podrá seleccionar entre los siguientes eventos: 

-   Registro
-   Primer Login
-   Login
-   Venta
-   Eventos Personalizados

> Los eventos son únicos y solo podrán ser seleccionados una vez en todas las campañas. Es decir, si en la campaña A se ha seleccionado que se va a atribuir el evento Registro, ese evento ya no estará disponible para ser seleccionado en otra campaña.
{.is-warning}


La configuración de la campaña se puede editar en cualquier momento desde el menú contextual de la tabla resumen de campañas. 
> No es obligatorio cubrir todas las URLs de redirección, con establecer una URL de redirección es suficiente para lanzar la campaña. Eso sí, es importante tener en cuenta que, si por ejemplo, solo se configura la URL de iOS y un usuario Android hace clic en la campaña, visualizará un error 404, no habr´ninguna URL de destino para esos usuariosy. Esto pasará siempre que se deje algún campo de URL vacío y los usuarios correspondientes hagan clic en el powlink.
{.is-info}

Una vez creada la campaña, automáticamente EMMA nos redirecciona a la pantalla de [Fuentes](https://docs.emma.io/es/adquisicion/fuentes-de-medios) para que podamos crear todos los powlinks (enlaces de seguimiento) necesarios. 

## Editar y clonar campañas

Si es necesario realizar alguna modificación en la campaña o crear otra con las mismas características, existe la posibilidad de editar o clonar la campaña de manera rápida y sencilla. 

Tan solo hay que hacer clic en el menú contextual y seleccionar la opción específica de ***Editar*** o ***Clonar.***

![](/campañas_3.png)

En caso de que se haya **editado** la campaña, se abrirá el formulario de creación de campaña con la configuración que haya establecido para poder hacer las modificaciones que sean pertinentes. Una vez se hayan realizado los cambios, tan solo deberemos hacer clic en el botón ***Guardar campaña*** para guardar los cambios realizados.

En caso de que se haya **clonado** la campaña, se abrirá el formulario de creación de la campaña copiando la configuración de la campaña clonada. De esta manera, con esta opción podremos ahorrar tiempo en la creación de campañas que tengan la misma configuración. 

## Establecer campañas favoritas

## Eliminar campañas

Si necesitamos eliminar una campaña por cualquier motivo, tan solo tenemos que ir al menú contextual y seleccionar la opción ***Eliminar.*** 

Una vez seleccionada la opción ***Eliminar*** la campaña será eliminada inmediatamente del sistema. 

## Redirección de usuarios según versión de OS

Si tu aplicación no es compatible con determinadas versiones de Android o iOS podrás establecer una versión mínima necesaria para que los usuarios sean redirigidos a la landing correspondiente.

El flujo habitual de redireccionamiento es que, cuando un usuario hace click en un tracking link de EMMA y no tiene la aplicación instalada, es redireccionado a la store correspondiente para que se pueda descargar la app.

Pero, ¿qué pasa si tu app no es compatible con determinadas versiones del sistema operativo? Lo único que tendrás que hacer es una pequeña configuración en EMMA para que solo se redirija a las stores a aquellos usuarios con una versión mínima del sistema operativo.

Para ello, debes seguir estos sencillos pasos. 

-   Haz [login](https://ng.emma.io) en EMMA y dirígete a la sección **Preferencias app**</br><p style="width:400px;">![pref_app.png](/pref_app.png)</p>
-   Edita la configuración de tu app haciendo clic en el botón ***Editar***.</br><p style="width:900px">![](/os_version_minima_4.png)</p>

Ve a la sección de configuración de **Android** y/o **iOS** en función de si quieres añadir o no una versión mínima para cada sistema operativo. Selecciona la versión deseada y guarda los cambios. </br><p style="width:700px">![redireccion_ios.png](/redireccion_ios.png)</p></br><p style="width:700px">![redireccion_android.png](/redireccion_android.png)</p>

Una vez realizada esta pequeña configuración, sólo serán redirigidos a la store para descargarse la app aquellos usuarios cuyos dispositivos cumplan con  la versión mínima establecida. Los usuarios que tengan una versión inferior a la mínima requerida, serán redirigidos a la URL alternativa configurada en tu campaña. En este [artículo](https://docs.emma.io/es/adquisicion/apptracker#introducci%C3%B3n) puedes ver más información acerca de las campañas de captación.

# Redirecciones en Apptracker

Al dar de alta una nueva campaña en el Apptracker de EMMA, es necesario establecer las redirecciones que queramos que haga esa campaña en concreto. 

De esta manera se puede establecer la URL exacta a la que van a ser redireccionados los usuarios de dispositivos iOS, los de Android, los de Huawei y el resto de usuarios que usan otro sistema operativo diferente. </br><p style="width:850px">![campañas_8.png](/campañas_8.png)</p>

Lo más común es realizar la siguiente configuración en lo que a URLs de redirección se refiere:

-   **iOS URL:** Normalmente se establece la URL de la app en App Store para que los usuarios sean redireccionados a la descarga de la app.
-   **Android URL:** Lo habitual es establecer la URL de la app en Google Play para que los usuarios sean redireccionados a la descarga de la app.
-   **Huawei URL:** Lo habitual es establecer la URL de la app en App Gallery para que los usuarios sean redireccionados a la descarga de la app.
-   **URL Alternativa:** Normalmente se establece una landing, que suele ser la página web de la empresa. Pero también es muy común establecer la URL de la store de Android ya que es el sistema operativo con mayor peso.

Si tu necesidad principal no es llevar al usuario a la descarga de la app, puedes establecer la URL  que consideres oportuna para cada OS sin ningún tipo de problema.

# Buscador de powlinks
Si necesitamos localizar a qué campaña pertenece un powlink en el apptracker, teniendo únicamente el powlink, en lugar de la campaña de EMMA a la que pertenece, tan solo tenemos que seguir estos pasos: 
- Haz [login](https://ng.emma.io/es/login?returnUrl=%2Findex) en tu cuenta de EMMA.
- Ve a la sección Captación > Apptracker.
![campañas_i.png](/campañas_i.png)
- Pon el filtro Básico o Retargeting en la barra de filtrado gris (más info [aquí](https://docs.emma.io/es/barras-de-menu#men%C3%BA-de-filtrado)), en función de si vas a consultar un powlink de una campaña básica o de retargeting. 
Los powlinks de una campaña básica tienen esta estructura: https://eat.emmasolutions.net/?entw=150991e32f7d4b2b3fa349d31ec70893
Los powlinks de una campaña de retargeting esta otra: https://emmaio.powlink.io/?ecid=6f43813a1ad6e509a3b915e791c53ce0&rt=true&aw=720
- En la tabla de resumen de campañas en el selector de columnas a mostrar, busca y habilita la columna **Número de fuentes**.
![campañas_ii.png](/campañas_ii.png)
- Una vez activa esta columna, ve al buscador que se encuentra en la parte superior derecha de la tabla de campañas y pega el código entw o ecid de tu powlink.
https://eat.emmasolutions.net/?entw=**150991e32f7d4b2b3fa349d31ec70893**
https://emmaio.powlink.io/?ecid=**6f43813a1ad6e509a3b915e791c53ce0**&rt=true&aw=720
![campañas_iii.png](/campañas_iii.png)
