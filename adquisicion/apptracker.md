---
title: Apptracker
description: Gestiona campañas de adquisición y fuentes de medios
published: true
date: 2024-12-30T12:57:50.645Z
tags: 
editor: markdown
dateCreated: 2020-11-25T22:44:41.393Z
---

# Introducción

https://youtu.be/XGBsGSAbzJU

En el apptracker de EMMA, podrás gestionar todas tus campañas y fuentes. De esta manera, podrás conocer el origen exacto de cada uno de los usuarios de tu app. 

Cuando las campañas estén activas y generando datos, podrás ver de manera general cómo están funcionando dichas campañas desde esta misma sección. 

Lo primero que se debe hacer antes de consultar los datos, es seleccionar el periodo de fechas del cual se quiere tratar los datos. Por defecto se muestran datos de los últimos 7 días. Para seleccionar un periodo de fechas, se debe utilizar el selector de fechas situado en la parte superior derecha de la pantalla. 

![](/r_ejecutivo_1.png)

A continuación, se pueden añadir filtros en función de los datos que se quieran consultar. Puedes ver más información sobre los filtros [aquí](https://docs.emma.io/es/barras-de-menu). Los filtros disponibles son:
-   **Agrupar por campañas / canales:** Esta agrupación permite visualiazar en modo de lecutra los datos de las fuentes (powlink) creados en base a un canal previamente establecido. De esta manera se puede realizar un análisis de los datos en base a los canales que se trabajan. 
Puedes ver más información sobre cómo vincular canales a una fuente [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios). 
> Al agrupar por canal, se verá la información agrupada por cada canal y, al hacer clic en el nombre de canal, podemos acceder a una pantalla de detalle dónde podremos ver **todas** las fuentes que componen ese canal. De esta manera podremos ver los datos de manera general a nivel de canal, pero también podremos llevar a cabo un análisis de ls fuentes que mejor funcionan dentro de cada canal.{.is-info}

</br><p style="width:1000px; margin-top:-50px;">![apptracker_canal_1.png](/apptracker_canal_1.png)</p>
> La vista de agrupación por canales es **sólo de lectura**. Si se quieren editar o crear nuevas [campañas](https://docs.emma.io/es/adquisicion/Campa%C3%B1as)/[fuentes](https://docs.emma.io/es/adquisicion/fuentes-de-medios) se debe realizar en la vist de agrupación por campaña.
{.is-warning}
-   **Todas las campañas / campañas favoritas:** Este filtro muestra por defecto todas las camapñas creadas, pero se puede modificar para que sólo muestre aquellas campañas que hemos marcado como favoritas. Puedes ver más info sobre como establecer campañas favoritas [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as#establecer-campa%C3%B1as-favoritas).
-   **Básico / Retargeting:** es un filtro fijo y obligatorio que por defecto muestra las campañas **Básicas**, o sea, de captación. Se puede cambiar a **Retargeting** para que muestre esta modalidad de campañas o seleccionar ambas opciones a la vez. De esta manera podremos ver todos los datos de un vistazo. En este enlace se puede consultar información más detallada sobre los modelos de atribución disponibles en EMMA.
-   **País:** este filtro permite fragmentar los datos según el país de procedencia de las instalaciones.
-   **Sistema operativo:** con este filtro podremos consultar los datos en base a un sistema operativo determinado (Android/iOS)

Una vez las campañas hayan generado actividad, se podrá  ver, siempre en base al rango de fechas seleccionado y a los filtros establecidos, los datos generales de todas las campañas en la gráfica principal.

![](/apptracker_2.png)

En esta gráfica se muestra la siguiente información:

|     |     |
| --- | --- |
| ***Clics*** | Número total de clics realizados en todas las campañas |
| ***Coste*** | Coste total de todas las campañas |
| ***Instalación*** | Instalaciones totales de todas las campañas |

Ya por último, en la tabla que se muestra en la segunda parte de la pantalla se puede ver más detalladamente los datos adquiridos por cada una de las diferentes campañas en lo que respecta a instalaciones y eventos inapp realizados por los usuarios.

> A la hora de analizar los datos del reporting de Apptracker, es necesario tener en cuenta lo siguiente: </br> - Los clics y las instalaciones tienden a mostrarse de manera más rápida en el reporting una vez están atribuidos. </br> - Los eventos inapp atribuidos a dichas campañas pueden tardar en mostrarse hasta una hora, ya que el reporting se actualiza cada hora. 
{.is-info}

Además, la tabla es configurable con los KPIs que se necesiten analizar. Para ello, tan solo se tiene que hacer clic en el selector superior izquierdo de la tabla y se mostrará un desplegable con todos los KPIs disponibles. Más info sobre los KPIs disponibles pinchando [aquí.](https://docs.emma.io/es/adquisicion/apptracker#exporta-tus-datos)

![](/apptracker_3.png)

Para acceder al detalle de las fuentes de cada campaña y conocer cuál es la que está funcionando mejor y obteniendo un mejor performance, hay que hacer clic en el menú contextual (los 3 puntitos a la izquierda del nombre de la campaña) y seleccionar la opción ***Fuentes.*** 

![](/apptracker_4.png)

Una vez en el detalle de las fuentes, tendremos una línea por cada fuente / powlink creado y se podrán ver los datos correspondientes a cada una de ellas. Al igual que en el resumen general de las campañas, se puede configurar la tabla con los KPIs que se desean analizar. Más detalle sobre los KPIs disponibles [aquí.](https://docs.emma.io/es/adquisicion/apptracker#exporta-tus-datos)

# Campañas Básicas y Re-Targeting

EMMA da la posibilidad de trabajar con dos modalidades de tracking links en función de los objetivos de la campañas.

-   Campañas Básicas
-   Campañas Re-Targeting
    -   Re-Targeting Last Click
    -   Re-Targeting Unique Click

## Campañas Básicas

Las campañas en modalidad Básica atribuyen la actividad de dispositivos que nunca hayan tenido la aplicación y lo siguen haciendo durante toda la vida del usuario sin limitar dicha atribución por una ventana de atribución.

El objetivo principal de este tipo de campañas es impactar a usuarios que nunca hayan tenido la app y conocer toda la actividad atribuida al origen del evento de instalación independientemente del tiempo que haya pasado.

### Casos de uso

A continuación se detallan distintos ejemplos de atribución:

**1\. El dispositivo nunca ha tenido la app y hace clic en una fuente Básica:**

|     |     |
| --- | --- |
| El usuario hace clic en una fuente Básica y se instala la app. | Se cuenta un clic y una instalación en el informe de la fuente. |
| El día 20 el usuario abre la app y se registra. | Un evento de *session* y de *register* se cuenta en la fuente Básica. |
| El día 380 el usuario abre la app y realiza una compra. | Un evento de *session* y de *conversion* se cuenta en la fuente Básica. |

**2\. El dispositivo ya tenía la app y hace clic en una fuente Básica (fuente B) distinta a la del origen de la instalación (fuente A):**

|     |     |
| --- | --- |
| El usuario hace clic en una fuente Básica distinta a la del origen de la instalación, abre la app y realiza un login. | Se cuenta un clic en la fuente Básica B y una *session* y un *login* en la fuente Básica A. |

## Campañas Re-Targeting

Las campañas en modalidad Re-Targeting atribuyen tanto la actividad de dispositivos que nunca hayan tenido la app como de aquellos que sí que la tienen durante una ventana de atribución limitada.

La **ventana de atribución** establece el tiempo máximo que puede transcurrir entre un clic y un evento (instalación o in-app) para que dicho evento sea asignado en el Apptracker.

El objetivo principal de este tipo de campañas es re-impactar a usuarios que ya tengan la app instalada y así poder atribuir cierta actividad in-app tras la interacción con este tipo de tracking links.

Dentro de las campañas en modalidad Re-Targeting, nos encontramos con dos posibles modelos de atribución:

-   Re-Targeting [**Last Click**](https://docs.emma.io/es/adquisicion/apptracker#re-targeting-last-click)**.**
-   Re-Targeting [**Unique Click**](https://docs.emma.io/es/adquisicion/apptracker#re-targeting-unique-click)**.**

### **Re-Targeting Last Click**

Las campañas de Re-Targeting en modalidad ***Last Click*** son las estándar del mercado. Este tipo de campañas te permiten atribuir cualquier evento de la app al último clic que haya realizado el usuario dentro de la ventana de atribución. Este clic debe haber sido realizado en alguna de las campañas y fuentes creadas en el Apptracker.

Para crear una campaña de Re-Targeting en modalidad Last Click sigue estos pasos:

1\. Vete a la sección ***Preferencias App*** y comprueba que la opción ***Unique click*** está inactiva. Si no es así, desactívala.

![](/rt.png)

2\. [Crea tu campaña en el Apptracker marcando la modalidad Re-Targeting](https://docs.emma.io/es/adquisicion/Campa%C3%B1as#gesti%C3%B3n-de-campa%C3%B1as).

### **Casos de uso Re-Targeting Last Click**

**1\. El dispositivo nunca ha tenido la app y hace clic en una fuente de Re-Targeting con una ventana de atribución de 30 días:**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en una fuente de Re-Targeting y se instala la app. | Se cuenta un clic, un engagement y una instalación en el informe de la fuente de Re-Targeting. |
| El día 30 el usuario abre la app y realiza un registro. | Un evento de *session* y de *register* se cuenta en la fuente de Re-Targeting. |
| El día 31 el usuario abre la app y realiza una compra. | Un evento de session y de conversion se cuenta en el histórico del usuario\* pero no en la fuente de Re-Targeting. |

\* El histórico del usuario es siempre accesible desde las distintas opciones de informes de la sección de Comportamiento.

**2\. El dispositivo nunca ha tenido la app y hace clic en una fuente de Re-Targeting (fuente A) con una ventana de atribución de 30 días y posteriormente en otra fuente de Re-Targeting con ventana de atribución de 4 días (fuente B):**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en la fuente de Re-Targeting A y se instala la app. | Se cuenta un clic, un engagement y una instalación en el informe de la fuente de Re-Targeting A. |
| El día 2 el usuario abre la app y realiza un registro. | Un evento de *session* y de *register* se cuenta en la fuente de Re-Targeting A. |
| El día 3 el usuario hace clic en la fuente de Re-Targeting B, abre la app y realiza una conversión. | Se cuenta un clic, un engagement, una *session* y una *conversion* en el informe de la fuente de Re-Targeting B. Un evento de session y de conversion se cuenta en el histórico del usuario\* pero no en la fuente de Re-Targeting A. |

\* El histórico del usuario es siempre accesible desde las distintas opciones de informes de la sección de Comportamiento.

**3\. El dispositivo ya tenía la app y hace clic en una fuente de Re-Targeting con una ventana de atribución de 14 días:**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en una fuente de Re-Targeting. | Se cuenta un clic y un engagement en el informe de la fuentede Re-Targeting. |
| El día 3 el usuario abre la app y realiza un registro. | Un evento de *session* y de *register* se cuenta en la fuente de Re-Targeting así como en la fuente de atribución de la instalación (*básica u orgánico*). |
| El día 15 el usuario abre la app y realiza una compra. | Un evento de *session* y de *conversion* se cuenta en la fuente de atribución de la instalación (*básica u orgánico*)  pero no en la fuente de Re-Targeting. |

**4\. El dispositivo ya tenía la app y hace clic en una fuente de Re-Targeting con una ventana de atribución de 2 días (fuente A) y posteriormente en otra fuente de Re-Targeting con ventana de atribución de 4 días (fuente B):**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en la fuente de Re-Targeting A y abre la app. | Se cuenta un clic, un engagement y una *session* en el informe de la fuente A. Se cuenta una *session* en la fuente de atribución de la instalación (*básica u orgánico*). |
| El día 3 el usuario abre la app y realiza un registro. | Un evento de *session* y de *registro* se cuenta en la fuente de atribución de la instalación (*básica u orgánico*) pero no en la fuente de Re-Targeting A. |
| El día 4 el usuario hace clic en la fuente de Re-Targeting B, abre la app y realiza una conversión. | Se cuenta un clic, un engagement, una *session* y una *conversion* en el informe de la fuente de Re-Targeting B. Se cuenta una *session* y una *conversion* en la fuente de atribución de la instalación (*básica u orgánico*). |
| El día 7 el usuario abre la app y realiza un login. | Se cuenta una *session* y un *login* en la fuente de Re-Targeting B así como en la en la fuente de atribución de la instalación (*básica u orgánico*). |
| El día 8 el usuario abre la app y realiza una nueva conversión. | Se cuenta una *session* y una *conversion* en la fuente de atribución de la instalación (*básica u orgánico*) pero no en la fuente de Re-Targeting. |

### **Re-Targeting Unique Click**

Las campañas de Re-Targeting en modalidad ***Unique Click*** te permiten atribuir sólo determinados eventos de la app a cada una de las campañas teniendo en cuenta el último clic por campaña dentro de la ventana de atribución. Esto quiere decir que cada campaña tendrá uno o varios eventos objetivo y que la atribución tendrá en cuenta que la asignación de la conversión se realice sólo en el último click de la campaña que tenga configurado dicho evento objetivo.

Todos los eventos que se seleccionen para una campaña de Re-Targeting en modalidad Unique Click serán comunes a todas sus fuentes. La atribución entre las distintas fuentes de una misma campaña se realizará teniendo en cuenta el último clic del usuario.

> Recuerda que los eventos sólo podrán ser atribuidos a una única campaña. Nunca se podrá usar el mismo evento para dos o más campañas diferentes.
{.is-warning}

Para crear una campaña de Re-Targeting en modalidad Unique Click sigue estos pasos:

1\. Vete a la sección **Preferencias App** y comprueba que la opción ***Unique click*** está activa. Si no es así, actívala.

![](/rt_2.png)

2\. [Crea tu campaña en el Apptracker marcando la modalidad Retargeting](https://docs.emma.io/es/adquisicion/Campa%C3%B1as#gesti%C3%B3n-de-campa%C3%B1as) y selecciona el evento o eventos objetivo de la campaña. Los eventos objetivo serán los únicos que se asignen a la campaña bajo este modelo de atribución.

>Cada vez que se active o desactive el selector de modalidad de atribución de Re-Targeting, se pararán todas las campañas que estén en ese momento funcionando en el Apptracker y se tendrán que crear nuevas campañas en la nueva modalidad de atribución.
{.is-warning}

### **Casos de uso Re-Targeting Unique Click**

**1\. El dispositivo nunca ha tenido la app y hace clic en una fuente de Re-Targeting con una ventana de atribución de 30 días de una campaña con el evento registro como objetivo:**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en una fuente de Re-Targeting y se instala la app. | Se cuenta un *clic*, un *engagement y* una *instalación* en el informe de la fuente de Re-Targeting. |
| El día 30 el usuario abre la app y realiza un registro y un first login. | Un evento de *register* se cuenta en la fuente de Re-Targeting. Un evento de first login se cuenta en el histórico del usuario\*. |
| El día 31 el usuario abre la app y realiza una compra. | Un evento de *conversion* se cuenta en el histórico del usuario\* pero no en la fuente de Re-Targeting. |

\* El histórico del usuario es siempre accesible desde las distintas opciones de informes de la sección de Comportamiento.

**2\. El dispositivo nunca ha tenido la app y hace clic en una fuente de Re-Targeting (de la campaña A con el evento conversión atribuido) con una ventana de atribución de 30 días y posteriormente en otra fuente de Re-Targeting (de la campaña B con el evento login atribuido) con ventana de atribución de 14 días:** 

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en la fuente de Re-Targeting de la campaña A y se instala la app. | Se cuenta un *clic*, un *engagement*, y una *instalación* en el informe de la fuente de la campaña A. |
| El día 2 el usuario abre la app y realiza un registro. | Un evento de *register* se cuenta en el histórico del usuario\*. |
| El día 9 el usuario hace clic en la fuente de Re-Targeting de la campaña B. | Se cuenta un *clic* y un *engagement* en el informe de la fuente de la campaña B. |
| El día 23 el usuario abre la app, realiza una conversión y un login. | Un evento de *conversion* se registra en la fuente Re-Targeting de la campaña A, un evento de *login* se registra en la fuente Re-Targeting de la campaña B. |
| El día 31 el usuario abre la app, realiza un login y una nueva conversión. | Un evento de *conversion y login* se registra en el histórico\* del usuario pero no en el reporting de la fuente de la campaña A ni de la campaña B. |

\* El histórico del usuario es siempre accesible desde las distintas opciones de informes de la sección de Comportamiento.

**3\. El dispositivo ya tenía la app y hace clic en una fuente de Re-Targeting con una ventana de atribución de 14 días de una campaña con el evento conversión como objetivo:**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en una fuente de Re-Targeting. | Se cuenta un *clic* y un *engagement* en el informe de la fuente. |
| El día 3 el usuario abre la app y realiza un registro. | Un evento de *session* y de *register* se cuenta en la fuente de atribución de la instalación y en el histórico\* del usuario. |
| El día 10 el usuario abre la app y realiza una conversión. | Un evento de *conversion* se registra en la fuente de Re-Targeting y un evento de *session* y *conversion* se registra en la fuente de atribución de la instalación y en el histórico\* del usuario. |
| El día 15 el usuario abre la app y realiza una nueva conversión. | Un evento de *session* y de *conversion* se cuenta en la fuente de atribución de la instalación y en el histórico\* del usuario pero no en la fuente de Re-Targeting. |

\* El histórico del usuario es siempre accesible desde las distintas opciones de informes de la sección de Comportamiento.

**4\. El dispositivo ya tenía la app y hace clic en una fuente de Re-Targeting con una ventana de atribución de 2 días (fuente A) y posteriormente en otra fuente de Re-Targeting con ventana de atribución de 4 días (fuente B). Ambas fuentes pertenecen a una campaña con los eventos login y conversion como objetivo:**

|     |     |
| --- | --- |
| El día 1 el usuario hace clic en la fuente de Re-Targeting A, abre la app y hace login. | Se cuenta un clic, un engagement y un *login* en el informe de la fuente A. Se cuenta una *session* en la fuente de atribución de la instalación y en el histórico\* del usuario. |
| El día 3 el usuario abre la app y realiza una conversión. | Un evento de *session* y de *conversion* se cuenta en la fuente de atribución de la instalación *y* en el histórico\* del usuario pero no en la fuente de Re-Targeting A. |
| El día 4 el usuario hace clic en la fuente de Re-Targeting B, abre la app y realiza una conversión. | Se cuenta un *clic*, un *engagement* y una *conversion* en el informe de la fuente de Re-Targeting B. Se cuenta una *session* y una *conversion* en la fuente de atribución de la instalación y en el histórico\* del usuario. |
| El día 7 el usuario abre la app y realiza un login. | Se cuenta un clic y un evento de *login* en la fuente de Re-Targeting B y un evento de *session* y *login* en la fuente de atribución de la instalación y en el histórico\* del usuario pero no en la fuente de Re-Targeting A. |
| El día 9 el usuario abre la app y realiza un login y una nueva conversión. | Se cuenta un evento de *session, login* y *conversion* en la fuente de atribución de la instalación (básica*)* o en el histórico\* del usuario pero no en la fuente de Re-Targeting. |

\* El histórico del usuario es siempre accesible desde las distintas opciones de informes de la sección de Comportamiento.

# Gestión de campañas

Para poder conocer en detalle el origen de los usuarios es necesaria la creación de campañas que nos permitan medir las diferentes fuentes de tráfico. En nuestra guía sobre [***Campañas de adquisición***](https://docs.emma.io/es/adquisicion/Campa%C3%B1as) podrás ver en detalle como crear, editar, clonar y eliminar campañas de adquisición. 

# Gestión de fuentes

Una vez configuradas las campañas, es necesario crear fuentes (tracking links, enlaces de seguimiento) que nos permitan conocer el origen exacto de cada una de las instalaciones. Puedes ver más información sobre cómo gestionar las fuentes de medios en nuestro artículo [***Fuentes de medios***](https://docs.emma.io/es/adquisicion/fuentes-de-medios)***.*** 

# Campañas favoritas
Gracias a la opción de marcar campañas como favoritas de EMMA, cada usuario podrá marcar las campañas que le interesen como favoritas para acceder a los datos de estas de manera rápida y sencilla. 

Para estabelcer campañas favoritas tan solo hay que seguir estos pasos: 
1. Hacer login en la cuenta de EMMA. 
2. Ir a ***Captación > Apptracker***.
3. Localizar la campaña deseada, se puede buscar por nombre a través del buscador para facilitar la taera. 
4. En el menú contextual, deberemos seleccionar la opción ***Favorito*** para marcar la campaña.</br><p style="width:550px">![favoritas_2.png](/favoritas_2.png)</p>
5. ¡Listo! Se debe repetir este proceso con todas las campañas que se quieran establecer como favoritas. 

Una vez la campaña ha sido establecida como favorita, se puede diferenciar facilmente de las demás ya que quedará marcada con un icono de un corazón. </br><p style="width:500px">![favortias_3.png](/favortias_3.png)</p>

> Las campañas favoritas funcionan a nivel de usuario, por lo que cada usuario con acceso a EMMA podrá establecer sus propias campañas como favoritas, independientemente de lo que tengan configurado otros usuarios.
{.is-info}

## Visualización de campañas favoritas
Por defecto, al acceder a Apptracker, se mostrarán todas las campañas existentes, pero en caso de querer que se visuaicen solo las campañas marcadas como favoritas, bastará con modificar el filtro y seleccionar la opción ***Favoritos***. <p style="width:500px">![favoritas_1.png](/favoritas_1.png)</p>

> La opción seleccionada en el filtro se mantendrá en los accesos futuros, por lo que si se selecciona la opción Favoritos, la próxima vez que se acceda al Apptracker, por defecto se mostrará esta selección.
{.is-info}

## Eliminar de favoritos
Para dejar de tener una campaña marcada como favorita tan solo hay que localizar la campaña en cuestión y en el menú contextual seleccionar la opción ***Eliminar favorito***.<p style="width:700px">![favotias_4.png](/favotias_4.png)</p>



# Rehabilitar el uso de los Universal Links en iOS
El powlink de EMMA funciona, en el caso de iOS como un Universal Link, y en el caso de Android como un App Link. Es decir, el enlace puede detectar si el usuario tiene o no la app instalada y comportarse de una manera u otra con base a esa información. Si quieres saber más sobre el powlink y su funcionamiento puedes ver esta [documentación](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu). 

En el caso del sistema operativo iOS, es posible que el usuario deshabilite el uso de los Universal Link y, en este caso el powlink de EMMA no podrá funcionar de la manera correcta y, si el usuario tiene la app instalada, en lugar de ser llevado a la app en cuestión, será redireccionado a la store. 

Si queremos volver a activar el Universal Link en el dispositivo, hay que seguir estos pasos: 
- Copiamos el powlink de EMMA, abrimos la app de NOTAS del iPhone y pegamos el powlink de EMMA. 
- Hacemos un tap largo sobre el powlink hasta que se abra un menú y hacemos clic sobre la opción *Abrir en <<nombre de la aplicación>>.*

>En caso de que al hacer un tap largo en el powlink no aparezca la opción *Abrir en <<nombre de la aplicación>>* significa que ha habido algún tipo de error con la asociación que hace iOS entre el dominio y la app. Este proceso es totalmente ajeno a EMMA.
{.is-info}

Tras realizar estos dos pasos, los Universal Links deberían quedar reactivados para el dominio correspondiente y la app se debería abrir llevando al usuario a la sección que corresponda en cada caso. 

En caso de ser necesario, se pueden repetir estos pasos las veces que sean necesariar para rehabilitar los Universal Links en los dispositivos. 

> Es importante que recuerdes que si el powlink de EMMA tiene vinculado un deeplink y los Universal Link están deshabilitados, el usuario será redireccionado a la store. Si desde la store abre la app, no se realizará la redirección del deeplink ya que al pasar por la store se pierde la información y no se puede redirecciona al usuario. Una vez que los Universal Link estén reactivados este flujo ya no debería producirse. 
{.is-info}

# ***Envío de clics vía server to server (S2S)***

Si necesitas que los clics de tus campañas con EMMA se envíen a un sistema externo puedes configurar una url/postback a través de la cual se notificarán los clics. Está configuración se realiza a nivel de proveedor/fuente de medios y afectará a todas las fuentes que tengan vinculado dicho proveedor. 

El envío de clics server to server está permitido para el proveedor *custom* (proveedor por defecto cuando no se trata de campañas con redes publicitarias) y también para los nuevos proveedores que se creen desde cero. Para ello, tan solo tenemos que seguir estos pasos: 

-   Haz login en [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex).  
-   Ve a la sección **Captación > Fuentes de medios.**

![](/clic_s2s_1.png)

-   Haz clic en el botón **\+ Nueva fuente de medios** para crear una nueva fuente/proveedor o busca y edita la fuente/proveedor ***custom**.* 

![](/clic_s2s_2.png)

![](/clic_s2s_3.png)

-   Ve al apartado ***Postback de click*** y configura la postback mediante la que se tienen que notificar los clics generados. 

![](/clic_s2s_4.png)

-   En el campo URL añade la url base de la postback y los parámetros fijos que quieras.
   > En caso de que vayas a añadir parámetros fijos diferentes para cada powlink creado en EMMA existen dos opciones:</br>
Crear una nueva fuente de medios/proveedor para vincular a cada powlink. Donde cada fuente de medios creada tendrá los parámetros fijos correspondiente a cada campaña. </br>
Crear una única fuente de medios/proveedor universal con los parámetros comunes y añadir al final de cada powlink los parámetros específicos de cada campaña. Todas los parámetros que se anexen al powlink, se propagarán a la postback y se notificarán de manera correcta. **Esta sería la opción recomendada por EMMA.**  </br>
En este ejemplo puedes ver como añadir los parámetros fijos al tracking link creado en EMMA: 
 [https://eat.emmasolutions.net?entw=1a763960aebd59fc34c6dd2c390496cc&enlace=1&campaña=inversiones&origen=web&param1=864](https://eat.emmasolutions.net?entw=1a763960aebd59fc34c6dd2c390496cc&enlace=1&campaña=inversiones&origen=web&param1=864)
    {.is-info}
 
-   Si quieres añadir parámetros dinámicos, haz clic en el botón **\+ Añadir parámetro** y selecciona el parámetro de EMMA pertinente en la columna ***Parámetro EMMA** * y en la columna ***Parámetro Fuente de medios*** configura el nombre bajo el que quieres recibir ese parámetro. Por ejemplo city - ciudad. De esta manera, la información de la ciudad se enviará anexado a la postback como ciudad=madrid.    
    [https://ejemplopostback.es?ciudad=madrid](https://ejemplopostback.es?ciudad=madrid)  
    Los parámetros dinámicos disponibles para esta configuración son:   
     

|     |     |
| --- | --- |
| **Parámetro de EMMA** | **Significado** |
| click\_id | Identificador del clic de EMMA. Es un código alfanumérico. |
| provider\_id | Identificador del proveedor en EMMA. Es un código numérico. |
| network\_id | Identificador de la fuente/powlink en EMMA. Es un código numérico. |
| campaign\_id | Identificador de la campaña en EMMA. Es un código numérico. |
| random | Identificador generado de manera aleatoria. |
| city | Información de la ciudad desde la que se ha realizado el clic. |
| country | Información del país desde el que se ha realizado el clic. |
| ip  | Dirección ip desde la que se ha realizado el clic. |
| language | Idioma del navegador desde el que se ha realizado el clic. |
| os  | Sistema operativo del navegador desde el que se ha realizado el clic. |

- Guarda los cambios. 
- Ve a la sección **Captación > Apptracker** y vincula el proveedor correspondiente a cada powlink. En esta [guía](https://docs.emma.io/es/adquisicion/fuentes-de-medios) puedes ver más info al respecto.

# Campañas autoatribuidas

Con EMMA podrás lanzar campaña con las siguientes redes autoatribuidas: 

-   [Apple Search Ads](https://docs.emma.io/es/apple-search-ads)
-   [Facebook/Instagram](https://docs.emma.io/es/guias-de-integracion/facebook)
-   [Google](https://docs.emma.io/es/google)
-   [Snapchat](https://docs.emma.io/es/snapchat)
-   [Twitter](https://docs.emma.io/es/twitter)
-   [TikTok](https://docs.emma.io/es/tiktok)

Haz clic en cada uno de los soportes para ver más información al respecto.

# Configurar Powlink y pwlnk (STU) 

Antes de ver en detalle la configuración necesaria para usar el powlink de EMMA, explicaremos qué es el powlink para poder entender las ventajas de su implementación y uso y a continuación veremos en profundidad la configuración pertinente para poder hacer uso del mismo. 

## ¿Qué es Powlink y pwlnk?

Powlink es una tecnología de EMMA que se basa en el funcionamiento de los App Links (Android) y los Universal Links (iOS) y nos permite obtener todo el potencial de los mismos. Teniendo esto en cuenta, veremos a continuación los diferentes escenarios que distingue el Powlink: 

-   El usuario no dispone de la App instalada en su dispositivo. En tal caso el enlace redireccionará al usuario a App Store o Google Play en función del sistema operativo del dispositivo (iOS / Android respectivamente).
-   El usuario ya tiene instalada la App en su dispositivo. En tal caso, el enlace abrirá la App, de modo que desde cualquier otra App es posible poder enlazar los contenidos que tengamos dentro de nuestra App.

Respecto a pwlnk, es una versión acortada de Powlink que comparte todas sus funcionalidades y características salvo la longitud del mismo. Tanto el Powlink clásico como el pwlnk pueden convivir juntos y se puede usar uno u otro indistintamente según se adapte más uno u otro a tus necesidades específicas.  
 

### Nota

Si antes de configurar la short url ya tenías fuentes dentro del Apptracker de EMMA, en la columna de pwlnk saldrá por defecto el valor N/A. El pwlnk sólo se mostrará en aquellas fuentes creadas después de la configuración del short url.

## Configuración para usar Powlink y pwlnk

Para poder disfrutar de todas las ventajas de Powlink es necesario en primer lugar configurar en la sección **Preferencias app** lo siguiente: 

-   **Google Play ID**, **Cert Fingerprints** **y Google Play URL** para soportar Powlink en Android
-   **App Store Bundle** **ID**, **App Team ID y AppStore URL** para Powlink en iOS.
-   **Subdominio de powlink.io** **y pwlnk.io** que se va a emplear.
</br><p style="width:850px">![](/ios_config.png)</p>
</br><p style="width:850px">![](/android_config.png)</p>
</br><p style="width:850px">![](/dominio_config.png)</p>

Una vez realizada esta configuración en tu app en EMMA, es necesario que la app esté configurada para soportar Powlink y pwlnk.  En esta [guía](https://developer.emma.io/es/android/integracion-sdk#integraci%C3%B3n-acquisition) encontrarás toda la información necesaria para la configuración de la app de Android y en esta otra [guía](https://developer.emma.io/es/ios/integracion-sdk#integraci%C3%B3n-acquisition) podrás ver toda la info para la app de iOS. 

## Usando powlink en tus campañas

A la hora de [crear](https://docs.emma.io/es/adquisicion/fuentes-de-medios#crear-fuentes) o [editar](https://docs.emma.io/es/adquisicion/fuentes-de-medios#editar-y-clonar-fuentes) una Fuente (fuente de tráfico) para una campaña, disponemos del apartado Deeplink, que podemos configurar por cada fuente que tengamos. Si quieres utilizar una landing con Deeplink, tan solo tienes que activar el check. Puedes ver más información sobre las landings con deeplink [aquí](https://docs.emma.io/es/adquisicion/apptracker#redireccionamiento-para-navegadores-no-nativos). 
</br><p style="width:800px">![](/deeplink_1.png)</p>

En este apartado, introduciremos el destino al que queremos enviar al usuario (en caso de disponer de deeplinks en nuestra App). A modo de ejemplo se sugiere una estructura del tipo page/product (página/producto). Esta será la pantalla y/o sección que se mostrará al usuario cuando sea redireccionado a la apertura de la App. Al guardar, este *path* se empleará para generar el Powlink sobre el dominio powlink.io.</br><p style="width:800px">![](/deeplink_2.png)</p>

Una vez guardado, en el listado de fuentes de la campaña, dispondrás de la url de nuestro Powlink que puedes compartir públicamente con tus proveedores / medios para la medición de tus campañas de captación de usuarios.

![](/deeplink_3.png)

# Redireccionamiento para navegadores no nativos

Desde EMMA hemos estado trabajando en un desarrollo para poder asegurar un rango más amplio de funcionamiento del AppLink / UniversalLink y ampliar su funcionamiento y lógica al mayor número de escenarios posible. Para que un AppLink / UniversalLink funcione como tal, es necesario que se realice una acción (clic) sobre el mismo y además, solo los navegadores por defecto del OS (Chrome en el caso de Android y Safari en el caso de iOS) son capaces de reconocer el AppLink / UniversalLink como tal y conseguir se comporte de la manera correcta. Con este desarrollo se amplia el espectro de acción del AppLink / UniversalLink de tal manera que también funciona como tal sin necesidad de que se realice un clic explícito sobre el mismo (redirección o pegado del powlink directamente en un navegador).

Para ello, se ha determinado que todos los powlinks que tengan configurado un deeplink y se configure desde EMMA que usen una landing de deeplinking tengan un comportamiento determinado en función del navegador por defecto que se tenga seleccionado. De esta manera, a continuación, veremos el comportamiento dentro de cada sistema operativo.

En cualquier caso, los powlinks que no tengan configurado un deeplink usando una landing de deeplink van a seguir funcionando con la misma lógica de siempre, este flujo sólo aplica en aquellos casos en los que el powlink tenga un deeplink con landing de deeplinking.

## Redireccionamiento en iOS

En el caso de iOS vamos a diferenciar el comportamiento en función del navegador por defecto que tenga el usuario configurado, diferenciando entre Safari (propio del OS) y otros navegadores “externos” como Chrome. 

### Safari

Si el usuario tiene configurado en su dispositivo como navegador por defecto Safari, el flujo del powlink será el siguiente:</br><p style="width:800px">![](https://docs.emma.io/ios_safari.png)</br>

\*En el caso de que se haga un clic explícito sobre el powlink con deeplink y la app esté instalada en el dispositivo, la aplicación se levantará inmediatamente. En caso de hacer clic y no se tenga la app instalada, el flujo es el mismo que el detallado arriba. 

> Las alertas que se muestran en el flujo, *“¿Quieres abrir la app?” y “Enlace no encontrado”*, es algo que hace por defecto Safari, es algo interno del OS y no se puede eliminar, es un elemento nativo sobre el que no tenemos control y no se puede evitar. {.is-warning}

### Chrome

Si el usuario tiene configurado en su dispositivo como navegador por defecto Chrome, el flujo del powlink será (en todos los casos) el siguiente:</br><p style="width:800px">![](https://docs.emma.io/ios_chrome_vii.png)</p>

## Redireccionamiento en Android

Al igual que en el caso de iOS, vamos a describir dos flujos y analizar el comportamiento del powlink en función del navegador seleccionado por el usuario como navegador por defecto. En este caso vamos a ver los flujos con el navegador nativo Chrome y con otros navegadores “externos” como pueden ser Firefox u Opera. 

### Chrome

Si el usuario tiene configurado en su dispositivo como navegador por defecto Chrome, el flujo del powlink será el siguiente:</br><p style="width:800px">![](https://docs.emma.io/android_chrome_vii.png)</p>



> En el caso de Chrome, si se añade la url a mano en el navegador, este va a bloquear el deeplinking y dejará al usuario en la landing. Se trata de un comportamiento propio de Chrome que hace automáticamente sobre el cual no tenemos control alguno y no se puede obviar. </br></br>Chrome sólo abrirá aquellos deeplinks que hayan sido ejecutados desde una acción (clic en un botón o redirección). {.is-warning}

### Firefox / Opera

En caso de que el usuario tenga configurado Firefox / Opera en su dispositivo como navegador por defecto, el flujo del powlink será, en todos los casos, el siguiente: </br><p style="width:800px">![](https://docs.emma.io/android_firefox.png)</p>

## Huawei HMS

Huawei tiene su propio navegador por defecto, *Huawei Browser,* pero aún con un único navegador, tenemos dos posibilidades de funcionamiento del powlink. En este caso, contamos con una primera opción, que es la opción por defecto que empleará EMMA y una segunda opción que implica un pequeño paso de configuración para poder usar el *AGConnect* (App Link) y la creación de una landing propia con la lógica de la landing por defecto de EMMA.

Veamos a continuación cada una de las opciones disponibles y sus especificaciones. 

### Huawei Browser (opción por defecto)

La opción que ofrecerá EMMA por defecto, presenta el siguiente flujo en el caso de que el usuario emplee *Huawei Browser* como navegador: </br><p style="width:800px">![](https://docs.emma.io/huawei_default_vii.png)</p>

En este caso, en el flujo del usuario que NO tiene la app instalada o que no da permiso para abrirla cuando la tiene instalada, la App Gallery se abrirá siempre *“embebida”* en el navegador. 

### AGConnect link (opción alternativa)

Si queremos que al no tener la app instalada se abra la app de App Gallery, sería necesario optar por esta alternativa. Para poder hacer uso de esta alternativa, será necesario una pequeña configuración y crear una nueva landing personalizada. Más abajo, podemos ver detalladamente ambos requerimientos detallado. 

En este caso, el flujo siempre será el siguiente: </br><p style="width:800px">![](https://docs.emma.io/huawei_agconnect_vii.png)</p>

### Configuración AGConnect Link

Habilitar el AGConnect Link para el uso en la landing de redireccionamiento es muy sencillo, tan solo hay que seguir dos pasos simples. 

-   Haz login en [***App Gallery Connect***](https://developer.huawei.com/consumer/es/)y seleccionamos la opción ***App Linking.*** </br><p style="width:700px">![](https://docs.emma.io/agconnect_i.png)</p>

-   Haz clic en el botón ***New URL prefix*** y crea un nuevo dominio de AGConnect Link.</br><p style="width:800px">![](https://docs.emma.io/agconnect_ii.png)</p></br><p style="width:900px">![](https://docs.emma.io/agconnect_iii.png)</p>

Una vez realizada esta configuración en App Gallery Connect, debemos crear una nueva landing personalizada en EMMA usando el código principal de nuestra landing (ponte en contacto con nosotros para solicitar el código html). 

Una vez tengas acceso a nuestro código, deberás llevar a cabo las siguientes acciones: 

1.  Cambiar el flag ***useHuaweiAppLink*** de *false* a ***true***
2.  Añadir dentro de la landing, en el código *javascript,* el ***packageName***
3.  Añadir dentro de la landing, en el código *javascript,* el dominio de AGConnect Link configurado previamente en [***App Gallery Connect***](https://developer.huawei.com/consumer/es/)

## **Configurar una landing de deeplink para los powlinks**

Para ver en detalle como vincular una landing de deeplink con un powlink, visita nuestro artículo sobre [Landings](https://docs.emma.io/es/landings#vincular-un-powlink-a-una-landing-de-redirecci%C3%B3n-deeplink). 

## Crear nuevas landings personalizadas

Puedes ver información más detallada sobre cómo crear nuevas landings personalizadas de Redirección (Deeplink) [aquí](https://docs.emma.io/es/landings#landings-personalizadas). 

# Exporta tus datos

EMMA ofrece la posibilidad de exportar los resultados de las campañas si así lo necesitas para su gestión. Hay disponibles dos maneras de exportar los datos:

-   ***Exportar tabla*** en el resumen de **campañas**. Esta opción se encuentra encima de la tabla de resumen de campañas hacia la izquierda.
-   **Exportar tabla** en el resumen de **fuentes**. Esta opción se encuentra encima de la tabla de resumen de fuentes hacia la izquierda.

En ambos casos, el sistema realiza la descarga directa de un archivo .csv con las columnas que haya seleccionadas para mostrarse en la tabla. 

Las opciones disponibles de KPIs y su significado son: 

## Exportación de campañas

|     |     |
| --- | --- |
| **Nombre** | Nombre de la campaña |
| **Clicks** | Número total de clics provenientes de las redirecciones |
| **CPC** | Coste por clic |
| **Coste** | Coste total basado en el modelo de pago elejido (CPC, CPI o Coste Fijo) |
| **Instalaciones** | Número de dispositivos atribuidos a alguna campaña que han abierto la app por primera vez |
| **CPI** | Coste por instalación |
| **ITR** | Install Through Rate: número de instalaciones por clic (%) |
| **Registros** | Dispositivos que han realizado un evento registeruser |
| **CPL** | Coste por registro o lead |
| **Ventas** | Número total de conversiones registradas con el evento trackorder |
| **CPS** | Coste por conversión |
| **Beneficio** | Valor de las órdenes de transacción |
| **Fecha de inicio** | Fecha de inicio de medición de datos |
| **Fecha de fin** | Fecha de fin de medición de datos |
| **Apertura** | Número de aperturas de la app |
| **Primer login** | La primera vez que el usuario ha realizado un inicio de sesión en la aplicación/sitio móvil |
| **Login** | Número total de inicio de seisión realizado por los usuarios |
| **Eventos** | Se puede añadir como columnas del reporting cada uno de los eventos personalizados que se estén midiendo con nuestro SDK |

## Exportación de fuentes

|     |     |
| --- | --- |
| **Nombre** | Nombre de la fuente |
| **Powlink** | Tracking link de seguimiento |
| **Pwlnk** | Short Tracking link de seguimiento |
| **QR** | QR generado con el tracking link de seguimiento |
| **Clicks** | Número total de clics por redirección |
| **CPC** | Coste por clic |
| **Coste** | Coste total basado en el modelo de pago elegido (CPC, CPI o Coste Fijo) |
| **Instalaciones** | Número de dispositivos atribuidos a alguna fuente que han abierto la app por primera vez |
| **CPI** | Coste por instalación |
| **ITR** | Install Through Rate: Número de instalaciones por clic (%) |
| **Registros** | Dispositivos que han realizado un evento de registeruser |
| **CPL** | Coste por registro o lead |
| **Ventas** | Número total de conversiones registradas con el evento trackorder |
| **CPS** | Coste por conversión (sale) |
| **Beneficio** | Valor de las órdenes de transacción |
| **Modelo de pago** | Modelo de pago seleccionado (CPC, CPI o Coste Fijo) |
| **Proveedor** | Proveedor con el que trabajas |
| **Apertura** | Número de aperturas de la app |
| **Primer login** | La primera vez que el usuario ha realizado un inicio de sesión en la aplicación/sitio móvil |
| **Login** | Número total de inicio de seisión realizado por los usuarios |
| **Eventos** | Se puede añadir como columnas del reporting cada uno de los eventos personalizados que se estén midiendo con nuestro SDK |

## Exportación informe de fraude

EMMA te permite obtener un reporting sobre las instalaciones del Apptracker susceptibles de haber sido realizadas con técnicas fraudulentas. 

Esta funcionalidad te dará mayor control sobre la calidad del tráfico por el que estás pagando obteniendo una información adicional muy valiosa para poder optimizar cada uno de tus canales de captación. 

### Reglas para detectar el fraude

Para poder detectar el fraude hemos establecido cinco reglas a tener en cuenta para determinar si una instalación es sospechosa de ser fraudulenta o no y el peso probabilísitico de la misma. 

En la siguiente tabla puedes ver cuales son esas reglas:

|     |     |     |
| --- | --- | --- |
| **REGLAS DE FRAUDE** | **DESCRIPCIÓN** | **PESO** |
| FR1 CLICK HIJACKING | Inyección de un segundo clic de un mismo dispositivo segundos después del clic inicial y antes de la instalación (los clics deben haber sido realizados en un intervalo inferior a 5 segundos y pertenecer a  fuentes  diferentes). | 50% |
| FR2 SECONDS TO INSTALL | Segundos desde el clic hasta la instalación. Menos de 10 segundos será posible fraude. | 75% |
| FR3 INSTALL TIME | Hora de instalación. Todas las que se realizan desde las 2 am a las 5 am (hora del dispositivo) será posible fraude. | 25% |
| FR4 COUNTRY | Instalaciones desde países donde no se ha negociado lanzar la campaña. Para poder definir el país en el que se va a lanzar la campaña haz clic [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). | 90% |
| FR5 TIMEZONE MATCH | Si el timezone de la instalación no es el mismo que el timezone de la ip de instalación será posible fraude. | 90% |

### Informe de fraude

Para obtener tu informe anti-fraude [accede](https://ng.emma.io) a EMMA y ve hasta la sección **Captación > AppTracker**. 

![](/fraude_1.png)

Selecciona el periodo de fechas deseado, ve a la tabla de las campañas, filtra la campaña que quieres analizar y haz clic en el botón **Exportar Fraude** para poder descargar un archivo .csv con la información de fraude (solo se exportará la información de la(s) campaña(s) que estemos filtrando en el buscador, si no filtramos ninguna campaña, nos descargará información de todas las campañas). 

![](/fraude_2.png)

Obtendrás un fichero .csv ([**ver ejemplo de informe anti-fraude**](https://docs.google.com/spreadsheets/d/16PiLPO_yJqvY2KADS09rj7ZWVpwFX0yqykmoKy9H4n0/edit?usp=sharing)) con tantas lineas como instalaciones con posibilidad de ser fraudulentas con la siguiente información:

|     |     |
| --- | --- |
| *Click ID* | ID del clic que se ha realizado. |
| *UDID* | Número de identificación único de iOS (IDFA) y Android (AAID). |
| *Fraud Percentage* | Probabilidad de que la instalación sea fraudulenta. |
| *FR1 Fraud Hijacking* | Verificación de sí se cumple fraude en esta regla. |
| *FR2 Seconds From Click to Install* | *Verificación de si se cumple o no fraude en esta regla.* |
| *FR3 Install Time* | *Verificación de si se cumple o no fraude en esta regla.* |
| *FR4 Country* | *Verificación de si se cumple o no fraude en esta regla.* |
| *FR5 TimeZone Match* | *Verificación de si se cumple o no fraude en esta regla.* |
| *Provider* | Proveedor seleccionado en el momento de creación del tracker. |
| *From Campaign* | Campaña de la que procede el usuario. |
| *From Source* | Tracker de la campaña de la que procede el usuario. |
| *Clicked at* | Hora en la que se ha realizado el clic atribuido a la instalación. |
| *Hijacked at* | Hora en la que se ha realizado el clic detectado como no fraudulento. |
| *Installed at* | Fecha en la que el usuario ha abierto la aplicación por primera vez tras haberla instalado. |
| *Seconds From Click to Install* | Segundos que han transcurrido desde que se hizo clic hasta que se instaló la app. |
| *Last session* | Última vez que el usuario abre la aplicación/sitio móvil. |
| *Device* | Modelo de dispositivo del usuario a través del cual ha utilizado la app/sitio móvil. |
| *App Version* | Última versión instalada en el dispositivo del usuario. |
| *OS Version* | Última versión del sistema operativo configurado en el dispositivo del usuario. |
| *Latitude* | Coordenadas de latitud del usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. |
| *Longitude* | Coordenadas de longitud del usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. |
| *City* | Ciudad en la que se encuentra el usuario mientras ha estado utilizando la geolocalización en la app /sitio móvil. |
| *Country* | País en el que se encuentra usuarios mientras ha estado utilizando la geolocalización en la app /sitio móvil. |
| *IP* | Dirección donde el usuario ha conectado la app/sitio web. |
| *Install TimeZone* | Zona horaria desde la que se realizó la instalación de la app. |
| *Device Language* | Idioma del dispositivo. |
| *Carrier* | Compañía de teléfono contratada por el usuario. |
| *Connection Type* | Indica si la conexión del usuario es a través de WIFI o 3G. |
| *Install Postback* | Notificación de la instalación enviada al "Provider". |
| *Fraud Rules* | Detalle de las reglas de fraude atribuidas a esa instalación. |
| *Params* | Detalle de todos los parámetros que la red envía a través del clic. |

#### Importante

La información de fraude sólo está disponible a día cerrado. No se puede consultar la información del día en curso. Además, es importante recordar que esta información de fraude estará disponible para todas las campañas salvo para las campañas de Apple Search Ads, Facebook/Instagram, Google Ads, Snapchat, Tiktok y Twitter. 

# Tracking de campañas sin el SDK de EMMA

Puedes conseguir conversiones a tiempo real en EMMA, incluso cuando tu aplicación tenga integrado un SDK distinto al SDK de EMMA, a través del pixel *server to server.* Si necesitas poder ver en EMMA instalaciones y/o eventos de una app que NO tiene el SDK de EMMA, tienes que configurar nuestras postbacks en tu plataforma de tracking para que cada vez que se realicen las acciones en la app se envíe una notificación a EMMA. 

### Dominio

EMMA da la opción de configurar tres dominios diferentes para configurar la postback. Se puede usar cualquiera de ellos indistintamente: 

-   es eat.emmasolutions.net
-   powlink.io
-   pwlnk.io.

La postback quedaría de la siguiente manera con cada uno de los posibles dominios: 

-   https://**eat.emmasolutions.net**/callback/event/emma\_install?aaid={gps\_adid}&idfa={idfa}&idfv={idfv}&ecid={emma\_click\_id}&api\_key=<cambiar por emma key de la app>&sender=sender\_name
-   https://**powlink.io**/callback/event/emma\_install?aaid={gps\_adid}&idfa={idfa}&idfv={idfv}&ecid={emma\_click\_id}&api\_key=<cambiar por emma key de la app>&sender=sender\_name
-   https://**pwlnk.io**/callback/event/emma\_install?aaid={gps\_adid}&idfa={idfa}&idfv={idfv}&ecid={emma\_click\_id}&api\_key=<cambiar por emma key de la app>&sender=sender\_name

### Postback de instalación

Nuestra postback global de instalación tiene la siguiente estructura:

https://**<dominio>**/callback/event/emma\_install?**aaid**\={gps\_adid}&**idfa**\={idfa}&**idfv**\={idfv}&**ecid**\={emma\_click\_id}&**api\_key**\=<cambiar por api key de la app>&**sender=sender\_name**

-   **<dominio>**: Establece el dominio de EMMA de entre los tres detallados anteriormente.
-   **aaid:** Parámetro para enviar dinámicamente el identificador de publicidad de Android (**A**ndroid **A**dvertising **ID**).
-   **idfa:** Parámetro para enviar dinámicamente el identificador de publicidad de iOS (**ID F**or **A**dvertisers).
-   **idfv:** Parámetro para enviar dinámicamente el identificador vendor en dispositivos iOS (**ID F**or **V**endors).
-   **api\_key:** Parámetro para enviar de manera fija el EMMA Key. Al crear una cuenta en EMMA se crea automáticamente un *EMMA Key* único para tu app. Puedes obtener este identificador único desde la sección [***Preferencias App***](https://docs.emma.io/es/configuracion#preferencias-de-la-app)***.*** 
-   **sender:** Parámetro para enviar de manera fija el sender, por ejemplo MUAK.

### Postback de evento

Si queremos recibir en EMMA eventos es necesario en primer lugar dar de alta esos eventos en la interfaz de EMMA. En esta [guía](https://docs.emma.io/es/primeros-pasos/eventos#crear-eventos) puedes ver en detalle como crear nuevos eventos. Una vez hemos creado los eventos, obtendremos un token vinculado a cada evento. Ese token es el identificador único de cada evento. 

Esta sería la postback de evento para notificar dichas acciones a EMMA. 

https://**<dominio>**/callback/event/**<cambiar por token de evento de EMMA>**?**aaid**\={gps\_adid}&**idfa**\={idfa}&**idfv**\={idfv}&**api\_key**\=<cambiar por api key de la app>&**sender=sender\_name**

-   **<dominio>**: Establece el dominio de EMMA de entre los tres detallados anteriormente.
-   **<cambiar por token de evento de EMMA>:** Sustituiremos todo lo marcado en negrita por el token que nos da EMMA al crear el evento.
-   **aaid:** Parámetro para enviar dinámicamente el identificador de publicidad de Android (**A**ndroid **A**dvertising **ID**).
-   **idfa:** Parámetro para enviar dinámicamente el identificador de publicidad de iOS (**ID F**or **A**dvertisers).
-   **idfv:** Parámetro para enviar dinámicamente el identificador vendor en dispositivos iOS (**ID F**or **V**endors).
-   **api\_key:** Parámetro para enviar de manera fija el EMMA Key. Al crear una cuenta en EMMA se crea automáticamente un *EMMA Key* único para tu app. Puedes obtener este identificador único desde la sección [***Preferencias App***](https://docs.emma.io/es/configuracion#preferencias-de-la-app)***.*** 
-   **sender:** Parámetro para enviar de manera fija el sender, por ejemplo MUAK.

\* Notas:

-   Si el usuario no existe se crea como orgánico. Si es la primera vez que se notifica el evento antes de que exista el usuario y se facilita el parámetro “ecid” intentará también atribuir el usuario al click.
-   Los parámetros **aaid, idfa, idfv, apikey** son prescindibles si se envía el param “ecid” y el click encontrado tiene un usuario atribuido como instalación.

### Parámetros soportados

|     |     |     |
| --- | --- | --- |
| **nombre** | **significado** | **ejemplo** |
| aaid | Identificador de publicidad de Android | cf6bef2e-50ab-46a9-9583-ada4ab07f113 |
| android\_id | Android id | 9fa7e549c1b1dba59897f3320ea420df |
| idfa | Identificador de publicidad de iOS | 573DEB13-C34C-457A-9B87-94F85B3602B7 |
| idfv | Identificador vendor de iOS | V-573DEB13-C34C-457A-9B87-94F85B3602B7 |
| ecid | EMMA click id | 9fa7e549c1b1dba59897f3320ea420df\_2017-11-07\_09:50:10\_609\_111 |
| api\_key | EMMA Key de la app en EMMA | 0051718341426c8c3dd2cdc7e155d44f |
| model | Modelo del dispositivo | SM-G965U |
| ip  | Dirección IP | 200.98.1.89 |
| country | País | Spain |
| city | Ciudad | Vigo |
| state | Comunidad Autónoma / Estado | Galicia |
| os\_version | Versión del sistema operativo | Android 8.0.0 |
| app\_version | Versión de la app | 2.5.1 |
| os\_build | Compilación del sistema operativo | R16NW |
| timezone | Huso horario | Europe/Madrid |
| language | Idioma ISO | es\_ES |
| latitude | Latitud | 48.8 |
| longitude | Longitud | 4.3 |
| carrier | Compañía telefónica | Vodafone |
| install\_timestamp | unix timestamp in seconds | 1540509909 |
| email | Email de usuario | emma@emma.io |
| customer\_id | Id de cliente interno | EFFXX001 |
| connection\_type | Tipo de conexión (Wifi o datos móviles) | WIFI |
| sender | Nombre del remitente, condiciona el parser de los argumentos | adjust |
| session\_duration | Segundos | 38  |

Además, también disponemos de un pixel web para campañas web. 

1\.  Ajusta el píxel a EMMA en 3rd Party Tracker:

-   http://eat.emmasolutions.net/postback?eid=\[eMMa\_ID\]
-   En el evento de clic, EMMA redirigirá al usuario a la URL del 3rd party tracker añadiendo el \[eMMa ID\] como parámetro.
-   Ej.: http://www.yoururl.com?eid=\[eMMa\_ID\]
-   La variable eid aporta el valor al EMMA ID. El 3rd party tracker debe devolver este parámetro en el evento de conversión, como se detalla.

2\.  Establece tu campaña de EMMA con el link enviado del 3rd party tracker. Visita nuestro artículo [cómo usar el EMMA Apptracker](https://docs.emma.io/es/adquisicion/Campa%C3%B1as) para gestionar tus campañas.

3\.  Crea las distintas *sources* o enlaces de seguimiento en tu nueva campaña, introduciendo en el parámetro ID de la transacción (*transaction ID)* el nombre requerido para el 3rd party.

EMMA envía, por defecto, la variable **eid** pero esta puede sustituirse mediante la edición de la *source:* 

-   Utiliza el parámetro **exid** si se va a notificar la conversión a través de una cuenta de EMMA.
-   Utiliza el parámetro **aid** si se va a notificar la conversión a través de AppBurn.
-   Utiliza el parámetro **aff\_sub** si se va a notificar la conversión a través de Hasoffers.
-   Utiliza el parámetro **eid** (no necesita ser reemplazado) si se va a notificar la conversión a través de Appsflyer.
-   Contacta con el equipo de 3rd party tracking para otras notificaciones de conversión *server to server.* 
  



# Discrepancias - ¿Cómo cuenta EMMA instalaciones comparado con App Store o Google Play?
  
En ocasiones pueden existir discrepancias entre los datos mostrados por EMMA y los informes de instalaciones de iTunes App Store o Google Play.

En algunas casos, EMMA puede mostrar menos instalaciones que las que se pueden ver en los reportings de App Store o Google Play y otras veces puede ocurrir lo contrario, que en EMMA se muestren más instalaciones de las que se muestran en las stores. 

A continuación, veremos más detalladamente los principales motivos que pueden generar ambas casuísticas. 

-   EMMA muestra menos datos que las stores
-   EMMA muestra más datos que las stores

## EMMA muestra menos datos que las stores

A continuación se muestran las causas principales que pueden generar que en EMMA se muestren menos datos que App Store y Google Play:

|     |     |     |     |
| --- | --- | --- | --- |
| **Causa** | **App Store Connect / Google Play Console** | **EMMA** | **Nota** |
| **Definición de instalación** | Las instalaciones se registran en el momento en el que se descarga la app en el dispositivo, independientemente de si el usuario abre o no la app. | EMMA registra nuevas instalaciones en el momento en el que el usuario lanza por primera vez la app (install+apertura). | Si un usuario no abre la app, EMMA no podrá registrar la instalación. |
| **Fecha de registro de la instalación** | App Store Connect y Google Play Console registran la instalación el día que se realiza la descarga de la app. | EMMA registra la instalación el día en el que la aplicación es abierta por primera vez. | Habitualmente es mejor extender el rango de tiempo al comparar EMMA con App Store Connect y Google Play Console. |
| **Time Zone** | Los datos se muestran según la zona horaria del anunciante. | Los datos se muestran en el Time Zone configurado en preferencias de la app. |     |
| **Reinstall en la ventana de atribución definida (Android)** | En algunas secciones, Google Play Console muestra 2 instalaciones únicas para el mismo usuario, independientemente del tiempo transcurrido tras la primera instalación.<br><br>En otras secciones muestran usuarios únicos. Este dato será más semejante al mostrado en EMMA. | EMMA no atribuye las reinstalaciones en ningún momento. |     |
  
**Ejemplos:**

1.  Un usuario instaló la App y no la abrió. EMMA no registra la instalación mientras que App Store Connect y Google Play Console cuentan una nueva instalación.
2.  Un usuario descargó la app y la abrió 2 semanas después. App Store Connect y Google Play Console registran la instalación en el momento de la descarga mientras que EMMA contabiliza esa instalación en el momento de la primera apertura de la App.


## EMMA muestra más datos que las stores

Ahora pasaremos a analizar los posibles causantes que provocan que en EMMA haya más datos que en las stores. 

|     |     |     |  
| --- | --- | --- | 
| **Causa** | **App Store/ Google Play** | **EMMA** | **Nota**
| **Instalación única** | Por cada usuario único se registra una única instalación, teniendo en cuenta:<br><br>-   Desinstalaciones de la App.<br>-   Usuarios con varios dispositivos.<br>-   Actualizaciones de dispositivos. | EMMA registra instalaciones únicas por dispositivo.<br><br>Si una app se despliega sin el SDK de EMMA y posteriormente se sube una nueva versión a las stores con el SDK de EMMA integrado, EMMA mostrará un pico de nuevas instalaciones orgánicas.<br><br>Este pico disminuye con el tiempo a medida que los usuarios actualizan la app.<br><br>**Esta es la principal razón por la que EMMA registra más instalaciones que App Store y Google Play.** |
| **Instalación en varios dispositivos** | Si un usuario se instala la app en varios dispositivos usando el mismo Apple ID o cuenta de Google Play, App Store y Google Play la considera como una única instalación. | Si un usuario se instala la aplicación en varios dispositivos EMMA contabiliza cada una de esas instalaciones como una nueva instalación. |
| **Instalaciones de fuera de Google Play** | Las instalaciones que no se realizan desde Google Play no son contabilizadas en Google Play | EMMA contabiliza cualquier instalación de la app venga o no de Google Play. |
| **Filtros aplicados en la consola de Google Play** | Por ejemplo filtro *Fuente de tráfico: Búsqueda en Google Play* | EMMA no puede diferenciar los dispositivos orgánicos (ni no orgánicos) en base a esa información. | Con ese filtro, Google Play Console muestra los usuarios que han ido a Google Play y han realizado una búsqueda expicita de la app en el buscador. |
| **Instalación en base a** | Google Play Console contabiliza las instalaciones en base a cuentas de Google Play únicas. </br></br> App Store Connect se basa en el IDFA para mostrar la información de *Primeras descargas* | En el caso de Android, EMMA contabiliza los dispositivos en base al GAID/AAID del dispositivo.</br></br> En iOS se basa en el IDFV y no en el IDFA como App Store Connect.* |  |

> Con la entrada del marco del App Tracking Transparency (ATT) en 2021, Apple ha dado el poder al usuario para poder determinar si permite el seguimiento publicitario y dar consentimiento para recoger el IDFA. Dado que este consentimiento es post instalación, para poder trabajar el mercado a optado por usar el IDFV. </br>
El IDFA es un identificador único a nivel de dispositivo que Apple puede seguir obteniendo, mientras que el IDFV es un identificador a nivel de proveedor de apps que cambia al desinstalarse y volverse a instalar la app. </br>
Esto hace que, al no ser un id único cada vez que el usuario se desinstala la app y la vuelve a instalar, en EMMA se contabilizará como una nueva instalación, mientras que App Store contará una única instalación.{.is-info}

**Ejemplos:**

1.  Un usuario descargó y abrió (instaló) la App en sus dos dispositivos (iPhone y iPad). EMMA cuenta esto como dos instalaciones mientras que App Store cuenta como una única instalación.
2.  Un usuario actualizó su dispositivo, sincronizado con su cuenta de App Store / Google Play y abre la App. EMMA cuenta una nueva instalación orgánica, mientras que App Store y Google Play no.
3.  La App se desplegó en 2016 sin el SDK de EMMA. Un usuario descargó y abrió la App. App Store y Google Play cuentan a ese usuario en 2016 mientras que EMMA no pudo registrar su instalación (no estaba el SDK instalado).   
    Posteriormente la App integra el SDK de EMMA (en Abril de 2017, por ejemplo). El usuario actualiza la versión de la App y la abre. EMMA registra ese usuario como una nueva instalación orgánica mientras que App Store y Google Play no.   
    Ésta es la principal causa para discrepancias, especialmente si la App tiene una amplia base de usuarios desde el periodo anterior a la instalación del SDK de EMMA.
4. Un usuario instaló la app hace 1 año y ahora ha cambiado su dispositvo Android y vuelve a instalar la aplicación usando la misma cuenta de Google Play. En el caso de Google Play Console, al estarse usando la misma cuenta, no contabilizará esa instalación como nueva, mientras que EMMA, al tratarse de un GAID/AAID nuevo, lo contará como una nueva instlación. 
5. Un usuario se ha instalado la app en un dispositivo iOS con versión del OS igual o superior a 14.5. Al cabo de x días la desinstala y posteriormente la vuelve a instalar. En este caso, App Store Connect no contará esa instalación como nueva ya que es el mismo IDFA, en el caso de EMMA, como no nos podemos basar en el IDFA, al ser un IDFV nuevo, se registrará esa instalación como nueva.

> Esto no afecta la exactitud al contar instalaciones no orgánicas. EMMA registra una sóla instalación por click en anuncio, incluso si el usuario específico tiene más de un dispositivo registrado en su cuenta de App Store / Google Play.  </br> </br>Habitualmente es mejor extender el rango de tiempo al comparar EMMA con App Store y Google Play. </br></br>Las plataformas no miden las instalaciones de la misma manera, sino que cada una emplea sus KPIs para determinar lo que es un instalación, por tanto los datos comparados no son iguales. {.is-warning}


Puedes ver más información sobre la atribución de descargas no orgánicas por EMMA [aquí](https://docs.emma.io/es/adquisicion/apptracker).
  
 # Cómo realizar pruebas de atribución
Una vez que ya hayas creado tus campañas de adquisición o re-targeting y tus fuentes de medios, si así lo consideras oportuno, puedes hacer pruebas de captación o engagement para revisar que tus campañas/fuentes realizan las redirecciones pertinentes o llevan a los usuarios a la sección de la app deseada (deeplinking). 
Es por ello que en esta guía te vamos a explicar paso a paso cómo tienes que llevar a cabo estas pruebas. 
 
 ## Pruebas de captación con un powlink de EMMA
 Antes de realizar cualquier prueba de atribución es importante que tengas en cuenta lo siguiente: 
> Si el dispositivo con el que se van a llevar a cabo las pruebas ya ha tenido la app instalada anteriormente, aunque se desinstale la app y  se vuelva a instalar, ese dispositivo no contará como una instalación atribuida al powlink desde el que se ha realizado clic, ya que ya existe en nuestra base de datos. 
  {.is-info}
  
Para que dicho dispositivo pueda ser empleado para múltiples pruebas de atribución, deberá ser añadido como dispositivo de test. Una vez el dispositivo está habilitado como dispositivo de test, se podran hacer tantas pruebas de atribución con powlink como sea necesario. 
  <br/>
### Pasos para las pruebas de captación
- Desinstalar la app del dispositivo en el que se va a realizar la prueba. 
- Hacer [login](https://ng.emma.io/es/login?returnUrl=%2Findex) en la cuenta de EMMA. 
- Ir a la sección Captación > Apptracker.<br/><p style="width:300px">![pruebas_atribución_1.png](/pruebas_atribución_1.png)</p>
- Crear o buscar una campaña ya creada e ir a las fuentes de la misma. Si no sabes cómo crear campañas de Apptracker puedes ver más info al respecto [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). 
- Crear o buscar la fuente que se quiere probar. Si no sabes cómo acceder a las fuentes de una campaña o como crear campañas, [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios) puedes ver como hacerlo.
- Copiar el enlace que aparece en la columna powlink, pwlnk y enviarlo por email, por ejemplo. O descargar el código QR y escanearlo para poder hacer la prueba. ![pruebas_atribución_2.png](/pruebas_atribución_2.png)
- Ir al dispositivo móvil en cuestión, abrir el email y hacer clic en el powlink enviado.
- Instalar la app.
- Ir a EMMA nuevamente a la sección Captación > Apptracker. 
- Revisar los datos de la campaña en cuestión en la que se han realizado las pruebas. Recuerda que los datos pueden tardar un poco en aparecer en el Apptracker ya que el reporting no es inmediato.

Si se quieren realizar más pruebas con diferentes enlaces tan solo hay que repetir los pasos listados aquí. 

> No es posible realizar pruebas de campañas con objetivo a descarga de la app de plataformas autoatribuidas como pueden ser por ejemplo Apple Search Ads, Facebook/Instagram o Google Ads.
{.is-info}
  
## Pruebas de engagement con un powlink de EMMA
Para realizar pruebas de engagement **no** hay que desinstalar la aplicación del dispositivo en el que se vayan a realizar las pruebas. Es más, la aplicación debe estar instalada en el dispositivo para poder llevar a cabo estas tipo de test. 
  
### Pasos para las pruebas de engagement
- Tener la app instalada en le dispositivo. De no tenerla, lo primero que se debe hacer es instalarla y abrirla por primera vez para que entre en la BBDD de EMMA. 
- Hacer [login](https://ng.emma.io/es/login?returnUrl=%2Findex) en tu cuenta de EMMA. 
- Ir a la sección Captación > Apptracker. <br/><p style="width:300px">![pruebas_atribución_1.png](/pruebas_atribución_1.png)</p>
- En el filtrado de la barra gris cambiar el modo Básico por Retargeting. Si no sabes cómo usar este nivel de filtrado [aquí](https://docs.emma.io/es/barras-de-menu#filtros-en-captaci%C3%B3n-apptracker) tienes más info al respecto.<br/><p style="width:450px">![pruebas_atribuición_3.png](/pruebas_atribuición_3.png)</p>
- Crear o buscar una campaña ya creada e ir a las fuentes de la misma. Si no sabes cómo crear campañas de Apptracker, puedes ver más info [aquí](https://docs.emma.io/es/adquisicion/Campa%C3%B1as). 
- Crear o buscar la fuente que se quiera probar. Si no sabes cómo acceder a las fuentes de una campaña o como crear campañas, [aquí](https://docs.emma.io/es/adquisicion/fuentes-de-medios) puedes ver como hacerlo. 
- Copiar el enlace que aparece en la columna powlink y enviarlo por email, por ejemplo. ![pruebas_atribución_2.png](/pruebas_atribución_2.png)
- Ir al dispositivo móvil en cuestión, abrir el email y hacer clic en el powlink enviado. 
- La app deberá levantarse y se podrán realizar los eventos inapp que se consideren oportunos. En caso de haber configurado un deeplink, la app deberá redireccionar a la sección específica correspondiente dentro de la app. 
- Ir a EMMA nuevamente a la sección Captación > Apptracker. 
- Revisar que en la barra de fitlrado gris está seleccionada la opción Retargeting. 
- Revisar los datos de la campaña en cuestión en la que se han realizado las pruebas. Recuerda que los datos pueden tardar un poco en aparecer en el Apptracker ya que el reporting no es inmediato. 

Si se quieren llevar a cabo más pruebas con diferentes powlinks, tan solo hay que repetir los pasos listados aquí. 

> Si el powlink de retargeting no levanta la app, lo primero que hay que hacer es confirmar con el equipo que ha integrado el SDK de EMMA que esta [funcionalidad](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu) está implementada en la app y revisar si la implementación es correcta o s detecta algún error. 
{.is-info}
  
# Uso de powlinks de EMMA sin el SDK integrado
Si no tienes el SDK de EMMA integrado pero quieres aprovechar la funcionalidad de los powlinks y poder llevar a los usuarios a la app, ya sea a la home o a alguna sección concreta dentro de la app, existen dos opciones para poder llevarlo a cabo.
  
> Ten en cuenta que esta solución solo vale para llevar al usuario a la app, pero si el SDK no está implementado no se van a poder medir instalaciones ni ninguna actividad. {.is-danger}
 
## Alternativa usando la landing de deeplink
  
En este caso, no sería necesario tocar código en la app ni tampoco en los ficheros de configuración de la misma, simplemente sería necesario activar en todos los powlinks la **[Landing de Redirección Deeplink](https://docs.emma.io/es/landings#vincular-un-powlink-a-una-landing-de-redirecci%C3%B3n-deeplink).**

> La landing de deeplink puede ser la predeterminada por defecto que tiene EMMA ya lista para usarse o, en caso de ser necesario, se puede desarrollar una personalizada. Puedes ver más información sobre este tema [aquí](https://docs.emma.io/es/landings#landings-personalizadas).{.is-info}
  
El funcionamiento de esta landing intermedia de deeplink, lo que hace es lanzar el deeplink básico para realizar la apertura de la app, por lo que si está consta de este esquema básico no será necesario realizar configuración adicional. Un ejemplo de esquema de deeplink básico sería **appscheme://dashboard**. 
Este deeplink, se configuraría al crear la [fuente de medios](https://docs.emma.io/es/adquisicion/fuentes-de-medios#crear-fuentes) y la landing lo lanzaría automáticamente como apertura. 
  
> Es imprescindible que exista un esquema de deeplink básico configurado en la app.{.is-warning}
  
Para poder usar esta funcionalidad es necesario seguir estos pasos: 
1. Asegurarse de que la app se abre a través de un esquema básico de deeplink, por ejempo **appscheme://**. En el caso de que esto es así, podríamos seguir con la configuración.
2. Accede a tu cuenta de [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex) y ve a **Captación > Apptracker**.
3. [Crea](https://docs.emma.io/es/adquisicion/Campa%C3%B1as) una nueva campaña o usa una ya existente. 
4. Desde el menú contextual, o haciendo clic en el nombre de la campaña, accede a la pantalla de **Fuentes** y [crea una nueva fuente](https://docs.emma.io/es/adquisicion/fuentes-de-medios). 
5. Al crear una nueva fuente, debes asegurarte de configurar los campos: 
  5.1. Activa el *check* de **¿Utilizar Landing de Redirección Deeplink?**.
  5.2. Configura un deeplink en el campo **Ruta Deeplink**.![apptracker_5.png](/apptracker_5.png)

Una vez creada la fuente, se genera automáticamente un **Powlink** y un **Pwlnk**. Al hacer clic en ese powlink automáticamente se lanza la landing intermedia por defecto e intentará abrir la app a través del deeplink configurado.
  
Puedes ver más información sobre cómo funciona la landing intermedia o cómo se configura una landing intermedia custom desde [aquí](https://docs.emma.io/es/adquisicion/redireccion-navegadores-no-nativos).

  
## Configurar dominio de EMMA como punto de entrada de la app

En este caso habría que configurar como punto de entrada de la app el dominio https://myapp.powlink.io. 

Para poder hacer uso de esta solución es necesario añadir unas líneas de código en los ficheros de configuración de la app y añadir una serie de configuraciones en el apartado [preferencias app](https://docs.emma.io/es/configuracion#preferencias-de-la-app) de EMMA.
Los campos a configurar en EMMA son:
- Google Play ID.
- Fingerprints de los certificados de firma en formato SHA-256.
- Google Play URL.
- App Store Bundle ID.
- App Team ID con el que se ha firmado la app de producción.
- AppStore URL .
  
Puedes ver más información sobre esto campos [aquí](https://docs.emma.io/es/adquisicion/apptracker#configurar-powlink-y-pwlnk-stu).

A nivel de aplicación habría que: 
1. Modificar la App de Android editando el AndroidManifest.xml para añadir con los demás dominios de apertura https://myapp.powlink.io
2. Modificar la App de iOS añadiendo como applinks:myapp.powlink.io en el target de la app.
> Una vez que la app esté en TestFlight se pueden llevar a caboo pruebas para validar que la configuración es correcta y que cualquier Powlink con dominino https://myapp.powlink.io abre la app. {.is-info}
  
> Es importante tener en cuenta que existen excepciones de apertura de los App Links / Universal Link, ya que por ejemplo, no funcionan de manera correcta en todas las aplicaciones y chats.{.is-warning}
  
Una vez configurados y realizados los pasos anteriores, cualquier dominio https://myapp.powlink.io (Powlink, Pwlnk o Acortador) va a levantar la app.

