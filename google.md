---
title: Google
description: Google
published: true
date: 2025-03-10T13:59:56.080Z
tags: 
editor: markdown
dateCreated: 2021-05-31T12:01:05.484Z
---

![google_ads.png](/google_ads.png)

# Medición de campañas con objetivo App Promotion

Para configurar campañas para Apps móviles bajo la modalidad [**App Campaign (AC)**](https://support.google.com/adwords/answer/6247380?hl=es) antes deberás empezar a revisar los siguientes puntos.

1.  Para la atribución de cualquier campaña de Google Ads es necesario que tu aplicación nos esté enviando el identificador publicitario del dispositivo (IDFA/AAID). Visita nuestras [guías de integración](https://developer.emma.io/es/home) para obtener más información.
2.  Vete a [**Preferencias app**](https://ng.emma.io/es/user/app) y asegúrate que el campo **BUNDLE ID de iOS** y **GOOGLEPLAY ID de Android** están correctamente configurados.

Si ya tienes todo listo... ¡Empezamos!


https://youtu.be/-HGwmhO0bsI

## Crea el LINK ID de Google Ads
> Esta acción solo puede ser realizada por el admin de la cuenta de Google Ads
{.is-info}


-   Inicia sesión en tu cuenta de Google Ads y vete a **Herramientas.**
-   Selecciona **"Cuentas vinculadas"** (*Linked Accounts*).

![google_1.png](/google_1.png)

-   En la caja ***Third-party app analytics*** haz click en ***DETAILS.***

![google_2.png](/google_2.png)
-   Añade una nueva cuenta haciendo click en el botón **"+".**

![google_3.png](/google_3.png)
-   Selecciona la opción de ***Other Provider*** en el desplegable

![google_4.png](/google_4.png)
-   Introduce el siguiente ***Provider ID:*** **5623804413**

![google_5.png](/google_5.png)
-   Copia el nuevo LINK ID que identifica tu mobile app en Google Ads. Recuerda generar un LINKID para cada una de tus aplicaciones (Android e iOS).

## Configura Google Ads en EMMA

-   Haz login en EMMA y selecciona tu app. Ve a ***Captación > Fuentes de medios.***

![fuentes_medios.png](/fuentes_medios.png)-   Busca la fuente de ***Google Ads*** y en el menú lateral haz clic en la opción ***Editar.***

![google_6.png](/google_6.png)
-   Introduce el LINK ID generado en Google Ads previamente para Android y/o iOS. Dale a ***Guardar.***

![google_7.png](/google_7.png)
## Consentimientos
Debido a la nueva **Ley de Mercados Digitales (DMA)** (también conocida como *Reglamento 2022/1925*) que entró en vigor el pasado 6 de Marzo de 2024, se han introducido requisitos adicionales para empresas como Google y otras grandes plataformas designadas como *"guardianes"*, donde se destaca la responsabilidad de estos *"guardianes"* en la recopilación del consentimiento de usuario. 

En esta línea, los nuevos consentimientos a notificar a Google son: 
- **eea:** Si el usuario pertenece a la zona económica Europea. 
- **ad_user_data:** Si el usuario permite compartir datos con Google.
- **ad_personalization:** Si el usuario permite anuncios personalizados.

Para cumplir con esta nueva Ley, en EMMA hemos añadido una nueva sección en los detalles del proveedor Google Ads llamada **Consentimientos** para poder configurarlos.

### Cómo afectan estos consentimientos a apps dentro y fuera de la EEE
Es importante que tengas en cuenta que, aunque si tu aplicación se encuentra fuera de la Zona Económica Europea los consentimientos no son obligatorios, Google recomienda la implementación de los mismos para todos los usuarios, ya que en un futuro es probable que se extienda su uso a otras regulaciones. 

En ese caso, este sería un ejempo de configuración de los consentimientos dentro de EMMA: 
- **eea**: false
- **ad_user_data**: true
- **ad_personalization**: true

Sin embargo, si tu aplicación se encuentra dentro de la Zona Económica Europea, es **obligatorio** el uso de estos constemientos y los podrás configurar con los pasos detallados [aquí](https://docs.emma.io/es/google#configuraci%C3%B3n-de-los-consentimientos). 

En este caso, este sería un ejemplo de configuración de los consentimientos dentro de EMMA: 
- **eea**: valor por defecto *true*. 
- **ad_user_data**: valor por defecto *false* y vinculación de la etiqueta que recoja el consentimiento, por ejemplo *ALLOW_USER_DATA*.
- **ad_personalization**: valor por defecto *false* y vinculación de la etiqueta que recoja el consentimiento, por ejemplo *ALLOW_AD_PERSONALIZATION*.

Puedes ver más información sobre las preguntas más frecuentes [aquí](https://docs.emma.io/es/google#google-consent-mode). 

### Configuración de los consentimientos
Para poder establecer la configuración de los consentimientos para la aplicación, es necesario seguir estos pasos: 
- Haz login en tu cuenta de [EMMA](https://ng.emma.io).
- Ve a la sección **Captación > Fuentes de medios**.
- Busca y edita el proveedor **Google Ads**.
- En la edición de proveedor, ve a la sección **Consentimientos**.
- Establece la etiqueta enviada a EMMA en la que se recojan cada uno de los consentimientos solicitados. **Si no tienes una etiqueta específica, deja este campo en blanco o [crea una etiqueta](https://docs.emma.io/es/etiquetas) que recoja el permiso para incluirla y poder cumplir con la ley.**
Si en una misma etiqueta se recogen varios consentimientos se puede aplicar la misma etiqueta en todos los campos.
- Establece el valor por defecto. Selecciona entre las opciones **Verdadero** y **Falso**. El valor configurado por defecto será el que se envíe en esos permisos a Google en caso de no tener una etiqueta que recoja esa información o si para algún usuario no existe el valor en la etiqueta. ![google_conent_mode.png](/google_conent_mode.png)

> Ten en cuenta que los valores pemitidos en las etiquetas empleadas para estos permisos son: ***true/false***, ***0/1***, ***si/no***, ***verdadero/falso***. </br> Estos valores no son sensibles a las mayúsculas y minúsculas, por lo que, por ejemplo, el valor *True* también es aceptado.{.is-warning}






## Habilita la notificación de eventos In-App (opcional)

Si además de instalaciones quieres mandar algún evento in-app a Google Ads, ve a ***Captación > Fuentes de Medios,*** busca Google Ads y haz clic en la opción ***Editar*** del menú lateral. 

-   En la sección de eventos, haz clic en el botón **Añadir evento** y selecciona los eventos a enviar.
> Es importante que tengas en cuenta que, debido a una limitación de Google, cuando configures eventos para notificar a Google Ads, el nombre que configures en la columna ***Identificador del Evento para la Fuente de medios*** debe contener entre 1 y 64 caracteres (utilizando la codificación UTF-8), de lo contrario, Google no podrá recibir este evento.
{.is-warning}

![google_8.png](/google_8.png)
-   Una vez seleccionados todos los eventos, haz clic en **Guardar.**

> Para que Google Ads empiece a registrar conversiones debes abrir tu aplicación y realizar los eventos que quieres enviar al menos una vez. Después de haberlo realizado pueden pasar hasta 6 horas para que las nuevas conversiones cambien su estado de “No recent conversion” a “recording conversion".
{.is-info}

## Añade tus conversiones en Google Ads

-   Vuelve a iniciar sesión en tu cuenta de Google Ads y vete a **Herramientas**.
-   Selecciona ***"Conversions".***

![google_9.png](/google_9.png)
-   Haz click en el botón **"+"** para añadir una nueva etiqueta de conversión.

![google_10.png](/google_10.png)
-   Selecciona la opción **"APP"** en listado de tipologías.

![google_11.png](/google_11.png)
-   Selecciona la opción **"Third-party app analytics"** y dale a continuar.

![google_12.png](/google_12.png)
-   Selecciona el evento **"first-open"** para cada app (obligatorio para medir instalaciones). Opcionalmente selecciona los eventos in-app que quieras atribuir en Google Ads.

![google_13.png](/google_13.png)
-   Haz click en **"Import and Continue"**.
-   Ahora verás el listado de conversiones integradas.
-   Para poder ver las descargas en el reporting de la campaña de Google Ads, asegúrate de que la etiqueta que has integrado está incluida en "conversiones". Para ello haz click en el nombre de la etiqueta de conversión y revisa que en el apartado *Include in "Conversions"* pone *YES.*

![google_14.png](/google_14.png)
## Cuenta global (MCC) con subcuentas

En caso de que tengas una cuenta global creada (MCC) que a su vez tenga muchas subcuentas, para poder medir los datos de todas las subcuentas en EMMA la configuración que se debe hacer es la siguiente.

Debes vincular la cuenta MCC con EMMA a través del link id.

-   Ve a la cuenta hija (subcuenta) y copia el **id de cliente.**

![google_15.png](/google_15.png)
-   Una vez que tengas el **id de cliente** de la cuenta hija, deberás volver a la cuenta global (**MCC**) e ir a **Herramientas > Cuentas vinculadas > Analíticas de aplicaciones de terceros.**

![google_16.png](/google_16.png)
-   Selecciona la opción **compartir con otra cuenta.**

![google_17.png](/google_17.png)
-   Añade el **id de cliente** de la cuenta hija y continua con el proceso que indica Google Ads.

![google_18.png](/google_18.png)
Estos pasos se deberán realizar para cada una de las subcuentas hijas que haya creadas bajo la cuenta global MCC. Si hay creadas 3 subcuentas, se deberán seguir estos pasos 3 veces y así sucesivamente.

# Campañas de SKAdNetwork 
EMMA permite medir camapñas de TikTok Ads orientadas a dispositivos iOS con versión del sistema operativo superior a iOS 14.5, lo que se conoce como campañas  de SkAdNetowork. Al permitir la medición de esta tipología de campañas, podremos obtener en EMMA los datos agregados de las instalaciones y los eventos realizados a través de estas campañas.

> Es importante que tengas en cuenta que debido a la política de privacidad de Apple, a través de las campañas de SkAdNetwork no podemos obtener información del usuario que se ha instalado la app o realizado eventos inapp, ya que Apple no comparte esta información con terceros. La única información que comparte con terceros es que se ha realizado una instalación o un evento vinculado a una determinada campaña. </br>
Es decir, solo podremos obtener datos agrerados de instalaciones y eventos inapp, sin poder saber bajo ningún concepto qué usuario ha realizado esas acciones.{.is-info}

> Para poder habilitar SKAD Network de Google es necesario tener una cuenta con centro de clientes (MCC) en Google. Puedes ver más información sobre este tipo de cuentas [aquí](https://ads.google.com/intl/es-419_co/home/tools/manager-accounts/).  
{.is-warning}

## Habilitar SkAdNetwork en EMMA para instalaciones

Para poder empezar a medir este tipo de campañas en EMMA, lo primero es validar estos dos pasos: 

- Inicia sesión en tu cuenta de [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex).
- Ve a preferencias de la app y asegúrate que el en el apartado de configuración relativo a iOS, el campo App Store ID está configurado. De no ser así, tan solo tienes que obtener este ID de la url de la store de tu app y configurarlo.<br>![google_29.png](/google_29.png)
- Asegúrate de que has realizado los pasos previos de esta guía.

Una vez validados los pasos anteriores, es necesario hacer esta configuración para poder medir SKAdNetwork de Google Ads. 
- Ve a la sección **Captación > Fuentes de medios**. </br><p style="width:300px">![google_35.png](/google_35.png)</p>
- Busca y edita el proveedor **Google Ads**.</br> <p style="width:900px">![google_6.png](/google_6.png)</p>
- Si no tienes la **Conexión con Google Ads** realizada, deberás hacer la conexión.</br> <p style="width:700px">![google_30.png](/google_30.png)</p>
- Una vez realizada la conexión, ya podrás habilitar el check de SKAdNetwork. </br> <p style="width:700px">![google_31.png](/google_31.png)</p>
- A continuación debes seleccionar la cuenta de administrador. </br><p style="width:700px">![google_32.png](/google_32.png)</p>
- Ya por último, selecciona la cuenta de cliente de la que se quieren obtener datos. Este selector es múltiple, por lo que en caso de que apliquen varias cuentas de cliente, podrás seleccionarlas.</br><p style="width:700px">![google_33.png](/google_33.png) </p>

> Además de esta configuración, es recomendable integrar el SDK de EMMA (versión 4.12 o posterior) para poder realizar un seguimiento del usuario a nivel de eventos y poder recibir asimismo una copia directa del postback de SkAdNetwork para obtener más información en EMMA.
{.is-info}

En cuanto se empiecen a recibir datos de las campañas de SkAdNetwork, aparecerá automáticamente en EMMA una nueva campaña llamada **SkAdNetwork Google Ads** que mostrará toda la información agregada. 
![google_34.png](/google_34.png)

### Consideraciones a tener en cuenta
La conexión de SkAdNetwork con Google Ads, se realiza a través de la propia API que Google Ads dispone en la que aporta la información de SKAdNetwork pertinente para su uso. 

Dado que nos conectamos a la API de Google, y esta tiene ciertas limitaciones, hay que tener en cuenta lo siguiente: 
- Los datos sólo están disponibles 72 horas después, por lo cual es probable que cuando revises los datos en Apptracker para *hoy*, *ayer* y *antesdeayer* veas 0 instalaciones. 
- Google actualiza los datos de hasta 5 días atrás. Esto significa que los datos de instalaciones para los últimos 5 días pueden variar en función de cuando se consulten. 

## Medición de eventos de SKAdNetwork
En caso de querer recibir la información de atribución de los eventos inapp realizados a través de campañas de SkAdNetwork es necesario en primer lugar tener integrada la versión del SDK 4.12 o posterior. 

Además, es necesario configurar en EMMA los eventos que se quieren medir. Para ello debemos seguir estos pasos: 
- Haz login en tu cuenta de EMMA. 
- Ve a la sección **Gestión > Eventos** </br><p style="width:300px">![crear_eventos_1.png](/crear_eventos_1.png)</p>
- Busca y edita el evento deseado. </br><p style="width:700px">![editar_evento_1.1.png](/editar_evento_1.1.png)</p>
- En la parte final de la pantalla debes habilitar el check de SKAdNetwork y añadir un valor de conversión entre 1 y 63. </br><p style="width:1000px">![google_36.png](/google_36.png)</p>

Los valores de conversión deben ser únicos, no puede usarse el mismo valor en múltiples eventos. En caso de seleccionar un valor en uso, EMMA nos avisará y nos mostrará el valor más cercano disponible para su configuración.</br><p style="width:800px">![google_37.png](/google_37.png)</p>

> Ten en cuenta que desde SKAdNetwork nos notifican solo la última conversión, el último evento realizado dentro de la ventana de atribución, por lo que si un usuario realiza 3 eventos de forma consecutiva dentro de la misma ventana de atribución, solo nos llegará la notificación del último y será este el que aparezca en EMMA.
{.is-warning}

# Campañas App Campaign de Re-Targeting

Además del clásico modelo ACi de Google para campañas de Aplicaciones  con objetivo instalación, también existe una modalidad para campañas de Aplicaciones con objetivo de Re-Targeting (ACe). 

Con EMMA podrás medir también está modalidad de campañas siguiendo la misma configuración detallada en el apartado anterior. 

# Seguimiento para clics y coste (opcional)

> Para poder habilitar el seguimiento de clics y coste, es necesario tener una cuenta con centro de clientes (MCC) en Google. Puedes ver más información sobre este tipo de cuentas [aquí](https://ads.google.com/intl/es-419_co/home/tools/manager-accounts/). {.is-warning}

Con EMMA es posible obtener la información de clics y coste de Google para poder tener esta información en el apptracker de EMMA, junto con la inforación de instalaciones y actividad inapp realizada por el usuario. De esta manera, podremos analizar toda la información referente a la campaña desde la interfaz. 

Para habilitar la descarga de esta información, tan solo tienes que seguir estos pasos: 
- Haz login en tu cuenta de [EMMA](https://ng.emma.io/es/login?returnUrl=%2Findex). 
- Ve a **Captación > Fuentes de medios**. Busca y edita el proveedor Google Ads. 
- Dentro de la edición, desplázate hasta el apartado **Clics y coste** y habilita el switch.</br><p>![google_39.png](/google_39.png)</p>
- Una vez habilitado, ya podrás seleccionar la cuenta de administrador y la cuenta de cliente respectivamente para poder descargar los datos de clics y coste que correspondan. </br><p>![google_40.png](/google_40.png)</p>

Y ¡listo! ya podrás ver los datos de clics y coste de tus campañas en EMMA.

> Es posible que existan pequeñas discrepancias en los clics y coste que muestra EMMA respecto a Google. Esto es debido a que, Google va procesando y reajustando los clics durante la vida de la campaña y el coste también se va modificando en función de ello. Puedes ver más información al respecto [aquí](https://support.google.com/google-ads/answer/11182074).{.is-warning}

# Compartir Audiencias con Google (opcional)

https://www.youtube.com/watch?v=_x6Rhkm5ubw

La creación de audiencias de Google es la mejor forma de orientar tus campañas a un público objetivo basándote en distintos criterios de segmentación.

Google permite crear estas audiencias desde su propia plataforma publicitaria o bien automatizar la creación y actualización de las mismas desde el propio motor de segmentación de EMMA.


El método de compartir audiencias desde EMMA tiene dos ventajas claras:
- Podrás usar todos los criterios de segmentación que te ofrece EMMA **sin necesidad de volcar los datos manualmente y periódicamente en Google**. Más rápido, sencillo y con menos integración técnica.
- Podrás crear **audiencias basadas en ID's internos de tu compañía** no compatibles en el creador de audiencias de Google. Por ejemplo, un listado de "ID de clientes".

> Debido a la política de segmentación por lista de clientes de Google, no todos los anunciantes pueden crear listas de clientes (audiencias), y por tanto, no todos los anunciantes podrán compartir audiencias con Google directamente desde EMMA. 
En este [enlace](https://support.google.com/adspolicy/answer/6299717) puedes ver los requisitos de Google para poder usar las listas de clientes y por tanto compartir audiencias directamente desde EMMA, y, en este [otro](https://support.google.com/google-ads/answer/6276125?hl=es), puedes ver cómo crear listas de clientes en Google. Si no puedes crear una lista de clientes de manera manual, no podrás acceder a las audiencias compartidas desde EMMA.  
{.is-warning}

¿Te parece interesante?

Antes de empezar asegúrate de que tienes completada la configuración de Google en EMMA. Es importante que cumplas los pasos del apartado [Medición de campañas con objetivo App Promotion](https://docs.emma.io/es/google#medición-de-campañas-con-objetivo-app-promotion), para poder realizar la conexión con la plataforma social correctamente y poder medir todas tus campañas.

## Habilitar audiencias para la cuenta de anunciante
Para poder compartir audiencias directamente con Google es necesario que tengas la conexión con la API de Google realizada. Para ello, tan solo tienes que seguir estos pasos: 

- Haz login en EMMA
- Dirígete a **Captación > Fuentes de medios** y busca la fuente **Google**. 
- Editar la fuente y haz la conexión con Google haciendo clic en el botón **Iniciar sesión en Google Ads** que se muestra en la sección **Google Ads**.![google_21.png](/google_21.png)
- Guarda los cambios.

Una vez realizada la conexión con la API de Google, en la fuente Google (Captación > Fuentes de medios) se habilitará inmediatamente la sección de **Audiencias**. 

Mientras no está la conexión con la API realizada se muestra el siguiente mensaje. 
![google_22.png](/google_22.png)
Una vez se ha realizado la conexión con la API, ya podemos seleccionar del listado la cuenta de anunciante correspondiente con la que queremos compartir las audiencias de manera directa. 
![google_23.png](/google_23.png)

## Compartir audiencias con Google
Ahora sí, ha llegado el momento de compartir tus audiencias con Google, pero no te preocupes, es un proceso super sencillo. Tan solo tienes que seguir estos pasos para compartirlas.
- Ve a la sección **Comportamiento > Audiencias** y crea una audiencia que deseas compartir con Google. Si ya tienes audiencias creadas y quieres compartir una de esas no hace falta que la vuelvas a crear. En esta [guía](/es/https://docs.emma.io/es/audiencias) puedes ver más información sobre cómo crear audiencias.
![google_19.png](/google_19.png)
- Una vez tengas tu audiencia lista, dirígete nuevamente a la sección **Captación > Fuentes de medios** y edita la fuente **Google**. 
- A continuación, dentro de la edición de la fuente Google, ve al apartado de **Audiencias** y selecciona la audiencia que deseas compartir con Google de la lista y guarda cambios. Listo, tu audiencia ya se está compartiendo automáticamente con Google. 
![google_24.png](/google_24.png)

Por defecto, en las audiencias, EMMA comparte únicamente el ID de dispositivo, por lo que, en caso de desear compartir también el email del usuario, se debe habilitar el check de **Compartir EMAIL con Google Ads**.![google_38.png](/google_38.png)

> La información de email se envía codificada mediante sha256.{.is-info}

Una vez compartida la audiencia con Google, desde la pantalla de audiencias (Comportamiento > Audiencias) podremos ver qué audiencias están siendo compartidas con Google ya que en la columna **Compartido** estas mostrarán el icono de Google. 
![google_20.png](/google_20.png)

> Google informa que las audiencias pueden tardar hasta **48 horas** en mostrar datos en su propio dashboard. 
Puedes ver tus audiencias en el dashboard de Google accediendo a la sección **Herramientas y configuración > Biblioteca Compartida > Gestor de audiencias**. 
![google_25.png](/google_25.png)
Ten en cuenta también, que el volumen de usuarios que muestra EMMA en la audiencia no tiene porque ser el mismo que muestra el dashboard de Google, ya que Google solo podrá procesar aquellos id's de dispositivo que tenga en su sistema. {.is-warning}

## Dejar de compartir audiencias con Google
Si quieres que se dejen de actualizar los datos de la audiencia con Google, tan solo tienes que seguir estos pasos: 

- Ve a **Captación > Fuentes de medios** y edita la fuente de **Google**.
- En el apartado audiencia, despliega el selector de audiencias a compartir y haz clic sobre aquellas que quieras dejar de compartir (puedes escribir el nombre de las mismas para localizarlas más fácilmente). Automáticamente verás como el check que las mantenía seleccionadas desaparecerá. 
![google_26.png](/google_26.png)
![google_27.png](/google_27.png)
- Guarda los cambios para que la audiencia deje de ser compartida con Google. 

> Ten en cuenta que, el efecto de esta acción es simplemente que la audiencia deje de actualizar la información nueva en la audiencia de Google, pero esta audiencia no actualizada seguirá estando disponible en Google y podrá seguir siendo usado para campañas a menos que se elimine también allí. {.is-info}
# Gestión de campañas con Powlinks

Google Ads tiene una estricta política relativa a las redirecciones en sus anuncios. 

> *Queremos que los consumidores tengan una buena experiencia cuando hagan clic en un anuncio, por lo que los destinos de los anuncios deben ofrecer un valor único a los usuarios y ser funcionales, útiles y sencillos.* {.is-info}

Teniendo esto en cuenta, han establecido una serie de buenas prácticas que los anunciantes deben cumplir. Entre esas buenas prácticas y la que afecta a EMMA, recomiendan evitar:

> *Redirecciones desde la URL final que llevan al usuario a un dominio distinto. </br>La URL final [*http://example.com*](http://example.com/) redirige a [*http://example2.com*](http://example2.com/)* {.is-info}

Este caso concreto afecta a todas aquellas campañas de Google Ads cuyo objetivo sea diferente de App Installs y permitan el uso de un powlink de EMMA para su medición. 

Para poder medir esta tipología de campañas con EMMA, tenemos dos alternativas: 
- [Uso de una plantilla de seguimiento](https://docs.emma.io/es/google#plantilla-de-seguimiento)
- [Uso de landing web](https://docs.emma.io/es/google#gesti%C3%B3n-de-landings-para-campa%C3%B1as-con-powlinks)

## Plantilla de seguimiento
Para poder medir las campañas que no sean de descarga de la app y permitan el uso de enlaces de terceros, se puede configurar el powlink de EMMA como plantilla de seguimiento a la hora de crear el anuncio en Google Ads. 

Para ello, tan sólo tenemos que crear un powlink con normalidad y, añadir al final del mismo el parámetro **&redirection_url=https://www.ejemplo.es** según las directrices de Google. 

El valor del parámetro, debe ser la web/landing a la que queremos que el usuario sea redireccionado. De esta manera, si por ejemplo queremos llevar al usuario a la web de Arkana, la configuración del parámetro sería la siguiente: 
- &redirection_url=https://arkana.io

> Es importante tener en cuenta que, al configurar la web de destino en el parámetro **redirection_url**, se ignorarán las redirecciones configuradas en la campaña de EMMA, siendo la redirección válida la configurada en el parámetro.{.is-warning}

Una vez generado el powlink con la configuración del parámetro, ya se puede añadir como plantilla de seguimiento en Google al crear el anuncio. 
La plantilla de seguimiento, se debe configurar en el paso de **Anuncio**, en concreto en el campo *Plantilla de seguimiento* desplegando las **Opciones de la URL del anuncio** </br><p style="width: 400px">![google_41.png](/google_41.png)</p>


## Gestión de landings 
EMMA dispone de una funcionalidad que permite dar de alta nuevas landings en EMMA, que alojaremos nosotros mismos en nuestro servidor, y así poder evitar el redireccionamiento que Google no permite. 

De esta manera, se podrán generar una o varias landings diferentes que podremos vincular posteriormente con un powlink determinado y que cumpla con todos los requisitos de redireccionamiento de Google Ads. 

### Crear landings
EMMA tiene una landing por defecto para estos casos y también permite la creación de landing personalizadas para aquellos casos en los que así sea necesario. Puedes ver más info sobre cómo crear landings [aquí](https://docs.emma.io/es/landings#crear-landings). 

### Vincular un powlink a una landing
Una vez que ya tenemos creada nuestra landing, ha llegado el momento de vincularla. Puedes ver más info sobre cómo vincular una landing a un powlink en nuestro artículo específico de [Landings](https://docs.emma.io/es/landings#vincular-un-powlink-a-una-landing). 

# Campañas de Re-Targeting con Google usando Powlink de EMMA

Ten en cuenta que, sea cual sea la modalidad de la campaña de Google, si no está englobado dentro de las campañas AC (campañas que no necesitan configurar un powlink para medirse), es posible que Google rechace los anuncios debido a su estricta política de redirección. Para solucionar esa casuística, en EMMA tenemos una solución planteada en el punto inmediatamente anterior de esta [guía](https://docs.emma.io/es/google#google-ads-gesti%C3%B3n-de-landings-para-campa%C3%B1as-con-powlinks). 

Para que Google pueda atribuir de manera correcta los eventos en este tipo de campañas específica, es necesario enviar tanto el parámetro [**GCLID**](https://support.google.com/google-ads/answer/9744275?hl=es) como el [**session start**](https://developers.google.com/app-conversion-tracking/api/request-response-specs#conversion_tracking_request) cada vez que hay un clic de Re-Targeting. 

En el caso del GCLID, EMMA está preparada para enviar ese parámetro a Google, siempre y cuando previamente Google haga el envío de esta información para que desde EMMA la podamos recoger y tratar de la manera adecuada. Este parámetro se envía en todos los eventos. 

Para que se envíe la información del **session start** con el clic de Re-Targeting, es necesario realizar una pequeña configuración en el dashboard de EMMA.

-   Ve a **Captación > Fuentes de medios.** 
-   Busca y edita la fuente ***Google Ads.*** 
-   En el apartado **Eventos**, añade el evento **Apertura de Retargeting** y en la columna *“Identificador del Evento para la Fuente de medios”* escribe **session\_start**    

![](/google_session_start.png)

-   Haz clic en el botón **Guardar** para que se apliquen los cambios. 

Con esta configuración ya se enviará el session\_start con cada apertura de Re-Targeting que se genere en la app a través de un powlink de EMMA que se esté usando en las campañas de Google. 

Esta configuración no afecta al resto de eventos, puedes configurar eventos para notificar a Google con normalidad. Puedes ver más info al respecto [aquí](https://docs.emma.io/es/google#habilita-la-notificaci%C3%B3n-de-eventos-in-app-opcional).
# Preguntas frecuentes

## Atribución

**¿Por qué no puedo ver los clics en EMMA?**

Las campañas de Google Ads UAC no permiten el uso de tracking links ni tampoco el envío de clics a terceros por lo que este dato solo lo podrás ver en la herramienta de Google Ads.

**¿Por qué no puedo ver el coste de mis campañas de Google Ads en EMMA?**

La nueva integración con la API solo permite que EMMA muestre datos de instalaciones y evento in-app para estas campañas.

**¿Dónde puedo ver los resultados de mis campañas de Google Ads en EMMA?**

En la sección de EMMA **Captación > Apptracker** o en **Comportamiento > Explorador** creando un segmento con el KPI **campaña**.

**¿Qué detalle de desglose de campaña ofrece EMMA?**

En el apptracker de EMMA podremos ver el desglose del nombre de campaña y grupo de anuncio de Google Ads que ha generado datos. 

La equivalencia en EMMA será: 
- Nombre de campaña en Google Ads = Nombre de campaña en EMMA.
- Nombre de grupo de anuncios en Google Ads = Nombre de fuente en EMMA.
              
**¿Si cambio el nombre de la campaña en el dashboard de Google, como lo voy a ver en EMMA?**
Si realiazas un cambio de nombre en una campaña que ya haya generado datos en EMMA, en cuanto recibamos la nueva nomenclatura por parte de Google, el nombre se actualizará en EMMA según el nuevo nombre recibido. 

**¿Cómo puedo lanzar una campaña de re-engagement para mi aplicación?**

En la nueva interfaz de Google Ads está opción no está disponible y como han [anunciado](https://support.google.com/google-ads/answer/6310653?hl=en) también dejará de estarlo en la en la antigua interfaz a partir del día 17 de Septiembre.

La única alternativa que ofrecen para promocionar una app móvil es crear una campaña "Universal de Aplicaciones" (UAC) y seleccionar cómo objetivo "Acciones en la app". Con esto se optimizará el tráfico para conseguir el evento deseado, pero en ningún caso se trata de una campaña de re-engagement.

## Discrepancias EMMA vs Google Ads

**¿Por qué hay discrepancias entre EMMA y Google Ads?**

Ten en cuenta que las discrepancias que puedan existir entre los datos mostrados en el dashboard de Google Ads y el Apptracker de EMMA se dan principalmente por el sistema de deduplicación que emplea EMMA.

Google Ads es una red auto atribuida, pero EMMA no, por lo que una instalación que Google Ads se auto atribuye, en EMMA puede estar atribuida a cualquier otra campaña activa que haya en el Apptracker.

Se pueden dar casos en los que la discrepancia sea a la inversa, ya que Google Ads puede tardar hasta 72 horas en actualizar la información del reporting mientras que en EMMA esta actualización se realiza en tiempo real.

Otra causa de discrepancia puede ser el momento de la atribución de la instalación. Google Ads atribuye la instalación en el momento en el que se realiza el click, mientras que EMMA atribuye la instalación en el momento en el que el usuario realiza la primera apertura de la misma. Por este motivo, una instalación puede estar atribuida a días distintos en ambas plataformas.

## Ley de Mercados Digitales 

**¿Qué ocurre si un usuario de la EEA envía el *ad_user_data* y *ad_personalization* a *false*?**

En este caso la precisión de atribución disminuye ya que no se pueden compartir identificadores de dispositivo con Google ni otros datos del usuario como el email (si está activo en audiencias).

**¿Qué ocurre si la app opera fuera y dentro de la EEA?**

En este caso si una app opera fuera y dentro de la EEA debe enviar una etiqueta de usuario desde el SDK de EMMA y vincularla en fuente de medios Google Ads al consentimiento, está etiqueta debe tener si el usuario pertenece a la EEA *true/false*.

**¿Qué ocurre si tengo vinculado un permiso de consentimiento a una etiqueta pero este usuario no envía la etiqueta con el consentimiento?**

En este caso si hay una etiqueta de usuario vinculada a un permiso y no se envia, EMMA coge el valor por defecto configurado en fuente de medios Google Ads para ese consentimiento.

**¿Qué ocurre si no hay ni valor por defecto asociado ni vinculación de etiquetas a estos consentimientos?**

EMMA deja de enviar datos a Google para las campañas de Google Ads ya que no puede identificar los consentimientos.

**Tengo audiencias sincronizadas con Google Ads. ¿Qué pasa con los datos enviados antes del 6 de marzo de 2024?**

Todos aquellos usuarios sincronizados antes del cambio que no tengan añadidos permisos a partir del día 6 de marzo de 2024 o estos estén denegados serán eliminados progresivamente de las listas.

**A partir del 6 de marzo de 2024 ¿Que permisos tienen que cumplir los usuarios para compartir datos en las audiencias de Google Ads?**

Para compartir datos en la audiencias los usuarios necesitan tener a *true* tanto el permiso *ad_user_data* (compartir datos con Google) como el permiso de *ad_personalization* (permitir anuncios personalizados). 
Si un usuario no tiene estos dos permisos a true no se añadirá a la audiencia, aunque cumpla la segmentación.

**¿Que pasa si un usuario tarda en enviar sus etiquetas de consentimiento después de realizar la instalación?**

Cuando EMMA recibe una nueva instalación intenta atribuirla a Google Ads siempre que haya una conexión previa realizada, si el usuario todavía no da los consentimientos cuando se ha procesado la instalación se envían a Google Ads los valores por defecto, en el caso de denegar todo por defecto los permisos se enviarían a *false*. La atribución no se perdería, ya que 24 horas se realizaría otro intento con los permisos ya actualizados.
