---
title: Huawei
description: Huawei
published: true
date: 2023-06-14T12:51:09.808Z
tags: 
editor: markdown
dateCreated: 2022-04-20T08:56:42.836Z
---

# Petal Ads

Con Huawei Ads puedes promocionar tu aplicación móvil de Huawei App Gallery. Al igual que otras plataformas, Hauwei permite este tipo de campañas y EMMA es partner integrado de Huawei para permitirte medir y atribuir todas las conversiones que provengan de campañas con objetivo de descarga de la aplicación.

Los anuncios de Huawei están integrados en varias aplicaciones de Huawei como:

-   Huawei App Gallery
-   Hauwei Browser
-   Huawei Video
-   Huawei Assistant
-   Huawei Reader
-   Huawei Music

Para poder medir con EMMA todas las instalaciones de las campañas enfocadas tienes que seguir estos sencillos pasos de configuración:


## Configuración en Petal Ads

Para empezar a medir tus campañas de Petal Ads con EMMA, tan solo tienes que seguir estos pasos: 
1. Haz login en tu cuenta de Petal Ads. 
2. Ve a la sección ***Tools > Delivery assistance > Analytics association***.![petal_ads_5.png](/petal_ads_5.png)
3. Haz clic en el enlace *analytics provider*. ![petal_ads_6.png](/petal_ads_6.png)
4. En la pantalla ***Tool provider*** crea un nuevo proveedor haciendo clic en el icono ***+***.![petal_ads_7.png](/petal_ads_7.png)
	-	En el campo ***Short name*** estbalece el nombre ***EMMAMobileSolutionSL*** y haz clic en el botón ***Submit***.![petal_ads_8.png](/petal_ads_8.png)
5. De vuelta en la pantalla de *Tool provider* copia y reserva el ***Developer token*** para configurarlo posteriormente en EMMA. ![petal_ads_9_bis.png](/petal_ads_9_bis.png)
6. Desde esa misma pantalla, haz clic en el botón ***New association***.![petal_ads_10.png](/petal_ads_10.png)
7. Serás llevado a la pantalla de ***Analytics association***.
8. Haz clic en el botón ***+ New association*** para vincular el proveedor. ![petal_ads_11.png](/petal_ads_11.png)
9. En la pantalla de configuración establece lo siguiente: ![petal_ads_12.png](/petal_ads_12.png)
	- Driver users to: selecciona la opción **App**.
	- App: selecciona la app que deseas vincular. 
	- Tool provider: seleccionar la opción **Other** y en el siguiente selector la opción **EMMAMobileSolutionsSL**.
10. Una vez creada la vinculación, copia y reserva el ***Key(Link ID).***![petal_ads_13.png](/petal_ads_13.png)



## Configuración en EMMA

Una vez realizados los pasos anteriores, debes ir al dashboard de EMMA para poder hacer la configuración necesaria para llevar a cabo la atribución. Tan solo tienes que seguir estos pasos:

1.  Haz login en tu cuenta de [EMMA](https://ng.emma.io/es/).
2.  Ve a la sección ***Captación > Fuentes de medios*** y busca y edita el proveedor ***Petal Ads***.![petal_ads_3.png](/petal_ads_3.png)
3. Establece el ***LinkID*** y el ***Token de provvedor*** que has obtenido previamente de la consola de Petal Ads.![petal_ads_1.png](/petal_ads_1.png)
4. Haz clic en el botón ***+ Guardar*** para guardar la configuración realizada. 

Si todo se ha realizado de manera correcta, en cuanto empiezen a llegar instalaciones de Petal Ads se se generará en EMMA de manera automática cada una de las diferentes campañas con datos atribuidos. Para ver los datos de las campañas, tan solo debes ir a Captación > Apptracker. 
![petal_ads_4.png](/petal_ads_4.png)

## Medición de eventos in app (opcional)
Además de medir las campañas de Petal Ads en EMMA, también puedes enviar cualquier evento inapp que estés midiendo en EMMA a Petal Ads para medirlos. Sigue estos pasos para configurar los eventos para Petal Ads:
- Haz login en tu cuenta de [EMMA](https://ng.emma.io/es/) y dirígete a **Captación > Fuentes de medios**.
- Edita la fuente de medios de **Petal Ads**.![petal_ads_3.png](/petal_ads_3.png)
- En la sección de Eventos, haz clic en el botón añadir evento para añadir los eventos que quieres enviar a Petal Ads. Es importante que en la columna *Identificador del Evento para la Fuente de medios* selecciones el evento de Petal Ads con el que se vinculará el evento evento enviado desde EMMA.![petal_ads_2.png](/petal_ads_2.png)

Decide con qué evento de Petal Ads vas a querer vincular el evento que se envía desde EMMA .
- Register
- Retain
- Paid
- Browse
- Collection
- PreOrder
- Subscribe
- Login
- Update
- Reservation
- AddToCart
- ThreeDayRetain
- SevenDayRetain
- Deliver
- OrderSigning
- FirstPurchaseMemberCard
- PurchaseMemberCard
- AddQuickApp
- AddToWishlist
- OpenedFromPushNotification
- ReEngage
- Form_submit
- Consult
- EffectiveLeadsForm
- PotentialCustomerForm
- Custom_acquisit
- Book
- ConsultOnline
- EffectiveLeadsOnline
- PotentialCustomerOnline
- PhoneDialing
- EffectiveLeadsPhone
- PotentialCustomerPhone
- FollowScan
- LeadsLottery
- AddPaymentInfo
- StartTrial
- InitiatedCheckout
- Invite
- Search
- Share
- TravelBooking
- Rate
- ContentView
- Custom
- Custom_landingpage
- LandingpageClick
- Coupon
- Navigate
- Lottery
- Vote
- Redirect
- GamePackageRedemption
- GamePackageClaiming
- CreateRole
- Authorize
- TutrialCompletion
- AchievementUnlocked
- SepntCredits
- LevelAchieved
- LoanCompletion
- PreCredit
- Credit
- Follow
- Fordward
- Read
- Like
- Comment

## Campañas disponibles
Con EMMA podrás medir las siguientes campañas de Petal Ads: 
- **Automated App Promotion**: 
	- Campaign type: App
	- Network: Display Network
	- Current delivery: Deliver to search network
- **Sales**: 
	- Campaign type: Display
	- Network: Display Network
	- Current delivery: Deliver to search network
# Filtro HMS

El 19 de Mayo de 2019, Google decidió finalizar con Huawei todos aquellos negocios que conllevasen transferencia de *hardware* y *software*, menos aquellos cubiertos por licencias *open source.* Esto se traduce en que los dispositivos fabricados a partir de esta fecha, no contarán con los servicios de Google como es la Play Store, aunque sí con el equivalente, la **App** **Gallery** de **Huawei**.

Desde entonces, en EMMA hemos estado trabajando para adaptar nuestro SDK a la tienda de aplicaciones de Huawei  con el objetivo de medir la atribución así como permitir el envío de notificaciones Push mediante su servicio. De esta manera, no se perderá la medición de estos usuarios. Puedes ver más información respecto a la implementación del SDK [aquí](https://developer.emma.io/es/android/integracion-sdk).

Además, también hemos trabajado en un filtro para poder diferenciar los dispositivos que usan los servicios de Google (*Google Mobile Services - GMS*) de los que usan los servicios de Huawei (*Huawei Mobile Services - HMS*). Puedes consultar este filtro desde [explorador](/es/analisis-de-comportamiento/segmentos), pero debes tener en cuenta las siguientes consideraciones:

-   El filtro diferencia los dispositivos que usan los servicios HMS de los que usan los servicios GMS.
-   Es posible que haya dispositivos que no son de marca Huawei que se filtren como HMS. Esto se puede dar porque si un usuarios tiene un dispositivo Samsung, Xiaomi o de cualquier otro fabricante y un smartwatch de Huawei, para utilizar este último necesita instalar en su dispositivo los servicios HMS, con lo cual pasarán a estar categorizados como HMS.
-   Los dispositivos de marca Huawei anteriores al cese de las relaciones entre Google y Huawei, tienen ambas stores, Google Play y App Gallery, pero actualmente no es posible saber desde que store el usuario ha descargado la app. Con lo cual existe la posibilidad de que haya dispositivos categorizados como HMS porque tienen los servicios de Huawei que se pueden haber descargado la app desde App Gallery.